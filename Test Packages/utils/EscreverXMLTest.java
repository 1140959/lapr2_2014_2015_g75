/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import eventoscientificos.controllers.CriarEventoController;
import eventoscientificos.controllers.CriarSessaoTematicaController;
import eventoscientificos.controllers.DefinirCPController;
import eventoscientificos.controllers.RegistarUtilizadorController;
import eventoscientificos.controllers.SubmeterArtigoController;
import eventoscientificos.domain.Autor;
import eventoscientificos.domain.Empresa;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Carlos
 */
public class EscreverXMLTest {
    
    public static Empresa m_empresa = new Empresa();

    
    public EscreverXMLTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        RegistarUtilizadorController m_controllerRU = new RegistarUtilizadorController(m_empresa);
        m_controllerRU.novoUtilizador();
        m_controllerRU.setDados("u1", "u1", "u1", "u1@isep.ipp.pt");
        m_controllerRU.novoUtilizador();
        m_controllerRU.setDados("u2", "u2", "u2", "u2@isep.ipp.pt");

        CriarEventoController m_controllerCE = new CriarEventoController(m_empresa);
        m_controllerCE.novoEvento();
        m_controllerCE.setTitulo("Titulo");
        m_controllerCE.setDescricao("Descricao");
        m_controllerCE.setLocal("Local");
        Calendar c = Calendar.getInstance();
        m_controllerCE.setDataInicio(c.getTime());
        c.set(Calendar.SECOND, c.get(Calendar.SECOND) + 5);
        m_controllerCE.setDataInicioSubmissao(c.getTime());
        c.set(Calendar.MINUTE, c.get(Calendar.MINUTE) + 5);
        m_controllerCE.setDataFim(c.getTime());
        m_controllerCE.setDataFimSubmissao(c.getTime());
        m_controllerCE.setDataInicioDistribuicao(c.getTime());
        m_controllerCE.setDataLimiteRevisao(c.getTime());
        m_controllerCE.setDataLimiteSubmissaoFinal(c.getTime());
        m_controllerCE.addOrganizador("u1");
        m_controllerCE.registaEvento();

        CriarSessaoTematicaController m_controllerCT = new CriarSessaoTematicaController(m_empresa);
        m_controllerCT.setEvento(m_controllerCT.getListaEventosRegistadosDoUtilizador("u1").get(0));
        Calendar c1 = Calendar.getInstance();
        c1.set(Calendar.SECOND, c.get(Calendar.SECOND) + 10);
        Calendar c2 = Calendar.getInstance();
        c2.set(Calendar.MINUTE, c.get(Calendar.MINUTE) + 5);
        Calendar c3 = Calendar.getInstance();
        c3.set(Calendar.MINUTE, c.get(Calendar.MINUTE) + 5);
        m_controllerCT.setDados("123", "st", c1.getTime(), c2.getTime(), c3.getTime(), c3.getTime(), c3.getTime());
        m_controllerCT.addProponente("u2");
        m_controllerCT.registaProponente();
        m_controllerCT.registaSessaoTematica();

        DefinirCPController m_controllerCP = new DefinirCPController(m_empresa);
        m_controllerCP.novaCP(m_controllerCP.getListaCPDefiniveisEmDefinicao("u1").get(0));
        m_controllerCP.novoMembroCP("u1");
        m_controllerCP.addMembroCP();
        boolean registaCP = m_controllerCP.registaCP();

        m_controllerCP = new DefinirCPController(m_empresa);
        m_controllerCP.novaCP(m_controllerCP.getListaCPDefiniveisEmDefinicao("u2").get(0));
        m_controllerCP.novoMembroCP("u1");
        m_controllerCP.addMembroCP();
        m_controllerCP.registaCP();

        m_controllerCT.getSessaoTematica().setStateEmSubmissao();

        SubmeterArtigoController m_controllerSA = new SubmeterArtigoController(m_empresa);
        m_controllerSA.selectSubmissivel(m_controllerCT.getSessaoTematica());
        m_controllerSA.setDados("Artigo 1", "Artigo - Resumo 1");
        Autor aut = m_controllerSA.novoAutor("u1", "strAfiliacao", "u1@isep.ipp.pt");
        System.out.println("\n\n\n\n\n teste " + m_controllerSA.addAutor(aut));
        m_controllerSA.setCorrespondente(aut);
        List<String> listPC = new ArrayList<>();
        listPC.add("palavra1");
        listPC.add("palavra2");
        m_controllerSA.setPalavrasChave(listPC);
        m_controllerSA.setFicheiro("Ficheiro PDF 1");
        System.out.println("ASDASD " + m_controllerSA.registarSubmissao());

        SubmeterArtigoController m_controllerSA2 = new SubmeterArtigoController(m_empresa);
        m_controllerSA2.selectSubmissivel(m_controllerCE.getEvento());
        m_controllerSA2.setDados("Artigo 2", "Artigo - Resumo 2");
        Autor aut2 = m_controllerSA2.novoAutor("u1", "strAfiliacao", "u1@isep.ipp.pt");
        System.out.println("\n\n\n\n\n teste " + m_controllerSA2.addAutor(aut2));
        m_controllerSA2.setCorrespondente(aut2);
        List<String> listPC2 = new ArrayList<>();
        listPC2.add("palavra1");
        listPC2.add("palavra2");
        m_controllerSA2.setPalavrasChave(listPC2);
        m_controllerSA2.setFicheiro("Ficheiro PDF 1");
        m_controllerSA2.registarSubmissao();

        CriarEventoController m_controllerCE2 = new CriarEventoController(m_empresa);
        m_controllerCE2.novoEvento();
        m_controllerCE2.setTitulo("Titulo - 2");
        m_controllerCE2.setDescricao("Descricao");
        m_controllerCE2.setLocal("Local");
        Calendar c4 = Calendar.getInstance();
        m_controllerCE2.setDataInicio(c4.getTime());
        c4.set(Calendar.SECOND, c4.get(Calendar.SECOND) + 5);
        m_controllerCE2.setDataInicioSubmissao(c4.getTime());
        c4.set(Calendar.MINUTE, c4.get(Calendar.MINUTE) + 5);
        m_controllerCE2.setDataFim(c4.getTime());
        m_controllerCE2.setDataFimSubmissao(c4.getTime());
        m_controllerCE2.setDataInicioDistribuicao(c4.getTime());
        m_controllerCE2.setDataLimiteRevisao(c4.getTime());
        m_controllerCE2.setDataLimiteSubmissaoFinal(c4.getTime());
        m_controllerCE2.addOrganizador("u1");
        m_controllerCE2.registaEvento();

        CriarSessaoTematicaController m_controllerCT2 = new CriarSessaoTematicaController(m_empresa);
        m_controllerCT2.setEvento(m_controllerCE2.getEvento());
        Calendar c5 = Calendar.getInstance();
        c5.set(Calendar.SECOND, c5.get(Calendar.SECOND) + 10);
        Calendar c6 = Calendar.getInstance();
        c6.set(Calendar.MINUTE, c6.get(Calendar.MINUTE) + 5);
        Calendar c7 = Calendar.getInstance();
        c7.set(Calendar.MINUTE, c7.get(Calendar.MINUTE) + 5);
        m_controllerCT2.setDados("123", "st", c5.getTime(), c6.getTime(), c7.getTime(), c7.getTime(), c7.getTime());
        m_controllerCT2.addProponente("u1");
        m_controllerCT2.registaProponente();
        m_controllerCT2.registaSessaoTematica();

        CriarEventoController m_controllerCE3 = new CriarEventoController(m_empresa);
        m_controllerCE3.novoEvento();
        m_controllerCE3.setTitulo("Titulo -3");
        m_controllerCE3.setDescricao("Descricao -3");
        m_controllerCE3.setLocal("Local -3");
        Calendar c10 = Calendar.getInstance();
        m_controllerCE3.setDataInicio(c10.getTime());
        c10.set(Calendar.SECOND, c10.get(Calendar.SECOND) + 5);
        m_controllerCE3.setDataInicioSubmissao(c10.getTime());
        c10.set(Calendar.MINUTE, c10.get(Calendar.MINUTE) + 5);
        m_controllerCE3.setDataFim(c10.getTime());
        m_controllerCE3.setDataFimSubmissao(c10.getTime());
        m_controllerCE3.setDataInicioDistribuicao(c10.getTime());
        m_controllerCE3.setDataLimiteRevisao(c10.getTime());
        m_controllerCE3.setDataLimiteSubmissaoFinal(c10.getTime());
        m_controllerCE3.addOrganizador("u1");
        m_controllerCE3.registaEvento();

    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of escreverUtilizadorXML method, of class EscreverXML.
     */
    @Test
    public void testEscreverUtilizadorXML() {
        System.out.println("escreverUtilizadorXML");
        Empresa empresa = m_empresa;
        String caminho = "testEscreverUtilizadorXML.xml";
        EscreverXML instance = new EscreverXML();
        boolean expResult = true;
        boolean result = instance.escreverUtilizadorXML(empresa, caminho);
        assertEquals(expResult, result);
    }

    /**
     * Test of escreverLocalXML method, of class EscreverXML.
     */
    @Test
    public void testEscreverLocalXML() {
        System.out.println("escreverLocalXML");
        Empresa empresa = m_empresa;
        String caminho = "testEscreverLocalXML.xml";
        EscreverXML instance = new EscreverXML();
        boolean expResult = true;
        boolean result = instance.escreverLocalXML(empresa, caminho);
        assertEquals(expResult, result);
    }

    /**
     * Test of escreverEventoXML method, of class EscreverXML.
     */
    @Test
    public void testEscreverEventoXML() {
        System.out.println("escreverEventoXML");
        Empresa empresa = m_empresa;
        String caminho = "testEscreverEventoXML.xml";
        EscreverXML instance = new EscreverXML();
        boolean expResult = true;
        boolean result = instance.escreverEventoXML(empresa, caminho);
        assertEquals(expResult, result);
    }
    
}
