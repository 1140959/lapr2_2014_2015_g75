package utils;

import java.math.BigDecimal;
import java.util.ArrayList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author David
 */
public class CodificacaoAritmeticaTest {

    private static ArrayList<String> caracteresMain = new ArrayList<>();
    private static ArrayList<BigDecimal> tabelaMain = new ArrayList<>();
    private static ArrayList<String> caracteresMain2 = new ArrayList<>();
    private static ArrayList<BigDecimal> tabelaMain2 = new ArrayList<>();

    public CodificacaoAritmeticaTest() {
        caracteresMain = caracteresteste();
        tabelaMain = tabelaTeste();
        caracteresMain2 = caracteresteste2();
        tabelaMain2 = tabelaTeste2();
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of codifica method, of class CodificacaoAritmetica.
     */
    @Test
    public void testCodifica() {
        System.out.println("codifica");
        String password = "eaii!";
        CodificacaoAritmetica instance = new CodificacaoAritmetica(caracteresMain, tabelaMain);
        BigDecimal expResult = BigDecimal.valueOf(0.23354);
        BigDecimal result = instance.codifica(password);
        assertEquals(expResult, result);
    }

    /**
     * Test of descodifica method, of class CodificacaoAritmetica.
     */
    @Test
    public void testDescodifica() {
        System.out.println("descodifica");
        BigDecimal codificado = BigDecimal.valueOf(0.23354);
        CodificacaoAritmetica instance = new CodificacaoAritmetica(caracteresMain, tabelaMain);
        String expResult = "eaii!";
        String result = instance.descodifica(codificado);
        assertEquals(expResult, result);
    }

    /**
     * testa a codificação de caracteres desconhecidos
     */
    @Test
    public void caracteresInvalidos() {
        String pass = "$a$z";
        String expResult = "@a@@";
        CodificacaoAritmetica instance = new CodificacaoAritmetica(caracteresMain2, tabelaMain2);

        BigDecimal codificado = instance.codifica(pass);
        String result = instance.descodifica(codificado);

        assertEquals(expResult, result);
    }

    private static ArrayList<BigDecimal> tabelaTeste() {
        ArrayList<BigDecimal> tabela = new ArrayList<>();
        tabela.add(BigDecimal.valueOf(0.2));
        tabela.add(BigDecimal.valueOf(0.3));
        tabela.add(BigDecimal.valueOf(0.1));
        tabela.add(BigDecimal.valueOf(0.2));
        tabela.add(BigDecimal.valueOf(0.1));
        tabela.add(BigDecimal.valueOf(0.1));

        return tabela;
    }

    private static ArrayList<BigDecimal> tabelaTeste2() {
        ArrayList<BigDecimal> tabela = new ArrayList<>();
        tabela.add(BigDecimal.valueOf(0.2));
        tabela.add(BigDecimal.valueOf(0.3));
        tabela.add(BigDecimal.valueOf(0.1));
        tabela.add(BigDecimal.valueOf(0.1));
        tabela.add(BigDecimal.valueOf(0.1));
        tabela.add(BigDecimal.valueOf(0.1));
        tabela.add(BigDecimal.valueOf(0.1));

        return tabela;
    }

    private static ArrayList<String> caracteresteste() {
        ArrayList<String> caracteres = new ArrayList<>();
        caracteres.add("a");
        caracteres.add("e");
        caracteres.add("i");
        caracteres.add("o");
        caracteres.add("u");
        caracteres.add("!");

        return caracteres;
    }

    private static ArrayList<String> caracteresteste2() {
        ArrayList<String> caracteres = new ArrayList<>();
        caracteres.add("a");
        caracteres.add("e");
        caracteres.add("i");
        caracteres.add("o");
        caracteres.add("u");
        caracteres.add("!");
        caracteres.add("@");

        return caracteres;
    }
}
