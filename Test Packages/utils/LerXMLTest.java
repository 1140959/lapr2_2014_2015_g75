/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import eventoscientificos.domain.Empresa;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Carlos
 */
public class LerXMLTest {
    
    public LerXMLTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of lerUtilizadorXML method, of class LerXML.
     */
    @Test
    public void testLerUtilizadorXML() {
        System.out.println("lerUtilizadorXML");
        Empresa empresa = new Empresa();
        String caminho = "testEscreverUtilizadorXML.xml";
        LerXML instance = new LerXML();
        boolean expResult = true;
        boolean result = instance.lerUtilizadorXML(empresa, caminho);
        assertEquals(expResult, result);
    }

    /**
     * Test of lerLocalXML method, of class LerXML.
     */
    @Test
    public void testLerLocalXML() {
        System.out.println("lerLocalXML");
        String caminho = "testEscreverLocalXML.xml";
        LerXML instance = new LerXML();
        boolean expResult = true;
        boolean result = instance.lerLocalXML(caminho);
        assertEquals(expResult, result);
    }

    /**
     * Test of lerEventoXML method, of class LerXML.
     */
    @Test
    public void testLerEventoXML() {
        System.out.println("lerEventoXML");
        Empresa empresa = new Empresa();
        String caminho = "testEscreverEventoXML.xml";
        LerXML instance = new LerXML();
        instance.lerUtilizadorXML(empresa, "testEscreverUtilizadorXML.xml");
        instance.lerLocalXML("testEscreverLocalXML.xml");
        boolean expResult = true;
        boolean result = instance.lerEventoXML(empresa, caminho);
        assertEquals(expResult, result);
    }
    
}
