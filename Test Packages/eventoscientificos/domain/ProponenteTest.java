/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.domain;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author BrunoFernandes
 */
public class ProponenteTest {

    private static Empresa empresa;

    public ProponenteTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getNome method, of class Proponente.
     */
    @Test
    public void testGetNome() {
        System.out.println("getNome");
        Utilizador u= new Utilizador("u1", "u1", "1231","u1@isep.pt");
        Proponente instance = new Proponente(u);
        String expResult = "u1";
        String result = instance.getNome();
        assertEquals(expResult, result);
    }


    /**
     * Test of valida method, of class Proponente.
     */
    @Test
    public void testValida() {
        System.out.println("valida");
        Utilizador u= new Utilizador("u1", "u1", "1231","u1@isep.pt");
        Proponente instance = new Proponente(u);
        boolean expResult = true;
        boolean result = instance.valida();
        assertEquals(expResult, result);
    }

}
