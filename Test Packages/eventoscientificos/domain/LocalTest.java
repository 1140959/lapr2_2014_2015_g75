/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.domain;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author BrunoFernandes
 */
public class LocalTest {
    
    public LocalTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of setLocal method, of class Local.
     */
    @Test
    public void testSetLocal() {
        System.out.println("setLocal");
        String strLocal = "Local";
        Local instance = new Local();
        instance.setLocal(strLocal);
    }

    /**
     * Test of valida method, of class Local.
     */
    @Test
    public void testValida() {
        System.out.println("valida");
        Local instance = new Local();
        boolean expResult = true;
        boolean result = instance.valida();
        assertEquals(expResult, result);
    }

    /**
     * Test of toString method, of class Local.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        Local instance = new Local();
        instance.setLocal("local");
        String expResult = "local";
        String result = instance.toString();
        assertEquals(expResult, result);
    }

    /**
     * Test of getLocal method, of class Local.
     */
    @Test
    public void testGetLocal() {
        System.out.println("getLocal");
        Local instance = new Local();
        instance.setLocal("local");
        String expResult = "local";
        String result = instance.getM_strLocal();
        assertEquals(expResult, result);
    }
    
}
