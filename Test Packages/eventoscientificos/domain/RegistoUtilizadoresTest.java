/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.domain;

import eventoscientificos.controllers.RegistarUtilizadorController;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Bruno e Ricardo
 */
public class RegistoUtilizadoresTest {

    private static Empresa testeEmpresa = new Empresa();

    private static List<Utilizador> testeListaUtilizadores = new ArrayList<Utilizador>();

    private static Utilizador testeUtilizador = new Utilizador();

    public RegistoUtilizadoresTest() {
    }

    @BeforeClass
    public static void setUpClass() {

    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Teste do método novoUtilizador da classe RegistoUtilizadores.
     */
    @Test
    public void testNovoUtilizador() {
        System.out.println("INÍCIO TESTE: novoUtilizador");
        RegistoUtilizadores instance = new RegistoUtilizadores();
        Utilizador expResult = testeUtilizador;
        Utilizador result = instance.novoUtilizador();
        assertEquals(expResult.getNome(), result.getNome());
        assertEquals(expResult.getUsername(), result.getUsername());
        assertEquals(expResult.getEmail(), result.getEmail());
        System.out.println("FIM TESTE: novoUtilizador");
    }

    /**
     * Teste do método registaUtilizador da classe RegistoUtilizadores.
     */
    @Test
    public void testRegistaUtilizador() {
        System.out.println("INÍCIO TESTE: registaUtilizador");
        Utilizador u = new Utilizador("nome", "username", "password", "email@email.com");
        RegistoUtilizadores instance = new RegistoUtilizadores();
        boolean expResult = true;
        boolean result = instance.registaUtilizador(u);
        assertEquals(expResult, result);
        System.out.println("FIM TESTE: registaUtilizador");
    }

    /**
     * Teste do método getUtilizadorByID da classe RegistoUtilizadores.
     */
    @Test
    public void testGetUtilizadorByID() {
        Utilizador u = new Utilizador("nome", "username", "password", "email@email.com");
        String strId = "username";
        System.out.println("INÍCIO TESTE: getUtilizadorByID");
        RegistoUtilizadores instance = new RegistoUtilizadores();
        instance.registaUtilizador(u);
        String expResult = u.getNome();
        String result = instance.getUtilizadorByID(strId).getNome();
        assertEquals(expResult, result);
        System.out.println("FIM TESTE: getUtilizadorByID");
    }

    /**
     * Teste do método getUtilizadorByEmail da classe RegistoUtilizadores.
     */
    @Test
    public void testGetUtilizadorByEmail() {
        System.out.println("INÍCIO TESTE: getUtilizadorByEmail");
        String strEmail = "";
        RegistoUtilizadores instance = new RegistoUtilizadores();
        Utilizador expResult = null;
        Utilizador result = instance.getUtilizadorByEmail(strEmail);
        assertEquals(expResult, result);
        System.out.println("FIM TESTE: getUtilizadorByEmail");
    }

    /**
     * Teste do método testAlteraUtilizador da classe RegistoUtilizadores.
     */
    @Test
    public void testAlteraUtilizador() {
        System.out.println("INÍCIO TESTE: testAlteraUtilizador");
        Utilizador uOriginal = new Utilizador("nome", "username", "password", "email@email.com");
        Utilizador uClone = new Utilizador("nome", "username", "password", "emailmodificado@email.com");
        RegistoUtilizadores instance = new RegistoUtilizadores();
        boolean expResult = true;
        boolean result = instance.alteraUtilizador(uOriginal, uClone);
        assertEquals(expResult, result);
        System.out.println("FIM TESTE: testAlteraUtilizador");
    }

    /**
     * Test of getListaUtilizadores method, of class RegistoUtilizadores.
     */
    @Test
    public void testGetListaUtilizadores() {
        System.out.println("INÍCIO TESTE: getListaUtilizadores");
        RegistoUtilizadores instance = new RegistoUtilizadores();
        List<Utilizador> expResult = testeListaUtilizadores;
        List<Utilizador> result = instance.getListaUtilizadores();
        assertEquals(expResult, result);
        System.out.println("FIM TESTE: getListaUtilizadores");
    }

    /**
     * Test of toString method, of class RegistoUtilizadores.
     */
    @Test
    public void testToString() {
        System.out.println("INÍCIO TESTE: toString");
        RegistoUtilizadores instance = new RegistoUtilizadores();
        String expResult = new ArrayList<>().toString();
        String result = instance.toString();
        assertEquals(expResult, result);
        System.out.println("FIM TESTE: toString");
    }

    /**
     * Test of showData method, of class RegistoUtilizadores.
     */
    @Test
    public void testShowData() {
        System.out.println("INÍCIO TESTE: showData");
        RegistoUtilizadores instance = new RegistoUtilizadores();
        String expResult = new ArrayList<>().toString();
        String result = instance.showData();
        assertEquals(expResult, result);
        System.out.println("FIM TESTE: showData");

    }

}
