package eventoscientificos.domain;

import eventoscientificos.controllers.CriarEventoController;
import eventoscientificos.controllers.CriarSessaoTematicaController;
import eventoscientificos.controllers.DefinirCPController;
import eventoscientificos.controllers.RegistarUtilizadorController;
import eventoscientificos.controllers.SubmeterArtigoController;
import eventoscientificos.state.STState;
import eventoscientificos.state.STStateCPDefinida;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Claudio
 */
public class SessaoTematicaTest {

    private static Empresa empresa = new Empresa();
    private static CriarSessaoTematicaController m_controllerCT;
    private static RegistarUtilizadorController m_controllerRU;
    private static CriarEventoController m_controllerCE;
    private static SubmeterArtigoController m_controllerSA;
    private static Calendar c;

    public SessaoTematicaTest() {
    }

    @BeforeClass
    public static void setUpClass() {
        c = Calendar.getInstance();
        c.getTime();
        empresa = new Empresa();

        m_controllerRU = new RegistarUtilizadorController(empresa);
        m_controllerRU.novoUtilizador();
        m_controllerRU.setDados("u1", "u1", "u1", "u1@isep.ipp.pt");
        m_controllerRU.novoUtilizador();
        m_controllerRU.setDados("u2", "u2", "u2", "u2@isep.ipp.pt");

        m_controllerCE = new CriarEventoController(empresa);
        m_controllerCE.novoEvento();
        m_controllerCE.setTitulo("Titulo");
        m_controllerCE.setDescricao("Descricao");
        m_controllerCE.setLocal("Local");
        m_controllerCE.setDataInicio(c.getTime());
        m_controllerCE.setDataInicioSubmissao(c.getTime());
        m_controllerCE.setDataFim(c.getTime());
        m_controllerCE.setDataFimSubmissao(c.getTime());
        m_controllerCE.setDataInicioDistribuicao(c.getTime());
        m_controllerCE.setDataLimiteRevisao(c.getTime());
        m_controllerCE.setDataLimiteSubmissaoFinal(c.getTime());
        m_controllerCE.addOrganizador("u1");
        m_controllerCE.registaEvento();

        m_controllerCT = new CriarSessaoTematicaController(empresa);
        m_controllerCT.setEvento(m_controllerCT.getListaEventosRegistadosDoUtilizador("u1").get(0));
        m_controllerCT.setDados("123", "st", c.getTime(), c.getTime(), c.getTime(), c.getTime(), c.getTime());
        m_controllerCT.addProponente("u2");
        m_controllerCT.registaProponente();
        m_controllerCT.registaSessaoTematica();

        DefinirCPController m_controllerCP = new DefinirCPController(empresa);
        m_controllerCP.novaCP(m_controllerCP.getListaCPDefiniveisEmDefinicao("u1").get(0));
        m_controllerCP.novoMembroCP("u1");
        m_controllerCP.addMembroCP();
        boolean registaCP = m_controllerCP.registaCP();

        m_controllerCP = new DefinirCPController(empresa);
        m_controllerCP.novaCP(m_controllerCP.getListaCPDefiniveisEmDefinicao("u2").get(0));
        m_controllerCP.novoMembroCP("u1");
        m_controllerCP.addMembroCP();
        m_controllerCP.registaCP();

        m_controllerSA = new SubmeterArtigoController(empresa);
        m_controllerSA.selectSubmissivel(m_controllerCT.getSessaoTematica());
        m_controllerSA.setDados("Artigo 1", "Artigo - Resumo 1");
        Autor aut = m_controllerSA.novoAutor("u1", "strAfiliacao", "u1@isep.ipp.pt");
        System.out.println("\n\n\n\n\n teste " + m_controllerSA.addAutor(aut));
        m_controllerSA.setCorrespondente(aut);
        List<String> listPC = new ArrayList<>();
        listPC.add("palavra1");
        listPC.add("palavra2");
        m_controllerSA.setPalavrasChave(listPC);
        m_controllerSA.setFicheiro("Ficheiro PDF 1");
        m_controllerSA.registarSubmissao();
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Teste do método valida da classe SessaoTematica
     */
    @Test
    public void testValida() {
        System.out.println("valida");
        Calendar c = Calendar.getInstance();
        SessaoTematica instance = new SessaoTematica("123", "descricao", c.getTime(),
                c.getTime(), c.getTime(), c.getTime(), c.getTime());
        boolean expResult = true;
        boolean result = instance.valida();
        assertEquals(expResult, result);

    }

    /**
     * Test of novaCP method, of class SessaoTematica.
     */
    @Test
    public void testNovaCP() {
        System.out.println("novaCP");
        Calendar c = Calendar.getInstance();
        SessaoTematica instance = new SessaoTematica("123", "descricao", c.getTime(),
                c.getTime(), c.getTime(), c.getTime(), c.getTime());
        CP expResult = new CPSessaoTematica();
        CP result = instance.novaCP();
        assertEquals(expResult, result);
    }

    /**
     * Test of getDataInicioSubmissao method, of class SessaoTematica.
     */
    @Test
    public void testGetDataInicioSubmissao() {
        System.out.println("getDataInicioSubmissao");
        SessaoTematica instance = m_controllerCT.getSessaoTematica();
        Date expResult = c.getTime();
        Date result = instance.getDataInicioSubmissao();
        assertEquals(expResult, result);
    }

    /**
     * Test of getDataInicioDistribuicao method, of class SessaoTematica.
     */
    @Test
    public void testGetDataInicioDistribuicao() {
        System.out.println("getDataInicioDistribuicao");
        SessaoTematica instance = m_controllerCT.getSessaoTematica();
        Date expResult = c.getTime();
        Date result = instance.getDataInicioDistribuicao();
        assertEquals(expResult, result);
    }


}
