/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.domain;

import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Bruno
 */
public class ListaRevisoesTest {

    ListaRevisoes m_listRevisao;
    ListaRevisoes outraLista;

    Revisao r1;
    Revisao r2;

    public ListaRevisoesTest() {
        m_listRevisao = new ListaRevisoes();
        outraLista= new ListaRevisoes();
        r1 = new Revisao();
        r2 = new Revisao();

        r1.setConfianca(3);
        r1.setJustificacao("teste1");
        r1.setAdequacao(2);
        r1.setOriginalidade(2);
        r1.setM_Qualidade(3);
        r1.setM_Recomendacao(0);

        r2.setConfianca(4);
        r2.setJustificacao("teste2");
        r2.setAdequacao(1);
        r2.setOriginalidade(4);
        r2.setM_Qualidade(4);
        r2.setM_Recomendacao(2);

        m_listRevisao.addRevisao(r1);
        outraLista.addRevisao(r2);

    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getRevisoesRevisor method, of class ListaRevisoes.
     */
    @Test
    public void testGetRevisoesRevisor() {
        System.out.println("getRevisoesRevisor");
        String id = "id";
        ListaRevisoes instance = new ListaRevisoes();
        List<Revisao> expResult = new ArrayList<>();
        expResult.add(new Revisao());
        List<Revisao> result = instance.getRevisoesRevisor(id);
        assertEquals(expResult, result);
    }

    /**
     * Test of valida method, of class ListaRevisoes.
     */
    @Test
    public void testValida() {
        System.out.println("valida");
        boolean expResult = true;
        boolean result = m_listRevisao.valida(r1);
        assertEquals(expResult, result);
    }

    /**
     * Test of equals method, of class ListaRevisoes.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        boolean expResult = false;
        boolean result = m_listRevisao.equals(outraLista);
        assertEquals(expResult, result);
    }

}
