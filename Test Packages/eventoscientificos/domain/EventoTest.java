package eventoscientificos.domain;

import eventoscientificos.state.EventoState;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author David
 */
public class EventoTest {

    public EventoTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of setDataLimiteRevisao method, of class Evento.
     */
    @Test
    public void testSetDataLimiteRevisao() {
        System.out.println("INÍCIO TESTE: setDataLimiteRevisao");
        Date m_strDataLimiteRevisao = new Date();
        Evento instance = new Evento();
        instance.setDataLimiteRevisao(m_strDataLimiteRevisao);
        System.out.println("FIM TESTE: setDataLimiteRevisao");
    }

    /**
     * Test of setCP method, of class Evento.
     */
    @Test
    public void testSetCP() {
        System.out.println("INÍCIO TESTE: setCP");
        CP cp = new CPEvento();
        Evento instance = new Evento();
        boolean expResult = false;
        boolean result = instance.setCP(cp);
        assertEquals(expResult, result);
        System.out.println("FIM TESTE: setCP");

    }

    /**
     * Test of isInRegistado method, of class Evento.
     */
    @Test
    public void testIsInRegistado() {
        System.out.println("INÍCIO TESTE: isInRegistado");
        Evento instance = new Evento();
        boolean expResult = false;
        boolean result = instance.isInRegistado();
        assertEquals(expResult, result);
        System.out.println("FIM TESTE: isInRegistado");
    }

    /**
     * Test of isInSTDefinidas method, of class Evento.
     */
    @Test
    public void testIsInSTDefinidas() {
        System.out.println("INÍCIO TESTE: isInSTDefinidas");
        Evento instance = new Evento();
        boolean expResult = false;
        boolean result = instance.isInSTDefinidas();
        assertEquals(expResult, result);
        System.out.println("FIM TESTE: isInSTDefinidas");
    }

    /**
     * Test of isInCPDefinida method, of class Evento.
     */
    @Test
    public void testIsInCPDefinida() {
        System.out.println("INÍCIO TESTE: isInCPDefinida");
        Evento instance = new Evento();
        boolean expResult = false;
        boolean result = instance.isInCPDefinida();
        assertEquals(expResult, result);
        System.out.println("FIM TESTE: isInCPDefinida");
    }

    /**
     * Test of isInEmSubmissao method, of class Evento.
     */
    @Test
    public void testIsInEmSubmissao() {
        System.out.println("INÍCIO TESTE: isInEmSubmissao");
        Evento instance = new Evento();
        boolean expResult = false;
        boolean result = instance.isInEmSubmissao();
        assertEquals(expResult, result);
        System.out.println("FIM TESTE: isInEmSubmissao");
    }

    /**
     * Test of isInEmDetecaoConflitos method, of class Evento.
     */
    @Test
    public void testIsInEmDetecaoConflitos() {
        System.out.println("INÍCIO TESTE: isInEmDetecaoConflitos");
        Evento instance = new Evento();
        boolean expResult = false;
        boolean result = instance.isInEmDetecaoConflitos();
        assertEquals(expResult, result);
        System.out.println("FIM TESTE: isInEmDetecaoConflitos");
    }

    /**
     * Test of isInEmLicitacao method, of class Evento.
     */
    @Test
    public void testIsInEmLicitacao() {
        System.out.println("INÍCIO TESTE: isInEmLicitacao");
        Evento instance = new Evento();
        boolean expResult = false;
        boolean result = instance.isInEmLicitacao();
        assertEquals(expResult, result);
        System.out.println("FIM TESTE: isInEmLicitacao");
    }

    /**
     * Test of isInEmDistribuicao method, of class Evento.
     */
    @Test
    public void testIsInEmDistribuicao() {
        System.out.println("INÍCIO TESTE: isInEmDistribuicao");
        Evento instance = new Evento();
        boolean expResult = false;
        boolean result = instance.isInEmDistribuicao();
        assertEquals(expResult, result);
        System.out.println("FIM TESTE: isInEmDistribuicao");
    }

    /**
     * Test of isInEmRevisao method, of class Evento.
     */
    @Test
    public void testIsInEmRevisao() {
        System.out.println("INÍCIO TESTE: isInEmRevisao");
        Evento instance = new Evento();
        boolean expResult = false;
        boolean result = instance.isInEmRevisao();
        assertEquals(expResult, result);
        System.out.println("FIM TESTE: isInEmRevisao");
    }

    /**
     * Test of isInEmDecisao method, of class Evento.
     */
    @Test
    public void testIsInEmDecisao() {
        System.out.println("INÍCIO TESTE: isInEmDecisao");
        Evento instance = new Evento();
        boolean expResult = false;
        boolean result = instance.isInEmDecisao();
        assertEquals(expResult, result);
        System.out.println("FIM TESTE: isInEmDecisao");
    }

    /**
     * Test of isInEmDecidido method, of class Evento.
     */
    @Test
    public void testIsInEmDecidido() {
        System.out.println("INÍCIO TESTE: isInEmDecidido");
        Evento instance = new Evento();
        boolean expResult = false;
        boolean result = instance.isInEmDecidido();
        assertEquals(expResult, result);
        System.out.println("FIM TESTE: isInEmDecidido");
    }

    /**
     * Test of setStateRegistado method, of class Evento.
     */
    @Test
    public void testSetStateRegistado() {
        System.out.println("INÍCIO TESTE: setStateRegistado");
        Evento instance = new Evento();
        boolean expResult = true;
        boolean result = instance.setStateRegistado();
        assertEquals(expResult, result);
        System.out.println("FIM TESTE: setStateRegistado");
    }

    /**
     * Test of setStateSTDefinidas method, of class Evento.
     */
    @Test
    public void testSetStateSTDefinidas() {
        System.out.println("INÍCIO TESTE: setStateSTDefinidas");
        Evento instance = new Evento();
        boolean expResult = false;
        boolean result = instance.setStateSTDefinidas();
        assertEquals(expResult, result);
        System.out.println("FIM TESTE: setStateSTDefinidas");
    }

    /**
     * Test of setStateCPDefinida method, of class Evento.
     */
    @Test
    public void testSetStateCPDefinida() {
        System.out.println("INÍCIO TESTE: setStateCPDefinida");
        Evento instance = new Evento();
        boolean expResult = false;
        boolean result = instance.setStateCPDefinida();
        assertEquals(expResult, result);
        System.out.println("FIM TESTE: setStateCPDefinida");
    }

    /**
     * Test of setStateEmSubmissao method, of class Evento.
     */
    @Test
    public void testSetStateEmSubmissao() {
        System.out.println("INÍCIO TESTE: setStateEmSubmissao");
        Evento instance = new Evento();
        boolean expResult = false;
        boolean result = instance.setStateEmSubmissao();
        assertEquals(expResult, result);
        System.out.println("FIM TESTE: setStateEmSubmissao");
    }

    /**
     * Test of setStateEmDetecaoConflitos method, of class Evento.
     */
    @Test
    public void testSetStateEmDetecaoConflitos() {
        System.out.println("INÍCIO TESTE: setStateEmDetecaoConflitos");
        Evento instance = new Evento();
        boolean expResult = false;
        boolean result = instance.setStateEmDetecaoConflitos();
        assertEquals(expResult, result);
        System.out.println("FIM TESTE: setStateEmDetecaoConflitos");
    }

    /**
     * Test of setStateEmLicitacao method, of class Evento.
     */
    @Test
    public void testSetStateEmLicitacao() {
        System.out.println("INÍCIO TESTE: setStateEmLicitacao");
        Evento instance = new Evento();
        boolean expResult = false;
        boolean result = instance.setStateEmLicitacao();
        assertEquals(expResult, result);
        System.out.println("FIM TESTE: setStateEmLicitacao");
    }

    /**
     * Test of setStateEmDistribuicao method, of class Evento.
     */
    @Test
    public void testSetStateEmDistribuicao() {
        System.out.println("INÍCIO TESTE: setStateEmDistribuicao");
        Evento instance = new Evento();
        boolean expResult = false;
        boolean result = instance.setStateEmDistribuicao();
        assertEquals(expResult, result);
        System.out.println("FIM TESTE: setStateEmDistribuicao");
    }

    /**
     * Test of setStateEmRevisao method, of class Evento.
     */
    @Test
    public void testSetStateEmRevisao() {
        System.out.println("INÍCIO TESTE: setStateEmRevisao");
        Evento instance = new Evento();
        boolean expResult = false;
        boolean result = instance.setStateEmRevisao();
        assertEquals(expResult, result);
        System.out.println("FIM TESTE: setStateEmRevisao");
    }

    /**
     * Test of setStateEmDecisao method, of class Evento.
     */
    @Test
    public void testSetStateEmDecisao() {
        System.out.println("INÍCIO TESTE: setStateEmDecisao");
        Evento instance = new Evento();
        boolean expResult = false;
        boolean result = instance.setStateEmDecisao();
        assertEquals(expResult, result);
        System.out.println("FIM TESTE: setStateEmDecisao");
    }

    /**
     * Test of setStateEmDecidido method, of class Evento.
     */
    @Test
    public void testSetStateEmDecidido() {
        System.out.println("INÍCIO TESTE: setStateEmDecidido");
        Evento instance = new Evento();
        boolean expResult = false;
        boolean result = instance.setStateEmDecidido();
        assertEquals(expResult, result);
        System.out.println("FIM TESTE: setStateEmDecidido");
    }

    /**
     * Test of getDataInicioSubmissao method, of class Evento.
     */
    @Test
    public void testGetDataInicioSubmissao() {
        System.out.println("INÍCIO TESTE: getDataInicioSubmissao");
        Evento instance = new Evento();
        Date expResult = null;
        Date result = instance.getDataInicioSubmissao();
        assertEquals(expResult, result);
        System.out.println("FIM TESTE: getDataInicioSubmissao");
    }

    /**
     * Test of getDataFimSubmissao method, of class Evento.
     */
    @Test
    public void testGetDataFimSubmissao() {
        System.out.println("INÍCIO TESTE: getDataFimSubmissao");
        Evento instance = new Evento();
        Date expResult = null;
        Date result = instance.getDataFimSubmissao();
        assertEquals(expResult, result);
        System.out.println("FIM TESTE: getDataFimSubmissao");
    }

    /**
     * Test of getDataInicioDistribuicao method, of class Evento.
     */
    @Test
    public void testGetDataInicioDistribuicao() {
        System.out.println("INÍCIO TESTE: getDataInicioDistribuicao");
        Evento instance = new Evento();
        Date expResult = null;
        Date result = instance.getDataInicioDistribuicao();
        assertEquals(expResult, result);
        System.out.println("FIM TESTE: getDataInicioDistribuicao");
    }

    /**
     * Test of getListaOrganizadores method, of class Evento.
     */
    @Test
    public void testGetListaOrganizadores() {
        System.out.println("INÍCIO TESTE: getListaOrganizadores");
        Empresa testeEmpresa = new Empresa();

        Evento instance = new Evento();
        ListaOrganizadores testeListaOrganizadores = new ListaOrganizadores();
        ListaOrganizadores expResult = testeListaOrganizadores;
        ListaOrganizadores result = instance.getListaOrganizadores();
        assertEquals(expResult, result);
        System.out.println("FIM TESTE: getListaOrganizadores");
    }

    /**
     * Test of setTitulo method, of class Evento.
     */
    @Test
    public void testSetTitulo() {
        System.out.println("INÍCIO TESTE: setTitulo");
        String strTitulo = "";
        Evento instance = new Evento();
        instance.setTitulo(strTitulo);
        System.out.println("FIM TESTE: setTitulo");
    }

    /**
     * Test of setDescricao method, of class Evento.
     */
    @Test
    public void testSetDescricao() {
        System.out.println("INÍCIO TESTE: setDescricao");
        String strDescricao = "";
        Evento instance = new Evento();
        instance.setDescricao(strDescricao);
        System.out.println("FIM TESTE: setDescricao");
    }

    /**
     * Test of setDataInicio method, of class Evento.
     */
    @Test
    public void testSetDataInicio() {
        System.out.println("INÍCIO TESTE: setDataInicio");
        Date strDataInicio = null;
        Evento instance = new Evento();
        instance.setDataInicio(strDataInicio);
        System.out.println("FIM TESTE: setDataInicio");
    }

    /**
     * Test of setDataFim method, of class Evento.
     */
    @Test
    public void testSetDataFim() {
        System.out.println("INÍCIO TESTE: setDataFim");
        Date strDataFim = null;
        Evento instance = new Evento();
        instance.setDataFim(strDataFim);
        System.out.println("FIM TESTE: setDataFim");
    }

    /**
     * Test of setDataInicioSubmissao method, of class Evento.
     */
    @Test
    public void testSetDataInicioSubmissao() {
        System.out.println("INÍCIO TESTE: setDataInicioSubmissao");
        Date strDataInicioSubmissao = null;
        Evento instance = new Evento();
        instance.setDataInicioSubmissao(strDataInicioSubmissao);
        System.out.println("FIM TESTE: setDataInicioSubmissao");
    }

    /**
     * Test of setDataFimSubmissao method, of class Evento.
     */
    @Test
    public void testSetDataFimSubmissao() {
        System.out.println("INÍCIO TESTE: setDataFimSubmissao");
        Date strDataFimSubmissao = null;
        Evento instance = new Evento();
        instance.setDataFimSubmissao(strDataFimSubmissao);
        System.out.println("FIM TESTE: setDataFimSubmissao");
    }

    /**
     * Test of setDataInicioDistribuicao method, of class Evento.
     */
    @Test
    public void testSetDataInicioDistribuicao() {
        System.out.println("INÍCIO TESTE: setDataInicioDistribuicao");
        Date strDataInicioDistribuicao = null;
        Evento instance = new Evento();
        instance.setDataInicioDistribuicao(strDataInicioDistribuicao);
        System.out.println("FIM TESTE: setDataInicioDistribuicao");
    }

    /**
     * Test of setLocal method, of class Evento.
     */
    @Test
    public void testSetLocal() {
        System.out.println("INÍCIO TESTE: setLocal");
        String strLocal = "";
        Evento instance = new Evento();
        instance.setLocal(strLocal);
        System.out.println("FIM TESTE: setLocal");
    }

    /**
     * Test of getListaSubmissoes method, of class Evento.
     */
    @Test
    public void testGetListaSubmissoes() {
        System.out.println("INÍCIO TESTE: getListaSubmissoes");
        Evento instance = new Evento();
        ListaSubmissoes testeListaSubmissoes = new ListaSubmissoes(instance);
        ListaSubmissoes expResult = testeListaSubmissoes;
        ListaSubmissoes result = instance.getListaSubmissoes();
        assertEquals(expResult, result);
        System.out.println("FIM TESTE: getListaSubmissoes");
    }

    /**
     * Test of getProcessoLicitacao method, of class Evento.
     */
    @Test
    public void testGetProcessoLicitacao() {
        System.out.println("INÍCIO TESTE: getProcessoLicitacao");
        Evento instance = new Evento();
        ProcessoLicitacao expResult = null;
        ProcessoLicitacao result = instance.getProcessoLicitacao();
        assertEquals(expResult, result);
        System.out.println("FIM TESTE: getProcessoLicitacao");
    }

    /**
     * Test of getSessaoTematicaByCodigo method, of class Evento.
     */
    @Test
    public void testGetSessaoTematicaByCodigo() {
        System.out.println("INÍCIO TESTE: getSessaoTematicaByCodigo");
        String codigo = "";
        Evento instance = new Evento();
        SessaoTematica expResult = null;
        SessaoTematica result = instance.getSessaoTematicaByCodigo(codigo);
        assertEquals(expResult, result);
        System.out.println("FIM TESTE: getSessaoTematicaByCodigo");
    }

}
