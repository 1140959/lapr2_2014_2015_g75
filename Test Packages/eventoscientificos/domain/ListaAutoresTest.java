/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.domain;

import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author BrunoFernandes
 */
public class ListaAutoresTest {
    private static Empresa empresa;
    public ListaAutoresTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of novoAutor method, of class ListaAutores.
     */
    @Test
    public void testNovoAutor_3args() {
        System.out.println("novoAutor");
        String strNome = "u1";
        String strAfiliacao = "u1";
        String strEmail = "u1@isep.pt";
        ListaAutores instance = new ListaAutores();
        Autor expResult = new Autor();
        expResult.setAfiliacao("u1");
        expResult.setEmail("u1@isep.pt");
        expResult.setNome("u1");
        Autor result = instance.novoAutor(strNome, strAfiliacao, strEmail);
        assertEquals(expResult, result);
    }
//
//    /**
//     * Test of novoAutor method, of class ListaAutores.
//     */
//    @Test
//    public void testNovoAutor_0args() {
//        System.out.println("novoAutor");
//        ListaAutores instance = new ListaAutores();
//        Autor expResult = new Autor();
//        Autor result = instance.novoAutor();
//        assertEquals(expResult, result);
//    }

    /**
     * Test of addAutor method, of class ListaAutores.
     */
    @Test
    public void testAddAutor() {
        System.out.println("addAutor");
        Autor autor = new Autor();
        autor.setAfiliacao("u1");
        autor.setEmail("u1@isep.pt");
        autor.setNome("u1");
        ListaAutores instance = new ListaAutores();
        boolean expResult = true;
        boolean result = instance.addAutor(autor);
        assertEquals(expResult, result);
    }

    /**
     * Test of getPossiveisAutoresCorrespondentes method, of class ListaAutores.
     */
    @Test
    public void testGetPossiveisAutoresCorrespondentes() {
        System.out.println("getPossiveisAutoresCorrespondentes");
        ListaAutores instance = new ListaAutores();
        Autor autor = new Autor();
        autor.setAfiliacao("u1");
        autor.setEmail("u1@isep.pt");
        autor.setNome("u1");
        instance.addAutor(autor);
        List<Autor> expResult = new ArrayList<>();
        Autor autor1 = new Autor();
        autor1.setAfiliacao("u1");
        autor1.setEmail("u1@isep.pt");
        autor1.setNome("u1");
        expResult.add(autor1);
        List<Autor> result = instance.getPossiveisAutoresCorrespondentes();
        assertEquals(expResult, result);
    }

    /**
     * Test of getAutores method, of class ListaAutores.
     */
    @Test
    public void testGetAutores() {
        System.out.println("getAutores");
        ListaAutores instance = new ListaAutores();
        Autor autor = new Autor();
        autor.setAfiliacao("u1");
        autor.setEmail("u1@isep.pt");
        autor.setNome("u1");
        instance.addAutor(autor);
        List<Autor> expResult = new ArrayList<>();
        Autor autor1 = new Autor();
        autor1.setAfiliacao("u1");
        autor1.setEmail("u1@isep.pt");
        autor1.setNome("u1");
        expResult.add(autor1);
        List<Autor> result = instance.getAutores();
        assertEquals(expResult, result);
    }

    /**
     * Test of hasAutor method, of class ListaAutores.
     */
    @Test
    public void testHasAutor() {
        System.out.println("hasAutor");
        String email = "u1@isep.pt";
        ListaAutores instance = new ListaAutores();
        Autor autor = new Autor();
        autor.setAfiliacao("u1");
        autor.setEmail("u1@isep.pt");
        autor.setNome("u1");
        instance.addAutor(autor);
        boolean expResult = true;
        boolean result = instance.hasAutor(email);
        assertEquals(expResult, result);
    }
    
}
