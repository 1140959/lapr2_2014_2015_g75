/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.domain;

import com.sun.javafx.scene.control.skin.VirtualFlow;
import eventoscientificos.controllers.CriarEventoController;
import eventoscientificos.controllers.CriarSessaoTematicaController;
import eventoscientificos.controllers.RegistarUtilizadorController;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Carlos
 */
public class ListaSessoesTematicasTest {

    private static Empresa empresa = new Empresa();
    private static RegistarUtilizadorController m_controllerRU;
    private static CriarSessaoTematicaController m_controllerCT;
    private static CriarEventoController m_controllerCE;

    public ListaSessoesTematicasTest() {
    }

    @BeforeClass
    public static void setUpClass() {
        Calendar c = Calendar.getInstance();

        m_controllerRU = new RegistarUtilizadorController(empresa);
        m_controllerRU.novoUtilizador();
        m_controllerRU.setDados("u1", "u1", "u1", "u1@isep.ipp.pt");
        m_controllerRU.novoUtilizador();
        m_controllerRU.setDados("u2", "u2", "u2", "u2@isep.ipp.pt");

        m_controllerCE = new CriarEventoController(empresa);
        m_controllerCE.novoEvento();
        m_controllerCE.setTitulo("Titulo");
        m_controllerCE.setDescricao("Descricao");
        m_controllerCE.setLocal("Local");
        m_controllerCE.setDataInicio(c.getTime());
        m_controllerCE.setDataFim(c.getTime());
        m_controllerCE.setDataInicioSubmissao(c.getTime());
        m_controllerCE.setDataFimSubmissao(c.getTime());
        m_controllerCE.setDataInicioDistribuicao(c.getTime());
        m_controllerCE.addOrganizador("u1");
        m_controllerCE.registaEvento();

        List<CPDefinivel> result = empresa.getRegistoEventos().getListaCPDefiniveisEmDefinicaoDoUtilizador("u2");

        m_controllerCT = new CriarSessaoTematicaController(empresa);
        m_controllerCT.setEvento(m_controllerCE.getEvento());
        m_controllerCT.setDados("123", "st", c.getTime(), c.getTime(), c.getTime(), c.getTime(), c.getTime());
        m_controllerCT.addProponente("u2");
        m_controllerCT.registaProponente();
        m_controllerCT.registaSessaoTematica();

    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getListaCPDefiniveisEmDefinicaoDoUtilizador method, of class
     * ListaSessoesTematicas.
     */
    @Test
    public void testGetListaCPDefiniveisEmDefinicaoDoUtilizador() {
        String strID = "u2";
        List<CPDefinivel> expResult = new ArrayList<>();
        List<CPDefinivel> result = new ArrayList<>();

        expResult.add(m_controllerCT.getSessaoTematica());
        result.addAll(empresa.getRegistoEventos().getListaCPDefiniveisEmDefinicaoDoUtilizador(strID));

        assertEquals(expResult, result);
    }

    /**
     * Test of getListaSessaoCPDefiniveisEmDefinicaoDoUtilizador method, of
     * class ListaSessoesTematicas.
     */
    @Test
    public void testGetListaSessaoCPDefiniveisEmDefinicaoDoUtilizador() {
        System.out.println("getListaSessaoCPDefiniveisEmDefinicaoDoUtilizador");
        String strID = "u2";
        ListaSessoesTematicas instance = new ListaSessoesTematicas(m_controllerCE.getEvento());
        List<SessaoTematica> expResult = new ArrayList<>();
        List<SessaoTematica> result = instance.getListaSessaoCPDefiniveisEmDefinicaoDoUtilizador(strID);
        assertEquals(expResult, result);
    }

    /**
     * Test of getDecisiveis method, of class ListaSessoesTematicas.
     */
    @Test
    public void testGetDecisiveis() {
        System.out.println("getDecisiveis");
        String strID = "u1";
        ListaSessoesTematicas instance = new ListaSessoesTematicas(m_controllerCE.getEvento());
        List<Decidivel> expResult = new ArrayList<>();
        List<Decidivel> result = instance.getDecisiveis(strID);
        assertEquals(expResult, result);
    }

    /**
     * Test of getListaSubmissiveisEmSubmissao method, of class
     * ListaSessoesTematicas.
     */
    @Test
    public void testGetListaSubmissiveisEmSubmissao() {
        System.out.println("getListaSubmissiveisEmSubmissao");
        ListaSessoesTematicas instance = new ListaSessoesTematicas(m_controllerCE.getEvento());
        List<Submissivel> expResult = new ArrayList<>();
        List<Submissivel> result = instance.getListaSubmissiveisEmSubmissao();
        assertEquals(expResult, result);
    }

    /**
     * Test of getListaSessaoEmSubmissao method, of class ListaSessoesTematicas.
     */
    @Test
    public void testGetListaSessaoEmSubmissao() {
        System.out.println("getListaSessaoEmSubmissao");
        ListaSessoesTematicas instance = new ListaSessoesTematicas(m_controllerCE.getEvento());
        List<SessaoTematica> expResult = new ArrayList<>();
        List<SessaoTematica> result = instance.getListaSessaoEmSubmissao();
        assertEquals(expResult, result);
    }

    /**
     * Test of getListaEventosSubmissaoById method, of class
     * ListaSessoesTematicas.
     */
    @Test
    public void testGetListaEventosSubmissaoById() {
        System.out.println("getListaEventosSubmissaoById");
        String id = "u1";
        ListaSessoesTematicas instance = new ListaSessoesTematicas(m_controllerCE.getEvento());
        boolean expResult = false;
        boolean result = instance.getListaEventosSubmissaoById(id);
        assertEquals(expResult, result);
    }

    /**
     * Test of getRevisaveisEmRevisaoDoRevisor method, of class
     * ListaSessoesTematicas.
     */
    @Test
    public void testGetRevisaveisEmRevisaoDoRevisor() {
        System.out.println("getRevisaveisEmRevisaoDoRevisor");
        String strID = "u1";
        ListaSessoesTematicas instance = new ListaSessoesTematicas(m_controllerCE.getEvento());
        List<Revisavel> expResult = new ArrayList<>();
        List<Revisavel> result = instance.getRevisaveisEmRevisaoDoRevisor(strID);
        assertEquals(expResult, result);
    }

    /**
     * Test of getListaDeSessaoTematica method, of class ListaSessoesTematicas.
     */
    @Test
    public void testGetListaDeSessaoTematica() {
        System.out.println("getListaDeSessaoTematica");
        ListaSessoesTematicas instance = new ListaSessoesTematicas(m_controllerCE.getEvento());
        List<SessaoTematica> expResult = new ArrayList<>();
        List<SessaoTematica> result = instance.getListaDeSessaoTematica();
        assertEquals(expResult, result);
    }

}
