/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.domain;

import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Ricardo
 */
public class RegistoTipoConflitosTest {
    private static String testeDescricao = "descricao";
    private static MecanismoDetecaoConflito1 testeMecanismo = new MecanismoDetecaoConflito1();
    private static TipoConflito testeTipoConflito = new TipoConflito(testeDescricao,testeMecanismo);
    public RegistoTipoConflitosTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of novoTipoConflito method, of class RegistoTipoConflitos.
     */
    @Test
    public void testNovoTipoConflito() {
        System.out.println("INÍCIO TESTE: novoTipoConflito");
        RegistoTipoConflitos instance = new RegistoTipoConflitos();
        TipoConflito expResult = testeTipoConflito;
        TipoConflito result = instance.novoTipoConflito(testeDescricao, testeMecanismo);
        String esperado = expResult.showData();
        String verificado = result.showData();
        assertEquals(esperado, verificado);
        System.out.println("FIM TESTE: novoTipoConflito");
    }

    /**
     * Test of registaTipoConflito method, of class RegistoTipoConflitos.
     */
    @Test
    public void testRegistaTipoConflito() {
        System.out.println("INÍCIO TESTE: registaTipoConflito");
        TipoConflito tpConflito = testeTipoConflito;
        RegistoTipoConflitos instance = new RegistoTipoConflitos();
        boolean expResult = true;
        boolean result = instance.registaTipoConflito(tpConflito);
        assertEquals(expResult, result);
        System.out.println("FIM TESTE: registaTipoConflito");
    }

    /**
     * Test of getListaTipoConflitos method, of class RegistoTipoConflitos.
     */
    @Test
    public void testGetListaTipoConflitos() {
        System.out.println("INÍCIO TESTE: getListaTipoConflitos");
        RegistoTipoConflitos instance = new RegistoTipoConflitos();
        List<TipoConflito> expResult = new ArrayList<>();
        List<TipoConflito> result = instance.getListaTipoConflitos();
        assertEquals(expResult, result);
        System.out.println("FIM TESTE: getListaTipoConflitos");
    }
    
}
