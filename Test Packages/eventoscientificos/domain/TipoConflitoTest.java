/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.domain;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Ricardo
 */
public class TipoConflitoTest {

    public TipoConflitoTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getDescricao method, of class TipoConflito.
     */
    @Test
    public void testGetDescricao() {
        System.out.println("INÍCIO TESTE: getDescricao");
        TipoConflito instance = new TipoConflito();
        instance.setDescricao("Teste descrição tipo conflito");
        String expResult = "Teste descrição tipo conflito";
        String result = instance.getDescricao();
        assertEquals(expResult, result);
        System.out.println("FIM TESTE: getDescricao");
    }

    /**
     * Test of setDescricao method, of class TipoConflito.
     */
    @Test
    public void testSetDescricao() {
        System.out.println("INÍCIO TESTE: setDescricao");
        String strDescricao = "Teste descrição tipo conflito";
        TipoConflito instance = new TipoConflito();
        instance.setDescricao(strDescricao);
        String expResult = "Teste descrição tipo conflito";
        String result = instance.getDescricao();
        assertEquals(expResult, result);
        System.out.println("FIM TESTE: setDescricao");

    }

    /**
     * Test of getMecanismoDetecaoConflito method, of class TipoConflito.
     */
    @Test
    public void testGetMecanismoDetecaoConflito() {
        System.out.println("INÍCIO TESTE: getMecanismoDetecaoConflito");
        MecanismoDetecaoConflito1 mecanismoTesteFalse = new MecanismoDetecaoConflito1();
        TipoConflito instance = new TipoConflito("Descrição teste", mecanismoTesteFalse);
        MecanismoDetecaoConflito expResult = mecanismoTesteFalse;
        MecanismoDetecaoConflito result = instance.getMecanismoDetecaoConflito();
        assertEquals(expResult, result);
        System.out.println("FIM TESTE: getMecanismoDetecaoConflito");
    }

    /**
     * Test of setMecanismoDetecaoConflito method, of class TipoConflito. Neste
     * teste é selecionado um mecanismo de deteção de conflito que devolve
     * sempre falso. O mesmo é atribuído à instância e, depois, é verificado se
     * é obtido valor false quando se deteta esse mesmo conflito num processo de
     * licitação de um Evento.
     *
     */
    @Test
    public void testSetMecanismoDetecaoConflito() {
        System.out.println("INÍCIO TESTE: setMecanismoDetecaoConflito");
        ProcessoLicitacaoEvento ple = new ProcessoLicitacaoEvento();
        Licitacao l = new Licitacao();
        TipoConflito tc = new TipoConflito();
        MecanismoDetecaoConflito1 mecanismoTesteFalse = new MecanismoDetecaoConflito1();
        TipoConflito instance = new TipoConflito();
        instance.setMecanismoDetecaoConflito(mecanismoTesteFalse);
        boolean expResult = false;
        boolean result = instance.getMecanismoDetecaoConflito().detetarConflito(ple, l, tc);
        assertEquals(expResult, result);
        System.out.println("FIM TESTE: setMecanismoDetecaoConflito");
    }

    /**
     * Test of valida method, of class TipoConflito.
     */
    @Test
    public void testValida() {
        System.out.println("INÍCIO TESTE: valida");
        TipoConflito instance = new TipoConflito();
        boolean expResult = true;
        boolean result = instance.valida();
        assertEquals(expResult, result);
        System.out.println("FIM TESTE: valida");
    }

    /**
     * Test of toString method, of class TipoConflito.
     */
    @Test
    public void testToString() {
        MecanismoDetecaoConflito1 testeMecanismo = new MecanismoDetecaoConflito1();
        String descricaoMecanismo = testeMecanismo.toString();
           
        System.out.println("INÍCIO TESTE: toString");
        TipoConflito instance = new TipoConflito("Descrição Teste",testeMecanismo);
        String expResult = "Descrição Teste - MecanismoDetecaoConflito1 -> sempre falso";
        String result = instance.toString();
        assertEquals(expResult, result);
        System.out.println("FIM TESTE: toString");
    }

}
