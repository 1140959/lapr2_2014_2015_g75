/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.domain;

import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author BrunoFernandes
 */
public class ListaProponentesTest {
    private static Empresa empresa;
    
    public ListaProponentesTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        empresa= new Empresa();
        empresa.getRegistoUtilizadores().novoUtilizador();
        empresa.getRegistoUtilizadores().registaUtilizador(new Utilizador("u1", "u1","13123","u1@isep.pt"));
        
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getListaProponentes method, of class ListaProponentes.
     */
    @Test
    public void testGetListaProponentes() {
        System.out.println("getListaProponentes");
        ListaProponentes instance = new ListaProponentes();
        List<Proponente> expResult = new ArrayList<>();
        List<Proponente> result = instance.getListaProponentes();
        assertEquals(expResult, result);
    }

    /**
     * Test of novoProponente method, of class ListaProponentes.
     */
    @Test
    public void testNovoProponente() {
        System.out.println("novoProponente");
        Utilizador u = new Utilizador("u1", "u1","13123","u1@isep.pt");
        ListaProponentes instance = new ListaProponentes();
        Proponente expResult = new Proponente(u);
        Proponente result = instance.novoProponente(u);
        assertEquals(expResult, result);
    }

    /**
     * Test of registaProponente method, of class ListaProponentes.
     */
    @Test
    public void testRegistaProponente() {
        System.out.println("registaProponente");
        Proponente prop = new Proponente(new Utilizador("u1", "u1","13123","u1@isep.pt"));
        ListaProponentes instance = new ListaProponentes();
        boolean expResult = true;
        boolean result = instance.registaProponente(prop);
        assertEquals(expResult, result);
    }

    /**
     * Test of hasProponente method, of class ListaProponentes.
     */
    @Test
    public void testHasProponente() {
        setUpClass();
        System.out.println("hasProponente");
        String strID = "u1";
        ListaProponentes instance = new ListaProponentes();
        boolean expResult =false;
        boolean result = instance.hasProponente(strID);
        assertEquals(expResult, result);
    }
    
}
