/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.domain;

import eventoscientificos.controllers.RegistarUtilizadorController;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Carlos
 */
public class ListaOrganizadoresTest {

    private static Empresa empresa;
    private static RegistarUtilizadorController Ucontroller;

    public ListaOrganizadoresTest() {
    }

    @BeforeClass
    public static void setUpClass() {
        empresa = new Empresa();

        Ucontroller = new RegistarUtilizadorController(empresa);
        Ucontroller.novoUtilizador();
        Ucontroller.setDados("u1", "u1", "1231", "u1@isep.pt");
        Ucontroller.novoUtilizador();
        Ucontroller.setDados("u2", "u2", "12314", "u2@isep.pt");
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getListaOrganizadores method, of class ListaOrganizadores.
     */
    @Test
    public void testGetListaOrganizadores() {
        System.out.println("getListaOrganizadores");
        ListaOrganizadores instance = new ListaOrganizadores();
        List<Organizador> expResult = new ArrayList<>();
        
        List<Organizador> result = instance.getListaOrganizadores();
        assertEquals(expResult, result);
    }

    /**
     * Test of addOrganizador method, of class ListaOrganizadores.
     */
    @Test
    public void testAddOrganizador() {
        System.out.println("addOrganizador");
        Utilizador u = new Utilizador("u1", "u1", "123412", "u1@isep.ip");
        ListaOrganizadores instance = new ListaOrganizadores();
        boolean expResult = true;
        boolean result = instance.addOrganizador(u);
        assertEquals(expResult, result);
    }

    /**
     * Test of hasOrganizador method, of class ListaOrganizadores.
     */
    @Test
    public void testHasOrganizador() {
        System.out.println("hasOrganizador");
        String strID = "id";
        ListaOrganizadores instance = new ListaOrganizadores();
        boolean expResult = false;
        boolean result = instance.hasOrganizador(strID);
        assertEquals(expResult, result);
    }


}
