/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.domain;

import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author BrunoFernandes
 */
public class CPSessaoTematicaTest {

    public CPSessaoTematicaTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getRevisores method, of class CPSessaoTematica.
     */
    @Test
    public void testGetRevisores() {
        System.out.println("getRevisores");
        Utilizador u = new Utilizador("u1", "u1", "123123", "u1@isep.pt");
        CPSessaoTematica instance = new CPSessaoTematica();
        Revisor r = new Revisor(u);
        instance.addMembroCP(r);
        List<Revisor> expResult = new ArrayList<>();
        expResult.add(r);
        List<Revisor> result = instance.getRevisores();
        assertEquals(expResult, result);
    }

    /**
     * Test of novoMembroCP method, of class CPSessaoTematica.
     */
    @Test
    public void testNovoMembroCP() {
        System.out.println("novoMembroCP");
        Utilizador u = new Utilizador("u1", "u1", "123123", "u1@isep.pt");
        CPSessaoTematica instance = new CPSessaoTematica();
        instance.novoMembroCP(u);
        Revisor expResult = new Revisor(u);
        Revisor result = instance.novoMembroCP(u);
        assertEquals(expResult, result);
    }

    /**
     * Test of addMembroCP method, of class CPSessaoTematica.
     */
    @Test
    public void testAddMembroCP() {
        System.out.println("addMembroCP");
        CPSessaoTematica instance = new CPSessaoTematica();
        Utilizador u = new Utilizador("u1", "u1", "123123", "u1@isep.pt");
        Revisor r = new Revisor(u);
        instance.getRevisores();
        boolean expResult = true;
        boolean result = instance.addMembroCP(r);
        assertEquals(expResult, result);
    }

    /**
     * Test of valida method, of class CPSessaoTematica.
     */
    @Test
    public void testValida() {
        System.out.println("valida");
        CPSessaoTematica instance = new CPSessaoTematica();
        Utilizador u = new Utilizador("u1", "u1", "123123", "u1@isep.pt");
        Revisor r = new Revisor(u);
        instance.addMembroCP(r);
        boolean expResult = true;
        boolean result = instance.valida();
        assertEquals(expResult, result);
    }

}
