/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.domain;

import eventoscientificos.controllers.AlterarSubmissaoController;
import eventoscientificos.controllers.AlterarUtilizadorController;
import eventoscientificos.controllers.CarregarArtigosPorFicheiroController;
import eventoscientificos.controllers.CriarEventoController;
import eventoscientificos.controllers.CriarSessaoTematicaController;
import eventoscientificos.controllers.DecidirSubmissoesController;
import eventoscientificos.controllers.DefinirCPController;
import eventoscientificos.controllers.DefinirTipoConflitoController;
import eventoscientificos.controllers.DetetarConflitosController;
import eventoscientificos.controllers.DistribuirRevisoesController;
import eventoscientificos.controllers.LicitarArtigoController;
import eventoscientificos.controllers.RegistarUtilizadorController;
import eventoscientificos.controllers.RetirarSubmissaoController;
import eventoscientificos.controllers.ReverArtigoController;
import eventoscientificos.controllers.SubmeterArtigoController;
import eventoscientificos.controllers.SubmeterArtigoFinalController;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Ricardo
 */


public class EmpresaTest {

    private static Empresa empresaTeste = new Empresa();
    private static AlterarSubmissaoController m_controllerAS;
    private static AlterarUtilizadorController m_controllerAU;
    private static CarregarArtigosPorFicheiroController m_controllerCAPF;
    private static CriarEventoController m_controllerCE;
    private static CriarSessaoTematicaController m_controllerCST;
    private static DecidirSubmissoesController m_controllerDS;
    private static DefinirCPController m_controllerDCP;
    private static DefinirTipoConflitoController m_controllerDTC;
    private static DetetarConflitosController m_controllerDC;
    private static DistribuirRevisoesController m_controllerDR;
    private static LicitarArtigoController m_controllerLA;
    private static RegistarUtilizadorController m_controllerRU;
    private static RetirarSubmissaoController m_controllerRS;
    private static ReverArtigoController m_controllerRA;
    private static SubmeterArtigoController m_controllerSA;
    private static SubmeterArtigoFinalController m_controllerSAF;
    private static RegistoUtilizadores m_registoUtilizadores;
    

    @BeforeClass
    public static void setUpClass() {
    
//m_controllerAU = new AlterarUtilizadorController(); (precisa de um ID de um utilizador existente na empresa)
        m_controllerCAPF = new CarregarArtigosPorFicheiroController(empresaTeste);
        m_controllerCE = new CriarEventoController(empresaTeste);
        m_controllerCST = new CriarSessaoTematicaController(empresaTeste);
        m_controllerDS = new DecidirSubmissoesController(empresaTeste,"u1");
        m_controllerDCP = new DefinirCPController(empresaTeste);
        m_controllerDTC = new DefinirTipoConflitoController(empresaTeste);
//m_controllerDC = new DetetarConflitosController(); (precisa de um objeto Licitavel)
        m_controllerDR = new DistribuirRevisoesController(empresaTeste);
        m_controllerLA = new LicitarArtigoController(empresaTeste);
        m_controllerRU = new RegistarUtilizadorController(empresaTeste);
 //       m_controllerRS = new RetirarSubmissaoController(empresaTeste, "string teste");
        m_controllerRA = new ReverArtigoController(empresaTeste, "string teste");
        m_controllerSA = new SubmeterArtigoController(empresaTeste);
        m_controllerSAF = new SubmeterArtigoFinalController(empresaTeste);
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getRegistoUtilizadores method, of class Empresa.
     */
    @Test
    public void testGetRegistoUtilizadores() {
        System.out.println("INÍCIO TESTE: getRegistoUtilizadores");
        Empresa instance = empresaTeste;
        RegistoUtilizadores expResult = empresaTeste.getRegistoUtilizadores();
        RegistoUtilizadores result = instance.getRegistoUtilizadores();
        assertEquals(expResult, result);
        System.out.println("FIM TESTE: getRegistoUtilizadores");
    }

    /**
     * Test of getRegistoEventos method, of class Empresa.
     */
    @Test
    public void testGetRegistoEventos() {
        System.out.println("INÍCIO TESTE: getRegistoEventos");
        Empresa instance = empresaTeste;
        RegistoEventos expResult = empresaTeste.getRegistoEventos();
        RegistoEventos result = instance.getRegistoEventos();
        assertEquals(expResult, result);
        System.out.println("FIM TESTE: getRegistoEventos");
    }

    /**
     * Test of getRegistoTipoConflitos method, of class Empresa.
     */
    @Test
    public void testGetRegistoTipoConflitos() {
        System.out.println("INÍCIO TESTE: getRegistoTipoConflitos");
        Empresa instance = empresaTeste;
        RegistoTipoConflitos expResult = empresaTeste.getRegistoTipoConflitos();
        RegistoTipoConflitos result = instance.getRegistoTipoConflitos();
        assertEquals(expResult, result);
        System.out.println("FIM TESTE: getRegistoTipoConflitos");
    }

    /**
     * Test of getListaAdministradores method, of class Empresa.
     */
    @Test
    public void testGetListaAdministradores() {
        System.out.println("INÍCIO TESTE: getListaAdministradores");
        Empresa instance = empresaTeste;
        ListaAdministradores expResult = empresaTeste.getListaAdministradores();
        ListaAdministradores result = instance.getListaAdministradores();
        assertEquals(expResult, result);
        System.out.println("FIM TESTE: getListaAdministradores");
    }

    /**
     * Test of schedule method, of class Empresa.
     
    @Test
    public void testSchedule() {
        System.out.println("INÍCIO TESTE: schedule");
        TimerTask task = null;
        Date date = null;
        Empresa instance = empresaTeste;
        instance.schedule(task, date);
        System.out.println("FIM TESTE: schedule");
    }
*/
    
    /**
     * Test of getMecanismosDecisao method, of class Empresa.
     */
    @Test
    public void testGetMecanismosDecisao() {
        System.out.println("INÍCIO TESTE: getMecanismosDecisao");
        Empresa instance = empresaTeste;
        List<MecanismoDecisao> expResult = new ArrayList<>();
        List<MecanismoDecisao> result = instance.getMecanismosDecisao();
        assertEquals(expResult, result);
        System.out.println("FIM TESTE: getMecanismosDecisao");
    }

    /**
     * Test of getMecanismosDetecaoConflito method, of class Empresa.
     */
    @Test
    public void testGetMecanismosDetecaoConflito() {
        System.out.println("INÍCIO TESTE: getMecanismosDetecaoConflito");
        Empresa instance = empresaTeste;
        List<MecanismoDetecaoConflito> expResult = empresaTeste.getMecanismosDetecaoConflito();
        List<MecanismoDetecaoConflito> result = instance.getMecanismosDetecaoConflito();
        assertEquals(expResult, result);
        System.out.println("FIM TESTE: getMecanismosDetecaoConflito");
    }

    /**
     * Test of getEventosOrganizador method, of class Empresa.
     */
    @Test
    public void testGetEventosOrganizador() {
        System.out.println("INÍCIO TESTE: getEventosOrganizador");
        String strId = "";
        Empresa instance = empresaTeste;
        List<Evento> expResult = new ArrayList<>();
        List<Evento> result = instance.getEventosOrganizador(strId);
        assertEquals(expResult, result);
        System.out.println("FIM TESTE: getEventosOrganizador");
    }

}
