/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.domain;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Bruno
 */
public class SubmissaoTest {
    private final Submissao instance;
    private final Artigo a;
    private Autor a1;
    
    public SubmissaoTest() {
        instance=new Submissao();
        instance.novoArtigo();
        
        ListaAutores la= new ListaAutores();
        a1= new Autor();
        Autor a2= new Autor();
        a1 = new Autor();
        a2 = new Autor();
        a1.setNome("Bruno");
        a1.setEmail("bruno@isep.pt");
        a1.setAfiliacao("isep");
        a2.setNome("teste");
        a2.setEmail("teste@isep.pt");
        a2.setAfiliacao("teste");       
        la.addAutor(a1);
        
        List<String >listaPalavras = new ArrayList<>();
        listaPalavras.add("ola");
        listaPalavras.add("quase");
        
        a= new Artigo("bla", "bla bla bla", la, listaPalavras, a1, "bla.pdf", "btuno@isep.pt", new Date(2015, 06, 25));
        instance.setArtigo(a);
    
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getInfo method, of class Submissao.
     */
    @Test
    public void testGetInfo() {
        System.out.println("getInfo");
        String expResult = "bla";
        String result = instance.toString();
        assertEquals(expResult, result);
    }

    /**
     * Test of setArtigo method, of class Submissao.
     */
    @Test
    public void testSetArtigo() {
        System.out.println("setArtigo");
        Artigo artigo = a;
        instance.setArtigo(artigo);
    }

    /**
     * Test of getM_artigo method, of class Submissao.
     */
    @Test
    public void testGetM_artigo() {
        System.out.println("getM_artigo");
        Artigo expResult = a;
        Artigo result = instance.getArtigo();
        assertEquals(expResult, result);
    }

    /**
     * Test of valida method, of class Submissao.
     */
    @Test
    public void testValida() {
        System.out.println("valida");
        boolean expResult = true;
        boolean result = instance.valida();
        assertEquals(expResult, result);
    }

    /**
     * Test of getAutorCorrespondente method, of class Submissao.
     */
    @Test
    public void testGetAutorCorrespondente() {
        System.out.println("getAutorCorrespondente");
        Autor expResult = a1;        
        Autor result = instance.getAutorCorrespondente();
        assertEquals(expResult.getNome(), result.getNome());
    }
    
}
