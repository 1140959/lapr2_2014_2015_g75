/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.domain;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Bruno
 */
public class RevisaoTest {

    Revisao instance;
    Revisao outraRevisao;

    public RevisaoTest() {
        instance = new Revisao();
        instance.setConfianca(3);
        instance.setJustificacao("teste");
        instance.setAdequacao(2);
        instance.setOriginalidade(2);
        instance.setM_Qualidade(3);
        instance.setM_Recomendacao(0);
    }

    @BeforeClass
    public static void setUpClass() {

    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    /**
     * Test of isConcluida method, of class Revisao.
     */
    @Test
    public void testIsConcluida() {
        System.out.println("isConcluida");
        boolean expResult = true;
        boolean result = instance.isConcluida();
        assertEquals(expResult, result);
    }

    /**
     * Test of isConcluida method, of class Revisao.
     */
    @Test
    public void testIsConcluidaNOT() {
        System.out.println("isConcluidaNOT");
        instance.setJustificacao(" ");
        instance.setM_Recomendacao(5);
        boolean expResult = false;
        boolean result = instance.isConcluida();
        assertEquals(expResult, result);
    }

    /**
     * Test of equals method, of class Revisao
     */
    @Test
    public void testValida() {
        System.out.println("valida");
        boolean expResult = true;
        boolean result = instance.isConcluida();
        assertEquals(expResult, result);
    }

    /**
     * Test of equals method, of class Revisao.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        outraRevisao = new Revisao();
        outraRevisao.setConfianca(1);
        outraRevisao.setJustificacao("teste2");

        boolean expResult = false;
        boolean result = instance.equals(outraRevisao);
        assertEquals(expResult, result);
    }

}
