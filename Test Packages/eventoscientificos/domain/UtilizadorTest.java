package eventoscientificos.domain;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author BrunoFernandes
 */
public class UtilizadorTest {

    private Utilizador instance = new Utilizador("Bruno", "bruno1", "11111", "bruno@isep.pt");

    public UtilizadorTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getNome method, of class Utilizador.
     */
    @Test
    public void testGetNome() {
        System.out.print("testGetNome --> ");

        String expResult = "Bruno";
        String result = instance.getNome();
        assertEquals(expResult, result);
        System.out.println("Passou no teste do getNome");
    }

    /**
     * Test of getPwd method, of class Utilizador.
     */
    @Test
    public void testGetPwd() {
        System.out.print("getPwd --> ");
        String expResult = "11111";
        String result = instance.getPwd();
        assertEquals(expResult, result);
        System.out.println("Passou no teste do getPwd");
    }

    /**
     * Test of getUsername method, of class Utilizador.
     */
    @Test
    public void testGetUsername() {
        System.out.print("getUsername --> ");
        String expResult = "bruno1";
        String result = instance.getUsername();
        assertEquals(expResult, result);
        System.out.println("Passou no teste do getUsername");
    }

    /**
     * Test of getEmail method, of class Utilizador.
     */
    @Test
    public void testGetEmail() {
        System.out.print("getEmail --> ");
        String expResult = "bruno@isep.pt";
        String result = instance.getEmail();
        assertEquals(expResult, result);
        System.out.println("Passou no teste do getEmail");
    }

    /**
     * Test of setNome method, of class Utilizador.
     */
    @Test
    public void testSetNome() {
        System.out.print("setNome --> ");

        String nomeInicial = instance.getNome();
        String strNome = "Jorge";
        instance.setNome(strNome);
        System.out.println("Nome Inicial: " + nomeInicial + ""
                + "\n --> Nome Modificado: " + instance.getNome());
    }

    /**
     * Test of setUsername method, of class Utilizador.
     */
    @Test
    public void testSetUsername() {
        System.out.print("setUsername --> ");

        String usernameInicial = instance.getUsername();
        String strUsername = "jorge1231";
        instance.setUsername(strUsername);
        System.out.println("Username inicial: " + usernameInicial
                + "\n --> Username Modificado: " + instance.getUsername());
    }

    /**
     * Test of setPassword method, of class Utilizador.
     */
    @Test
    public void testSetPassword() {
        System.out.print("setPassword --> ");

        String passInicial = instance.getPwd();
        String strPassword = "12345";
        instance.setPassword(strPassword);
        System.out.println("Pwd inicial: " + passInicial
                + "\n --> PWD Modificado: " + instance.getPwd());
    }

    /**
     * Test of setEmail method, of class Utilizador.
     */
    @Test
    public void testSetEmail() {
        System.out.print("setEmail -->");

        String emailInicial = instance.getEmail();
        String strEmail = "manuel@isep.pt";
        instance.setEmail(strEmail);
        System.out.println("Email inicial: " + emailInicial
                + "\n --> Email Modificado: " + instance.getEmail());
    }

    /**
     * Test of valida method, of class Utilizador.
     */
    @Test
    public void testValida() {
        System.out.print("valida -->");
        boolean expResult = true;
        boolean result = instance.valida();
        assertEquals(expResult, result);
        System.out.println("Utilizador válido");
    }
    /**
     * Test of valida method, of class Utilizador.
     */
    @Test
    public void testValidaNOT() {
        System.out.print("validaNOT ---> ");
        instance.setEmail("bruno");
        boolean expResult = false;
        boolean result = instance.valida();
        assertEquals(expResult, result);
        System.out.println("Utilizador inválido"+instance.toString());
    }

}
