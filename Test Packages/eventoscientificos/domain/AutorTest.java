/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.domain;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author David
 */
public class AutorTest {
    
    public AutorTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of setNome method, of class Autor.
     */
    @Test
    public void testSetNome() {
        System.out.println("setNome");
        String strNome = "u1";
        Autor instance = new Autor();
        instance.setNome("u1");
        instance.setAfiliacao("afiliza");
        instance.setEmail("asd@asd.ad");
        instance.setNome(strNome);
    }

    /**
     * Test of setEmail method, of class Autor.
     */
    @Test
    public void testSetEmail() {
        System.out.println("setEmail");
        String strEmail = "teste@email.com";
        Autor instance = new Autor();
        instance.setNome("u1");
        instance.setAfiliacao("afiliza");
        instance.setEmail("asd@asd.ad");
        instance.setEmail(strEmail);
    }

    /**
     * Test of setAfiliacao method, of class Autor.
     */
    @Test
    public void testSetAfiliacao() {
        System.out.println("setAfiliacao");
        String strAfiliacao = "afiliacao";
        Autor instance = new Autor();
        instance.setNome("u1");
        instance.setAfiliacao("afiliza");
        instance.setEmail("asd@asd.ad");
        instance.setAfiliacao(strAfiliacao);
    }

    /**
     * Test of valida method, of class Autor.
     */
    @Test
    public void testValida() {
        System.out.println("valida");
        Autor instance = new Autor();
        instance.setNome("nome");
        instance.setAfiliacao("afiliacao");
        instance.setEmail("email@ipp.pt");
        boolean expResult = true;
        boolean result = instance.valida();
        assertEquals(expResult, result);
    }

    /**
     * Test of podeSerCorrespondente method, of class Autor.
     */
    @Test
    public void testPodeSerCorrespondente() {
        System.out.println("podeSerCorrespondente");
        Autor instance = new Autor();
        instance.setNome("u1");
        instance.setAfiliacao("afiliza");
        instance.setEmail("asd@asd.ad");
        boolean expResult = true;
        boolean result = instance.podeSerCorrespondente();
        assertEquals(expResult, result);
    }

    /**
     * Test of equals method, of class Autor.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        Autor outroObjeto = new Autor();
        outroObjeto.setAfiliacao("1");
        outroObjeto.setEmail("2");
        outroObjeto.setNome("3");
        
        Autor instance = new Autor();
        instance.setAfiliacao("1");
        instance.setEmail("2");
        instance.setNome("3");
        
        boolean expResult = true;
        boolean result = instance.equals(outroObjeto);
        assertEquals(expResult, result);
    }

    /**
     * Test of toString method, of class Autor.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        Autor instance = new Autor();
        instance.setNome("nome");
        instance.setAfiliacao("afiliação");
        instance.setEmail("email");
        String expResult = "Nome: nome | Afiliação: afiliação | Email: email ";
        String result = instance.toString();
        assertEquals(expResult, result);
    }
    
}
