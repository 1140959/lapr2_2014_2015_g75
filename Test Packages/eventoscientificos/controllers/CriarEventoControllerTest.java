package eventoscientificos.controllers;

import eventoscientificos.domain.Empresa;
import eventoscientificos.domain.Evento;
import eventoscientificos.domain.Utilizador;
import java.util.Calendar;
import java.util.Date;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author David
 */
public class CriarEventoControllerTest {

    private static Empresa empresa = new Empresa();
    private static CriarEventoController controller = new CriarEventoController(empresa);

    public CriarEventoControllerTest() {
    }

    @BeforeClass
    public static void setUpClass() {
        controller.novoEvento();
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of novoEvento method, of class CriarEventoController.
     */
    @Test
    public void testNovoEvento() {
        System.out.println("novoEvento");
        CriarEventoController instance = controller;
        instance.novoEvento();
    }

    /**
     * Test of setTitulo method, of class CriarEventoController.
     */
    @Test
    public void testSetTitulo() {
        System.out.println("setTitulo");
        String strTitulo = "um titulo";
        CriarEventoController instance = controller;
        instance.setTitulo(strTitulo);
    }

    /**
     * Test of setDescricao method, of class CriarEventoController.
     */
    @Test
    public void testSetDescricao() {
        System.out.println("setDescricao");
        String strDescricao = "uma descricao";
        CriarEventoController instance = controller;
        instance.setDescricao(strDescricao);
    }

    /**
     * Test of setLocal method, of class CriarEventoController.
     */
    @Test
    public void testSetLocal() {
        System.out.println("setLocal");
        String strLocal = "";
        CriarEventoController instance = controller;
        instance.setLocal(strLocal);
    }

    /**
     * Test of setDataInicio method, of class CriarEventoController.
     */
    @Test
    public void testSetDataInicio() {
        System.out.println("setDataInicio");
        Date strDataInicio = new Date();
        CriarEventoController instance = controller;
        instance.setDataInicio(strDataInicio);
    }

    /**
     * Test of setDataFim method, of class CriarEventoController.
     */
    @Test
    public void testSetDataFim() {
        System.out.println("setDataFim");
        Date strDataFim = new Date();
        CriarEventoController instance = controller;
        instance.setDataFim(strDataFim);
    }

    /**
     * Test of setDataInicioSubmissao method, of class CriarEventoController.
     */
    @Test
    public void testSetDataInicioSubmissao() {
        System.out.println("setDataInicioSubmissao");
        Date strDataInicioSubmissao = new Date();
        CriarEventoController instance = controller;
        instance.setDataInicioSubmissao(strDataInicioSubmissao);
    }

    /**
     * Test of setDataFimSubmissao method, of class CriarEventoController.
     */
    @Test
    public void testSetDataFimSubmissao() {
        System.out.println("setDataFimSubmissao");
        Date strDataFimSubmissao = new Date();
        CriarEventoController instance = controller;
        instance.setDataFimSubmissao(strDataFimSubmissao);
    }

    /**
     * Test of setDataInicioDistribuicao method, of class CriarEventoController.
     */
    @Test
    public void testSetDataInicioDistribuicao() {
        System.out.println("setDataInicioDistribuicao");
        Date strDataInicioDistribuicao = new Date();
        CriarEventoController instance = controller;
        instance.setDataInicioDistribuicao(strDataInicioDistribuicao);
    }

    /**
     * Test of setDataLimiteRevisao method, of class CriarEventoController.
     */
    @Test
    public void testSetDataLimiteRevisao() {
        System.out.println("setDataLimiteRevisao");
        Date strDataLimiteRevisao = new Date();
        CriarEventoController instance = controller;
        instance.setDataLimiteRevisao(strDataLimiteRevisao);
    }

    /**
     * Test of setDataLimiteSubmissaoFinal method, of class
     * CriarEventoController.
     */
    @Test
    public void testSetDataLimiteSubmissaoFinal() {
        System.out.println("setDataLimiteSubmissaoFinal");
        Date strDataLimiteSubmissaoFinal = new Date();
        CriarEventoController instance = controller;
        instance.setDataLimiteSubmissaoFinal(strDataLimiteSubmissaoFinal);
    }

    /**
     * Test of getUtilizadorByID method, of class CriarEventoController.
     */
    @Test
    public void testGetUtilizadorByID() {
        System.out.println("getUtilizadorByID");
        String id = "u1";
        CriarEventoController instance = controller;
        Utilizador expResult = empresa.getRegistoUtilizadores().getUtilizadorByID(id);
        Utilizador result = instance.getUtilizadorByID(id);
        assertEquals(expResult, result);
    }

    /**
     * Test of addOrganizador method, of class CriarEventoController.
     */
    @Test
    public void testAddOrganizador() {
        System.out.println("addOrganizador");
        String strId = "";
        CriarEventoController instance = controller;
        boolean expResult = false;
        boolean result = instance.addOrganizador(strId);
        assertEquals(expResult, result);
    }

    /**
     * Test of getEvento method, of class CriarEventoController.
     */
    @Test
    public void testGetEvento() {
        System.out.println("getEvento");
        CriarEventoController instance = controller;
        Evento expResult = controller.getEvento();
        Evento result = instance.getEvento();
        assertEquals(expResult, result);
    }

    /**
     * Test of registaEvento method, of class CriarEventoController.
     */
    @Test
    public void testRegistaEvento() {
        System.out.println("registaEvento");
        CriarEventoController instance = new CriarEventoController(empresa);

        CriarEventoController m_controllerCE = new CriarEventoController(empresa);
        m_controllerCE.novoEvento();
        m_controllerCE.setTitulo("Titulo");
        m_controllerCE.setDescricao("Descricao");
        m_controllerCE.setLocal("Local");
        Calendar c = Calendar.getInstance();
        m_controllerCE.setDataInicio(c.getTime());
        m_controllerCE.setDataInicioSubmissao(c.getTime());
        m_controllerCE.setDataFim(c.getTime());
        m_controllerCE.setDataFimSubmissao(c.getTime());
        m_controllerCE.setDataInicioDistribuicao(c.getTime());
        m_controllerCE.setDataLimiteRevisao(c.getTime());
        m_controllerCE.setDataLimiteSubmissaoFinal(c.getTime());
        m_controllerCE.addOrganizador("u1");
        m_controllerCE.registaEvento();

        instance.novoEvento();
        instance.setTitulo("Titulo");
        instance.setDescricao("Descricao");
        instance.setLocal("Local");
        instance.setDataInicio(c.getTime());
        instance.setDataInicioSubmissao(c.getTime());
        instance.setDataFim(c.getTime());
        instance.setDataFimSubmissao(c.getTime());
        instance.setDataInicioDistribuicao(c.getTime());
        instance.setDataLimiteRevisao(c.getTime());
        instance.setDataLimiteSubmissaoFinal(c.getTime());
        instance.addOrganizador("u1");
        instance.registaEvento();

        Evento expResult = instance.getEvento();
        Evento result = instance.registaEvento();
        assertEquals(expResult, result);
    }

}
