/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.controllers;

import eventoscientificos.domain.Empresa;
import eventoscientificos.domain.Utilizador;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Bruno
 */
public class RegistarUtilizadorControllerTest {
    private Empresa empresa;
    private RegistarUtilizadorController instance;
    private Utilizador u1;
    public RegistarUtilizadorControllerTest() {
        empresa=new Empresa();
        instance=new RegistarUtilizadorController(empresa);
       u1= new Utilizador("bruno","bruno","1234Ab","bruno@isep.pt");
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of novoUtilizador method, of class RegistarUtilizadorController.
     */
    @Test
    public void testNovoUtilizador() {
        System.out.println("novoUtilizador");
        instance.novoUtilizador();
    }

}
