/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.controllers;

import eventoscientificos.domain.Autor;
import eventoscientificos.domain.Empresa;
import eventoscientificos.domain.Evento;
import eventoscientificos.domain.SessaoTematica;
import eventoscientificos.domain.Submissao;
import eventoscientificos.domain.Submissivel;
import eventoscientificos.domain.Utilizador;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author BrunoFernandes
 */
public class SubmeterArtigoControllerTest {
    
    private static Empresa empresa;    
    private static CriarEventoController m_controllerCE;
    private static CriarSessaoTematicaController m_controllerCT;
    private static SubmeterArtigoController m_controllerSA;
    private static SubmeterArtigoController m_controllerSA2;
    
    public SubmeterArtigoControllerTest() {
        
    }
    
    @BeforeClass
    public static void setUpClass() {
        empresa = new Empresa();
        RegistarUtilizadorController con = new RegistarUtilizadorController(empresa);
        con.novoUtilizador();
        con.setDados("u1", "1231", "u1", "u1@isep.pt");
        con.novoUtilizador();
        con.setDados("u2", "1231","u2",  "u2@isep.pt");
        
        m_controllerCE = new CriarEventoController(empresa);
        m_controllerCE.novoEvento();
        m_controllerCE.setTitulo("Titulo");
        m_controllerCE.setDescricao("Descricao");
        m_controllerCE.setLocal("Local");
        Calendar c = Calendar.getInstance();
        m_controllerCE.setDataInicio(c.getTime());
        c.set(Calendar.SECOND, c.get(Calendar.SECOND) + 5);
        m_controllerCE.setDataInicioSubmissao(c.getTime());
        c.set(Calendar.MINUTE, c.get(Calendar.MINUTE) + 5);
        m_controllerCE.setDataFim(c.getTime());
        m_controllerCE.setDataFimSubmissao(c.getTime());
        m_controllerCE.setDataInicioDistribuicao(c.getTime());
        m_controllerCE.setDataLimiteRevisao(c.getTime());
        m_controllerCE.setDataLimiteSubmissaoFinal(c.getTime());
        m_controllerCE.addOrganizador("u1");
        m_controllerCE.registaEvento();
        
        m_controllerCT = new CriarSessaoTematicaController(empresa);
        m_controllerCT.setEvento(m_controllerCT.getListaEventosRegistadosDoUtilizador("u1").get(0));
        Calendar c1 = Calendar.getInstance();
        c1.set(Calendar.SECOND, c.get(Calendar.SECOND) + 10);
        Calendar c2 = Calendar.getInstance();
        c2.set(Calendar.MINUTE, c.get(Calendar.MINUTE) + 5);
        Calendar c3 = Calendar.getInstance();
        c3.set(Calendar.MINUTE, c.get(Calendar.MINUTE) + 5);
        m_controllerCT.setDados("123", "st", c1.getTime(), c2.getTime(), c3.getTime(), c3.getTime(), c3.getTime());
        m_controllerCT.addProponente("u2");
        m_controllerCT.registaProponente();
        m_controllerCT.registaSessaoTematica();
        
        DefinirCPController m_controllerCP = new DefinirCPController(empresa);
        m_controllerCP.novaCP(m_controllerCP.getListaCPDefiniveisEmDefinicao("u1").get(0));
        m_controllerCP.novoMembroCP("u1");
        m_controllerCP.addMembroCP();
        
        m_controllerCP = new DefinirCPController(empresa);
        m_controllerCP.novaCP(m_controllerCP.getListaCPDefiniveisEmDefinicao("u2").get(0));
        m_controllerCP.novoMembroCP("u1");
        m_controllerCP.addMembroCP();
        m_controllerCP.registaCP();
        
        m_controllerCE.getEvento().setStateEmSubmissao();
        m_controllerCT.getSessaoTematica().setStateEmSubmissao();
        
        m_controllerSA = new SubmeterArtigoController(empresa);
        m_controllerSA.selectSubmissivel(m_controllerCT.getSessaoTematica());
        m_controllerSA.setDados("Artigo 1", "Artigo - Resumo 1");
        Autor aut = m_controllerSA.novoAutor("u1", "strAfiliacao", "u1@isep.ipp.pt");
        System.out.println("\n\n\n\n\n teste " + m_controllerSA.addAutor(aut));
        m_controllerSA.setCorrespondente(aut);
        List<String> listPC = new ArrayList<>();
        listPC.add("palavra1");
        listPC.add("palavra2");
        m_controllerSA.setPalavrasChave(listPC);
        m_controllerSA.setFicheiro("Ficheiro PDF 1");
        System.out.println("ASDASD " + m_controllerSA.registarSubmissao());
        
        m_controllerSA2 = new SubmeterArtigoController(empresa);
        m_controllerSA2.selectSubmissivel(m_controllerCE.getEvento());
        m_controllerSA2.setDados("Artigo 2", "Artigo - Resumo 2");
        Autor aut2 = m_controllerSA2.novoAutor("u1", "strAfiliacao", "u1@isep.ipp.pt");
        System.out.println("\n\n\n\n\n teste " + m_controllerSA2.addAutor(aut2));
        m_controllerSA2.setCorrespondente(aut2);
        List<String> listPC2 = new ArrayList<>();
        listPC2.add("palavra1");
        listPC2.add("palavra2");
        m_controllerSA2.setPalavrasChave(listPC2);
        m_controllerSA2.setFicheiro("Ficheiro PDF 1");
        m_controllerSA2.registarSubmissao();
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getPossiveisAutoresCorrespondentes method, of class
     * SubmeterArtigoController.
     */
    @Test
    public void testGetPossiveisAutoresCorrespondentes() {
        System.out.println("getPossiveisAutoresCorrespondentes");
        Autor u1 = new Autor();
        u1.setAfiliacao("strAfiliacao");
        u1.setEmail("u1@isep.pt");
        u1.setNome("u1");
        
        Autor u2 = new Autor();
        u2.setNome("u2");
        u2.setEmail("u2@isep.pt");
        u2.setAfiliacao("strAfiliacao2");
        
        List<Autor> lista = new ArrayList<>();
        lista.add(u1);
        lista.add(u2);
        
        SubmeterArtigoController instance = new SubmeterArtigoController(empresa);
        List<Autor> expResult = new ArrayList<>();
        expResult.add(u1);
        expResult.add(u2);
        List<Autor> result = instance.getPossiveisAutoresCorrespondentes(lista);
        assertEquals(expResult, result);
    }

    /**
     * Test of getListaSubmissiveisEmSubmissao method, of class
     * SubmeterArtigoController.
     */
    @Test
    public void testGetListaSubmissiveisEmSubmissao() {
        System.out.println("getListaSubmissiveisEmSubmissao");
        SubmeterArtigoController instance = new SubmeterArtigoController(empresa);
        List<Submissivel> expResult = new ArrayList<>();
        expResult.add(m_controllerCT.getSessaoTematica());
        List<Submissivel> result = instance.getListaSubmissiveisEmSubmissao();
        assertEquals(expResult, result);
    }

    /**
     * Test of getListaEventosEmSubmissao method, of class
     * SubmeterArtigoController.
     */
    @Test
    public void testGetListaEventosEmSubmissao() {
        setUpClass();
        System.out.println("getListaEventosEmSubmissao");
        SubmeterArtigoController instance = new SubmeterArtigoController(empresa);
        List<Evento> expResult = new ArrayList<>();
//        expResult=empresa.getRegistoEventos().getListaEventos();
        List<Evento> result = instance.getListaEventosEmSubmissao();
        assertEquals(expResult, result);
    }

    /**
     * Test of getListaSessaoEmSubmissao method, of class
     * SubmeterArtigoController.
     */
    @Test
    public void testGetListaSessaoEmSubmissao() {
        System.out.println("getListaSessaoEmSubmissao");
        Evento evento = m_controllerCE.getEvento();
        SubmeterArtigoController instance = new SubmeterArtigoController(empresa);
        List<SessaoTematica> expResult = new ArrayList<>();
        expResult = m_controllerCE.getEvento().getListaDeSessoesTematicas().getListaDeSessaoTematica();
        List<SessaoTematica> result = instance.getListaSessaoEmSubmissao(evento);
        assertEquals(expResult, result);
    }

    /**
     * Test of setDados method, of class SubmeterArtigoController.
     */
    @Test
    public void testSetDados() {
        System.out.println("setDados");
        String strTitulo = "Titulo";
        String strResumo = "Resumo";
        SubmeterArtigoController instance = new SubmeterArtigoController(empresa);
        instance.selectSubmissivel(m_controllerCE.getEvento());
        instance.setDados(strTitulo, strResumo);
    }

    /**
     * Test of setCorrespondente method, of class SubmeterArtigoController.
     */
    @Test
    public void testSetCorrespondente() {
        System.out.println("setCorrespondente");
        Autor autor = new Autor();
        autor.setAfiliacao("strAfiliacao");
        autor.setEmail("u1@isep.pt");
        autor.setNome("u1");
        SubmeterArtigoController instance = new SubmeterArtigoController(empresa);
        instance.selectSubmissivel(m_controllerCE.getEvento());
        instance.setCorrespondente(autor);
    }

    /**
     * Test of setFicheiro method, of class SubmeterArtigoController.
     */
    @Test
    public void testSetFicheiro() {
        System.out.println("setFicheiro");
        String strFicheiro = "ficheiro";
        SubmeterArtigoController instance = new SubmeterArtigoController(empresa);
        instance.selectSubmissivel(m_controllerCE.getEvento());
        instance.setFicheiro(strFicheiro);
    }

    /**
     * Test of setPalavrasChave method, of class SubmeterArtigoController.
     */
    @Test
    public void testSetPalavrasChave() {
        System.out.println("setPalavrasChave");
        List<String> palavrasChave = new ArrayList<>();
        palavrasChave.add("ola");
        palavrasChave.add("treta");
        SubmeterArtigoController instance = new SubmeterArtigoController(empresa);
        instance.selectSubmissivel(m_controllerCE.getEvento());        
        instance.setPalavrasChave(palavrasChave);
    }

    /**
     * Test of addPalavra method, of class SubmeterArtigoController.
     */
    @Test
    public void testAddPalavra() {
        System.out.println("addPalavra");
        String palavraChave = "ola";
        SubmeterArtigoController instance = new SubmeterArtigoController(empresa);
        instance.selectSubmissivel(m_controllerCE.getEvento());
        instance.addPalavra(palavraChave);
    }

    /**
     * Test of setData method, of class SubmeterArtigoController.
     */
    @Test
    public void testSetData() {
        System.out.println("setData");
        Date data = new Date();
        SubmeterArtigoController instance = new SubmeterArtigoController(empresa);
        instance.selectSubmissivel(m_controllerCE.getEvento());
        instance.setData(data);
    }

    /**
     * Test of setUtilizador method, of class SubmeterArtigoController.
     */
    @Test
    public void testSetUtilizador() {
        System.out.println("setUtilizador");
        String utilizador = "u1";
        SubmeterArtigoController instance = new SubmeterArtigoController(empresa);
        instance.selectSubmissivel(m_controllerCE.getEvento());
        instance.setUtilizador(utilizador);
    }

    /**
     * Test of selectSubmissivel method, of class SubmeterArtigoController.
     */
    @Test
    public void testSelectSubmissivel() {
        System.out.println("selectSubmissivel");
        Submissivel est = m_controllerCE.getEvento();
        SubmeterArtigoController instance = new SubmeterArtigoController(empresa);
        instance.selectSubmissivel(est);
    }

    /**
     * Test of novoAutor method, of class SubmeterArtigoController.
     */
    @Test
    public void testNovoAutor_3args() {
        System.out.println("novoAutor");
        String strNome = "u1";
        String strAfiliacao = "isep";
        String strEmail = "u1@isep.pt";
        SubmeterArtigoController instance = new SubmeterArtigoController(empresa);
        instance.selectSubmissivel(m_controllerCE.getEvento());
        Autor expResult = new Autor();
        expResult.setNome("u1");
        expResult.setEmail("u1@isep.pt");
        expResult.setAfiliacao("isep");
        
        Autor result = instance.novoAutor(strNome, strAfiliacao, strEmail);
        assertEquals(expResult, result);
    }

//    /**
//     * Test of novoAutor method, of class SubmeterArtigoController.
//     */
//    @Test
//    public void testNovoAutor_0args() {
//        System.out.println("novoAutor");
//        SubmeterArtigoController instance = new SubmeterArtigoController(empresa);
//        instance.selectSubmissivel(m_controllerCE.getEvento());
//        Autor expResult = new Autor();
//        expResult.setNome("asd");
//        expResult.setEmail("asd");
//        expResult.setAfiliacao("asd");
//        Autor result = instance.novoAutor();
//        assertEquals(expResult, result);
//    }

    /**
     * Test of addAutor method, of class SubmeterArtigoController.
     */
    @Test
    public void testAddAutor() {
        System.out.println("addAutor");
        Autor autor = new Autor();
        autor.setNome("asd");
        autor.setEmail("asd");
        autor.setAfiliacao("asd@isep.ipp");
        SubmeterArtigoController instance = new SubmeterArtigoController(empresa);
        instance.selectSubmissivel(m_controllerCE.getEvento());
        boolean expResult = false;
        boolean result = instance.addAutor(autor);
        assertEquals(expResult, result);
    }

    /**
     * Test of registarSubmissao method, of class SubmeterArtigoController.
     */
    @Test
    public void testRegistarSubmissao() {
        System.out.println("registarSubmissao");
        SubmeterArtigoController instance = new SubmeterArtigoController(empresa);
        instance.selectSubmissivel(m_controllerCE.getEvento());
        boolean expResult = true;
        boolean result = instance.registarSubmissao();
        assertEquals(expResult, result);
    }

}
