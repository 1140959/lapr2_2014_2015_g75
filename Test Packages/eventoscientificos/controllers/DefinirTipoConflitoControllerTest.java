/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.controllers;

import eventoscientificos.domain.Empresa;
import eventoscientificos.domain.MecanismoDetecaoConflito1;
import eventoscientificos.domain.RegistoTipoConflitos;
import eventoscientificos.domain.TipoConflito;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Ricardo
 */
public class DefinirTipoConflitoControllerTest {

    private static Empresa testeEmpresa = new Empresa();
    private static MecanismoDetecaoConflito1 testeMecanismo = new MecanismoDetecaoConflito1();
    private static TipoConflito testeTipoConflito = new TipoConflito("Descrição teste", testeMecanismo);
    private static DefinirTipoConflitoController testeDefinirTipoConflitoController = new DefinirTipoConflitoController(testeEmpresa);
    private static RegistoTipoConflitos testRegistoTipoConflitos = new RegistoTipoConflitos();

    @BeforeClass
    public static void setUpClass() {

    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of novoTipoConflito method, of class DefinirTipoConflitoController.
     */
    @Test
    public void testNovoTipoConflito() {
        String descricao = "Descrição teste";
        DefinirTipoConflitoController instance = new DefinirTipoConflitoController(testeEmpresa);

        System.out.println("INÍCIO TESTE: novoTipoConflito");
        instance.novoTipoConflito(descricao, testeMecanismo);
        TipoConflito expResult = testeTipoConflito;
        TipoConflito result = instance.novoTipoConflito(descricao, testeMecanismo);
        String esperado = expResult.showData();
        String verificado = result.showData();
        assertEquals(esperado, verificado);
        System.out.println("FIM TESTE: novoTipoConflito");
    }

    /**
     * Test of registaTipoConflito method, of class
     * DefinirTipoConflitoController.
     */
    @Test
    public void testRegistaTipoConflito() {
        System.out.println("INÍCIO TESTE: registaTipoConflito");
        TipoConflito tpConflito = testeTipoConflito;
        DefinirTipoConflitoController instance = testeDefinirTipoConflitoController;
        boolean expResult = true;
        boolean result = instance.registaTipoConflito(tpConflito);
        assertEquals(expResult, result);
        System.out.println("FIM TESTE: registaTipoConflito");
    }

}
