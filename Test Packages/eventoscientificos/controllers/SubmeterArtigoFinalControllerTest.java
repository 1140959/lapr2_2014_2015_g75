/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.controllers;

import eventoscientificos.domain.Autor;
import eventoscientificos.domain.Empresa;
import eventoscientificos.domain.Evento;
import eventoscientificos.domain.Submissao;
import eventoscientificos.domain.Utilizador;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author BrunoFernandes
 */
public class SubmeterArtigoFinalControllerTest {

    private static Empresa empresa;
    private static CriarEventoController m_controllerCE;
    private static CriarSessaoTematicaController m_controllerCT;
    private static SubmeterArtigoController m_controllerSA;
    private static SubmeterArtigoController m_controllerSA2;

    public SubmeterArtigoFinalControllerTest() {
    }

    @BeforeClass
    public static void setUpClass() {
        empresa = new Empresa();
        RegistarUtilizadorController con = new RegistarUtilizadorController(empresa);
        con.novoUtilizador();
        con.setDados("u1", "1231", "u1", "u1@isep.pt");
        con.novoUtilizador();
        con.setDados("u2", "u2", "1231", "u2@isep.pt");

        m_controllerCE = new CriarEventoController(empresa);
        m_controllerCE.novoEvento();
        m_controllerCE.setTitulo("Titulo");
        m_controllerCE.setDescricao("Descricao");
        m_controllerCE.setLocal("Local");
        Calendar c = Calendar.getInstance();
        m_controllerCE.setDataInicio(c.getTime());
        c.set(Calendar.SECOND, c.get(Calendar.SECOND) + 5);
        m_controllerCE.setDataInicioSubmissao(c.getTime());
        c.set(Calendar.MINUTE, c.get(Calendar.MINUTE) + 5);
        m_controllerCE.setDataFim(c.getTime());
        m_controllerCE.setDataFimSubmissao(c.getTime());
        m_controllerCE.setDataInicioDistribuicao(c.getTime());
        m_controllerCE.setDataLimiteRevisao(c.getTime());
        m_controllerCE.setDataLimiteSubmissaoFinal(c.getTime());
        m_controllerCE.addOrganizador("u1");
        m_controllerCE.registaEvento();

        m_controllerCT = new CriarSessaoTematicaController(empresa);
        m_controllerCT.setEvento(m_controllerCT.getListaEventosRegistadosDoUtilizador("u1").get(0));
        Calendar c1 = Calendar.getInstance();
        c1.set(Calendar.SECOND, c.get(Calendar.SECOND) + 10);
        Calendar c2 = Calendar.getInstance();
        c2.set(Calendar.MINUTE, c.get(Calendar.MINUTE) + 5);
        Calendar c3 = Calendar.getInstance();
        c3.set(Calendar.MINUTE, c.get(Calendar.MINUTE) + 5);
        m_controllerCT.setDados("123", "st", c1.getTime(), c2.getTime(), c3.getTime(), c3.getTime(), c3.getTime());
        m_controllerCT.addProponente("u2");
        m_controllerCT.registaProponente();
        m_controllerCT.registaSessaoTematica();

        DefinirCPController m_controllerCP = new DefinirCPController(empresa);
        m_controllerCP.novaCP(m_controllerCP.getListaCPDefiniveisEmDefinicao("u1").get(0));
        m_controllerCP.novoMembroCP("u1");
        m_controllerCP.addMembroCP();

        m_controllerCP = new DefinirCPController(empresa);
        m_controllerCP.novaCP(m_controllerCP.getListaCPDefiniveisEmDefinicao("u2").get(0));
        m_controllerCP.novoMembroCP("u1");
        m_controllerCP.addMembroCP();
        m_controllerCP.registaCP();

        m_controllerCE.getEvento().setStateEmSubmissao();
        m_controllerCT.getSessaoTematica().setStateEmSubmissao();

        m_controllerSA = new SubmeterArtigoController(empresa);
        m_controllerSA.selectSubmissivel(m_controllerCT.getSessaoTematica());
        m_controllerSA.setDados("Artigo 1", "Artigo - Resumo 1");
        Autor aut = m_controllerSA.novoAutor("u1", "strAfiliacao", "u1@isep.ipp.pt");
        System.out.println("\n\n\n\n\n teste " + m_controllerSA.addAutor(aut));
        m_controllerSA.setCorrespondente(aut);
        List<String> listPC = new ArrayList<>();
        listPC.add("palavra1");
        listPC.add("palavra2");
        m_controllerSA.setPalavrasChave(listPC);
        m_controllerSA.setFicheiro("Ficheiro PDF 1");
        System.out.println("ASDASD " + m_controllerSA.registarSubmissao());

        m_controllerSA.getSubmissao().setStateEmDetecaoConflitos();
        m_controllerSA.getSubmissao().setStateEmLicitacao();
        m_controllerSA.getSubmissao().setStateEmDistribuicao();
        m_controllerSA.getSubmissao().setStateEmRevisao();
        m_controllerSA.getSubmissao().setStateEmDecisao();
        m_controllerSA.getSubmissao().setStateAceite();
        
        System.out.println("\n\n\n\n\n\n"+m_controllerSA.getSubmissao().getState());

        m_controllerSA2 = new SubmeterArtigoController(empresa);
        m_controllerSA2.selectSubmissivel(m_controllerCE.getEvento());
        m_controllerSA2.setDados("Artigo 2", "Artigo - Resumo 2");
        Autor aut2 = m_controllerSA2.novoAutor("u1", "strAfiliacao", "u1@isep.ipp.pt");
        System.out.println("\n\n\n\n\n teste " + m_controllerSA2.addAutor(aut2));
        m_controllerSA2.setCorrespondente(aut2);
        List<String> listPC2 = new ArrayList<>();
        listPC2.add("palavra1");
        listPC2.add("palavra2");
        m_controllerSA2.setPalavrasChave(listPC2);
        m_controllerSA2.setFicheiro("Ficheiro PDF 1");
        m_controllerSA2.registarSubmissao();
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getPossiveisAutoresCorrespondentes method, of class
     * SubmeterArtigoFinalController.
     */
    @Test
    public void testGetPossiveisAutoresCorrespondentes() {
        System.out.println("getPossiveisAutoresCorrespondentes");
        Autor u1 = new Autor();
        u1.setAfiliacao("strAfiliacao");
        u1.setEmail("u1@isep.pt");
        u1.setNome("u1");

        Autor u2 = new Autor();
        u2.setNome("u2");
        u2.setEmail("u2@isep.pt");
        u2.setAfiliacao("strAfiliacao2");

        List<Autor> lista = new ArrayList<>();
        lista.add(u1);
        lista.add(u2);

        SubmeterArtigoFinalController instance = new SubmeterArtigoFinalController(empresa);
        List<Autor> expResult = new ArrayList<>();
        expResult.add(u1);
        expResult.add(u2);
        List<Autor> result = instance.getPossiveisAutoresCorrespondentes(lista);
        assertEquals(expResult, result);
    }

    /**
     * Test of getListaSubmissoesDoAutorEmAceite method, of class
     * SubmeterArtigoFinalController.
     */
    @Test
    public void testGetListaSubmissoesDoAutorEmAceite() {
        System.out.println("getListaSubmissoesDoAutorEmAceite");
        String email = "u1@isep.ipp.pt";
        SubmeterArtigoFinalController instance = new SubmeterArtigoFinalController(empresa);
        List<Submissao> expResult = new ArrayList<>();
        expResult.add(m_controllerSA.getSubmissao());
        List<Submissao> result = instance.getListaSubmissoesDoAutorEmAceite(email);
        assertEquals(expResult, result);
    }


    /**
     * Test of setDados method, of class SubmeterArtigoFinalController.
     */
    @Test
    public void testSetDados() {
        System.out.println("setDados");
        String strTitulo = "titulo";
        String strResumo = "resumo";
        SubmeterArtigoFinalController instance = new SubmeterArtigoFinalController(empresa);
        instance.selectSubmissao(m_controllerSA.getSubmissao(), "u1");
        instance.setDados(strTitulo, strResumo);
    }

    /**
     * Test of setCorrespondente method, of class SubmeterArtigoFinalController.
     */
    @Test
    public void testSetCorrespondente() {
        System.out.println("setCorrespondente");
        Autor autor = new Autor();
        autor.setNome("u1");
        autor.setEmail("u1@isep.ipp.pt");
        autor.setAfiliacao("u1");
        SubmeterArtigoFinalController instance = new SubmeterArtigoFinalController(empresa);
        instance.selectSubmissao(m_controllerSA.getSubmissao(), "u1");
        instance.setCorrespondente(autor);
    }

    /**
     * Test of setFicheiro method, of class SubmeterArtigoFinalController.
     */
    @Test
    public void testSetFicheiro() {
        System.out.println("setFicheiro");
        String strFicheiro = "c:";
        SubmeterArtigoFinalController instance = new SubmeterArtigoFinalController(empresa);
        instance.selectSubmissao(m_controllerSA.getSubmissao(), "u1");
        instance.setFicheiro(strFicheiro);
    }

    /**
     * Test of setPalavrasChave method, of class SubmeterArtigoFinalController.
     */
    @Test
    public void testSetPalavrasChave() {
        System.out.println("setPalavrasChave");
        List<String> palavrasChave = new ArrayList<>();
        SubmeterArtigoFinalController instance = new SubmeterArtigoFinalController(empresa);
        instance.selectSubmissao(m_controllerSA.getSubmissao(), "u1");
        palavrasChave.add("ola");
        instance.setPalavrasChave(palavrasChave);
    }

    /**
     * Test of addPalavra method, of class SubmeterArtigoFinalController.
     */
    @Test
    public void testAddPalavra() {
        System.out.println("addPalavra");
        String palavraChave = "ola";
        SubmeterArtigoFinalController instance = new SubmeterArtigoFinalController(empresa);
        instance.selectSubmissao(m_controllerSA.getSubmissao(), "u1");
        instance.addPalavra(palavraChave);
    }

    /**
     * Test of setData method, of class SubmeterArtigoFinalController.
     */
    @Test
    public void testSetData() {
        System.out.println("setData");
        Calendar cal = Calendar.getInstance();
        Date data = cal.getTime();
        SubmeterArtigoFinalController instance = new SubmeterArtigoFinalController(empresa);
        instance.selectSubmissao(m_controllerSA.getSubmissao(), "u1");
        instance.setData(data);
    }

    /**
     * Test of setUser method, of class SubmeterArtigoFinalController.
     */
    @Test
    public void testSetUser() {
        System.out.println("setUser");
        String id = "u1";
        SubmeterArtigoFinalController instance =new SubmeterArtigoFinalController(empresa);
        instance.selectSubmissao(m_controllerSA.getSubmissao(), "u1");
        instance.setUser(id);
    }

    /**
     * Test of selectSubmissao method, of class SubmeterArtigoFinalController.
     */
    @Test
    public void testSelectSubmissao() {
        System.out.println("selectSubmissao");
        Submissao est = m_controllerSA.getSubmissao();
        String id = "u1";
        SubmeterArtigoFinalController instance =new SubmeterArtigoFinalController(empresa);
        instance.selectSubmissao(est, id);
    }

    /**
     * Test of addAutor method, of class SubmeterArtigoFinalController.
     */
    @Test
    public void testAddAutor() {
        System.out.println("addAutor");
        Autor autor = new Autor();
        autor.setNome("u1");
        autor.setEmail("u1@isep.ipp.pt");
        autor.setAfiliacao("u1");
        SubmeterArtigoFinalController instance = new SubmeterArtigoFinalController(empresa);
        instance.selectSubmissao(m_controllerSA.getSubmissao(), "u1");
        boolean expResult = true;
        boolean result = instance.addAutor(autor);
        assertEquals(expResult, result);
    }

    /**
     * Test of getUtilizadorByID method, of class SubmeterArtigoFinalController.
     */
    @Test
    public void testGetUtilizadorByID() {
        System.out.println("getUtilizadorByID");
        String idUtilizador = "u1";
        SubmeterArtigoFinalController instance = new SubmeterArtigoFinalController(empresa);
        Utilizador expResult = new Utilizador();
        expResult.setNome("u1");
        expResult.setPasswordCodificada("1231");
        expResult.setEmail("u1@isep.ipp.pt");
        Utilizador result = instance.getUtilizadorByID(idUtilizador);
        assertEquals(expResult.getNome(), result.getNome());
    }

    /**
     * Test of registarSubmissao method, of class SubmeterArtigoFinalController.
     */
    @Test
    public void testRegistarSubmissao() {
        System.out.println("registarSubmissao");
        SubmeterArtigoFinalController instance = new SubmeterArtigoFinalController(empresa);
        instance.selectSubmissao(m_controllerSA.getSubmissao(), "u1");
        instance.setDados("titulo", "resumo");
        instance.setFicheiro("FICHEIRO");
        List<String> lstPC = new ArrayList<>();
        lstPC.add("pc");
        instance.setPalavrasChave(lstPC);
        List<Autor> lst = new ArrayList<>();
        Autor autor = new Autor();
        autor.setNome("u1");
        autor.setEmail("u1@isep.ipp.pt");
        autor.setAfiliacao("u1");
        lst.add(autor);
        instance.addAutor(autor);
        instance.setCorrespondente(autor);
        boolean expResult = true;
        boolean result = instance.registarSubmissao();
        assertEquals(expResult, result);
    }

}
