package eventoscientificos.controllers;

import eventoscientificos.domain.Empresa;
import eventoscientificos.domain.Evento;
import eventoscientificos.domain.RegistoEventos;
import eventoscientificos.domain.SessaoTematica;
import eventoscientificos.domain.Utilizador;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Claudio
 */
public class CriarSessaoTematicaControllerTest {

    private static Empresa empresa = new Empresa();
    private static CriarSessaoTematicaController m_controllerCST = new CriarSessaoTematicaController(empresa);
    private static CriarEventoController m_controllerCE;
    private static Calendar c;

    public CriarSessaoTematicaControllerTest() {
    }

    @BeforeClass
    public static void setUpClass() {
        c = Calendar.getInstance();
        RegistarUtilizadorController m_controllerRU = new RegistarUtilizadorController(empresa);
        m_controllerRU.novoUtilizador();
        m_controllerRU.setDados("u1", "u1", "u1", "u1@isep.ipp.pt");
        m_controllerRU.novoUtilizador();
        m_controllerRU.setDados("u2", "u2", "u2", "u2@isep.ipp.pt");

        m_controllerCE = new CriarEventoController(empresa);
        m_controllerCE.novoEvento();
        m_controllerCE.setTitulo("Titulo");
        m_controllerCE.setDescricao("Descricao");
        m_controllerCE.setLocal("Local");
        m_controllerCE.setDataInicio(c.getTime());
        m_controllerCE.setDataInicioSubmissao(c.getTime());
        m_controllerCE.setDataFim(c.getTime());
        m_controllerCE.setDataFimSubmissao(c.getTime());
        m_controllerCE.setDataInicioDistribuicao(c.getTime());
        m_controllerCE.setDataLimiteRevisao(c.getTime());
        m_controllerCE.setDataLimiteSubmissaoFinal(c.getTime());
        m_controllerCE.addOrganizador("u1");
        m_controllerCE.registaEvento();

        m_controllerCST = new CriarSessaoTematicaController(empresa);
        m_controllerCST.setEvento(m_controllerCE.getEvento());
        m_controllerCST.setDados("123", "st", c.getTime(), c.getTime(), c.getTime(), c.getTime(), c.getTime());
        m_controllerCST.addProponente("u2");
        m_controllerCST.registaProponente();
        m_controllerCST.registaSessaoTematica();
        
        RegistarUtilizadorController controller = new RegistarUtilizadorController(empresa);
        controller.novoUtilizador();
        controller.setDados("u1", "u1", "1231", "u1@isep.pt");
        controller.novoUtilizador();
        controller.setDados("u2", "u2", "12314", "u2@isep.pt");

        m_controllerCE = new CriarEventoController(empresa);
        m_controllerCE.novoEvento();
        m_controllerCE.setTitulo("Titulo");
        m_controllerCE.setDescricao("Descricao");
        m_controllerCE.setLocal("Local");
        m_controllerCE.setDataInicio(c.getTime());
        m_controllerCE.setDataInicioSubmissao(c.getTime());
        m_controllerCE.setDataFim(c.getTime());
        m_controllerCE.setDataFimSubmissao(c.getTime());
        m_controllerCE.setDataInicioDistribuicao(c.getTime());
        m_controllerCE.setDataLimiteRevisao(c.getTime());
        m_controllerCE.setDataLimiteSubmissaoFinal(c.getTime());
        m_controllerCE.addOrganizador("u1");
        m_controllerCE.registaEvento();
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of setDados method, of class CriarSessaoTematicaController.
     */
    @Test
    public void testSetDados() {
        System.out.println("setDados");
        String cod = "123456";
        String desc = "descrição Sessão Tematica";
        Date dtInicioSub = new Date();
        Date dtFimSub = new Date();
        Date dtInicioDistr = new Date();
        Date dtLimRevisao = new Date();
        Date dtLimArtigoFinal = new Date();
        CriarSessaoTematicaController instance = m_controllerCST;
        instance.setDados(cod, desc, dtInicioSub, dtFimSub, dtInicioDistr, dtLimRevisao, dtLimArtigoFinal);

    }

    /**
     * Test of addProponente method, of class CriarSessaoTematicaController.
     */
    @Test
    public void testAddProponente() {
        System.out.println("addProponente");
        String strId = "u1";
        CriarSessaoTematicaController instance = new CriarSessaoTematicaController(empresa);
        instance.setEvento(m_controllerCE.getEvento());
        instance.setDados("123", "123", c.getTime(), c.getTime(), c.getTime(), c.getTime(), c.getTime());
        boolean expResult = true;
        boolean result = instance.addProponente(strId);
        assertEquals(expResult, result);

    }

    /**
     * Test of getUtilizadorByID method, of class CriarEventoController.
     */
    @Test
    public void testGetUtilizadorByID() {
        System.out.println("getUtilizadorByID");
        String id = "u1";
        CriarSessaoTematicaController instance = m_controllerCST;
        Utilizador expResult = empresa.getRegistoUtilizadores().getUtilizadorByID(id);
        Utilizador result = instance.getUtilizadorByID(id);
        assertEquals(expResult, result);

    }



    /**
     * Test of setEvento method, of class CriarSessaoTematicaController.
     */
    @Test
    public void testSetEvento() {
        System.out.println("setEvento");
        Evento e = m_controllerCE.getEvento();
        CriarSessaoTematicaController instance = new CriarSessaoTematicaController(empresa);
        instance.setEvento(e);
    }


}
