/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.controllers;

import static eventoscientificos.controllers.RetirarSubmissaoControllerTest.setUpClass;
import eventoscientificos.domain.Artigo;
import eventoscientificos.domain.Autor;
import eventoscientificos.domain.Empresa;
import eventoscientificos.domain.Evento;
import eventoscientificos.domain.SessaoTematica;
import eventoscientificos.domain.Submissao;
import eventoscientificos.domain.Submissivel;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Carlos
 */
public class AlterarSubmissaoControllerTest {

    private static Empresa empresa;
    private static AlterarSubmissaoController m_controllerAS;
    private static CriarEventoController m_controllerCE;
    private static CriarSessaoTematicaController m_controllerCT;
    private static SubmeterArtigoController m_controllerSA;
    private static SubmeterArtigoController m_controllerSA2;
    private static Autor aut;

    public AlterarSubmissaoControllerTest() {
    }

    @BeforeClass
    public static void setUpClass() {
        empresa = new Empresa();

        RegistarUtilizadorController Ucontroller = new RegistarUtilizadorController(empresa);
        Ucontroller.novoUtilizador();
        Ucontroller.setDados("u1", "u1", "1231", "u1@isep.pt");
        Ucontroller.novoUtilizador();
        Ucontroller.setDados("u2", "u2", "12314", "u2@isep.pt");

        m_controllerCE = new CriarEventoController(empresa);
        m_controllerCE.novoEvento();
        m_controllerCE.setTitulo("Titulo");
        m_controllerCE.setDescricao("Descricao");
        m_controllerCE.setLocal("Local");
        Calendar c = Calendar.getInstance();
        m_controllerCE.setDataInicio(c.getTime());
        c.set(Calendar.SECOND, c.get(Calendar.SECOND) + 5);
        m_controllerCE.setDataInicioSubmissao(c.getTime());
        c.set(Calendar.MINUTE, c.get(Calendar.MINUTE) + 5);
        m_controllerCE.setDataFim(c.getTime());
        m_controllerCE.setDataFimSubmissao(c.getTime());
        m_controllerCE.setDataInicioDistribuicao(c.getTime());
        m_controllerCE.setDataLimiteRevisao(c.getTime());
        m_controllerCE.setDataLimiteSubmissaoFinal(c.getTime());
        m_controllerCE.addOrganizador("u1");
        m_controllerCE.registaEvento();

        m_controllerCT = new CriarSessaoTematicaController(empresa);
        m_controllerCT.setEvento(m_controllerCT.getListaEventosRegistadosDoUtilizador("u1").get(0));
        Calendar c1 = Calendar.getInstance();
        c1.set(Calendar.SECOND, c.get(Calendar.SECOND) + 10);
        Calendar c2 = Calendar.getInstance();
        c2.set(Calendar.MINUTE, c.get(Calendar.MINUTE) + 5);
        Calendar c3 = Calendar.getInstance();
        c3.set(Calendar.MINUTE, c.get(Calendar.MINUTE) + 5);
        m_controllerCT.setDados("123", "st", c1.getTime(), c2.getTime(), c3.getTime(), c3.getTime(), c3.getTime());
        m_controllerCT.addProponente("u2");
        m_controllerCT.registaProponente();
        m_controllerCT.registaSessaoTematica();

        DefinirCPController m_controllerCP = new DefinirCPController(empresa);
        m_controllerCP.novaCP(m_controllerCP.getListaCPDefiniveisEmDefinicao("u1").get(0));
        m_controllerCP.novoMembroCP("u1");
        m_controllerCP.addMembroCP();

        m_controllerCP = new DefinirCPController(empresa);
        m_controllerCP.novaCP(m_controllerCP.getListaCPDefiniveisEmDefinicao("u2").get(0));
        m_controllerCP.novoMembroCP("u1");
        m_controllerCP.addMembroCP();
        m_controllerCP.registaCP();

        m_controllerCE.getEvento().setStateEmSubmissao();
        m_controllerCT.getSessaoTematica().setStateEmSubmissao();

        m_controllerAS = new AlterarSubmissaoController(empresa, "u1");

        m_controllerSA = new SubmeterArtigoController(empresa);
        m_controllerSA.selectSubmissivel(m_controllerCT.getSessaoTematica());
        m_controllerSA.setDados("Artigo 1", "Artigo - Resumo 1");
        Autor aut = m_controllerSA.novoAutor("u1", "strAfiliacao", "u1@isep.ipp.pt");
        m_controllerSA.addAutor(aut);
        m_controllerSA.setCorrespondente(aut);
        List<String> listPC = new ArrayList<>();
        listPC.add("palavra1");
        listPC.add("palavra2");
        m_controllerSA.setPalavrasChave(listPC);
        m_controllerSA.setFicheiro("Ficheiro PDF 1");
        System.out.println("ASDASD " + m_controllerSA.registarSubmissao());

        m_controllerSA2 = new SubmeterArtigoController(empresa);
        m_controllerSA2.selectSubmissivel(m_controllerCE.getEvento());
        m_controllerSA2.setDados("Artigo 2", "Artigo - Resumo 2");
        Autor aut2 = m_controllerSA2.novoAutor("u1", "strAfiliacao", "u1@isep.ipp.pt");
        System.out.println("\n\n\n\n\n teste " + m_controllerSA2.addAutor(aut2));
        m_controllerSA2.setCorrespondente(aut2);
        List<String> listPC2 = new ArrayList<>();
        listPC2.add("palavra1");
        listPC2.add("palavra2");
        m_controllerSA2.setPalavrasChave(listPC2);
        m_controllerSA2.setFicheiro("Ficheiro PDF 1");
        m_controllerSA2.registarSubmissao();
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getListaSubmissaoSessaoDoAutor method, of class
     * AlterarSubmissaoController.
     */
    @Test
    public void testGetListaSubmissaoSessaoDoAutor() {
        System.out.println("getListaSubmissaoSessaoDoAutor");
        AlterarSubmissaoController instance = new AlterarSubmissaoController(empresa, "u1");
        List<SessaoTematica> expResult = new ArrayList<>();
        List<SessaoTematica> result = instance.getListaSubmissaoSessaoDoAutor();
        assertEquals(expResult, result);
    }

    /**
     * Test of getListaSubmissaoDoAutor method, of class
     * AlterarSubmissaoController.
     */
    @Test
    public void testGetListaSubmissaoDoAutor() {
        System.out.println("getListaSubmissaoDoAutor");
        Submissivel submissivel = m_controllerCT.getSessaoTematica();
        AlterarSubmissaoController instance = new AlterarSubmissaoController(empresa, "u1");
        List<Submissao> expResult = new ArrayList<>();
        List<Submissao> result = instance.getListaSubmissaoDoAutor(submissivel);
        assertEquals(expResult, result);
    }

    /**
     * Test of selectSubmissao method, of class AlterarSubmissaoController.
     */
    @Test
    public void testSelectSubmissao() {
        System.out.println("selectSubmissao");
        Submissao submissao = new Submissao();
        AlterarSubmissaoController instance = new AlterarSubmissaoController(empresa, "u1");
        Artigo expResult = submissao.getArtigo();
        Artigo result = instance.selectSubmissao(submissao);
        assertEquals(expResult, result);
    }

    /**
     * Test of validaDadosAutor method, of class AlterarSubmissaoController.
     */
    @Test
    public void testValidaDadosAutor() {
        System.out.println("validaDadosAutor");
        Autor autor = new Autor();
        autor.setNome("u1");
        autor.setAfiliacao("strAfiliacao");
        autor.setEmail("u1@isep.ipp.pt");
        AlterarSubmissaoController instance = new AlterarSubmissaoController(empresa, "u1");
        boolean expResult = true;
        boolean result = instance.validaDadosAutor(autor);
        assertEquals(expResult, result);
    }

    /**
     * Test of getPossiveisAutoresCorrespondentes method, of class
     * AlterarSubmissaoController.
     */
    @Test
    public void testGetPossiveisAutoresCorrespondentes() {
        System.out.println("getPossiveisAutoresCorrespondentes");
        AlterarSubmissaoController instance = new AlterarSubmissaoController(empresa, "u1");
        instance.selectSubmissao(m_controllerSA.getSubmissao());
        List<Autor> expResult = new ArrayList<>();
        Autor a= new Autor();
        a.setNome("u1");
        a.setAfiliacao("strAfiliacao");
        a.setEmail("u1@isep.pt");
        expResult.add(a);
        instance.setDadosAutores(expResult);
        List<Autor> result = instance.getPossiveisAutoresCorrespondentes(expResult);
        assertEquals(expResult, result);
    }

    /**
     * Test of getTitulo method, of class AlterarSubmissaoController.
     */
    @Test
    public void testGetTitulo() {
        setUpClass();
        System.out.println("getTitulo");
        Artigo artigo = m_controllerSA.getSubmissao().getArtigo();
        AlterarSubmissaoController instance = new AlterarSubmissaoController(empresa, "u1");
        String expResult = "Artigo 1";
        String result = instance.getTitulo(artigo);
        assertEquals(expResult, result);
    }

    /**
     * Test of getResumo method, of class AlterarSubmissaoController.
     */
    @Test
    public void testGetResumo() {
        System.out.println("getResumo");
        Artigo artigo = m_controllerSA.getSubmissao().getArtigo();
        AlterarSubmissaoController instance = new AlterarSubmissaoController(empresa, "u1");
        String expResult = "Artigo - Resumo 1";
        String result = instance.getResumo(artigo);
        assertEquals(expResult, result);
    }

    /**
     * Test of getFicheiro method, of class AlterarSubmissaoController.
     */
    @Test
    public void testGetFicheiro() {
        System.out.println("getFicheiro");
        Artigo artigo = m_controllerSA.getSubmissao().getArtigo();
        AlterarSubmissaoController instance = new AlterarSubmissaoController(empresa, "u1");
        String expResult = "Ficheiro PDF 1";
        String result = instance.getFicheiro(artigo);
        assertEquals(expResult, result);
    }

    /**
     * Test of getAutores method, of class AlterarSubmissaoController.
     */
    @Test
    public void testGetAutores() {
        System.out.println("getAutores");
        Artigo artigo = m_controllerSA.getSubmissao().getArtigo();
        AlterarSubmissaoController instance = new AlterarSubmissaoController(empresa, "u1");
        List<Autor> expResult = new ArrayList<>();
        Autor aut = new Autor();
        aut.setNome("u1");
        aut.setAfiliacao("strAfiliacao");
        aut.setEmail("u1@isep.ipp.pt");
        expResult.add(aut);
        List<Autor> result = instance.getAutores(artigo);
        assertEquals(expResult, result);
    }

    /**
     * Test of getPalavrasChave method, of class AlterarSubmissaoController.
     */
    @Test
    public void testGetPalavrasChave() {
        System.out.println("getPalavrasChave");
        Artigo artigo = m_controllerSA.getSubmissao().getArtigo();
        AlterarSubmissaoController instance = new AlterarSubmissaoController(empresa, "u1");
        List<String> expResult = new ArrayList<>();
        expResult.add("palavra1");
        expResult.add("palavra2");
        List<String> result = instance.getPalavrasChave(artigo);
        assertEquals(expResult, result);
    }

    /**
     * Test of getAutorCorrespondente method, of class
     * AlterarSubmissaoController.
     */
    @Test
    public void testGetAutorCorrespondente() {
        System.out.println("getAutorCorrespondente");
        Artigo artigo = m_controllerSA.getSubmissao().getArtigo();
        AlterarSubmissaoController instance = new AlterarSubmissaoController(empresa, "u1");
        Autor expResult = new Autor();
        expResult.setNome("u1");
        expResult.setAfiliacao("strAfiliacao");
        expResult.setAfiliacao("u1@isep.ipp.pt");
        Autor result = instance.getAutorCorrespondente(artigo);
        assertEquals(expResult.getNome(), result.getNome());
    }

    /**
     * Test of setDadosTituloResumo method, of class AlterarSubmissaoController.
     */
    @Test
    public void testSetDadosTituloResumo() {
        System.out.println("setDadosTituloResumo");
        String strTitulo = "bruno";
        String strResumo = "bruno-resumo";
        Submissao a= new Submissao();
        AlterarSubmissaoController instance = new AlterarSubmissaoController(empresa, "u1");
        instance.setDadosTituloResumo(strTitulo, strResumo);
    }

    /**
     * Test of setDadosAutorCorrespondente method, of class
     * AlterarSubmissaoController.
     */
    @Test
    public void testSetDadosAutorCorrespondente() {
        System.out.println("setDadosAutorCorrespondente");
        Autor autorCorrespondente = new Autor();
        AlterarSubmissaoController instance = new AlterarSubmissaoController(empresa, "u1");
        instance.setDadosAutorCorrespondente(autorCorrespondente);
    }

    /**
     * Test of setDadosAutores method, of class AlterarSubmissaoController.
     */
    @Test
    public void testSetDadosAutores() {
        System.out.println("setDadosAutores");
        List<Autor> listAutor = new ArrayList<>();
        AlterarSubmissaoController instance = new AlterarSubmissaoController(empresa, "u1");
        instance.setDadosAutores(listAutor);
    }

    /**
     * Test of setListaPalavras method, of class AlterarSubmissaoController.
     */
    @Test
    public void testSetListaPalavras() {
        System.out.println("setListaPalavras");
        List<String> listPalavras = m_controllerSA.getSubmissao().getArtigo().getListaPalavrasChave();
        AlterarSubmissaoController instance = new AlterarSubmissaoController(empresa, "u1");
        instance.setListaPalavras(listPalavras);
    }

    /**
     * Test of setDadosFicheiro method, of class AlterarSubmissaoController.
     */
    @Test
    public void testSetDadosFicheiro() {
        System.out.println("setDadosFicheiro");
        String strFicheiro = "file.txt";
        AlterarSubmissaoController instance = new AlterarSubmissaoController(empresa, "u1");
        instance.setDadosFicheiro(strFicheiro);
    }

    /**
     * Test of setDadosSubmissao method, of class AlterarSubmissaoController.
     */
    @Test
    public void testSetDadosSubmissao() {
        System.out.println("setDadosSubmissao");
        Date data = new Date();
        String idUser = "u1";
        AlterarSubmissaoController instance = new AlterarSubmissaoController(empresa, "u1");
        instance.setDadosSubmissao(data, idUser);
    }

}
