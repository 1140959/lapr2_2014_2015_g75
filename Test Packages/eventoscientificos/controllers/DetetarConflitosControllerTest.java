/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.controllers;

import eventoscientificos.domain.Empresa;
import eventoscientificos.domain.Evento;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;


/**
 *
 * @author Ricardo
 */
public class DetetarConflitosControllerTest {
    private static Empresa testeEmpresa = new Empresa();
    private static Evento testeEvento = new Evento();
    

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of run method, of class DetetarConflitosController.
     */
    @Test
    public void testRun() {
        System.out.println("INÍCIO TESTE: run");
        DetetarConflitosController instance = new DetetarConflitosController(testeEmpresa,testeEvento);
        instance.run();
        System.out.println("FIM TESTE: run");
    }

}
