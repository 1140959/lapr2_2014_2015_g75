/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.controllers;

import eventoscientificos.domain.Autor;
import eventoscientificos.domain.Empresa;
import eventoscientificos.domain.Evento;
import eventoscientificos.domain.SessaoTematica;
import eventoscientificos.domain.Submissao;
import eventoscientificos.domain.Submissivel;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Carlos
 */
public class ListarSubmissoesRetiradasControllerTest {

    private static Empresa empresa;
    private static CriarEventoController m_controllerCE;
    private static CriarSessaoTematicaController m_controllerCT;
    private static SubmeterArtigoController m_controllerSA;

    public ListarSubmissoesRetiradasControllerTest() {
    }

    @BeforeClass
    public static void setUpClass() {
        empresa = new Empresa();

        RegistarUtilizadorController m_controllerRU = new RegistarUtilizadorController(empresa);
        m_controllerRU.novoUtilizador();
        m_controllerRU.setDados("u1", "u1", "u1", "u1@isep.ipp.pt");
        m_controllerRU.novoUtilizador();
        m_controllerRU.setDados("u2", "u2", "u2", "u2@isep.ipp.pt");

        m_controllerCE = new CriarEventoController(empresa);
        m_controllerCE.novoEvento();
        m_controllerCE.setTitulo("Titulo");
        m_controllerCE.setDescricao("Descricao");
        m_controllerCE.setLocal("Local");
        Calendar c = Calendar.getInstance();
        m_controllerCE.setDataInicio(c.getTime());
        c.set(Calendar.SECOND, c.get(Calendar.SECOND) + 5);
        m_controllerCE.setDataInicioSubmissao(c.getTime());
        c.set(Calendar.MINUTE, c.get(Calendar.MINUTE) + 5);
        m_controllerCE.setDataFim(c.getTime());
        m_controllerCE.setDataFimSubmissao(c.getTime());
        m_controllerCE.setDataInicioDistribuicao(c.getTime());
        m_controllerCE.setDataLimiteRevisao(c.getTime());
        m_controllerCE.setDataLimiteSubmissaoFinal(c.getTime());
        m_controllerCE.addOrganizador("u1");
        m_controllerCE.registaEvento();

        m_controllerCT = new CriarSessaoTematicaController(empresa);
        m_controllerCT.setEvento(m_controllerCT.getListaEventosRegistadosDoUtilizador("u1").get(0));
        Calendar c1 = Calendar.getInstance();
        c1.set(Calendar.SECOND, c.get(Calendar.SECOND) + 10);
        Calendar c2 = Calendar.getInstance();
        c2.set(Calendar.MINUTE, c.get(Calendar.MINUTE) + 5);
        Calendar c3 = Calendar.getInstance();
        c3.set(Calendar.MINUTE, c.get(Calendar.MINUTE) + 5);
        m_controllerCT.setDados("123", "st", c1.getTime(), c2.getTime(), c3.getTime(), c3.getTime(), c3.getTime());
        m_controllerCT.addProponente("u2");
        m_controllerCT.registaProponente();
        m_controllerCT.registaSessaoTematica();

        DefinirCPController m_controllerCP = new DefinirCPController(empresa);
        m_controllerCP.novaCP(m_controllerCP.getListaCPDefiniveisEmDefinicao("u1").get(0));
        m_controllerCP.novoMembroCP("u1");
        m_controllerCP.addMembroCP();
        boolean registaCP = m_controllerCP.registaCP();

        m_controllerCP = new DefinirCPController(empresa);
        m_controllerCP.novaCP(m_controllerCP.getListaCPDefiniveisEmDefinicao("u2").get(0));
        m_controllerCP.novoMembroCP("u1");
        m_controllerCP.addMembroCP();
        m_controllerCP.registaCP();

        m_controllerSA = new SubmeterArtigoController(empresa);
        m_controllerSA.selectSubmissivel(m_controllerCT.getSessaoTematica());
        m_controllerSA.setDados("Artigo 1", "Artigo - Resumo 1");
        Autor aut = m_controllerSA.novoAutor("u1", "strAfiliacao", "u1@isep.ipp.pt");
        m_controllerSA.addAutor(aut);
        m_controllerSA.setCorrespondente(aut);
        List<String> listPC = new ArrayList<>();
        listPC.add("palavra1");
        listPC.add("palavra2");
        m_controllerSA.setPalavrasChave(listPC);
        m_controllerSA.setFicheiro("Ficheiro PDF 1");
        m_controllerSA.registarSubmissao();
        m_controllerSA.getSubmissao().setStateRetirada();
//
//        RetirarSubmissaoController m_controllerRSC = new RetirarSubmissaoController(empresa, "u1");
//        m_controllerRSC.selectSubmissao(m_controllerSA.getSubmissao());
//        m_controllerRSC.setStateRetirado();
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getListaSessaoComSubmissaoRetiradas method, of class
     * ListarSubmissoesRetiradasController.
     */
    @Test
    public void testGetListaSessaoComSubmissaoRetiradas() {
        System.out.println("getListaSessaoComSubmissaoRetiradas");
        Evento evento = m_controllerCE.getEvento();
        ListarSubmissoesRetiradasController instance = new ListarSubmissoesRetiradasController(empresa, "u1");
        List<SessaoTematica> expResult = new ArrayList<>();
        List<SessaoTematica> result = instance.getListaSessaoComSubmissaoRetiradas();
        assertEquals(expResult, result);
    }

    /**
     * Test of getListaSubmissaoRetiradas method, of class
     * ListarSubmissoesRetiradasController.
     */
    @Test
    public void testGetListaSubmissaoRetiradas() {
        System.out.println("getListaSubmissaoRetiradas");
        Submissivel submissivel = m_controllerCT.getSessaoTematica();
        ListarSubmissoesRetiradasController instance = new ListarSubmissoesRetiradasController(empresa, "u1");
        List<Submissao> expResult = new ArrayList<>();
        expResult.add(m_controllerSA.getSubmissao());
        List<Submissao> result = instance.getListaSubmissaoRetiradas(submissivel);
        assertEquals(expResult, result);
    }

    /**
     * Test of selectSubmissao method, of class
     * ListarSubmissoesRetiradasController.
     */
    @Test
    public void testSelectSubmissao() {
        System.out.println("selectSubmissao");
        Submissao submissao = m_controllerSA.getSubmissao();
        ListarSubmissoesRetiradasController instance = new ListarSubmissoesRetiradasController(empresa, "id");
        String expResult = "Titulo:\n"
                + "Artigo 1\n"
                + "Resumo: Artigo - Resumo 1\n"
                + "ListaAutores: [Nome: u1 | Afiliação: strAfiliacao | Email: u1@isep.ipp.pt ]\n"
                + "ListaPalavrasChave: [palavra1, palavra2]\n"
                + "AutorCorrespondente: Nome: u1 | Afiliação: strAfiliacao | Email: u1@isep.ipp.pt \n"
                + "Ficheiro Ficheiro PDF 1\n"
                + "Data de Submissao 2015/06/28";
        String result = instance.selectSubmissao(submissao);
        assertEquals(expResult, result);
    }

}
