/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.controllers;

import eventoscientificos.domain.Decidivel;
import eventoscientificos.domain.Decisao;
import eventoscientificos.domain.Empresa;
import eventoscientificos.domain.Evento;
import eventoscientificos.domain.MecanismoDecisao;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Bruno
 */
public class DecidirSubmissoesControllerTest {

    private static Empresa empresa;
    private static DecidirSubmissoesController controllerDS;

    public DecidirSubmissoesControllerTest() {
    }

    @BeforeClass
    public static void setUpClass() {
        empresa = new Empresa();
        String id = "u1";
        controllerDS = new DecidirSubmissoesController(empresa, id);
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getDecisiveis method, of class DecidirSubmissoesController.
     */
    @Test
    public void testGetDecisiveis() {
        System.out.println("getDecisiveis");
        String id = "u1";
        DecidirSubmissoesController instance = new DecidirSubmissoesController(empresa, id);
        List<Decidivel> expResult = new ArrayList<>();
        List<Decidivel> result = instance.getDecisiveis();
        assertEquals(expResult, result);
    }

    /**
     * Test of novoProcessoDecisao method, of class DecidirSubmissoesController.
     */
    @Test
    public void testNovoProcessoDecisao() {
        System.out.println("novoProcessoDecisao");
        Decidivel d = new Evento();
        String id = "u1";
        DecidirSubmissoesController instance = new DecidirSubmissoesController(empresa, id);
        instance.novoProcessoDecisao(d);
    }

    /**
     * Test of getMecanismosDecisao method, of class
     * DecidirSubmissoesController.
     */
    @Test
    public void testGetMecanismosDecisao() {
        System.out.println("getMecanismosDecisao");
        String id = "u1";
        DecidirSubmissoesController instance = new DecidirSubmissoesController(empresa, id);
        List<MecanismoDecisao> expResult = new ArrayList<>();
        List<MecanismoDecisao> result = instance.getMecanismosDecisao();
        assertEquals(expResult, result);
    }

}
