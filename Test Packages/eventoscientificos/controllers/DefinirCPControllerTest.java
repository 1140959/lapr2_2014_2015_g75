/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.controllers;

import eventoscientificos.domain.CPDefinivel;
import eventoscientificos.domain.Empresa;
import eventoscientificos.domain.Evento;
import eventoscientificos.domain.SessaoTematica;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Carlos
 */
public class DefinirCPControllerTest {

    private static Empresa empresa = new Empresa();
    private static RegistarUtilizadorController m_controllerRU;
    private static CriarEventoController m_controllerCE;
    private static CriarSessaoTematicaController m_controllerCST;

    public DefinirCPControllerTest() {
    }

    @BeforeClass
    public static void setUpClass() {
        m_controllerRU = new RegistarUtilizadorController(empresa);
        m_controllerRU.novoUtilizador();
        m_controllerRU.setDados("u1", "u1", "u1", "u1@isep.ipp.pt");
        m_controllerRU.novoUtilizador();
        m_controllerRU.setDados("u2", "u2", "u2", "u2@isep.ipp.pt");

        m_controllerCE = new CriarEventoController(empresa);
        m_controllerCE.novoEvento();
        m_controllerCE.setTitulo("Titulo");
        m_controllerCE.setDescricao("Descricao");
        m_controllerCE.setLocal("Local");
        Calendar c = Calendar.getInstance();
        m_controllerCE.setDataInicio(c.getTime());
        m_controllerCE.setDataInicioSubmissao(c.getTime());
        m_controllerCE.setDataFim(c.getTime());
        m_controllerCE.setDataFimSubmissao(c.getTime());
        m_controllerCE.setDataInicioDistribuicao(c.getTime());
        m_controllerCE.setDataLimiteRevisao(c.getTime());
        m_controllerCE.setDataLimiteSubmissaoFinal(c.getTime());
        m_controllerCE.addOrganizador("u1");
        m_controllerCE.registaEvento();

        m_controllerCST = new CriarSessaoTematicaController(empresa);
        m_controllerCST.setEvento(m_controllerCE.getEvento());
        m_controllerCST.setDados("123", "st", c.getTime(), c.getTime(), c.getTime(), c.getTime(), c.getTime());
        m_controllerCST.addProponente("u2");
        m_controllerCST.registaProponente();
        m_controllerCST.registaSessaoTematica();
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }


    /**
     * Test of getListaCPDefiniveisEmDefinicaoEvento method, of class
     * DefinirCPController.
     */
    @Test
    public void testGetListaCPDefiniveisEmDefinicaoEvento() {
        System.out.println("getListaCPDefiniveisEmDefinicaoEvento");
        String strID = "u1";
        DefinirCPController instance = new DefinirCPController(empresa);
        List<Evento> expResult = new ArrayList<>();
        expResult.add(m_controllerCE.getEvento());
        List<Evento> result = instance.getListaCPDefiniveisEmDefinicaoEvento(strID);
        assertEquals(expResult, result);
    }

    /**
     * Test of novaCP method, of class DefinirCPController.
     */
    @Test
    public void testNovaCP() {
        System.out.println("novaCP");
        DefinirCPController instance = new DefinirCPController(empresa);
        CPDefinivel cpDefinivel = m_controllerCE.getEvento();
        instance.novaCP(cpDefinivel);
    }

    /**
     * Test of novoMembroCP method, of class DefinirCPController.
     */
    @Test
    public void testNovoMembroCPTrue() {
        System.out.println("novoMembroCP");
        String strId = "u1";
        DefinirCPController instance = new DefinirCPController(empresa);
        instance.novaCP(m_controllerCE.getEvento());
        boolean expResult = true;
        boolean result = instance.novoMembroCP(strId);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of novoMembroCP method, of class DefinirCPController.
     */
    @Test
    public void testNovoMembroCPFalse() {
        System.out.println("novoMembroCP");
        String strId = "u3";
        DefinirCPController instance = new DefinirCPController(empresa);
        instance.novaCP(m_controllerCE.getEvento());
        boolean expResult = false;
        boolean result = instance.novoMembroCP(strId);
        assertEquals(expResult, result);
    }

    /**
     * Test of addMembroCP method, of class DefinirCPController.
     */
    @Test
    public void testAddMembroCP() {
        System.out.println("addMembroCP");
        DefinirCPController instance = new DefinirCPController(empresa);
        instance.novaCP(m_controllerCE.getEvento());
        instance.novoMembroCP("u1");
        boolean expResult = true;
        boolean result = instance.addMembroCP();
        assertEquals(expResult, result);
    }

    /**
     * Test of registaCP method, of class DefinirCPController.
     */
    @Test
    public void testRegistaCP() {
        System.out.println("registaCP");
        DefinirCPController instance = new DefinirCPController(empresa);
        instance.novaCP(m_controllerCE.getEvento());
        instance.novoMembroCP("u1");
        instance.addMembroCP();
        instance.registaCP();
    }
    
}
