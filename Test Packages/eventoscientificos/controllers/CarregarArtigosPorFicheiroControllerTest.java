/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.controllers;

import eventoscientificos.domain.Autor;
import eventoscientificos.domain.Empresa;
import eventoscientificos.domain.Evento;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Carlos
 */
public class CarregarArtigosPorFicheiroControllerTest {

    private static Empresa empresa;
    private static RegistarUtilizadorController m_controllerRU;
    private static CriarEventoController m_controllerCE;

    public CarregarArtigosPorFicheiroControllerTest() {
    }

    @BeforeClass
    public static void setUpClass() {
        empresa = new Empresa();

        m_controllerRU = new RegistarUtilizadorController(empresa);
        m_controllerRU.novoUtilizador();
        m_controllerRU.setDados("u1", "u1", "u1", "u1@isep.ipp.pt");
        m_controllerRU.novoUtilizador();
        m_controllerRU.setDados("u2", "u2", "u2", "u2@isep.ipp.pt");

        m_controllerCE = new CriarEventoController(empresa);
        m_controllerCE.novoEvento();
        m_controllerCE.setTitulo("Titulo");
        m_controllerCE.setDescricao("Descricao");
        m_controllerCE.setLocal("Local");
        Calendar c = Calendar.getInstance();
        m_controllerCE.setDataInicio(c.getTime());
        c.set(Calendar.SECOND, c.get(Calendar.SECOND) + 5);
        m_controllerCE.setDataInicioSubmissao(c.getTime());
        c.set(Calendar.MINUTE, c.get(Calendar.MINUTE) + 5);
        m_controllerCE.setDataFim(c.getTime());
        m_controllerCE.setDataFimSubmissao(c.getTime());
        m_controllerCE.setDataInicioDistribuicao(c.getTime());
        m_controllerCE.setDataLimiteRevisao(c.getTime());
        m_controllerCE.setDataLimiteSubmissaoFinal(c.getTime());
        m_controllerCE.addOrganizador("u1");
        m_controllerCE.registaEvento();

        CriarSessaoTematicaController m_controllerCT = new CriarSessaoTematicaController(empresa);
        m_controllerCT.setEvento(m_controllerCT.getListaEventosRegistadosDoUtilizador("u1").get(0));
        Calendar c1 = Calendar.getInstance();
        c1.set(Calendar.SECOND, c.get(Calendar.SECOND) + 10);
        Calendar c2 = Calendar.getInstance();
        c2.set(Calendar.MINUTE, c.get(Calendar.MINUTE) + 5);
        Calendar c3 = Calendar.getInstance();
        c3.set(Calendar.MINUTE, c.get(Calendar.MINUTE) + 5);
        m_controllerCT.setDados("123", "st", c1.getTime(), c2.getTime(), c3.getTime(), c3.getTime(), c3.getTime());
        m_controllerCT.addProponente("u2");
        m_controllerCT.registaProponente();
        m_controllerCT.registaSessaoTematica();

        DefinirCPController m_controllerCP = new DefinirCPController(empresa);
        m_controllerCP.novaCP(m_controllerCP.getListaCPDefiniveisEmDefinicao("u1").get(0));
        m_controllerCP.novoMembroCP("u1");
        m_controllerCP.addMembroCP();
        boolean registaCP = m_controllerCP.registaCP();

        m_controllerCP = new DefinirCPController(empresa);
        m_controllerCP.novaCP(m_controllerCP.getListaCPDefiniveisEmDefinicao("u2").get(0));
        m_controllerCP.novoMembroCP("u1");
        m_controllerCP.addMembroCP();
        m_controllerCP.registaCP();

        m_controllerCE.getEvento().setStateEmSubmissao();

        SubmeterArtigoController m_controllerSA = new SubmeterArtigoController(empresa);
        m_controllerSA.selectSubmissivel(m_controllerCT.getSessaoTematica());
        m_controllerSA.setDados("Artigo 1", "Artigo - Resumo 1");
        Autor aut = m_controllerSA.novoAutor("u1", "strAfiliacao", "u1@isep.ipp.pt");
        System.out.println("\n\n\n\n\n teste " + m_controllerSA.addAutor(aut));
        m_controllerSA.setCorrespondente(aut);
        List<String> listPC = new ArrayList<>();
        listPC.add("palavra1");
        listPC.add("palavra2");
        m_controllerSA.setPalavrasChave(listPC);
        m_controllerSA.setFicheiro("Ficheiro PDF 1");
        m_controllerSA.registarSubmissao();

    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getListaEventosEmSubmissao method, of class
     * CarregarArtigosPorFicheiroController.
     */
    @Test
    public void testGetListaEventosEmSubmissao() {
        System.out.println("getListaEventosEmSubmissao");
        CarregarArtigosPorFicheiroController instance = new CarregarArtigosPorFicheiroController(empresa);
        List<Evento> expResult = new ArrayList<Evento>();
        expResult.add(m_controllerCE.getEvento());
        List<Evento> result = instance.getListaEventosEmSubmissao();
        assertEquals(expResult, result);
    }

    /**
     * Test of selectEvento method, of class
     * CarregarArtigosPorFicheiroController.
     */
    @Test
    public void testSelectEvento() {
        System.out.println("selectEvento");
        Evento est = m_controllerCE.getEvento();
        CarregarArtigosPorFicheiroController instance = new CarregarArtigosPorFicheiroController(empresa);
        instance.selectEvento(est);
    }

    /**
     * Test of importarFicheiro method, of class
     * CarregarArtigosPorFicheiroController.
     */
    @Test
    public void testImportarFicheiro() {
        System.out.println("importarFicheiro");
        String ficheiro = "TABELA_TESTE.csv";
        String id = "u1";
        CarregarArtigosPorFicheiroController instance = new CarregarArtigosPorFicheiroController(empresa);
        instance.selectEvento(m_controllerCE.getEvento());
        boolean expResult = false;
        boolean result = instance.importarFicheiro(ficheiro);
        assertEquals(expResult, result);
    }

}
