/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.controllers;

import eventoscientificos.domain.Empresa;
import eventoscientificos.domain.Evento;
import eventoscientificos.domain.Revisao;
import eventoscientificos.domain.Revisavel;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Bruno
 */
public class ReverArtigoControllerTest {

    private final Empresa empresa;
    private final ReverArtigoController instance;
    private final String id;

    public ReverArtigoControllerTest() {
        empresa = new Empresa();
        id = "bruno";
        instance = new ReverArtigoController(empresa, id);
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getRevisaveisEmRevisaoDoRevisor method, of class
     * ReverArtigoController.
     */
    @Test
    public void testGetRevisaveisEmRevisaoDoRevisor() {
        System.out.println("getRevisaveisEmRevisaoDoRevisor");
        
        List<Revisavel> expResult = new ArrayList<>();
        
        List<Revisavel> result = instance.getRevisaveisEmRevisaoDoRevisor(id);
        assertEquals(expResult, result);
    }

    /**
     * Test of selecionaRevisavel method, of class ReverArtigoController.
     */
    @Test
    public void testSelecionaRevisavel() {
        System.out.println("selecionaRevisavel");
        Evento e= new Evento();
        Revisavel r = e;
        List<Revisao> expResult = null;
        List<Revisao> result = instance.selecionaRevisavel(r, id);
        assertEquals(expResult, result);
    }

    /**
     * Test of selecionaRevisao method, of class ReverArtigoController.
     */
    @Test
    public void testSelecionaRevisao() {
        System.out.println("selecionaRevisao");
        Revisao rev = null;
        instance.selecionaRevisao(rev);}

    /**
     * Test of setDadosRevisao method, of class ReverArtigoController.
     */
    @Test
    public void testSetDadosRevisao() {
        System.out.println("setDadosRevisao");
        String confianca = "1";
        String just = "razoavel";
        String adequacao = "2";
        String originalidade = "2";
        String qualidade = "1";
        String recomendacao = "1";
        instance.setDadosRevisao(confianca, just, adequacao, originalidade, qualidade, recomendacao);
        System.out.println(instance);
    }

}
