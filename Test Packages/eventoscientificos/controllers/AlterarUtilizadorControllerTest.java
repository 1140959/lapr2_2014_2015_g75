/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.controllers;

import eventoscientificos.domain.Empresa;
import eventoscientificos.domain.Utilizador;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Ricardo
 */
public class AlterarUtilizadorControllerTest {

    public AlterarUtilizadorControllerTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Teste ao método getUtilizador da classe AlterarUtilizadorController.
     */
    @Test
    public void testGetUtilizador() {

        Empresa testeEmpresa = new Empresa();
        Utilizador testUser = new Utilizador();
        testUser.setEmail("testeemail@email.com");
        testUser.setNome("test_nome");
        testUser.setPassword("test_password");
        testUser.setUsername("test_username");
        testeEmpresa.getRegistoUtilizadores().registaUtilizador(testUser);
        String id = testUser.getUsername();

        System.out.println("INÍCIO TESTE: getUtilizador");
        String strID = "test_username";
        AlterarUtilizadorController instance = new AlterarUtilizadorController(id, testeEmpresa);
        Utilizador expResult = testUser;
        Utilizador result = instance.getUtilizador(strID);
        assertEquals(expResult, result);
        System.out.println("FIM TESTE: getUtilizador");
    }

    /**
     * Teste ao método alteraDados da classe AlterarUtilizadorController.
     */
    @Test
    public void testAlteraDados() {

        Empresa testeEmpresa = new Empresa();
        Utilizador m_user = new Utilizador();
        m_user.setEmail("testeemail@email.com");
        m_user.setNome("test_nome");
        m_user.setPassword("test_password");
        m_user.setUsername("test_username");
        testeEmpresa.getRegistoUtilizadores().registaUtilizador(m_user);
        String id = m_user.getUsername();
        

        System.out.println("INÍCIO TESTE: alteraDados");
        String strNome = "novo_nome";
        String strUsername = "novo_username";
        String strPwd = "nova_password";
        String strEmail = "novo_email@email.com";
        AlterarUtilizadorController instance = new AlterarUtilizadorController(id, testeEmpresa);
        instance.getUtilizador("test_username");
        boolean expResult = true;
        boolean result = instance.alteraDados(strNome, strUsername, strPwd, strEmail);
        assertEquals(expResult, result);
        System.out.println("FIM TESTE: alteraDados");
    }

    /**
     * Teste ao  método getM_empresa da classe AlterarUtilizadorController.
     */
    @Test
    public void testGetM_empresa() {
        System.out.println("INÍCIO TESTE: getM_empresa");
        AlterarUtilizadorController instance = new AlterarUtilizadorController();
        Empresa expResult = null;
        Empresa result = instance.getM_empresa();
        assertEquals(expResult, result);
        System.out.println("FIM TESTE: getM_empresa");
    }

    /**
     * Teste ao método getM_user da classe AlterarUtilizadorController.
     */
    @Test
    public void testGetM_user() {
        System.out.println("INÍCIO TESTE: getM_user");
        AlterarUtilizadorController instance = new AlterarUtilizadorController();
        Utilizador expResult = null;
        Utilizador result = instance.getM_user();
        assertEquals(expResult, result);
        System.out.println("FIM TESTE: getM_user");
    }

}
