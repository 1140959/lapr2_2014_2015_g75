package eventoscientificos.ui;

import eventoscientificos.domain.Autor;
import eventoscientificos.domain.Empresa;
import eventoscientificos.domain.Evento;
import eventoscientificos.domain.SessaoTematica;
import eventoscientificos.domain.Submissao;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.transform.TransformerException;
import utils.*;

/**
 *
 * @author Nuno Silva
 */
public class MenuUI {

    private Empresa m_empresa;
    private String opcao;

    public MenuUI(Empresa empresa) {
        m_empresa = empresa;
    }

    public void run() throws IOException {
        do {
            System.out.println("1. Registar utilizador");
            System.out.println("2. Criar evento científico");
            System.out.println("3. Definir CP");
            System.out.println("4. Submeter artigo científico");
            System.out.println("5. Rever artigo");
            System.out.println("6. Criar sessão temática");
            System.out.println("8. Distribuir revisões");
            System.out.println("9. Alterar utilizador");
            System.out.println("10. Alterar submissão de artigo");
            System.out.println("11. Licitar artigos para revisão");
            System.out.println("12. Definir tipo de conflito");
            System.out.println("13. Decidir sobre submissões");
            System.out.println("0. Sair");

            opcao = Utils.readLineFromConsole("Introduza opção: ");

            if (opcao.equals("1")) {
                RegistarUtilizadorUI ui = new RegistarUtilizadorUI(m_empresa);
                ui.run();
            } else if (opcao.equals("2")) {
                CriarEventoUI ui = new CriarEventoUI(m_empresa);
                ui.run();
            } else if (opcao.equals("3")) {
                DefinirCPUI ui = new DefinirCPUI(m_empresa);
                ui.run();
            } else if (opcao.equals("4")) {
                SubmeterArtigoUI ui = new SubmeterArtigoUI(m_empresa);
                ui.run();
            } else if (opcao.equals("5")) {
                String id = "";
                ReverArtigoUI ui = new ReverArtigoUI(m_empresa, id);
                ui.run();
            } else if (opcao.equals("6")) {
                CriarSessaoTematicaUI ui = new CriarSessaoTematicaUI(m_empresa);
                ui.run();
            } else if (opcao.equals("8")) {
                DistribuirRevisoesUI ui = new DistribuirRevisoesUI(m_empresa);
                ui.run();
            } else if (opcao.equals("9")) {
                AlterarUtilizadorUI ui = new AlterarUtilizadorUI(m_empresa);
                ui.run();
            } else if (opcao.equals("10")) {
                AlterarSubmissaoUI ui = new AlterarSubmissaoUI(m_empresa);
                ui.run();
            } else if (opcao.equals("11")) {
                LicitarArtigoUI ui = new LicitarArtigoUI(m_empresa);
                ui.run();
            } else if (opcao.equals("12")) {
                DefinirTipoConflitoUI ui = new DefinirTipoConflitoUI(m_empresa);
                ui.run();
            } else if (opcao.equals("13")) {
                DecidirSubmissoesUI ui = new DecidirSubmissoesUI(m_empresa);
                ui.run();
            } else if (opcao.equals("14")) {
                DecidirSubmissoesUI ui = new DecidirSubmissoesUI(m_empresa);
                ui.run();
            } else if (opcao.equals("15")) {
                for (Evento listaEvento : m_empresa.getRegistoEventos().getListaEventos()) {
                    System.out.println("Evento");
                    for (Submissao submissoe : listaEvento.getSubmissoes()) {
                        System.out.println("\t" + submissoe.getArtigo().getTitulo() + "\t" + submissoe.getState().toString());
                        System.out.println("\t" + submissoe.getArtigo().getTitulo() + "\t" + submissoe.getState().toString());
//                        System.out.println("\t" + submissoe.getArtigo().getAutorCorrespondente());
//                        for (Autor autor : submissoe.getArtigo().getListaAutores().getAutores()) {
//                            System.out.println("\t " + autor.toString());
//                        }
                    }
                    for (SessaoTematica sessao : listaEvento.getListaDeSessoesTematicas().getListaDeSessaoTematica()) {
                        System.out.println("Sessao");
                        for (Submissao submissoe : sessao.getSubmissoes()) {
                            System.out.println("\t" + submissoe.getArtigo().getTitulo() + "\t" + submissoe.getState().toString());
//                            System.out.println("\t" + submissoe.getArtigo().getAutorCorrespondente());
//                            for (Autor autor : submissoe.getArtigo().getListaAutores().getAutores()) {
//                                System.out.println("\t " + autor.toString());
//                            }
                        }
                    }
                }
            } else if (opcao.equals("20")) {
                for (Evento listaEvento : m_empresa.getRegistoEventos().getListaEventos()) {
                    System.out.println(listaEvento.getTitulo());
                    for (Object listaOrganizadore : listaEvento.getListaOrganizadores().getListaOrganizadores()) {
                        System.out.println(listaOrganizadore);
                    }
                }
            } else if (opcao.equals("40")) {
                for (Evento listaEvento : m_empresa.getRegistoEventos().getListaEventos()) {
                    System.out.println("Evento");
                    for (Submissao submissoe : listaEvento.getSubmissoes()) {
                        System.out.println("\t" + submissoe.getArtigoFinal().getTitulo() + " - " + submissoe.getState());
                    }
                    for (SessaoTematica sessao : listaEvento.getListaDeSessoesTematicas().getListaDeSessaoTematica()) {
                        System.out.println("Sessao");
                        for (Submissao submissoe : sessao.getSubmissoes()) {
                            System.out.println("\t" + submissoe.getArtigoFinal().getTitulo() + " - " + submissoe.getState());
                        }
                    }
                }
            } else if (opcao.equals("50")) {
                for (Evento col : m_empresa.getRegistoEventos().getListaEventos()) {
                    System.out.println(col + " " + col.getState());
                }
            } else if (opcao.equals("100")) {
                for (Evento listaEvento : m_empresa.getRegistoEventos().getListaEventos()) {
                    System.out.println("Evento");
                    for (Submissao submissoe : listaEvento.getSubmissoes()) {
                        System.out.println("\nemail:" + submissoe.getArtigo().getAutorCorrespondente().getEmail());
                        System.out.println("\n\t" + submissoe.getArtigoFinal().toString() + "\t" + submissoe.getArtigoFinal().getAutorCorrespondente());
                    }
                    for (SessaoTematica sessao : listaEvento.getListaDeSessoesTematicas().getListaDeSessaoTematica()) {
                        System.out.println("Sessao");
                        for (Submissao submissoe : sessao.getSubmissoes()) {
                            System.out.println("\nemail:" + submissoe.getArtigo().getAutorCorrespondente().getEmail());
                            System.out.println("\n\t" + "\t" + submissoe.getArtigoFinal().getAutorCorrespondente().getEmail());
                        }
                    }
                }
            } else if (opcao.equals("200")) {
                for (Evento listaEvento : m_empresa.getRegistoEventos().getListaEventos()) {
                    System.out.println("Evento");
                    for (Submissao submissoe : listaEvento.getSubmissoes()) {
                        System.out.println("\nemail:" + submissoe.getArtigo().getAutorCorrespondente().getEmail());
                        System.out.println("\n\t" + submissoe.getArtigo().toString());
                    }
                    for (SessaoTematica sessao : listaEvento.getListaDeSessoesTematicas().getListaDeSessaoTematica()) {
                        System.out.println("Sessao");
                        for (Submissao submissoe : sessao.getSubmissoes()) {
                            System.out.println("\nemail:" + submissoe.getArtigo().getAutorCorrespondente().getEmail());
                            System.out.println("\n\t" + "\t" + submissoe.getArtigo());
                        }
                    }
                }
            }

        } while (!opcao.equals("0"));

    }
}
