/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.ui;

import eventoscientificos.controllers.CriarEventoController;
import eventoscientificos.controllers.CriarSessaoTematicaController;
import eventoscientificos.controllers.DefinirCPController;
import eventoscientificos.controllers.RegistarUtilizadorController;
import eventoscientificos.controllers.SubmeterArtigoController;
import eventoscientificos.domain.Autor;
import eventoscientificos.domain.Empresa;
import eventoscientificos.domain.ListaAdministradores;
import eventoscientificos.domain.RegistoUtilizadores;
import eventoscientificos.domain.Utilizador;
import eventoscientificos.gui.JanelaLogin;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 *
 * @author Nuno Silva
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Empresa empresa = new Empresa();
        try {
//            Empresa empresa = teste();
//            LerXML f = new LerXML();
//            f.lerUtilizadorXML(empresa);
//            
//            EscreverXML f2 = new EscreverXML();
//            f2.escreverEventoXML(empresa);
//            
//            f.lerLocalXML();
//            f.lerEventoXML(empresa);
            
//            EscreverXML f2 = new EscreverXML();
////            f.escreverUtilizadorXML(empresa);
//            f2.escreverLocalXML(empresa);
//            f2.escreverEventoXML(empresa);
//            
//            Empresa empresa = new Empresa();
//            FicheiroXML f = new FicheiroXML();
//            f.lerEventoXML(empresa);
//            f.lerUtilizadorXML(empresa);
//           
//
//            for (Evento e : empresa.getRegistoEventos().getListaEventos()) {
//                System.out.println(e.getState());
//            }
            instanciarAdministradores(empresa);
            new JanelaLogin(empresa, true);
////
            MenuUI uiMenu = new MenuUI(empresa);
//
            uiMenu.run();
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.exit(0);
    }

    public static Empresa teste() {
        Empresa empresa = new Empresa();

        RegistarUtilizadorController m_controllerRU = new RegistarUtilizadorController(empresa);
        m_controllerRU.novoUtilizador();
        m_controllerRU.setDados("u1", "u1", "u1", "u1@isep.ipp.pt");
        m_controllerRU.novoUtilizador();
        m_controllerRU.setDados("u2", "u2", "u2", "u2@isep.ipp.pt");

        CriarEventoController m_controllerCE = new CriarEventoController(empresa);
        m_controllerCE.novoEvento();
        m_controllerCE.setTitulo("Titulo");
        m_controllerCE.setDescricao("Descricao");
        m_controllerCE.setLocal("Local");
        Calendar c = Calendar.getInstance();
        m_controllerCE.setDataInicio(c.getTime());
        c.set(Calendar.SECOND, c.get(Calendar.SECOND) + 5);
        m_controllerCE.setDataInicioSubmissao(c.getTime());
        c.set(Calendar.MINUTE, c.get(Calendar.MINUTE) + 5);
        m_controllerCE.setDataFim(c.getTime());
        m_controllerCE.setDataFimSubmissao(c.getTime());
        m_controllerCE.setDataInicioDistribuicao(c.getTime());
        m_controllerCE.setDataLimiteRevisao(c.getTime());
        m_controllerCE.setDataLimiteSubmissaoFinal(c.getTime());
        m_controllerCE.addOrganizador("u1");
        m_controllerCE.registaEvento();

        CriarSessaoTematicaController m_controllerCT = new CriarSessaoTematicaController(empresa);
        m_controllerCT.setEvento(m_controllerCT.getListaEventosRegistadosDoUtilizador("u1").get(0));
        Calendar c1 = Calendar.getInstance();
        c1.set(Calendar.SECOND, c.get(Calendar.SECOND) + 10);
        Calendar c2 = Calendar.getInstance();
        c2.set(Calendar.MINUTE, c.get(Calendar.MINUTE) + 5);
        Calendar c3 = Calendar.getInstance();
        c3.set(Calendar.MINUTE, c.get(Calendar.MINUTE) + 5);
        m_controllerCT.setDados("123", "st", c1.getTime(), c2.getTime(), c3.getTime(), c3.getTime(), c3.getTime());
        m_controllerCT.addProponente("u2");
        m_controllerCT.registaProponente();
        m_controllerCT.registaSessaoTematica();

        DefinirCPController m_controllerCP = new DefinirCPController(empresa);
        m_controllerCP.novaCP(m_controllerCP.getListaCPDefiniveisEmDefinicao("u1").get(0));
        m_controllerCP.novoMembroCP("u1");
        m_controllerCP.addMembroCP();
        boolean registaCP = m_controllerCP.registaCP();

        m_controllerCP = new DefinirCPController(empresa);
        m_controllerCP.novaCP(m_controllerCP.getListaCPDefiniveisEmDefinicao("u2").get(0));
        m_controllerCP.novoMembroCP("u1");
        m_controllerCP.addMembroCP();
        m_controllerCP.registaCP();

        m_controllerCT.getSessaoTematica().setStateEmSubmissao();

        SubmeterArtigoController m_controllerSA = new SubmeterArtigoController(empresa);
        m_controllerSA.selectSubmissivel(m_controllerCT.getSessaoTematica());
        m_controllerSA.setDados("Artigo 1", "Artigo - Resumo 1");
        Autor aut = m_controllerSA.novoAutor("u1", "strAfiliacao", "u1@isep.ipp.pt");
        System.out.println("\n\n\n\n\n teste " + m_controllerSA.addAutor(aut));
        m_controllerSA.setCorrespondente(aut);
        List<String> listPC = new ArrayList<>();
        listPC.add("palavra1");
        listPC.add("palavra2");
        m_controllerSA.setPalavrasChave(listPC);
        m_controllerSA.setFicheiro("Ficheiro PDF 1");
        System.out.println("ASDASD " + m_controllerSA.registarSubmissao());

        SubmeterArtigoController m_controllerSA2 = new SubmeterArtigoController(empresa);
        m_controllerSA2.selectSubmissivel(m_controllerCE.getEvento());
        m_controllerSA2.setDados("Artigo 2", "Artigo - Resumo 2");
        Autor aut2 = m_controllerSA2.novoAutor("u1", "strAfiliacao", "u1@isep.ipp.pt");
        System.out.println("\n\n\n\n\n teste " + m_controllerSA2.addAutor(aut2));
        m_controllerSA2.setCorrespondente(aut2);
        List<String> listPC2 = new ArrayList<>();
        listPC2.add("palavra1");
        listPC2.add("palavra2");
        m_controllerSA2.setPalavrasChave(listPC2);
        m_controllerSA2.setFicheiro("Ficheiro PDF 1");
        m_controllerSA2.registarSubmissao();

        CriarEventoController m_controllerCE2 = new CriarEventoController(empresa);
        m_controllerCE2.novoEvento();
        m_controllerCE2.setTitulo("Titulo - 2");
        m_controllerCE2.setDescricao("Descricao");
        m_controllerCE2.setLocal("Local");
        Calendar c4 = Calendar.getInstance();
        m_controllerCE2.setDataInicio(c4.getTime());
        c4.set(Calendar.SECOND, c4.get(Calendar.SECOND) + 5);
        m_controllerCE2.setDataInicioSubmissao(c4.getTime());
        c4.set(Calendar.MINUTE, c4.get(Calendar.MINUTE) + 5);
        m_controllerCE2.setDataFim(c4.getTime());
        m_controllerCE2.setDataFimSubmissao(c4.getTime());
        m_controllerCE2.setDataInicioDistribuicao(c4.getTime());
        m_controllerCE2.setDataLimiteRevisao(c4.getTime());
        m_controllerCE2.setDataLimiteSubmissaoFinal(c4.getTime());
        m_controllerCE2.addOrganizador("u1");
        m_controllerCE2.registaEvento();

        CriarSessaoTematicaController m_controllerCT2 = new CriarSessaoTematicaController(empresa);
        m_controllerCT2.setEvento(m_controllerCE2.getEvento());
        Calendar c5 = Calendar.getInstance();
        c5.set(Calendar.SECOND, c5.get(Calendar.SECOND) + 10);
        Calendar c6 = Calendar.getInstance();
        c6.set(Calendar.MINUTE, c6.get(Calendar.MINUTE) + 5);
        Calendar c7 = Calendar.getInstance();
        c7.set(Calendar.MINUTE, c7.get(Calendar.MINUTE) + 5);
        m_controllerCT2.setDados("123", "st", c5.getTime(), c6.getTime(), c7.getTime(), c7.getTime(), c7.getTime());
        m_controllerCT2.addProponente("u1");
        m_controllerCT2.registaProponente();
        m_controllerCT2.registaSessaoTematica();

        CriarEventoController m_controllerCE3 = new CriarEventoController(empresa);
        m_controllerCE3.novoEvento();
        m_controllerCE3.setTitulo("Titulo -3");
        m_controllerCE3.setDescricao("Descricao -3");
        m_controllerCE3.setLocal("Local -3");
        Calendar c10 = Calendar.getInstance();
        m_controllerCE3.setDataInicio(c10.getTime());
        c10.set(Calendar.SECOND, c10.get(Calendar.SECOND) + 5);
        m_controllerCE3.setDataInicioSubmissao(c10.getTime());
        c10.set(Calendar.MINUTE, c10.get(Calendar.MINUTE) + 5);
        m_controllerCE3.setDataFim(c10.getTime());
        m_controllerCE3.setDataFimSubmissao(c10.getTime());
        m_controllerCE3.setDataInicioDistribuicao(c10.getTime());
        m_controllerCE3.setDataLimiteRevisao(c10.getTime());
        m_controllerCE3.setDataLimiteSubmissaoFinal(c10.getTime());
        m_controllerCE3.addOrganizador("u1");
        m_controllerCE3.registaEvento();

        return empresa;
    }

    public static void instanciarAdministradores(Empresa empresa) {
        RegistoUtilizadores ru = empresa.getRegistoUtilizadores();
        ListaAdministradores la = empresa.getListaAdministradores();
        Utilizador u1 = ru.novoUtilizador();
        u1.setEmail("1140959@isep.ipp.pt");
        u1.setNome("David Pinheiro");
        u1.setPassword("david");
        u1.setUsername("1140959");
        ru.registaUtilizador(u1);
        la.addAdministrador(la.novoAdministrador(u1.getUsername()));

        Utilizador u2 = ru.novoUtilizador();
        u2.setEmail("1140858@isep.ipp.pt");
        u2.setNome("Carlos Moutinho");
        u2.setPassword("carlos");
        u2.setUsername("1140858");
        ru.registaUtilizador(u2);
        la.addAdministrador(la.novoAdministrador(u2.getUsername()));

        Utilizador u3 = ru.novoUtilizador();
        u3.setEmail("1140958@isep.ipp.pt");
        u3.setNome("Bruno Fernandes");
        u3.setPassword("bruno");
        u3.setUsername("1140958");
        ru.registaUtilizador(u3);
        la.addAdministrador(la.novoAdministrador(u3.getUsername()));

        Utilizador u4 = ru.novoUtilizador();
        u4.setEmail("1140249@isep.ipp.pt");
        u4.setNome("Ricardo Moreira");
        u4.setPassword("ricardo");
        u4.setUsername("1140249");
        ru.registaUtilizador(u4);
        la.addAdministrador(la.novoAdministrador(u4.getUsername()));

        Utilizador u5 = ru.novoUtilizador();
        u5.setEmail("1081019@isep.ipp.pt");
        u5.setNome("Claudio Felgueiras");
        u5.setPassword("claudio");
        u5.setUsername("1081019");
        ru.registaUtilizador(u5);
        la.addAdministrador(la.novoAdministrador(u5.getUsername()));
    }

}
