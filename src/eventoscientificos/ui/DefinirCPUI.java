package eventoscientificos.ui;

import eventoscientificos.controllers.DefinirCPController;
import eventoscientificos.domain.CPDefinivel;
import eventoscientificos.domain.Empresa;
import java.util.List;
import utils.Utils;

/**
 *
 * @author Nuno Silva
 */
public class DefinirCPUI implements UI {

    private Empresa m_empresa;
    private DefinirCPController m_controller;

    public DefinirCPUI(Empresa empresa) {
        m_empresa = empresa;
        m_controller = new DefinirCPController(m_empresa);
    }

    @Override
    public void run() {
        String strOrg = Utils.readLineFromConsole("Introduza ID Organizador/Proponente: ");

        List<CPDefinivel> lsObjs = m_controller.getListaCPDefiniveisEmDefinicao(strOrg);
        Utils.apresentaLista(lsObjs, "Selecione o Evento/Sessão Temática:");
        CPDefinivel cpdef = (CPDefinivel) Utils.selecionaObject(lsObjs);

        if (cpdef != null) {
            m_controller.novaCP(cpdef);

            while (Utils.confirma("Pretende inserir membro CP (s/n)?")) {
                String strMembro = Utils.readLineFromConsole("Introduza ID Membro CP: ");
//                Revisor r = m_controller.novoMembroCP(strMembro);
//                r.toString();
                if (Utils.confirma("Confirma adicão do Membro da CP?") && m_controller.novoMembroCP(strMembro)) {
                    m_controller.addMembroCP();
                }
            }
            m_controller.registaCP();
        } else {
            System.out.println("Operação terminada");
        }
    }
}
