/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.ui;

import eventoscientificos.controllers.LicitarArtigoController;
import eventoscientificos.domain.Empresa;

/**
 *
 * @author nunosilva
 */
class LicitarArtigoUI implements UI
{
    private Empresa m_empresa;
    private LicitarArtigoController m_controller;

    public LicitarArtigoUI( Empresa empresa )
    {
        m_empresa = empresa;
        m_controller = new LicitarArtigoController(m_empresa);
    }

    public void run()
    {

    }
}
