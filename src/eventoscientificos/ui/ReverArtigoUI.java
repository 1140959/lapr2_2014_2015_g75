/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.ui;

import eventoscientificos.controllers.ReverArtigoController;
import eventoscientificos.domain.Empresa;
import eventoscientificos.domain.Revisao;
import eventoscientificos.domain.Revisavel;
import java.util.List;
import java.util.ListIterator;
import utils.Utils;

/**
 *
 * @author nunosilva
 */
class ReverArtigoUI implements UI
{
    private Empresa m_empresa;
    private ReverArtigoController m_controller;

    public ReverArtigoUI( Empresa empresa,String id )
    {
        m_empresa = empresa;
        m_controller = new ReverArtigoController(m_empresa,id);
    }

    public void run()
    {
        String strOrg = Utils.readLineFromConsole("Introduza ID Organizador/Proponente: ");
        
        List<Revisavel> lsObjs = m_controller.getRevisaveisEmRevisaoDoRevisor(strOrg);
        Utils.apresentaLista(lsObjs, "Selecione o Evento/Sessão Temática:");
        Revisavel r = (Revisavel) Utils.selecionaObject(lsObjs);
        solicitaRevisao(r, strOrg);
    }
    
    public void solicitaRevisao(Revisavel r , String strOrg)
    {
        List<Revisao> lrev = m_controller.selecionaRevisavel(r, strOrg);
        
        for( ListIterator<Revisao > it = lrev.listIterator(); it.hasNext(); )
        {
            Revisao rev = it.next();
            apresentaRevisao( rev );
            String decisao = Utils.readLineFromConsole("Introduza decicao: ");
            String justificacao = Utils.readLineFromConsole("Introduza justificacao: ");
//            m_controller.setDadosRevisao(decisao,justificacao);
        }
    }
    
        private void apresentaRevisao( Revisao rev )
    {
        if(rev == null)
            System.out.println("Decisao inválida.");
        else
            System.out.println(rev.toString() );
    }
}
