package eventoscientificos.domain;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

/**
 * Classe Lista de Proponentes
 * @author Paulo Maio 
 */
public class ListaProponentes {

    private List<Proponente> m_listaProponentes;

    public ListaProponentes() {
        m_listaProponentes = new ArrayList<Proponente>();
    }

    public List<Proponente> getListaProponentes() {
        return m_listaProponentes;
    }
    
    public Proponente novoProponente(Utilizador u) {
        if (u == null) {
            return null;
        }
        System.out.println("ListaProponentes: addProponente: " + u.showData());
        Proponente p = new Proponente(u);

        if (p.valida() && validaProponente(p)) {
            return p;
        } else {
            return null;
        }
    }

    public boolean registaProponente(Proponente prop) {

        System.out.println("ListaProponentes: registaProponente: " + prop.showData());

        if (prop.valida() && validaProponente(prop)) {
            return addProponente(prop);
        } else {
            return false;
        }
    }

    private boolean addProponente(Proponente p) {
        return m_listaProponentes.add(p);
    }

    private boolean validaProponente(Proponente p) {
        System.out.println("ListaProponentes: validaProponente:" + p.showData());
        return true;
    }

    public boolean hasProponente(String strID) {
        for (Proponente prop : this.m_listaProponentes) {
            if (prop.getUtilizador().getUsername().equals(strID)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean equals(Object outroObjeto) {
        if (this == outroObjeto) {
            return true;
        }
        if (outroObjeto == null || this.getClass() != outroObjeto.getClass()) {
            return false;
        }

        ListaProponentes outraListaProponentes = (ListaProponentes) outroObjeto;

        if (this.m_listaProponentes.size() != outraListaProponentes.m_listaProponentes.size()) {
            return false;
        }

        Iterator<Proponente> it1 = this.m_listaProponentes.iterator();
        Iterator<Proponente> it2 = outraListaProponentes.m_listaProponentes.iterator();

        boolean res = true;

        while (it1.hasNext() && it2.hasNext() && res) {
            Proponente p1 = it1.next();
            Proponente p2 = it2.next();
            res = p1.equals(p2);
        }

        return res;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 53 * hash + Objects.hashCode(this.m_listaProponentes);
        return hash;
    }
}
