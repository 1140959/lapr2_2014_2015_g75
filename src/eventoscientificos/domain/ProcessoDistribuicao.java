package eventoscientificos.domain;

import java.util.List;

/**
 * Interface Processo Distribuição
 * @author 1081019
 */
public interface ProcessoDistribuicao {

    public List<Revisor> getListaDeRevisoes();
    String getId();
    boolean Valido();
}
