package eventoscientificos.domain;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

/**
 * Classe Processo Licitação Sessão Temática
 * @author iazevedo
 */
public class ProcessoLicitacaoST implements ProcessoLicitacao {

    List<Licitacao> m_lstLicitacao;

    /** Construtor de nova Licitação
     * @param r O Revisor a adicionar à Licitação da Sessão Temática
     * @param s A Submissão a adicionar à Licitação da Sessão Temática
     * @return A Licitação da Sessão Temática instanciada com os dados recebidos
     * como parâmetro
     */
    @Override
    public Licitacao novaLicitação(Revisor r, Submissao s) {
        Licitacao l = new Licitacao();
        l.setRevisor(r);
        l.setSubmissao(s);

        return l;
    }

    @Override
    public boolean addLicitacao(Licitacao l) {
        throw new UnsupportedOperationException();
    }

    /**
     * Pelo facto dos mecanismos de validação serem classes "mockup", o método
     * de validação retorna sempre um boolean "true"
     *
     * @return Devolve sempre "true"
     */
    @Override
    public boolean valida() {
        return true;
    }

    @Override
    public boolean equals(Object outroObjeto) {
        if (this == outroObjeto) {
            return true;
        }
        if (outroObjeto == null || this.getClass() != outroObjeto.getClass()) {
            return false;
        }

        ProcessoLicitacaoST outroProcessoLicitacaoST = (ProcessoLicitacaoST) outroObjeto;

        if (this.m_lstLicitacao.size() != outroProcessoLicitacaoST.m_lstLicitacao.size()) {
            return false;
        }

        Iterator<Licitacao> it1 = this.m_lstLicitacao.iterator();
        Iterator<Licitacao> it2 = outroProcessoLicitacaoST.m_lstLicitacao.iterator();

        boolean res = true;

        while (it1.hasNext() && it2.hasNext() && res) {
            Licitacao l1 = it1.next();
            Licitacao l2 = it2.next();
            res = l1.equals(l2);
        }

        return res;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + Objects.hashCode(this.m_lstLicitacao);
        return hash;
    }

    @Override
    public List<Licitavel> getLicitaveisUtilizador(Utilizador utilizador) {
        return new ArrayList<>();
    }

    @Override
    public boolean registaLicitacao() {
        return true;
    }
}
