package eventoscientificos.domain;

import TOCS.iTOCS;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

/**
 * Classe Lista de Autores
 *
 * @author Paulo Maio
 */
public class ListaAutores implements iTOCS {

    private List<Autor> m_listaAutores;

    public ListaAutores() {
        m_listaAutores = new ArrayList<Autor>();
    }

    /**
     * Construtor de Autor
     *
     * @param strNome Nome
     * @param strAfiliacao Afiliação
     * @param strEmail email
     * @return Autor
     */
    public Autor novoAutor(String strNome, String strAfiliacao, String strEmail) {
        Autor autor = new Autor();

        autor.setNome(strNome);
        autor.setAfiliacao(strAfiliacao);
        autor.setEmail(strEmail);

        return autor;
    }

    /**
     * Construtor vazio de Autor
     *
     * @return Autor
     */
    public Autor novoAutor() {
        return new Autor();
    }

    /**
     * Método que adiciona um novo Autor
     *
     * @param autor Autor
     * @return resultado da adição do objeto Autor
     */
    public boolean addAutor(Autor autor) {
        if (validaAutor(autor)) {
            return m_listaAutores.add(autor);
        } else {
            return false;
        }

    }

    /**
     * Método que valida o email do Autor
     *
     * @param autor Autpr
     * @return resultado da validação do email
     */
    private boolean validaAutor(Autor autor) {
        String emailAut = autor.getEmail();
        ArrayList<String> emails = new ArrayList<>();
        for (Autor aut : this.m_listaAutores) {
            emails.add(aut.getEmail());
        }
        return autor.valida() && !emails.contains(emailAut);
    }

    /**
     * Método que retorna uma lista de Possiveis Autores Correspondentes.
     *
     * @return Lista de Autores Correspondentes possiveis.
     */
    protected List<Autor> getPossiveisAutoresCorrespondentes() {
        List<Autor> la = new ArrayList<Autor>();

        for (Autor autor : this.m_listaAutores) {
            if (autor.podeSerCorrespondente()) {
                la.add(autor);
            }
        }
        return la;
    }

    /**
     * Metodo que retorna uma lista de Autores.
     *
     * @return lista de Autores.
     */
    public List<Autor> getAutores() {
        List<Autor> la = new ArrayList<Autor>();
        for (Autor autor : this.m_listaAutores) {
            la.add(autor);
        }
        return la;
    }

    public boolean hasAutor(String email) {
        for (Autor a : this.m_listaAutores) {
            if (a.getEmail().equals(email)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean equals(Object outroObjeto) {
        if (this == outroObjeto) {
            return true;
        }
        if (outroObjeto == null || this.getClass() != outroObjeto.getClass()) {
            return false;
        }

        ListaAutores outraListaAutores = (ListaAutores) outroObjeto;

        if (this.m_listaAutores.size() != outraListaAutores.m_listaAutores.size()) {
            return false;
        }

        Iterator<Autor> it1 = this.m_listaAutores.iterator();
        Iterator<Autor> it2 = outraListaAutores.m_listaAutores.iterator();

        boolean res = true;

        while (it1.hasNext() && it2.hasNext() && res) {
            Autor a1 = it1.next();
            Autor a2 = it2.next();
            res = a1.equals(a2);
        }

        return res;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 17 * hash + Objects.hashCode(this.m_listaAutores);
        return hash;
    }

    @Override
    public String toString() {
        return m_listaAutores.toString();
    }

    @Override
    public String showData() {
        return this.toString();
    }

}
