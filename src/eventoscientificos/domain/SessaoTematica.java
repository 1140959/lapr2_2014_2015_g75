package eventoscientificos.domain;

import TOCS.iTOCS;
import eventoscientificos.state.STState;
import eventoscientificos.state.STStateCriada;
import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 * Classe Sessão Temática
 *
 * @author Paulo Maio
 */
public class SessaoTematica implements CPDefinivel, Submissivel, Distribuivel, Licitavel, Revisavel, iTOCS {

    /**
     * O codigo da SessaoTematica
     */
    private String m_codigo;

    /**
     * A descricao da SessaoTematica
     */
    private String m_descricao;

    /**
     * A data de inicio da submissao da SessaoTematica
     */
    private Date m_strDataInicioSubmissao;

    /**
     * A data de fim da submissao da SessaoTematica
     */
    private Date m_strDataFimSubmissao;

    /**
     * A data de inicio de distribuicao da SessaoTematica
     */
    private Date m_strDataInicioDistribuicao;

    /**
     * A data de limite de revisao da SessaoTematica
     */
    private Date m_strDataLimRevisao;

    /**
     * A data de limiete de artigo final da SessaoTematica
     */
    private Date m_strDataLimArtigoFinal;

    private ListaProponentes m_lsProp;
    private STState m_state;
    private CP m_cp;
    private ListaSubmissoes m_lstSubmissoes;
    private ProcessoLicitacao m_processoLicitacao;
    private ProcessoDecisao m_processoDecisao;
    private ProcessoDistribuicao m_processoDistribuicao;

    public SessaoTematica() {
    }

    
    /**
     * Construtor de Sessão Tematica
     *
     * @param codigo Codigo da ST
     * @param descricao Descrição da ST
     * @param dtInicioSub data de inicio de submissão
     * @param dtFimSub data de fim de submissão
     * @param dtInicioDistr data de inicio de distribuição
     * @param dtLimRevisao data limite de revisão
     * @param dtLimArtigoFinal data limite de submissao final
     */
    public SessaoTematica(String codigo, String descricao, Date dtInicioSub, Date dtFimSub, Date dtInicioDistr, Date dtLimRevisao, Date dtLimArtigoFinal) {
        this.m_codigo = codigo;
        this.m_descricao = descricao;
        this.m_strDataInicioSubmissao = dtInicioSub;
        this.m_strDataFimSubmissao = dtFimSub;
        this.m_strDataInicioDistribuicao = dtInicioDistr;
        this.m_strDataLimRevisao = dtLimRevisao;
        this.m_strDataLimArtigoFinal = dtLimArtigoFinal;

        this.m_lsProp = new ListaProponentes();
        this.m_lstSubmissoes = new ListaSubmissoes(this);
        STStateCriada stStateCriada = new STStateCriada(this);
    }

    public boolean isInRegistado() {
        return this.m_state.isInRegistado();
    }

    @Override
    public boolean isInCPDefinida() {
        return this.m_state.isInCPDefinida();
    }

    public boolean isInEmSubmissao() {
        return this.m_state.isInEmSubmissao();
    }

    public boolean isInEmDetecaoConflitos() {
        return this.m_state.isInEmDetecaoConflitos();
    }

    public boolean isInEmLicitacao() {
        return this.m_state.isInEmLicitacao();
    }

    public boolean isInEmDistribuicao() {
        return this.m_state.isInEmDistribuicao();
    }

    public boolean isInEmRevisao() {
        return this.m_state.isInEmRevisao();
    }

    public boolean isInEmDecisao() {
        return this.m_state.isInEmDecisao();
    }

    public boolean isInEmDecidido() {
        return this.m_state.isInEmDecidido();
    }

    public boolean setStateRegistado() {
        return this.m_state.setStateRegistado();
    }

    @Override
    public boolean setStateEmSubmissao() {
        return this.m_state.setStateEmSubmissao();
    }

    public boolean setStateEmDetecaoConflitos() {
        return this.m_state.setStateEmDetecaoConflitos();
    }

    public boolean setStateEmLicitacao() {
        return this.m_state.setStateEmLicitacao();
    }

    public boolean setStateEmDistribuicao() {
        return this.m_state.setStateEmDistribuicao();
    }

    public boolean setStateEmRevisao() {
        return this.m_state.setStateEmRevisao();
    }

    public boolean setStateEmDecisao() {
        return this.m_state.setStateEmDecisao();
    }

    public boolean setStateEmDecidido() {
        return this.m_state.setStateEmDecidido();
    }

    public String getCodigo() {
        return m_codigo;
    }

    public String getDescricao() {
        return m_descricao;
    }

    @Override
    public CP novaCP() {
        m_cp = new CPSessaoTematica();

        return m_cp;
    }

    @Override
    public boolean setCP(CP cp) {
        if (cp.valida()) {
            m_state.setStateCPDefinida();
            m_cp = cp;
            return true;
        }
        return false;
    }

    public boolean setState(STState stState) {
        this.m_state = stState;
        return true;
    }

    public boolean valida() {
        return this.m_state.valida();
    }

    public ListaProponentes getListaProponentes() {
        return this.m_lsProp;
    }

    public Date getDataInicioSubmissao() {
        return this.m_strDataInicioSubmissao;
    }

    public Date getDataFimSubmissao() {
        return this.m_strDataFimSubmissao;
    }

    public Date getDataInicioDistribuicao() {
        return this.m_strDataInicioDistribuicao;
    }

    public Date getDataLimiteRevisao() {
        return this.m_strDataLimRevisao;
    }

    public Date getDataLimiteSubmissaoFinal() {
        return this.m_strDataFimSubmissao;
    }

    @Override
    public ListaSubmissoes getListaSubmissoes() {
        return this.m_lstSubmissoes;
    }

    @Override
    public List<Submissao> getListaSubmissaoByEmail(String email) {
        return m_lstSubmissoes.getSubmissoesbyEmail(email);
    }

    /**
     * Instancia um novo processo de Licitação e coloca a Sessão Temática no
     * estado de DetecaoConflitos
     *
     * @return O ProcessoLicitacaoST instanciado
     */
    @Override
    public ProcessoLicitacao iniciaDetecao() {
        m_state.setStateEmDetecaoConflitos();
        return new ProcessoLicitacaoST();
    }

    @Override
    public List<Revisor> getRevisores() {
        return m_cp.getRevisores();
    }

    public Licitacao novaLicitação(Revisor r, Submissao s) {
        throw new UnsupportedOperationException();
    }

    public boolean addLicitacao(Licitacao l) {
        throw new UnsupportedOperationException();
    }

    /**
     * Método sem retorno que atribui o processo de Licitação recebido como
     * parâmetro à instância de Sessão Temática caso esse ProcessoLicitacao
     * passe no método de validação (neste momento, passa sempre pois o return
     * do método valida é sempre "true"). Coloca também o estado da Sessão
     * Temática no estado "EmLicitacao".
     *
     * @param pl O Processo de Licitação da Sessão Temática
     */
    @Override
    public void setProcessoLicitacao(ProcessoLicitacao pl) {
        if (pl.valida()) {
            m_processoLicitacao = pl;
            m_state.setStateEmLicitacao();
        }
    }

    @Override
    public ProcessoLicitacao getProcessoLicitacao() {
        return m_processoLicitacao;
    }

    /**
     * Devolve a lista de submissões da sessão temática
     *
     * @return A lista de submissões da sessão temática
     */
    @Override
    public List<Submissao> getSubmissoes() {
        return m_lstSubmissoes.getSubmissoes();
    }

    @Override
    public ProcessoDistribuicao getProcessoDistribuicao() {
        return m_processoDistribuicao;
    }

    boolean hasRevisor(String strID) {
        return false;
    }

    @Override
    public void alteraParaEmDecisao() {
        ListaRevisoes lr = (ListaRevisoes) m_processoDistribuicao.getListaDeRevisoes();
        if (lr.isRevisoesConcluidas()) {
            setEmDecisao();
        }

    }

    private boolean setEmDecisao() {
        return this.m_state.setStateEmDecisao();
    }

    @Override
    public String toString() {
        String str = "Sessao Tematica: ";
        str += " Codigo " + this.m_codigo;
        str += " Descricao " + this.m_descricao;
        return str;
    }

    @Override
    public boolean equals(Object outroObjeto) {
        if (this == outroObjeto) {
            return true;
        }
        if (outroObjeto == null || this.getClass() != outroObjeto.getClass()) {
            return false;
        }

        SessaoTematica outroSessaoTematica = (SessaoTematica) outroObjeto;

        return this.m_codigo.equals(outroSessaoTematica.m_codigo)
                && this.m_descricao.equals(outroSessaoTematica.m_descricao)
                && this.m_strDataInicioSubmissao.equals(outroSessaoTematica.m_strDataInicioSubmissao)
                && this.m_strDataFimSubmissao.equals(outroSessaoTematica.m_strDataFimSubmissao)
                && this.m_strDataInicioDistribuicao.equals(outroSessaoTematica.m_strDataInicioDistribuicao)
                && this.m_lsProp.equals(outroSessaoTematica.m_lsProp)
                && this.m_state.equals(outroSessaoTematica.m_state)
                && this.m_cp.equals(outroSessaoTematica.m_cp)
                && this.m_lstSubmissoes.equals(outroSessaoTematica.m_lstSubmissoes)
                && this.m_processoLicitacao.equals(outroSessaoTematica.m_processoLicitacao)
                && this.m_processoDecisao.equals(outroSessaoTematica.m_processoDecisao)
                && this.m_processoDistribuicao.equals(outroSessaoTematica.m_processoDistribuicao);

    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 59 * hash + Objects.hashCode(this.m_codigo);
        hash = 59 * hash + Objects.hashCode(this.m_descricao);
        hash = 59 * hash + Objects.hashCode(this.m_strDataInicioSubmissao);
        hash = 59 * hash + Objects.hashCode(this.m_strDataFimSubmissao);
        hash = 59 * hash + Objects.hashCode(this.m_strDataInicioDistribuicao);
        hash = 59 * hash + Objects.hashCode(this.m_lsProp);
        hash = 59 * hash + Objects.hashCode(this.m_state);
        hash = 59 * hash + Objects.hashCode(this.m_cp);
        hash = 59 * hash + Objects.hashCode(this.m_lstSubmissoes);
        hash = 59 * hash + Objects.hashCode(this.m_processoLicitacao);
        hash = 59 * hash + Objects.hashCode(this.m_processoDecisao);
        hash = 59 * hash + Objects.hashCode(this.m_processoDistribuicao);
        return hash;
    }

    public String getDetalhes() {
        return this.m_codigo;
    }

    @Override
    public void alteraSubmissao(Submissao su, Submissao suClone) {
        ListaSubmissoes lstClone = this.m_lstSubmissoes;

        lstClone.remove(su);
        lstClone.add(suClone);

        this.m_lstSubmissoes = lstClone;
    }

    @Override
    public void remove(Submissao su) {
        m_lstSubmissoes.remove(su);
    }

    @Override
    public void add(Submissao suClone) {
        m_lstSubmissoes.add(suClone);
    }

    public STState getState() {
        return this.m_state;
    }

    public boolean contemSubmissaoRetirada() {
        return m_lstSubmissoes.contemSubmissaoRetirada();
    }

    @Override
    public List<Submissao> getListaSubmissoesRetiradas() {
        return this.m_lstSubmissoes.getSubmissoesRetiradas();
    }

    @Override
    public String showData() {
        return this.toString();
    }

    public boolean contemSubmissaoSessaoDoAutor(String email) {
        return m_lstSubmissoes.contemSubmissaoDoAutor(email);
    }

}
