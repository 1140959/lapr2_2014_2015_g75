package eventoscientificos.domain;

import TOCS.iTOCS;
import java.util.Objects;

/**
 * Classe Autor
 *
 * @author Paulo Maio
 * @author 1140958@isep.ipp.pt
 */
public class Autor implements iTOCS {

    /**
     * nome do Autor
     */
    private String m_strNome;
    /**
     * email do autor
     */
    private String m_strEmail;
    /**
     * nome da instituição de afiliação
     */
    private String m_strAfiliacao;

    /**
     * Contrutor de classe vazio
     */
    public Autor() {
    }

    /**
     * Metodo de leitura do nome do Autor
     *
     * @return String
     */
    public String getNome() {
        return m_strNome;
    }

    /**
     * Metodo de leitura de do email do autor
     *
     * @return String
     */
    public String getEmail() {
        return m_strEmail;
    }

    /**
     * Metodo de leitura da instituição de afiliação do autor
     *
     * @return String
     */
    public String getAfiliacao() {
        return m_strAfiliacao;
    }

    /**
     * Metodo de alteração do nome
     *
     * @param strNome Nome do autor
     */
    public void setNome(String strNome) {
        this.m_strNome = strNome;
    }

    /**
     * Metodo de alteração do email
     *
     * @param strEmail Email do autor
     */
    public void setEmail(String strEmail) {
        this.m_strEmail = strEmail;
    }

    /**
     * Metodo de alteração de uma institução de afiliação
     *
     * @param strAfiliacao institução de afiliação
     */
    public void setAfiliacao(String strAfiliacao) {
        this.m_strAfiliacao = strAfiliacao;
    }

    /**
     * Metodo de validação de um autor
     *
     * @return boolean
     */
    public boolean valida() {
        if (!validaEmail(m_strEmail)) {
            return false;
        }
        System.out.println("Autor:valida");
        return true;
    }

    /**
     * Metodo que indica se um autor pode ser autor correspondente
     *
     * @return boolean
     */
    public boolean podeSerCorrespondente() {
        System.out.println("Autor:podeSerCorrespondente");
        return true;
    }

    /**
     * Metodo equals
     *
     * @param outroObjeto objeto de comparaçao do metodo equals
     * @return boolean
     */
    @Override
    public boolean equals(Object outroObjeto) {
        if (this == outroObjeto) {
            return true;
        }
        if (outroObjeto == null || this.getClass() != outroObjeto.getClass()) {
            return false;
        }

        Autor outroAutor = (Autor) outroObjeto;

        return this.m_strNome.equals(outroAutor.m_strNome)
                && this.m_strAfiliacao.equals(outroAutor.m_strAfiliacao)
                && this.m_strEmail.equals(outroAutor.m_strEmail);
    }

    /**
     * Metodo hasCode
     *
     * @return int
     */
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 83 * hash + Objects.hashCode(this.m_strNome);
        hash = 83 * hash + Objects.hashCode(this.m_strAfiliacao);
        hash = 83 * hash + Objects.hashCode(this.m_strEmail);
        return hash;
    }

    /**
     * Metodo toString
     *
     * @return String
     */
    @Override
    public String toString() {
        String retorno = "Nome: " + m_strNome
                + " | Afiliação: " + m_strAfiliacao
                + " | Email: " + m_strEmail + " ";
        return retorno;
    }

    /**
     * @return boolean, sendo false quando não passa o email não é correto e
     * true quando passa em todos os parametros
     * @param email email
     *
     */
    private boolean validaEmail(String email) {
        boolean valida = true;

        email = email.trim();

        if (email == null || email.equals("")) {
            valida = false;
        }

        if (!email.matches("^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,6}$")) {
            valida = false;

        }

        return valida;
    }

    /**
     * Metodo que implenta a interface showData
     *
     * @return string
     */
    @Override
    public String showData() {
        return this.toString();
    }

}
