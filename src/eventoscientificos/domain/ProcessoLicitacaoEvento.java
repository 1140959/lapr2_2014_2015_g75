package eventoscientificos.domain;

import java.util.ArrayList;
import java.util.List;

/**
 * Classe Processo Licitação Evento
 *
 * @author iazevedo
 */
public class ProcessoLicitacaoEvento implements ProcessoLicitacao {

    List<Licitacao> m_lstLicitacao;

    /**
     * Construtor de Licitaçao
     *
     * @param r O Revisor a adicionar à Licitação do Evento
     * @param s A Submissão a adicionar à Licitação do Evento
     * @return A Licitação do Evento instanciada com os dados recebidos como
     * parâmetro
     */
    @Override
    public Licitacao novaLicitação(Revisor r, Submissao s) {
        Licitacao l = new Licitacao();
        l.setRevisor(r);
        l.setSubmissao(s);

        return l;
    }

    /**
     * Metodo que permite adicionar uma nova Licitação.
     *
     * @param l
     * @return
     */
    @Override
    public boolean addLicitacao(Licitacao l) {
        return this.m_lstLicitacao.add(l);
    }

    /**
     * Pelo facto dos mecanismos de validação serem classes "mockup", o método
     * de validação retorna sempre um boolean "true"
     *
     * @return Devolve sempre "true"
     */
    @Override
    public boolean valida() {
        return true;
    }

    @Override
    public List<Licitavel> getLicitaveisUtilizador(Utilizador utilizador) {
        return new ArrayList<>();
    }

    @Override
    public boolean registaLicitacao() {
        return true;
    }

}
