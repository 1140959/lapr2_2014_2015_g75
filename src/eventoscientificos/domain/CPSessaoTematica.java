package eventoscientificos.domain;

import TOCS.iTOCS;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

/**
 * Classe Comissão Programa Sessao Tematica
 *
 * @author Paulo Maio
 */
public class CPSessaoTematica implements CP, iTOCS {

    /**
     * A lista de revisores da CPSessaoTematica
     */
    private List<Revisor> m_listaRevisor;

    /**
     * Constrói uma instância CPSessaoTematica sem parametros
     */
    public CPSessaoTematica() {
        this.m_listaRevisor = new ArrayList<Revisor>();
    }

    /**
     * Devolve a lista de revisores da CPSessaoTematica
     *
     * @return a lista de revisores
     */
    @Override
    public List<Revisor> getRevisores() {
        return this.m_listaRevisor;
    }

    /**
     * Cria um novo membro da comissao de programa recebendo por parametro o
     * utilizador
     *
     * @param u o utilizador que podera ser o novo membro da comissao de
     * programa
     * @return um revisor
     */
    @Override
    public Revisor novoMembroCP(Utilizador u) {
        Revisor r = new Revisor(u);

        if (r.valida() && validaMembroCP(r)) {
            return r;
        } else {
            return null;
        }
    }

    /**
     * Valida o revisor recebido por parametro na comissao de programa
     *
     * @param r o revisor
     * @return a validação do membro da comissao de programa
     */
    private boolean validaMembroCP(Revisor r) {
        System.out.println("CPSssaoTematica: validaMembroCP:" + r.showData());

        for (Revisor revisor : this.m_listaRevisor) {
            if (revisor.equals(r)) {
                return false;
            }
        }

        return true;
    }

    /**
     * Adiciona o revisor passado por parametro como novo membro da comissao de
     * programa
     *
     * @param r o revisor
     * @return o resultado do registo do revisor
     */
    @Override
    public boolean addMembroCP(Revisor r) {
        if (r == null) {
            return false;
        }
        System.out.println("CPSssaoTematica: registaMembroCP: " + r.showData());

        if (r.valida() && validaMembroCP(r)) {
            return m_listaRevisor.add(r);
        } else {
            return false;
        }
    }

    /**
     * Metodo que compara esta instancia com um objecto recebido por parametro e
     * determina se são iguais ou não
     *
     * @param outroObjeto outro objecto para comparar
     * @return o resultado da operação
     */
    @Override
    public boolean equals(Object outroObjeto) {
        if (this == outroObjeto) {
            return true;
        }
        if (outroObjeto == null || this.getClass() != outroObjeto.getClass()) {
            return false;
        }

        CPSessaoTematica outraCPSessaoTematica = (CPSessaoTematica) outroObjeto;

        if (this.m_listaRevisor.size() != outraCPSessaoTematica.m_listaRevisor.size()) {
            return false;
        }

        Iterator<Revisor> it1 = this.m_listaRevisor.iterator();
        Iterator<Revisor> it2 = outraCPSessaoTematica.m_listaRevisor.iterator();

        boolean res = true;

        while (it1.hasNext() && it2.hasNext() && res) {
            Revisor r1 = it1.next();
            Revisor r2 = it2.next();
            res = r1.equals(r2);
        }

        return res;
    }

    /**
     *
     * @return Hash
     */
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 11 * hash + Objects.hashCode(this.m_listaRevisor);
        return hash;
    }

    /**
     * Valida a lista de revisores
     *
     * @return resultado da operação de validação
     */
    @Override
    public boolean valida() {
        return m_listaRevisor.size() > 0;
    }

    /**
     * Método showData.
     *
     * @return toString.
     */
    @Override
    public String showData() {
        return this.toString();
    }
}
