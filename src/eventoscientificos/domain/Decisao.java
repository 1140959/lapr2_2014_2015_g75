package eventoscientificos.domain;

import TOCS.iTOCS;
import java.util.Objects;

/**
 * Classe Decisao
 *
 * @author iazevedo
 */
public class Decisao implements iTOCS {

    private String m_strAceitacao;
    private String m_strClassificacao;
    private Submissao m_Submissao;
    private Notificacao m_Notificacao;

    /**
     * Método que altera aceitação da decisão.
     *
     * @param a Aceitação da decisão.
     */
    public void setAceitacao(String a) {
        m_strAceitacao = a;
    }

    /**
     * Método que altera a classificação.
     *
     * @param c Classificação.
     */
    public void setClassificacao(String c) {
        m_strClassificacao = c;
    }

    /**
     * Método que altera submissão.
     *
     * @param s Submissao.
     */
    public void setSubmissao(Submissao s) {
        m_Submissao = s;
    }

    /**
     * Método que notifca o Autor da submissão.
     */
    public void notifica() {
        Autor ac;
        ac = m_Submissao.getAutorCorrespondente();
        setNotificacao(new Notificacao(ac));
    }

    /**
     * Método que altera a notificação.
     *
     * @param n
     */
    private void setNotificacao(Notificacao n) {
        m_Notificacao = n;
    }

    /**
     * Método toString da classe Decisão.
     *
     * @return
     */
    @Override
    public String toString() {
        return this.m_strClassificacao + "+ ...";
    }

    /**
     * Método equals.
     *
     * @param outroObjeto objeto de comparação.
     * @return resultado do metodo equals.
     */
    @Override
    public boolean equals(Object outroObjeto) {
        if (this == outroObjeto) {
            return true;
        }
        if (outroObjeto == null || this.getClass() != outroObjeto.getClass()) {
            return false;
        }

        Decisao outraDecisao = (Decisao) outroObjeto;

        return this.m_strAceitacao.equals(outraDecisao.m_strAceitacao)
                && this.m_strClassificacao.equals(outraDecisao.m_strClassificacao)
                && this.m_Submissao.equals(outraDecisao.m_Submissao)
                && this.m_Notificacao.equals(outraDecisao.m_Notificacao);
    }

    /**
     * Método hasCode
     * 
     * @return valor inteiro do hasCode
     */
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 29 * hash + Objects.hashCode(this.m_strAceitacao);
        hash = 29 * hash + Objects.hashCode(this.m_strClassificacao);
        hash = 29 * hash + Objects.hashCode(this.m_Submissao);
        hash = 29 * hash + Objects.hashCode(this.m_Notificacao);
        return hash;
    }

    /**
     * Método showData
     * 
     * @return string
     */
    @Override
    public String showData() {
        return this.toString();
    }
}
