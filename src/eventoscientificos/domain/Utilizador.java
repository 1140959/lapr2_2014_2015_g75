package eventoscientificos.domain;

import TOCS.iTOCS;
import java.io.File;
import java.math.BigDecimal;
import java.util.Objects;
import utils.CodificacaoAritmetica;

/**
 * Classe Utilizador
 *
 * @author Nuno Silva
 */
public class Utilizador implements iTOCS {

    private String m_strNome;
    private String m_strUsername;
    private String m_strPassword;
    private String m_strEmail;

    public Utilizador() {
    }

    /**
     * Construtor de Utilizador
     *
     * @param strNome nome do utilizador
     * @param strUsername username do utilizador
     * @param strPwd password do utilizador
     * @param strEmail email do utilizador
     */
    public Utilizador(String strNome, String strUsername, String strPwd, String strEmail) {
        setEmail(strEmail);
        setNome(strNome);
        setPassword(strPwd);
        setUsername(strUsername);
    }

    public String getNome() {
        return this.m_strNome;
    }

    public String getPwd() {
        File ficheiro = new File("tabela.csv");
        String[] pos = m_strPassword.split(";");
        int tabela = Integer.parseInt(pos[1]);
        CodificacaoAritmetica desc = new CodificacaoAritmetica(ficheiro, tabela);
        return desc.descodifica(new BigDecimal(pos[3]));
    }

    public String getPwdCodificada() {
        return this.m_strPassword;
    }

    public String getUsername() {
        return this.m_strUsername;
    }

    public String getEmail() {
        return this.m_strEmail;
    }

    public void setNome(String strNome) {
        this.m_strNome = strNome;
    }

    public void setUsername(String strUsername) {
        m_strUsername = strUsername;
    }

    public void setPassword(String strPassword) {
        File ficheiro = new File("tabela.csv");
        int tabelaCodificacao = 1 + (int) (Math.random() * ((4 - 1) + 1));
        CodificacaoAritmetica cod = new CodificacaoAritmetica(ficheiro, tabelaCodificacao);
        String pass = cod.codifica(strPassword).toString();
        int qntDigits = pass.replaceAll("\\.", "").length();
        m_strPassword = "CA;" + tabelaCodificacao + ";" + qntDigits + ";" + pass;
    }

    public void setPasswordCodificada(String strPassword) {
        this.m_strPassword = strPassword;
    }

    public void setEmail(String strEmail) {
        this.m_strEmail = strEmail;
    }

    public boolean valida() {
        boolean valida = true;

        if ((m_strNome.trim().isEmpty()) || (m_strPassword.trim().isEmpty())
                || (m_strUsername.trim().isEmpty()) || (m_strEmail.trim().isEmpty())) {
            valida = false;
        }

        if ((!validaEmail(m_strEmail))) {
            valida = false;
        }

        return valida;
    }

    @Override
    public String toString() {
        String str = "Utilizador:\n";
        str += "\t Nome: " + this.m_strNome + "\n";
        str += "\t Username: " + this.m_strUsername + "\n";
        str += "\t Password: " + this.getPwd() + "\n";
        str += "\t Pass Codificada: " + getPwdCodificada() + "\n";
        str += "\t Email: " + this.m_strEmail + "\n";

        return str;
    }

    /**
     * Método sem parâmetros que devolve uma cópia do Utilizador instanciado
     * através dos métodos get dos seguintes atributos: nome, username, passowrd
     * e email.
     *
     * @return Objeto do tipo Utilizador, sendo esse Utilizador retornado, uma
     * cópia do objeto instanciado.
     */
    @Override
    public Utilizador clone() {
        return new Utilizador(this.getNome(), this.getUsername(), this.getPwd(), this.getEmail());
    }

    /**
     * @return boolean, sendo false quando não passa o email não é correto e
     * true quando passa em todos os parametros
     * @param email
     *
     */
    private boolean validaEmail(String email) {
        boolean valida = true;

        email = email.trim();

        if (email == null || email.equals("")) {
            valida = false;
        }

        if (!email.matches("^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,6}$")) {
            valida = false;

        }

        return valida;
    }

    @Override
    public boolean equals(Object outroObjeto) {
        if (this == outroObjeto) {
            return true;
        }
        if (outroObjeto == null || this.getClass() != outroObjeto.getClass()) {
            return false;
        }

        Utilizador outroUtilizador = (Utilizador) outroObjeto;

        return this.m_strNome.equals(outroUtilizador.m_strNome)
                && this.m_strUsername.equals(outroUtilizador.m_strUsername)
                && this.m_strPassword.equals(outroUtilizador.m_strPassword)
                && this.m_strEmail.equals(outroUtilizador.m_strEmail);
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 11 * hash + Objects.hashCode(this.m_strNome);
        hash = 11 * hash + Objects.hashCode(this.m_strUsername);
        hash = 11 * hash + Objects.hashCode(this.m_strPassword);
        hash = 11 * hash + Objects.hashCode(this.m_strEmail);
        return hash;
    }

    @Override
    public String showData() {
        return this.toString();
    }
}
