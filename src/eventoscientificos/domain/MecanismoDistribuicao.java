package eventoscientificos.domain;

import TOCS.iTOCS;
import java.util.ArrayList;
import java.util.List;

/**
 * Classe MecanismoDistribuição
 *
 * @author 1081019
 */
public class MecanismoDistribuicao implements ProcessoDistribuicao, iTOCS {

    private Artigo artigo;
    private List<Revisor> revisores;
/**
 * Construtor de Mecanismo Distribuição
 * @param a Artigo
 * @param r Revisor
 */
    public MecanismoDistribuicao(Artigo a, List<Revisor> r) {
        this.artigo = a;
        this.revisores = r;
    }

    @Override
    public String getId() {
        return "Mecanismo";
    }

    @Override
    public boolean Valido() {
        int contador = 0;

        List<Revisor> listaRevisoresValidos = (List<Revisor>) getListaDeRevisoes();

        for (Revisor revisor : listaRevisoresValidos) {
            contador++;
        }

        return contador >= 3;
    }

    /**
     * Metodo que retorna ListaDeRevisoes
     *
     * @return lista de Revisoes
     */
    @Override
    public List<Revisor> getListaDeRevisoes() {
        List<Revisor> resultado = new ArrayList<>();

        for (Revisor revisor : revisores) {
            if (TemAfinidadeComTopicosArtigo(artigo, revisor)) {
                resultado.add(revisor);
            }
        }

        return resultado;
    }

    /**
     * Metodo que verifica a afinidade de um revisor com os topicos de um artigo
     *
     * @return resultado da verificação
     */
    private static boolean TemAfinidadeComTopicosArtigo(Artigo artigo, Revisor revisor) {
        for (Topico topico : revisor.getListaTopicos()) {
            if (artigo.getListaTopicos().contains(topico)) {
                return true;
            }
        }

        return false;
    }

    @Override
    public String showData() {
        return this.toString();
    }

}
