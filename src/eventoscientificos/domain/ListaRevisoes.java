package eventoscientificos.domain;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

/**
 * Classe Lista de Revisões
 * @author iazevedo
 */
public class ListaRevisoes {

    List<Revisao> m_lstRevisoes;

    public ListaRevisoes() {
        m_lstRevisoes = new ArrayList<>();
    }

    public List<Revisao> getRevisoesRevisor(String id) {
       m_lstRevisoes.add(new Revisao());
        return m_lstRevisoes;
//        }
//        return null;
    }

    public boolean addRevisao(Revisao r) {
        if (valida(r)) {
            m_lstRevisoes.add(r);
            return true;
        }
        return false;
    }

    public boolean isRevisoesConcluidas() {
        for (Revisao r : m_lstRevisoes) {
            r.isConcluida();
            return true;
        }
        return false;
    }

    public boolean valida(Revisao m_revisao) {
        return m_revisao.valida();
    }

    @Override
    public boolean equals(Object outroObjeto) {
        if (this == outroObjeto) {
            return true;
        }
        if (outroObjeto == null || this.getClass() != outroObjeto.getClass()) {
            return false;
        }

        ListaRevisoes outraListaRevisoes = (ListaRevisoes) outroObjeto;

        if (this.m_lstRevisoes.size() != outraListaRevisoes.m_lstRevisoes.size()) {
            return false;
        }

        Iterator<Revisao> it1 = this.m_lstRevisoes.iterator();
        Iterator<Revisao> it2 = outraListaRevisoes.m_lstRevisoes.iterator();

        boolean res = true;

        while (it1.hasNext() && it2.hasNext() && res) {
            Revisao r1 = it1.next();
            Revisao r2 = it2.next();
            res = r1.equals(r2);
        }

        return res;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 89 * hash + Objects.hashCode(this.m_lstRevisoes);
        return hash;
    }

}
