package eventoscientificos.domain;

import java.util.List;

/**
 * Interface Processo Decisao
 * @author iazevedo
 */
public interface ProcessoDecisao {

    void setMecanismoDecisao(MecanismoDecisao m);

    void decide();

    void setListDecisoes(List<Decisao> ld);

    Decisao novaDecisao();

    boolean notifica();

    List<Decisao> getDecisoes();

}
