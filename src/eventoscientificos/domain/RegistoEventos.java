package eventoscientificos.domain;

import java.util.ArrayList;
import java.util.List;

/**
 * Classe Registo Eventos
 * @author Paulo Maio
 */
public class RegistoEventos {

    private List<Evento> m_listaEventos;

    public RegistoEventos() {
        m_listaEventos = new ArrayList<Evento>();
    }

    public List<Evento> getListaEventos() {
        return this.m_listaEventos;
    }

    public Evento novoEvento() {
        return new Evento();
    }

    public boolean registaEvento(Evento e) {
        if (e.valida() && validaEvento(e)) {
            return addEvento(e);
        } else {
            return false;
        }
    }

    private boolean addEvento(Evento e) {
        e.setStateRegistado();
        return m_listaEventos.add(e);
    }

    private boolean validaEvento(Evento e) {
        System.out.println("RegistoEventos: validaEvento:" + e.showData());
        return true;
    }

    public List<Decidivel> getDecisiveis(String strID) {
        List<Decidivel> ld = new ArrayList<>();
        for (Evento e : this.m_listaEventos) {
            ListaOrganizadores lorg = e.getListaOrganizadores();
            if (e.isInEmDecisao() && lorg.hasOrganizador(strID)) {
                ld.add(e);
            }

            ld.addAll(e.getListaDeSessoesTematicas().getDecisiveis(strID));
        }
        return ld;
    }
    public List<Decidivel> getDecisiveisEmDecidido(String strID) {
        List<Decidivel> ld = new ArrayList<>();
        for (Evento e : this.m_listaEventos) {
            ListaOrganizadores lorg = e.getListaOrganizadores();
            if (e.isInEmDecidido()&& lorg.hasOrganizador(strID)) {
                ld.add(e);
            }

            ld.addAll(e.getListaDeSessoesTematicas().getDecisiveis(strID));
        }
        return ld;
    }

    public List<CPDefinivel> getListaCPDefiniveisEmDefinicaoDoUtilizador(String strID) {
        List<CPDefinivel> ls = new ArrayList<CPDefinivel>();
        for (Evento e : this.m_listaEventos) {
            ListaOrganizadores lorg = e.getListaOrganizadores();
            if (e.isInSTDefinidas() && lorg.hasOrganizador(strID)) {
                ls.add(e);
            }

            ls.addAll(e.getListaDeSessoesTematicas().getListaCPDefiniveisEmDefinicaoDoUtilizador(strID));
        }
        return ls;
    }

    public List<Evento> getListaEventosRegistadosDoUtilizador(String strID) {
        List<Evento> ls = new ArrayList<Evento>();
        for (Evento e : this.m_listaEventos) {
            ListaOrganizadores lorg = e.getListaOrganizadores();
            if ((e.isInRegistado() || e.isInSTDefinidas()) && lorg.hasOrganizador(strID)) {
                ls.add(e);
            }
        }
        return ls;

    }
    
    public List<Evento> getListaEventosDoUtilizador(String strID) {
        List<Evento> ls = new ArrayList<>();
        for (Evento e : this.m_listaEventos) {
            ListaOrganizadores lorg = e.getListaOrganizadores();
            if (lorg.hasOrganizador(strID)) {
                ls.add(e);
            }
        }
        return ls;

    }
    

    public List<Submissivel> getListaSubmissiveisEmSubmissao() {
        List<Submissivel> ls = new ArrayList<Submissivel>();
        for (Evento e : this.m_listaEventos) {
            if (e.isInEmSubmissao()) {
                ls.add(e);
            }

            ls.addAll(e.getListaDeSessoesTematicas().getListaSubmissiveisEmSubmissao());
        }
        return ls;
    }

    public List<Submissao> getListaSubmissaoEmAceite(String email) {
        List<Submissao> ls = new ArrayList<>();

        for (Submissao s : getListaSubmissaoUtilizador(email)) {
            if (s.isInAceite()) {
                ls.add(s);
            }
        }

        return ls;
    }

    public List<Evento> getListaEventosEmSubmissao() {
        List<Evento> ls = new ArrayList<>();
        for (Evento e : this.m_listaEventos) {
            if (e.isInEmSubmissao()) {
                ls.add(e);
            }
        }
        return ls;
    }

    public List<SessaoTematica> getListaSessaoEmSubmissao(Evento evento) {
        return evento.getListaDeSessoesTematicas().getListaSessaoEmSubmissao();
    }
    

    public List<Revisavel> getRevisaveisEmRevisaoDoRevisor(String strID) {
        List<Revisavel> lr = new ArrayList<>();
        for (Evento e : this.m_listaEventos) {

            if (e.isInEmRevisao() && e.hasRevisor(strID)) {
                lr.add(e);
            }

            lr.addAll(e.getListaDeSessoesTematicas().getRevisaveisEmRevisaoDoRevisor(strID));
        }
        return lr;
    }

    /**
     *
     * @param strID String ID
     * @return Lista de Evento
     */
    public List<Evento> getListaEventoCPDEmDefinicaoDoUtilizadorEvento(String strID) {
        List<Evento> ls = new ArrayList<Evento>();
        for (Evento e : this.m_listaEventos) {
            ListaOrganizadores lorg = e.getListaOrganizadores();

            if (e.isInSTDefinidas() && lorg.hasOrganizador(strID)) {
                ls.add(e);
            }
        }

        return ls;
    }

    /**
     *
     * @param strID String ID
     * @return Lista de SessaoTematica
     */
    public List<SessaoTematica> getListaSessaoRegistadosDoUtilizador(String strID) {
        List<SessaoTematica> ls = new ArrayList<SessaoTematica>();
        for (Evento e : this.m_listaEventos) {
            ls.addAll(e.getListaDeSessoesTematicas().getListaSessaoCPDefiniveisEmDefinicaoDoUtilizador(strID));
        }
        return ls;

    }

    public List<Evento> getListaEventosSubmissaoById(String id) {
        List<Evento> ls = new ArrayList<>();
        for (Evento e : this.m_listaEventos) {
//            if (e.getState().isInEmSubmissao()) {
            if (!e.getListaSubmissoes().getSubmissoesbyEmail(id).isEmpty()) {
                ls.add(e);
            }
            if (e.getListaDeSessoesTematicas().getListaEventosSubmissaoById(id)) {
                if (!ls.contains(e)) {
                    ls.add(e);
                }
            }
//            }
        }
        return ls;
    }

    public List<Submissao> getListaSubmissaoUtilizador(String id) {
        List<Submissao> ls = new ArrayList<>();
        for (Evento e : this.m_listaEventos) {
            for (SessaoTematica st : e.getListaDeSessoesTematicas().getListaDeSessaoTematica()) {
                for (Submissao ste : st.getListaSubmissaoByEmail(id)) {
                    ls.add(ste);
                }
            }
            for (Submissao se : e.getListaSubmissaoByEmail(id)) {
                ls.add(se);
            }
        }
//            ls.addAll(e.getListaSubmissoesById(id));
        return ls;
    }

    public List<Evento> getListaSubmissaoEventoDoAutor(String email) {
        List<Evento> ls = new ArrayList<>();
        for (Evento e : this.m_listaEventos) {
            if (e.contemSubmissaoEventoDoAutor(email)) {
                ls.add(e);
            }
        }
        return ls;
    }

    public List<SessaoTematica> getListaSubmissaoSessaoDoAutor(String email) {
        List<SessaoTematica> ls = new ArrayList<>();
        for (Evento e : this.m_listaEventos) {
            ls.addAll(e.getListaDeSessoesTematicas().getListaSubmissaoSessaoDoAutor(email));
        }
        return ls;
    }

    public List<Evento> getListaEventosComSubmissaoRetiradas(String id) {
        List<Evento> ls = new ArrayList<>();
        for (Evento e : this.m_listaEventos) {
            ListaOrganizadores lorg = e.getListaOrganizadores();
            if (e.contemSubmissaoRetirada() && lorg.hasOrganizador(id)) {
                ls.add(e);
            }
        }
        return ls;
    }

    public List<SessaoTematica> getListaSessaoComSubmissaoRetiradas(String m_strID) {
        List<SessaoTematica> ls = new ArrayList<>();
        for (Evento e : this.m_listaEventos) {
            ls.addAll(e.getListaDeSessoesTematicas().getListaSessaoComSubmissaoRetiradas(m_strID));
        }
        return ls;
    }

}
