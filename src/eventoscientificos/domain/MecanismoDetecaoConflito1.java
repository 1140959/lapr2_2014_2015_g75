package eventoscientificos.domain;

import TOCS.iTOCS;

/**
 * Classe Mecanismo Deteção Conflito 1
 * @author Paulo Maio 
 */
public class MecanismoDetecaoConflito1 implements MecanismoDetecaoConflito, iTOCS {

    /**
     *
     * @param pl O Processo de Licitacção
     * @param l A Licitação
     * @param tc O Tipo de Conflito
     * @return sempre "False" (é apenas uma classe "mockup")
     */
    @Override
    public boolean detetarConflito(ProcessoLicitacao pl, Licitacao l, TipoConflito tc) {
        return false;
    }

    /**
     * Pelo facto de ser uma classe "mockup", neste momento retorna apenas uma
     * String com o texto: "MecanismoDetecaoConflito1 -D sempre falso"
     *
     * @return String com o texto "MecanismoDetecaoConflito1 -D sempre falso"
     */
    @Override
    public String toString() {
        return "MecanismoDetecaoConflito1 -> sempre falso";
    }

    /**
     * Implementa o método toString da instância
     * @return A String que estiver definida no método toString da instância
     */
    @Override
    public String showData() {
        return this.toString();
    }

}
