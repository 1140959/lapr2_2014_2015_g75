package eventoscientificos.domain;

import TOCS.iTOCS;

/**
 * Classe Mecanismo Deteção Conflito 2
 * @author Paulo Maio 
 */
public class MecanismoDetecaoConflito2 implements MecanismoDetecaoConflito, iTOCS {

    /**
     * @param pl O Processo de Licitacção
     * @param l A Licitação
     * @param tc O Tipo de Conflito
     * @return sempre "True" (é apenas uma classe "mockup")
     */
    @Override
    public boolean detetarConflito(ProcessoLicitacao pl, Licitacao l, TipoConflito tc) {
        return true;
    }

    /**
     * Pelo facto de ser uma classe "mockup", neste momento retorna apenas uma
     * String com o texto: "MecanismoDetecaoConflito1 -D sempre falso"
     *
     * @return String com o texto "MecanismoDetecaoConflito2 -D sempre
     * verdadeiro"
     */
    @Override
    public String toString() {
        return "MecanismoDetecaoConflito2 -> sempre verdadeiro";
    }

    /**
     * Implementa o método toString da instância
     * @return A String que estiver definida no método toString da instância
     */
    @Override
    public String showData() {
        return this.toString();
    }
}
