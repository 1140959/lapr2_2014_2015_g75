package eventoscientificos.domain;

/**
 * Classe Administrador
 *
 * @author David
 */
public class Administrador {

    private String username;

    public Administrador() {

    }

    /**
     * Método que altera o username.
     *
     * @param username Username.
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * Método que retorna o username.
     *
     * @return Username.
     */
    public String getUsername() {
        return username;
    }

}
