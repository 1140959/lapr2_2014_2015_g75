package eventoscientificos.domain;

import java.util.List;

/**
 * Interface Submissivel
 * @author Paulo Maio
 */
public interface Submissivel {

    ListaSubmissoes getListaSubmissoes();
    
    List<Submissao> getListaSubmissaoByEmail(String email);

    boolean isInCPDefinida();

    boolean setStateEmSubmissao();

    void alteraSubmissao(Submissao su, Submissao suClone);

    void remove(Submissao su);

    void add(Submissao suClone);
    
    List<Submissao> getListaSubmissoesRetiradas();
}
