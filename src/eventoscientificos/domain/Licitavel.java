package eventoscientificos.domain;

import java.util.List;

/**
 * Interface Licitavel
 * @author iazevedo
 */
public interface Licitavel {

    ProcessoLicitacao iniciaDetecao();

    List<Revisor> getRevisores();

    List<Submissao> getSubmissoes();

    void setProcessoLicitacao(ProcessoLicitacao pl);

    ProcessoLicitacao getProcessoLicitacao();

}
