package eventoscientificos.domain;

import java.util.List;

/**
 * Classe Interface Processo Licitação
 * @author iazevedo
 */
public interface ProcessoLicitacao {

    Licitacao novaLicitação(Revisor r, Submissao s);
    
    List<Licitavel> getLicitaveisUtilizador(Utilizador utilizador);
    
    boolean registaLicitacao();

    boolean addLicitacao(Licitacao l);

    boolean valida();

}
