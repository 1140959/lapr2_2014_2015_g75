package eventoscientificos.domain;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Classe Empresa
 *
 * @author Nuno Silva
 */
public class Empresa {

    private RegistoUtilizadores m_regUtilizadores;
    private RegistoEventos m_regEventos;
    private RegistoTipoConflitos m_regTipoConflitos;
    private Timer m_timer;
    private ListaAdministradores m_listaAdministradores;
    private List<MecanismoDecisao> m_listaMecanismoDecisao;
    private List<MecanismoDetecaoConflito> m_listaMecanismoDetecao;

    /**
     * Construtor de Empresa
     */
    public Empresa() {
        m_regUtilizadores = new RegistoUtilizadores();
        m_regEventos = new RegistoEventos();
        m_regTipoConflitos = new RegistoTipoConflitos();
        m_timer = new Timer();
        m_listaAdministradores = new ListaAdministradores();
        m_listaMecanismoDecisao = new ArrayList<MecanismoDecisao>();
        m_listaMecanismoDetecao = new ArrayList<MecanismoDetecaoConflito>();

        addDefaultData();
    }

    /**
     * Mecanismos de Detecao de Conflitos
     */
    private void addDefaultData() {

        m_listaMecanismoDetecao.add(new MecanismoDetecaoConflito1());
        m_listaMecanismoDetecao.add(new MecanismoDetecaoConflito2());
    }

    /**
     * Metodo que retorna RegistoUtilizadores.
     *
     * @return RegistoUtilizadores.
     */
    public RegistoUtilizadores getRegistoUtilizadores() {
        return this.m_regUtilizadores;
    }

    /**
     * Metodo que retorna RegistoEventos.
     *
     * @return RegistoEventos.
     */
    public RegistoEventos getRegistoEventos() {
        return this.m_regEventos;
    }

    /**
     * Metodo que retorna RegistoTipoConflitos.
     *
     * @return RegistoTipoConflitos.
     */
    public RegistoTipoConflitos getRegistoTipoConflitos() {
        return this.m_regTipoConflitos;
    }

    /**
     * Metodo que retorna ListaAdministradores.
     *
     * @return ListaAdministradores.
     */
    public ListaAdministradores getListaAdministradores() {
        return m_listaAdministradores;
    }

    public void schedule(TimerTask task, Date date) {
        m_timer.schedule(task, date);

    }

    /**
     * Método que retorna lista de mecanismos de decisão.
     *
     * @return lista de mecanismos de decisão.
     */
    public List<MecanismoDecisao> getMecanismosDecisao() {
        List<MecanismoDecisao> lMec = new ArrayList<MecanismoDecisao>();

        for (ListIterator<MecanismoDecisao> it = m_listaMecanismoDecisao.listIterator(); it.hasNext();) {
            lMec.add(it.next());
        }

        return lMec;
    }

    /**
     * Método que retorna lista de Mecanismos Detecao Conflito.
     *
     * @return lista de mecanismos de Mecanismos Detecao Conflito.
     */
    public List<MecanismoDetecaoConflito> getMecanismosDetecaoConflito() {
        List<MecanismoDetecaoConflito> lMec = new ArrayList<>();

        for (ListIterator<MecanismoDetecaoConflito> it = m_listaMecanismoDetecao.listIterator(); it.hasNext();) {
            lMec.add(it.next());
        }

        return lMec;
    }

    /**
     * Método que retorna uma lista de Eventos do Organizador.
     *
     * @param strId ID
     * @return lista de Eventos do Organizador.
     */
    public List<Evento> getEventosOrganizador(String strId) {
        List<Evento> leOrganizador = new ArrayList<Evento>();

        Utilizador u = m_regUtilizadores.getUtilizadorByID(strId);

        if (u != null) {
            for (Iterator<Evento> it = m_regEventos.getListaEventos().listIterator(); it.hasNext();) {
                Evento e = it.next();
                List<Organizador> lOrg = (List<Organizador>) e.getListaOrganizadores();

                boolean bRet = false;
                for (Organizador org : lOrg) {
                    if (org.getUtilizador().equals(u)) {
                        bRet = true;
                        break;
                    }
                }
                if (bRet) {
                    leOrganizador.add(e);
                }
            }
        }
        return leOrganizador;
    }
}
