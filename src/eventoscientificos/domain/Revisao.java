package eventoscientificos.domain;

import TOCS.iTOCS;

/**
 * Classe Revisão
 * @author iazevedo
 */
public class Revisao implements iTOCS{

    private int m_Confianca;
    private String m_Justificacao;
    private int m_Adequacao;
    private int m_Originalidade;
    private int m_Qualidade;
    private int m_Recomendacao;

    public void setConfianca(int confianca) {
        m_Confianca = confianca;
    }

    /**
     * @return the m_Decisao
     */
    public int getConfianca() {
        return m_Confianca;
    }

    public void setJustificacao(String just) {
        m_Justificacao = just;
    }

    public String getJustificacao() {
        return m_Justificacao;
    }

    /**
     * @return the m_Justificacao
     */
    /**
     * @return the m_Adequacao
     */
    public int getAdequacao() {
        return m_Adequacao;
    }

    /**
     * @param Adequacao the m_Adequacao to set
     */
    public void setAdequacao(int Adequacao) {
        this.m_Adequacao = Adequacao;
    }

    /**
     * @return the m_Originalidade
     */
    public int getOriginalidade() {
        return m_Originalidade;
    }

    /**
     * @param Originalidade the m_Originalidade to set
     */
    public void setOriginalidade(int Originalidade) {
        this.m_Originalidade = Originalidade;
    }

    /**
     * @return the m_Qualidade
     */
    public int getQualidade() {
        return m_Qualidade;
    }

    /**
     * @param m_Qualidade the m_Qualidade to set
     */
    public void setM_Qualidade(int m_Qualidade) {
        this.m_Qualidade = m_Qualidade;
    }

    /**
     * @return the m_Recomendacao
     */
    public int getM_Recomendacao() {
        return m_Recomendacao;
    }

    /**
     * @param m_Recomendacao the m_Recomendacao to set
     */
    public void setM_Recomendacao(int m_Recomendacao) {
        this.m_Recomendacao = m_Recomendacao;
    }

    public boolean isConcluida() {
        if (this.getAdequacao() > 6 || this.getConfianca() > 6 || this.getJustificacao().trim().isEmpty()
                || this.getOriginalidade() > 6 || this.getQualidade() > 6 || this.getM_Recomendacao() > 3) {
            return false;
        }
        if (this.getAdequacao() <= 0 || this.getConfianca() <= 0 || this.getJustificacao().trim().isEmpty()
                || this.getOriginalidade() <= 0 || this.getQualidade() <= 0 || this.getM_Recomendacao() < -3) {
            return false;
        }
        return true;
    }
    
    public boolean valida(){
         if (this.getAdequacao() > 6 || this.getConfianca() > 6 || this.getJustificacao().trim().isEmpty()
                || this.getOriginalidade() > 6 || this.getQualidade() > 6 || this.getM_Recomendacao() > 3) {
            return false;
        }
        if (this.getAdequacao() <= 0 || this.getConfianca() <= 0 || this.getJustificacao().trim().isEmpty()
                || this.getOriginalidade() <= 0 || this.getQualidade() <= 0 || this.getM_Recomendacao() < -3) {
            return false;
        }
        return true;
    }

    @Override
    public boolean equals(Object outroObjeto) {
        if (this == outroObjeto) {
            return true;
        }
        if (outroObjeto == null || this.getClass() != outroObjeto.getClass()) {
            return false;
        }

        Revisao outraRevisao = (Revisao) outroObjeto;

        return String.valueOf(this.getAdequacao()).equals(String.valueOf(outraRevisao.getAdequacao()))
                && String.valueOf(this.getConfianca()).equals(String.valueOf(outraRevisao.getConfianca()))
                && String.valueOf(this.getJustificacao()).equals(String.valueOf(outraRevisao.getJustificacao()))
                && String.valueOf(this.getOriginalidade()).equals(String.valueOf(outraRevisao.getOriginalidade()))
                && String.valueOf(this.getM_Recomendacao()).equals(String.valueOf(outraRevisao.getM_Recomendacao()))
                && String.valueOf(this.getQualidade()).equals(String.valueOf(outraRevisao.getQualidade()));
    }

    @Override
    public String toString() {
        return "Adequação: "+getAdequacao()+
                " Confiança: "+getConfianca()+
                " Originalidade: "+getOriginalidade()+
                " Qualidade: "+getQualidade()+
                " Recomendação: "+getM_Recomendacao()+
                " Justificação: "+getJustificacao();
    }

    @Override
    public String showData() {
       return this.toString();
    }
    

}
