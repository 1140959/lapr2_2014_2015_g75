package eventoscientificos.domain;

import java.util.List;

/**
 * Interface Decidivel
 * @author iazevedo
 */
public interface Decidivel {

    ProcessoDecisao novoProcessoDecisao();

    boolean setPD(ProcessoDecisao pd);

    List<Submissao> getListaSubmissoesDecididas();
}
