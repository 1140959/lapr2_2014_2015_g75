package eventoscientificos.domain;

import java.util.Objects;

/**
 * Classe Notificação
 *
 * @author iazevedo
 */
public class Notificacao {

    private Autor m_autorCorrespondente;

    /**
     * Construtor de Notificação
     *
     * @param ac Autor
     */
    public Notificacao(Autor ac) {
        this.m_autorCorrespondente = ac;
    }

    @Override
    public boolean equals(Object outroObjeto) {
        if (this == outroObjeto) {
            return true;
        }
        if (outroObjeto == null || this.getClass() != outroObjeto.getClass()) {
            return false;
        }

        Notificacao outraNotificacao = (Notificacao) outroObjeto;

        return this.m_autorCorrespondente.equals(outraNotificacao.m_autorCorrespondente);
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 83 * hash + Objects.hashCode(this.m_autorCorrespondente);
        return hash;
    }

}
