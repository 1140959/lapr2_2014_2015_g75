package eventoscientificos.domain;

import TOCS.iTOCS;
import eventoscientificos.state.EventoState;
import eventoscientificos.state.EventoStateCriado;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Classe Evento Cientifico
 *
 * @author Nuno Silva
 *
 */
public class Evento implements CPDefinivel, Submissivel, Distribuivel, Decidivel, Licitavel, Revisavel, iTOCS {

    /**
     * o titulo do evento
     */
    private String m_strTitulo;

    /**
     * a descrição do evento
     */
    private String m_strDescricao;

    /**
     * o local do evento
     */
    private Local m_local;

    /**
     * a data de inicio do evento
     */
    private Date m_strDataInicio;

    /**
     * a data de fim do evento
     */
    private Date m_strDataFim;

    /**
     * a data de inicio de submissao do evento
     */
    private Date m_strDataInicioSubmissao;

    /**
     * a data de fim de submissao do evento
     */
    private Date m_strDataFimSubmissao;

    /**
     * a data de inicio de distribuição do evento
     */
    private Date m_strDataInicioDistribuicao;

    /**
     * a data limite de revisao do evento
     */
    private Date m_strDataLimiteRevisao;

    /**
     * a data limite de submissao do artigo final do evento
     */
    private Date m_strDataLimiteSubmissaoFinal;

    /**
     * o estado do evento
     */
    private EventoState m_state;

    /**
     * a cp do evento
     */
    private CP m_cp;

    /**
     * a lista de organizadores do evento
     */
    private ListaOrganizadores m_lstOrganizadores;

    /**
     * a lista de sessoes tematicas do evento
     */
    private ListaSessoesTematicas m_lstSessoesTematicas;

    /**
     * a lista de submissoes do evento
     */
    private ListaSubmissoes m_lstSubmissoes;

    /**
     * o processo de decisao do evento
     */
    private ProcessoDecisao m_processoDecisao;

    /**
     * o processo de licitação do evento
     */
    private ProcessoLicitacao m_processoLicitacao;

    /**
     * o processo de distribuição do evento
     */
    private ProcessoDistribuicao m_processoDistribuicao;

    /**
     * cria uma instancia de evento
     */
    public Evento() {
        m_state = new EventoStateCriado(this);
        m_local = new Local();
        m_lstOrganizadores = new ListaOrganizadores();
        m_lstSessoesTematicas = new ListaSessoesTematicas(this);
        m_lstSubmissoes = new ListaSubmissoes(this);

    }

    /**
     * devolve o estado do evento
     *
     * @return EventoState
     */
    public EventoState getState() {
        return m_state;
    }

    /**
     * altera o estado do evento
     *
     * @param state Estado do evento
     * @return do estado em booleano
     */
    public boolean setState(EventoState state) {
        this.m_state = state;
        return true;
    }

    /**
     * cria uma cp do evento
     *
     * @return do objeto cp criado
     */
    @Override
    public CP novaCP() {
        m_cp = new CPEvento();

        return m_cp;
    }

    /**
     * altera o state da CP
     *
     * @param cp estado da CP
     * @return booleano da CP se valida
     */
    @Override
    public boolean setCP(CP cp) {
        if (cp.valida()) {
            m_state.setStateCPDefinida();
            m_cp = cp;
            return true;
        }
        return false;
    }

    /**
     * Método isInRegistasdo
     * @return verifica se esta em estado registado
     */
    public boolean isInRegistado() {
        return this.m_state.isInRegistado();
    }

    /**
     * Método isInSTDefinidas
     * @return verifica se esta em estado sessao tematica definida
     */
    public boolean isInSTDefinidas() {
        return this.m_state.isInSTDefinidas();
    }

    /**
     * Método isInCPDefinida
     * @return verifica se esta em estado comissao de programas definida
     */
    @Override
    public boolean isInCPDefinida() {
        return this.m_state.isInCPDefinida();
    }

    /**
     * Método isInEmSubmissao
     * @return verifica se esta em estado em submissao
     */
    public boolean isInEmSubmissao() {
        return this.m_state.isInEmSubmissao();
    }

    /**
     * Método isInEmDetecaoConflitos
     * @return verifica se esta em estado em detecao de conflitos
     */
    public boolean isInEmDetecaoConflitos() {
        return this.m_state.isInEmDetecaoConflitos();
    }

    /**
     * Método isInEmLicitacao
     * @return verifica se esta em estado em licitacao
     */
    public boolean isInEmLicitacao() {
        return this.m_state.isInEmLicitacao();
    }

    /**
     * Método isInEmDistribuicao
     * @return verifica se esta em estado em distribuicao
     */
    public boolean isInEmDistribuicao() {
        return this.m_state.isInEmDistribuicao();
    }

    /**
     * Método isInEmRevisao
     * @return verifica se esta em estado em revisao
     */
    public boolean isInEmRevisao() {
        return this.m_state.isInEmRevisao();
    }

    /**
     * Método isInEmDecisao
     * @return verifica se esta em estado em decisao
     */
    public boolean isInEmDecisao() {
        return this.m_state.isInEmDecisao();
    }

    /**
     * Método isInEmDecidido
     * @return verifica se esta em estado decidido
     */
    public boolean isInEmDecidido() {
        return this.m_state.isInEmDecidido();
    }

    /**
     * Método setStateRegistado
     * @return muda o estado para registado
     */
    public boolean setStateRegistado() {
        return this.m_state.setStateRegistado();
    }

    /**
     * Método setStateSTDefinidas
     * @return muda o estado para sessao tematica definida
     */
    public boolean setStateSTDefinidas() {
        return this.m_state.setStateSTDefinidas();
    }

    /**
     * Método setStateCPDefinida
     * @return muda o estado para comissao de programa definida
     */
    public boolean setStateCPDefinida() {
        return this.m_state.setStateCPDefinida();
    }

    /**
     * Método setStateEmSubmissao
     * @return muda o estado para em submissao
     */
    @Override
    public boolean setStateEmSubmissao() {
        return this.m_state.setStateEmSubmissao();
    }

    /**
     * Método setStateEmDetecaoConflitos
     * @return muda o estado para em detecao de conflitos
     */
    public boolean setStateEmDetecaoConflitos() {
        return this.m_state.setStateEmDetecaoConflitos();
    }

    /**
     * Método setStateEmLicitacao
     * @return muda o estado para em licitacao
     */
    public boolean setStateEmLicitacao() {
        return this.m_state.setStateEmLicitacao();
    }

    /**
     * Método setStateEmDistribuicao
     * @return muda o estado para em distribuicao
     */
    public boolean setStateEmDistribuicao() {
        return this.m_state.setStateEmDistribuicao();
    }

    /**
     * Método setStateEmRevisao
     * @return muda o estado para em revisao
     */
    public boolean setStateEmRevisao() {
        return this.m_state.setStateEmRevisao();
    }

    /**
     * Método setStateEmDecisao
     * @return muda o estado para em decisao
     */
    public boolean setStateEmDecisao() {
        return this.m_state.setStateEmDecisao();
    }

    /**
     * Método setStateEmDecidido
     * @return muda o estado para em decidico
     */
    public boolean setStateEmDecidido() {
        return this.m_state.setStateEmDecidido();
    }

    /**
     * devolve o titulo do evento
     *
     * @return titulo do evento
     */
    public String getTitulo() {
        return this.m_strTitulo;
    }

    /**
     * devolve a data de inicio de submissao do evento
     *
     * @return data de inicio de submissao do evento
     */
    public Date getDataInicioSubmissao() {
        return this.m_strDataInicioSubmissao;
    }

    /**
     * devolve a data de fim de submissoes do evento
     *
     * @return da data de fim de submissoes do evento
     */
    public Date getDataFimSubmissao() {
        return this.m_strDataFimSubmissao;
    }

    /**
     * devolve a data de inicio de distribuição
     *
     * @return da data de inicio de distribuição
     */
    public Date getDataInicioDistribuicao() {
        return this.m_strDataInicioDistribuicao;
    }

    /**
     * devolve a data de limite de revisao do evento
     *
     * @return da data de limite de revisao do evento
     */
    public Date getDataLimiteRevisao() {
        return this.m_strDataLimiteRevisao;
    }

    /**
     * devolve a data limite de submissao do artigo final do evento
     *
     * @return da data limite de submissao do artigo final do evento
     */
    public Date getDataLimiteSubmissaoFinal() {
        return this.m_strDataLimiteSubmissaoFinal;
    }

    /**
     * devolve a lista de organizadores do evento
     *
     * @return da lista de organizadores do evento
     */
    public ListaOrganizadores getListaOrganizadores() {
        return this.m_lstOrganizadores;
    }

    /**
     * modifica o titulo do evento
     *
     * @param strTitulo titulo do evento
     */
    public void setTitulo(String strTitulo) {
        this.m_strTitulo = strTitulo;
    }

    /**
     * modifica a descrição do evento
     *
     * @param strDescricao descricao do evento
     */
    public void setDescricao(String strDescricao) {
        this.m_strDescricao = strDescricao;
    }

    /**
     * modifica a data de inicio do evento
     *
     * @param strDataInicio data de inicio do evento
     */
    public void setDataInicio(Date strDataInicio) {
        this.m_strDataInicio = strDataInicio;
    }

    /**
     * modifica a data de fim do evento
     *
     * @param strDataFim data de fim do evento
     */
    public void setDataFim(Date strDataFim) {
        this.m_strDataFim = strDataFim;
    }

    /**
     * modifica a data de inicio de submissao do evento
     *
     * @param strDataInicioSubmissao data de inicio de submissao do evento
     */
    public void setDataInicioSubmissao(Date strDataInicioSubmissao) {
        this.m_strDataInicioSubmissao = strDataInicioSubmissao;
    }

    /**
     * modifica a data de fim de submissao do evento
     *
     * @param strDataFimSubmissao data de fim de submissao do evento
     */
    public void setDataFimSubmissao(Date strDataFimSubmissao) {
        this.m_strDataFimSubmissao = strDataFimSubmissao;
    }

    /**
     * modifica a data de inicio da distribuição
     *
     * @param strDataInicioDistribuicao data de inicio da distribuição
     */
    public void setDataInicioDistribuicao(Date strDataInicioDistribuicao) {
        this.m_strDataInicioDistribuicao = strDataInicioDistribuicao;
    }

    /**
     * altera a data limite de revisao do evento
     *
     * @param m_strDataLimiteRevisao data limite de revisao do evento
     */
    public void setDataLimiteRevisao(Date m_strDataLimiteRevisao) {
        this.m_strDataLimiteRevisao = m_strDataLimiteRevisao;
    }

    /**
     * altera a data limite de submissao do artigo dinal do evento
     *
     * @param m_strDataLimiteSubmissaoFinal data limite de submissao do artigo
     * dinal do evento
     */
    public void setDataLimiteSubmissaoFinal(Date m_strDataLimiteSubmissaoFinal) {
        this.m_strDataLimiteSubmissaoFinal = m_strDataLimiteSubmissaoFinal;
    }

    /**
     * altera o local
     *
     * @param strLocal local do evento
     */
    public void setLocal(String strLocal) {
        m_local.setLocal(strLocal);
    }

    /**
     * verifica se o evento e valido
     *
     * @return booleano se evento valido
     */
    public boolean valida() {
        return this.m_state.valida();
    }

    /**
     * devolve a descrição textual do evetno
     *
     * @return toString do Evento
     */
    @Override
    public String toString() {
        String detalhes = "Titulo: " + this.m_strTitulo
                + " Descrição: " + this.m_strDescricao
                + " Local: " + this.m_local
                + " Data de Inicio: " + this.m_strDataInicio
                + " Data de Fim: " + this.m_strDataFim
                + " Data Inicio da Submissão: " + this.m_strDataInicioSubmissao
                + " Data Fim da Submissão: " + this.m_strDataFimSubmissao
                + " Data Inicio da Distribuição: " + this.m_strDataInicioDistribuicao
                + " State: " + this.m_state
                + " Organizadores " + this.m_lstOrganizadores.showData();

        return detalhes;
    }

    /**
     * Devolve o titulo do evento
     * 
     * @return o titutlo do evento
     */
    public String titulosEventos() {
        return this.m_strTitulo;
    }

    /**
     * Devolve uma lista de sessoes tematicas
     * 
     * @return uma lista de sessoes tematicas
     */
    public ListaSessoesTematicas getListaDeSessoesTematicas() {
        return this.m_lstSessoesTematicas;
    }

    /**
     * Devolve um novo processo de decisao
     * 
     * @return um novo processo de decisao
     */
    @Override
    public ProcessoDecisao novoProcessoDecisao() {
        return new ProcessoDecisaoEvento();
    }

    /**
     * Modifica o Processo de Decisao
     * 
     * @param pd novo processo de decisao
     * @return devolve o sucesso da operacao
     */
    @Override
    public boolean setPD(ProcessoDecisao pd) {
        this.m_processoDecisao = pd;
        return true;
    }

    /**
     * Devolve uma lista de submissoes
     * 
     * @return a lista de submissoes
     */
    @Override
    public ListaSubmissoes getListaSubmissoes() {
        return this.m_lstSubmissoes;
    }

    /**
     * Devolve o processo de licitacao
     * 
     * @return o processo de licitacao
     */
    @Override
    public ProcessoLicitacao getProcessoLicitacao() {
        return m_processoLicitacao;
    }

    /**
     * Método sem retorno que atribui o processo de Licitação recebido como
     * parâmetro à instância de Evento caso esse ProcessoLicitacao passe no
     * método de validação (neste momento, passa sempre pois o return do método
     * valida é sempre "true"). Coloca também o estado do Evento no estado
     * "EmLicitacao".
     *
     * @param pl O Processo de Licitação do Evento
     */
    @Override
    public void setProcessoLicitacao(ProcessoLicitacao pl) {
        if (pl.valida()) {
            m_processoLicitacao = pl;
            m_state.setStateEmLicitacao();
        }
    }

    /**
     * Devolve a lista de submissões da sessão temática
     *
     * @return A lista de submissões da sessão temática
     */
    @Override
    public List<Submissao> getSubmissoes() {
        return m_lstSubmissoes.getSubmissoes();
    }

    /**
     * Devolve uma lista de revisor
     * 
     * @return uma lista de revisor
     */
    @Override
    public List<Revisor> getRevisores() {
        return m_cp.getRevisores();
    }

    /**
     * Instancia um novo processo de Licitação e coloca o Evento no estado de
     * DetecaoConflitos
     *
     * @return O ProcessoLicitacaoEvento instanciado
     */
    @Override
    public ProcessoLicitacao iniciaDetecao() {
        m_state.setStateEmDetecaoConflitos();
        return new ProcessoLicitacaoEvento();
    }

    /**
     * Devolve um processo de distribuicao
     * 
     * @return um processo de distribuicao
     */
    @Override
    public ProcessoDistribuicao getProcessoDistribuicao() {
        return m_processoDistribuicao;
    }

    /**
     * Valida se o Evento contem uma lista de revisores
     * 
     * @param strID id do revisor
     * @return lista de organizadores
     */
    public boolean hasRevisor(String strID) {
        return m_lstOrganizadores.hasOrganizador(strID);
    }

    @Override
    public void alteraParaEmDecisao() {
        ListaRevisoes lr = (ListaRevisoes) m_processoDistribuicao.getListaDeRevisoes();
        if (lr.isRevisoesConcluidas()) {
            setEmDecisao();
        }
    }

    private boolean setEmDecisao() {
        return this.m_state.setStateEmDecisao();
    }

    public String getDescricao() {
        return this.m_strDescricao;
    }

    public Date getDataInicio() {
        return m_strDataInicio;
    }

    public Date getDataFim() {
        return m_strDataFim;
    }

    @Override
    public List<Submissao> getListaSubmissaoByEmail(String email) {
        return m_lstSubmissoes.getSubmissoesbyEmail(email);
    }

    @Override
    public void alteraSubmissao(Submissao su, Submissao suClone) {
        ListaSubmissoes lstClone = this.m_lstSubmissoes;
        lstClone.remove(su);

        lstClone.add(suClone);

        this.m_lstSubmissoes = lstClone;
    }

    @Override
    public void remove(Submissao su) {
        m_lstSubmissoes.remove(su);
    }

    @Override
    public void add(Submissao suClone) {
        m_lstSubmissoes.add(suClone);
    }

    @Override
    public String showData() {
        return this.toString();
    }

    public String getLocal() {
        return m_local.toString();
    }

    public boolean contemSubmissaoEventoDoAutor(String email) {
        return m_lstSubmissoes.contemSubmissaoDoAutor(email);
    }

    public boolean contemSubmissaoRetirada() {
        return m_lstSubmissoes.contemSubmissaoRetirada();
    }

    public List<SessaoTematica> getListaSessaoComSubmissaoRetiradas() {
        List<SessaoTematica> ls = new ArrayList<>();
        for (SessaoTematica st : this.m_lstSessoesTematicas.getListaDeSessaoTematica()) {
            if (st.contemSubmissaoRetirada()) {
                ls.add(st);
            }
        }
        return ls;
    }

    @Override
    public List<Submissao> getListaSubmissoesRetiradas() {
        return this.m_lstSubmissoes.getSubmissoesRetiradas();
    }

    public SessaoTematica getSessaoTematicaByCodigo(String codigo) {
        return this.m_lstSessoesTematicas.getSessaoTematicaByCodigo(codigo);
    }

    @Override
    public List<Submissao> getListaSubmissoesDecididas() {
        return m_lstSubmissoes.getListaSubmissoes();
    }

    
    public List<Revisao> getSubmissoeEmRevisao() {
        List<Revisao> ls = new ArrayList<>();
        for (Submissao s : m_lstSubmissoes.getListaSubmissoes()) {
            if (s.isInEmRevisao()) {
//                ls.add(new Revisao(s.getArtigo()));
            }
        }
        return ls;
    }

    @Override
    public boolean equals(Object outroObjeto) {
        if (this == outroObjeto) {
            return true;
        }
        if (outroObjeto == null || this.getClass() != outroObjeto.getClass()) {
            return false;
        }

        Evento outroEvento = (Evento) outroObjeto;

        return this.m_strTitulo.equals(outroEvento.m_strTitulo) &&
                m_strDataInicio.equals(outroEvento.m_strDataInicio) &&
                m_strDataFim.equals(outroEvento.m_strDataFim);
    }

}
