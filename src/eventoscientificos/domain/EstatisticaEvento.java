package eventoscientificos.domain;

import eventoscientificos.controllers.EstatisticaController;
import java.util.HashMap;
import java.util.List;

/**
 * Classe de estatisticas do Evento
 *
 * @author Bruno
 * @author David
 */
public class EstatisticaEvento {

    private Empresa empresa;
    private EstatisticaController controller;
    private double taxaAceitacao;


    public EstatisticaEvento(Empresa empresa) {
        this.empresa = empresa;
        this.controller = new EstatisticaController(empresa);
    }

    public double getTaxaAceitacaoEvento(Evento evento) {
        return controller.getTaxaAceitacaoEvento(evento);
    }

//    public float getTaxaAceitacaoDecidivel(List<Decidivel> listaDecidivel) {
//        float taxa = .0f;
//        int i = 1;
//        for (Decidivel d : listaDecidivel) {
//            List<Submissao> listaSubmissoes = d.getListaSubmissoesDecididas();
//            for (Submissao submissao : listaSubmissoes) {
//                //FaltaDecisao-> verificar
//                //if(artigo==aceite){
//                for (String string : submissao.getArtigo().getListaPalavrasChave()) {
//                    if (palavrasArtigosAceites.containsKey(string)) {
//                        palavrasArtigosAceites.put(string, i);
//                    }
//
//                }
//
//            }
//
//        }
//        return taxa;
//    }
}
/**
 *  /**
 * Método que calcula a taxa de aceitação de um evento
 *
 * @return taxa de aceitação de um evento
 */
//    public float calcTaxaAceitacao(Evento e) {
//        int c = 0;
//        int totalArtigos = e.getListaSubmissoes().size();
//
//        List<Submissao> listaDeSubmissoes = e.getListaSubmissoes();
//        for (Submissao s : listaDeSubmissoes) {
//            if (s.getArtigo().getRevisoes() != null) {
//                for (Revisao r : s.getArtigo().getRevisoes()) {
//                    if (r.getRecomendacao() != false) {
//                        c++;
//                    }
//                }
//            }
//        }
//        return (c / totalArtigos) * 100;
//    }
