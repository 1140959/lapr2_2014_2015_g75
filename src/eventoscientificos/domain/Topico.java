package eventoscientificos.domain;

import TOCS.iTOCS;
import java.util.ArrayList;
import java.util.List;

/**
 * Classe Topico
 * @author 1081019
 */
public class Topico implements iTOCS {

    private String m_strDescricao;

    private String m_strCodigoACM;

    private List<Topico> m_listaTopicos = new ArrayList<Topico>();

    public void setDescricao(String strDescricao) {
        this.m_strDescricao = strDescricao;
    }

    public List<Topico> getListaTopicos() {
        return m_listaTopicos;
    }

    public final String getDescricao() {
        return this.m_strDescricao;
    }

    public void setCodigoACM(String codigoACM) {
        this.m_strCodigoACM = codigoACM;
    }

    @Override
    public String toString() {
        return "\nCodigo: " + this.m_strCodigoACM + "\nDescricao: " + this.m_strDescricao;
    }

    public boolean valida() {
        return true;
    }

    @Override
    public String showData() {
        return this.toString();
    }
}
