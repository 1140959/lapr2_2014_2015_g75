package eventoscientificos.domain;

import TOCS.iTOCS;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 * Classe Artigo
 * @author Paulo Maio
 */
public class Artigo implements iTOCS {

    /**
     * titulo do Artigo
     */
    private String m_strTitulo;
    /**
     * resumo do Artigo
     */
    private String m_strResumo;
    /**
     * Lista de autores do Artigo
     */
    private ListaAutores m_listaAutores;
    /**
     * Lista de Palavras Chave do Artigo
     */
    private List<String> m_listaPalavrasChave;
    /**
     * Autor do artigo
     */
    private Autor m_autorCorrespondente;
    /**
     * ficheiro do artigo
     */
    private String m_strFicheiro;
    /**
     * utilizador que submeteu o artigo
     */
    private String m_idUtilizador;
    /**
     * data de submissao do artigo
     */
    private Date dataSubmissao;
    /**
     * topicos de avaliação do artigo
     */
    private List<Topico> m_listaTopicos;

    /**
     * Construtor vazio da classe
     */
    public Artigo() {
        m_listaAutores = new ListaAutores();
        m_listaPalavrasChave = new ArrayList<>();
        m_listaTopicos = new ArrayList<>();
    }

    /**
     * Construtor da classe que recebe varios argumentos por parametro
     *
     * @param strTitulo Titulo do artigo
     * @param strResumo Resumo do artigo
     * @param listaAutores Lista de Autores do artigo
     * @param listaPalavrasChave Lista de palavras Chave do artigo
     * @param autorCorrespondente Autor Correspondente do artigo
     * @param strFicheiro Ficheiro do artigo
     * @param idUtilizador ID do utilizador
     * @param dataSubmissao data de submissao do artigo
     */
    public Artigo(String strTitulo, String strResumo, ListaAutores listaAutores, List<String> listaPalavrasChave, Autor autorCorrespondente, String strFicheiro, String idUtilizador, Date dataSubmissao) {
        this.m_strTitulo = strTitulo;
        this.m_strResumo = strResumo;
        this.m_autorCorrespondente = autorCorrespondente;
        this.dataSubmissao = dataSubmissao;
        this.m_listaAutores = listaAutores;
        this.m_listaPalavrasChave = listaPalavrasChave;
        this.m_idUtilizador = idUtilizador;
        this.m_strFicheiro = strFicheiro;
    }

    /**
     * Metodo para alteração do titulo
     *
     * @param strTitulo titulo do artigo
     */
    public void setTitulo(String strTitulo) {
        this.m_strTitulo = strTitulo;
    }

    /**
     * Metodo para alteração do resumo
     *
     * @param strResumo resumo do artigo
     */
    public void setResumo(String strResumo) {
        this.m_strResumo = strResumo;
    }

    /**
     * Metodo para alteração da lista de palavras
     *
     * @param palavra palavra chave do artigo
     * @return sucesso da inserção
     */
    public boolean setPalavraChave(String palavra) {
        if (this.m_listaPalavrasChave.size() == 5 || this.m_listaPalavrasChave.contains(palavra)) {
            return false;
        } else {
            this.m_listaPalavrasChave.add(palavra);
            return true;
        }
    }

    /**
     * Metodo para leitura do ficheiro
     *
     * @return string getFicheiro
     */
    public String getFicheiro() {
        return m_strFicheiro;
    }

    /**
     * Metodo para leitura do titulo
     *
     * @return string getTitulo
     */
    public String getTitulo() {
        return m_strTitulo;
    }

    /**
     * Metodo para leitura do Resumo
     *
     * @return string getResumo
     */
    public String getResumo() {
        return m_strResumo;
    }

    /**
     * Metodo para leitura da lista de autores
     *
     * @return listaAutores
     */
    public ListaAutores getListaAutores() {
        return this.m_listaAutores;
    }

    /**
     * Metodo para leitura da listaPalavrasChave
     *
     * @return listaPalavrasChave
     */
    public List<String> getListaPalavrasChave() {
        return this.m_listaPalavrasChave;

    }

    /**
     * Metodo para leitura de possiveis AutoresCorrespondentees
     *
     * @return listaAutoresCorrespondentes
     */
    public List<Autor> getPossiveisAutoresCorrespondentes() {
        return this.m_listaAutores.getPossiveisAutoresCorrespondentes();
    }

    /**
     * Metodo para alterar o autorCorrespondentes
     *
     * @param autor AutorCorrespondente
     */
    public void setAutorCorrespondente(Autor autor) {
        this.m_autorCorrespondente = autor;
    }

    /**
     * Metodo para leitura do autorCorrespondente
     *
     * @return autor
     */
    public Autor getAutorCorrespondente() {
        return this.m_autorCorrespondente;
    }

    /**
     * Metodo para alteração o ficheiro
     *
     * @param strFicheiro Ficheiro do artigo
     */
    public void setFicheiro(String strFicheiro) {
        this.m_strFicheiro = strFicheiro;
    }

    /**
     * Metodo para alteração da listaAutores
     *
     * @param m_listaAutores Lista de Autores
     */
    public void setListaAutores(ListaAutores m_listaAutores) {
        this.m_listaAutores = m_listaAutores;
    }

    /**
     * Metodo para leitura do autor da submissao
     *
     * @return the idUtilizador
     */
    public String getIdUtilizador() {
        return this.m_idUtilizador;
    }

    /**
     * Metodo para alteração do autor da submissao
     *
     * @param idUtilizador the idUtilizador to set
     */
    public void setIdUtilizador(String idUtilizador) {
        this.m_idUtilizador = idUtilizador;
    }

    /**
     * Metodo para leitura da data de submissao
     *
     * @return the dataSubmissao
     */
    public Date getDataSubmissao() {
        return dataSubmissao;
    }

    /**
     * Metodo para alteração da data de submissao
     *
     * @param dataSubmissao the dataSubmissao to set
     */
    public void setDataSubmissao(Date dataSubmissao) {
        this.dataSubmissao = dataSubmissao;
    }

    /**
     * Metodo que devolve um texto
     *
     * @return string
     */
    public String getInfo() {
        return "Artigo:getInfo";
    }

    /**
     * Metodo para validação de um artigo
     *
     * @return de um boolean que valida o artigo
     */
    public boolean valida() {
        System.out.println("Artigo:valida");
        return true;
    }

    /**
     * Metodo para leitura dos topicos de avaliação
     *
     * @return Lista de Topicos
     */
    public List<Topico> getListaTopicos() {
        return m_listaTopicos;
    }

    /**
     * Metodo para alteração dos topicos de avaliação
     *
     * @param listaTopicos Lista de Topicos
     */
    public void setListaTopicos(List<Topico> listaTopicos) {
        this.m_listaTopicos.addAll(listaTopicos);
    }

    /**
     * Metodo equals
     *
     * @param outroObjeto Objeto de comparação do metodo equals
     * @return boolean do metodo equals
     */
    @Override
    public boolean equals(Object outroObjeto) {
        if (this == outroObjeto) {
            return true;
        }
        if (outroObjeto == null || this.getClass() != outroObjeto.getClass()) {
            return false;
        }

        Artigo outroArtigo = (Artigo) outroObjeto;

        return this.m_strTitulo.equals(outroArtigo.m_strTitulo)
                && this.m_strResumo.equals(outroArtigo.m_strResumo)
                && this.m_listaAutores.equals(outroArtigo.m_listaAutores)
                && this.m_autorCorrespondente.equals(outroArtigo.m_autorCorrespondente)
                && this.m_strFicheiro.equals(outroArtigo.m_strFicheiro)
                && this.m_listaPalavrasChave.equals(outroArtigo.m_listaPalavrasChave);
    }

    /**
     * Metodo hashCode
     */
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 41 * hash + Objects.hashCode(this.m_strTitulo);
        hash = 41 * hash + Objects.hashCode(this.m_strResumo);
        hash = 41 * hash + Objects.hashCode(this.m_listaAutores);
        hash = 41 * hash + Objects.hashCode(this.m_autorCorrespondente);
        hash = 41 * hash + Objects.hashCode(this.m_strFicheiro);
        return hash;
    }

    /**
     * Metodo toString
     */
    @Override
    public String toString() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
        String strRetorno = "Titulo:\n" + m_strTitulo
                + "\nResumo: " + m_strResumo
                + "\nListaAutores: " + m_listaAutores.showData()
                + "\nListaPalavrasChave: " + m_listaPalavrasChave.toString()
                + "\nAutorCorrespondente: " + m_autorCorrespondente
                + "\nFicheiro " + m_strFicheiro
                + "\nData de Submissao " + sdf.format(dataSubmissao);
        return strRetorno;
    }

    /**
     * Metodo showData da interface iTOCS
     *
     * @return the showData
     */
    @Override
    public String showData() {
        return this.toString();
    }

    public boolean contemAutorByEmail(String email) {
        return this.m_listaAutores.hasAutor(email);
    }

}
