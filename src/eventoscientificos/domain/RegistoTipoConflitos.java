package eventoscientificos.domain;

import java.util.ArrayList;
import java.util.List;

/**
 * Classe Registo Tipo de Conflitos
 * @author Paulo Maio 
 */
public class RegistoTipoConflitos {

    private List<TipoConflito> m_lstTipos;

    public RegistoTipoConflitos() {
        m_lstTipos = new ArrayList<TipoConflito>();
    }

    public TipoConflito novoTipoConflito(String strDescricao, MecanismoDetecaoConflito mecanismo) {
        return new TipoConflito(strDescricao, mecanismo);
    }

    public boolean registaTipoConflito(TipoConflito tpConflito) {
        if (tpConflito.valida() && validaTipoConflito(tpConflito)) {
            return m_lstTipos.add(tpConflito);
        }
        return false;
    }

    private boolean validaTipoConflito(TipoConflito tpConflito) {
        System.out.println("RegistoTipoConflitos: validaTipoConflito: " + tpConflito.showData());
        return true;
    }

    public List<TipoConflito> getListaTipoConflitos() {
        return this.m_lstTipos;
    }
}
