package eventoscientificos.domain;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Objects;

/**
 * Classe Processo Decisão ST
 * @author iazevedo
 */
public class ProcessoDecisaoST {

    List<Decisao> m_listaDecisoes;
    MecanismoDecisao m_mecanismoDecisao;

    public ProcessoDecisaoST() {
        m_listaDecisoes = new ArrayList<>();

    }

    public void setMecanismoDecisao(MecanismoDecisao m) {
        m_mecanismoDecisao = m;
    }

    public void decide() {
        List<Decisao> ld = m_mecanismoDecisao.decide((ProcessoDecisao) this);
    }

    public void setListDecisoes(List<Decisao> ld) {
        m_listaDecisoes = ld;
    }

    public Decisao novaDecisao() {
        return new Decisao();
    }

    public boolean notifica() {

        for (ListIterator<Decisao> it = m_listaDecisoes.listIterator(); it.hasNext();) {
            Decisao d = it.next();
            d.notifica();
        }
        return true;
    }

    public List<Decisao> getDecisoes() {
        return m_listaDecisoes;
    }

    @Override
    public boolean equals(Object outroObjeto) {
        if (this == outroObjeto) {
            return true;
        }
        if (outroObjeto == null || this.getClass() != outroObjeto.getClass()) {
            return false;
        }

        ProcessoDecisaoST outroProcessoDecisaoST = (ProcessoDecisaoST) outroObjeto;
        
        if (this.m_listaDecisoes.size() != outroProcessoDecisaoST.m_listaDecisoes.size()) {
            return false;
        }
        
        Iterator<Decisao> it1 = this.m_listaDecisoes.iterator();
        Iterator<Decisao> it2 = outroProcessoDecisaoST.m_listaDecisoes.iterator();

        boolean res = true;
        
        while (it1.hasNext() && it2.hasNext() && res) {
            Decisao d1 = it1.next();
            Decisao d2 = it2.next();
            res = d1.equals(d2);
        }
        
        return this.m_mecanismoDecisao.equals(outroProcessoDecisaoST.m_mecanismoDecisao)
                && res;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 89 * hash + Objects.hashCode(this.m_listaDecisoes);
        hash = 89 * hash + Objects.hashCode(this.m_mecanismoDecisao);
        return hash;
    }

}
