package eventoscientificos.domain;

import TOCS.iTOCS;
import java.util.ArrayList;
import java.util.List;

/**
 * Classe Registo de Utilizadores
 * @author Paulo Maio, Bruno e Ricardo
 *
 */
public class RegistoUtilizadores implements iTOCS {

    /**
     * Variável to tipo List, contentor de objetos do tipo Utilizador.
     */
    private final List<Utilizador> m_listaUtilizadores;

    /**
     * Construtor vazio da class RegistoUtilizadores. Instancia um novo objeto
     * ArrayList de Utilizadores.
     */
    public RegistoUtilizadores() {
        m_listaUtilizadores = new ArrayList<Utilizador>();
    }

    /**
     * Método de instância sem parâmetros que instancia um novo Utilizador,
     * através do construtor vazio da classe Utilizador.
     *
     * @return Novo Utilizador.
     */
    public Utilizador novoUtilizador() {
        return new Utilizador();
    }

    /**
     *
     * @param u Utilizador para adicionar à lista de Utilizadores.
     * @return "false" caso o utilizador não fique registado (não passou nos
     * métodos de validação da classe Utilizador e da classe Registo
     * Utilizadores). "true" caso, o utilizador fique registado (passa nos
     * mesmos métodos de validação)
     */
    public boolean registaUtilizador(Utilizador u) {
        if (u.valida() && validaUtilizador(u)) {
            return addUtilizador(u);
        } else {
            return false;
        }
    }

    /**
     *
     * @param u Utilizador a ser validado
     * @return "true" caso o objeto do tipo Utilizador recebido como parâmetro
     * passe nos métodos de validação: não pode ter email ou username já
     * existente na lista de utilizadores. "false" caso não passe nestes mesmos
     * métodos de validação.
     */
    private boolean validaUtilizador(Utilizador u) {

        for (int i = 0; i < m_listaUtilizadores.size(); i++) {
            if (u.getEmail().equals(m_listaUtilizadores.get(i).getEmail())) {
                return false;
            } else if (u.getUsername().equals(m_listaUtilizadores.get(i).getUsername())) {
                return false;
            }
        }
        return true;
    }

    /**
     *
     * @param u Utilizador a ser validado
     * @return "true" caso o objeto do tipo Utilizador recebido como parâmetro
     * passe nos métodos de validação: verifica que o email do utilizador
     * recebido como parâmetro não existe noutro utilizador da lista de
     * utilizadores. "false" caso não passe neste mesmo método de validação.
     */
    private boolean validaAlteraUtilizador(Utilizador u) {

        for (int i = 0; i < m_listaUtilizadores.size(); i++) {
            if (u.getEmail().equals(m_listaUtilizadores.get(i).getEmail()) && !(u.getUsername().equals(m_listaUtilizadores.get(i).getUsername()))) {
                return false;
            }

        }
        return true;
    }

    /**
     *
     * @param u Utilizador a ser adicionado à lista de utilizadores
     * @return "true" caso o utilizador seja adicionado à lista. "false" caso
     * não seja adicionado.
     */
    private boolean addUtilizador(Utilizador u) {
        return m_listaUtilizadores.add(u);
    }

    /**
     * Devolve a lista de utilizadores do Registo de Utilizadores
     *
     * @return devolve a lista de utilizadores
     */
    public List<Utilizador> getListaUtilizadores() {
        return m_listaUtilizadores;
    }

    /**
     * @param strId O ID do utilizador (username)
     * @return Utilizador correspondente ao ID recebido como parâmetro da lista
     * de utilizadores. Caso não consiga obter nenhum utilizador da listagem de
     * utilizadores através do ID, devolve "null".
     */
    public Utilizador getUtilizadorByID(String strId) {
        for (Utilizador u : this.m_listaUtilizadores) {
            if (u.getUsername().equals(strId)) {
                return u;
            }
        }
        return null;
    }

    /**
     * @param strEmail O email do utilizador
     * @return Utilizador correspondente ao email recebido como parâmetro da
     * lista de utilizadores. Caso não consiga obter nenhum utilizador da
     * listagem de utilizadores através do email, devolve "null".
     */
    public Utilizador getUtilizadorByEmail(String strEmail) {
        for (Utilizador u : this.m_listaUtilizadores) {
            if (u.getEmail().equals(strEmail)) {
                return u;
            }
        }
        return null;
    }

    /**
     * Método de instância que recebe como parâmetros dois objetos do tipo
     * Utilizador: o primeiro, o utilizador original, o segundo o utilizador
     * clone. Caso o utilizador clone passe no método "valida" da classe
     * Utilizador, instancia uma nova lista de Utilizadores contendo essa lista
     * a listagem dos utilizadores já existentes. Remove o utilizador original
     * recebido como parâmetro e adiciona o utilizador clone recebido como
     * parâmetro a essa lista. De seguida valida se esse mesmo utilizador não
     * entra em conflito com a lista. Caso passe também nessa validação,
     * substitui os dados do utlizador original pelos dados do utilizador clone.
     *
     * @param uOriginal Utilizador original
     * @param uClone Utilizador clone
     * @return "false" caso o utilizador orginal não sofra alterações. "true"
     * caso o utilizador original fique com os dados substituídos pelos dados do
     * utilizador clone.
     */
    public boolean alteraUtilizador(Utilizador uOriginal, Utilizador uClone) {
        boolean utilizadorAlterado = false;
        if (uClone.valida()) {
            List<Utilizador> lstUtilizadores = new ArrayList<Utilizador>(m_listaUtilizadores);
            lstUtilizadores.remove(uOriginal);
            lstUtilizadores.add(uClone);
            if (validaAlteraUtilizador(uClone)) {
                uOriginal.setNome(uClone.getNome());
                uOriginal.setEmail(uClone.getEmail());
                uOriginal.setUsername(uClone.getUsername());
                uOriginal.setPassword(uClone.getPwd());
                utilizadorAlterado = true;
            }
        }
        return utilizadorAlterado;
    }

    /**
     * Método não utilizado. Retorna sempre "true"
     *
     * @param lista Lista de utilizadores
     * @return "true"
     */
    private boolean validaLista(List<Utilizador> lista) {
        return true;
    }

    /**
     * Método toString da classe RegistoUtilizadores.
     *
     * @return Lista de utilizadores no formato String
     */
    @Override
    public String toString() {
        return m_listaUtilizadores.toString();
    }

    /**
     * Devolve o resultado do método toString da classe RegistoUtilizadores
     * @return String idêntica ao resultado do método toString da classe RegistoUtilizadores
     */
    @Override
    public String showData() {
        return this.toString();
    }

}
