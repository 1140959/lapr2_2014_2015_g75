package eventoscientificos.domain;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

/**
 * Classe Lista de Submissões
 * @author Paulo Maio 
 */
public class ListaSubmissoes {

    private Submissivel m_submissivel;
    private List<Submissao> m_lstSubmissoes;

    public ListaSubmissoes(Submissivel est) {
        this.m_submissivel = est;
        this.m_lstSubmissoes = new ArrayList<Submissao>();
    }

    public Submissao novaSubmissao() {
        return new Submissao();
    }

    public boolean addSubmissao(Submissao submissao) {
        if (validaSubmissao(submissao)) {
            return this.m_lstSubmissoes.add(submissao);
        } else {
            return false;
        }
    }

    public List<Submissao> getSubmissoes() {
        return m_lstSubmissoes;
    }

    public List<Submissao> getSubmissoesbyEmail(String email) {
        List<Submissao> submissoesByEmail = new ArrayList<>();
        for (Submissao sub : m_lstSubmissoes) {
            if (!sub.isInRetirada() && !sub.isInCameraReady()) {
                if(sub.contemAutor(email)) {
                    submissoesByEmail.add(sub);
                }
            }
        }
        return submissoesByEmail;
    }

    private boolean validaSubmissao(Submissao submissao) {
        System.out.println("ListaSubmissoes:validaSubmissao");
        return submissao.valida();
    }

    public boolean remove(Submissao su) {
        for (Submissao s : this.m_lstSubmissoes) {
            if (s.equals(su)) {
                m_lstSubmissoes.remove(s);
                return true;
            }
        }
        return false;
    }

    public boolean add(Submissao suClone) {
        this.m_lstSubmissoes.add(suClone);
        for (Submissao s : this.m_lstSubmissoes) {
            if (s.equals(suClone)) {
                return true;
            }
        }
        return false;
    }

    public List<Submissao> getListaSubmissoes() {
        return m_lstSubmissoes;
    }
    public List<Submissao> getListaSubmissoesDecididas(){
        return null;
    }

    @Override
    public boolean equals(Object outroObjeto) {
        if (this == outroObjeto) {
            return true;
        }
        if (outroObjeto == null || this.getClass() != outroObjeto.getClass()) {
            return false;
        }

        ListaSubmissoes outraListaSubmissoes = (ListaSubmissoes) outroObjeto;

        if (this.m_lstSubmissoes.size() != outraListaSubmissoes.m_lstSubmissoes.size()) {
            return false;
        }

        Iterator<Submissao> it1 = this.m_lstSubmissoes.iterator();
        Iterator<Submissao> it2 = outraListaSubmissoes.m_lstSubmissoes.iterator();

        boolean res = true;

        while (it1.hasNext() && it2.hasNext() && res) {
            Submissao s1 = it1.next();
            Submissao s2 = it2.next();
            res = s1.equals(s2);
        }

        return this.m_submissivel.equals(outraListaSubmissoes.m_submissivel)
                && res;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + Objects.hashCode(this.m_submissivel);
        hash = 97 * hash + Objects.hashCode(this.m_lstSubmissoes);
        return hash;
    }

    public List<Submissao> getSubmissoesRetiradas() {
        List<Submissao> ls = new ArrayList<>();

        for (Submissao submissao : this.m_lstSubmissoes) {
            if (submissao.isInRetirada()) {
                ls.add(submissao);
            }
        }

        return ls;
    }
    
    public boolean contemSubmissaoDoAutor(String email) {
        for (Submissao submissao : m_lstSubmissoes) {
            if((!submissao.isInRetirada() && !submissao.isInCameraReady())) {
                if (submissao.contemAutor(email)) {
                    return true;
                }
            }
        }
        return false;
    }
    
    public boolean contemSubmissaoRetirada() {
        for (Submissao submissao : this.getSubmissoes()) {
            if (submissao.isInRetirada()) {
                return true;
            }
        }
        return false;
    }
}
