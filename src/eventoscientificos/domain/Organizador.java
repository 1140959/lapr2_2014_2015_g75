package eventoscientificos.domain;

import TOCS.iTOCS;

/**
 * Classe Organizador
 *
 * @author Nuno Silva
 */
public class Organizador implements iTOCS {

    private final String m_strNome;
    private Utilizador m_utilizador;

    /**
     * Construtor de Organizador
     *
     * @param u Utilizador
     */
    public Organizador(Utilizador u) {
        m_strNome = u.getNome();
        this.setUtilizador(u);
    }

    /**
     * Método que retorna o nome.
     *
     * @return nome
     */
    public String getNome() {
        return this.m_strNome;
    }

    private void setUtilizador(Utilizador u) {
        m_utilizador = u;
    }

    /**
     * Método que retorna o Utilizador.
     *
     * @return Utilizador
     */
    public Utilizador getUtilizador() {
        return this.m_utilizador;
    }

    public boolean valida() {
        System.out.println("Organizador:valida: " + this.showData());
        return true;
    }

    /**
     * Metodo toString de Organizador.
     *
     * @return toString
     */
    @Override
    public String toString() {
        return m_utilizador.showData();
    }

    @Override
    public String showData() {
        return this.toString();
    }

}
