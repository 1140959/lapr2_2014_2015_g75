package eventoscientificos.domain;

import java.util.TimerTask;

/**
 * Classe AlterarStateParaEmSubmissao
 * @author Paulo Maio 
 */
public class AlterarStateParaEmSubmissao extends TimerTask {

    private Submissivel m_e_st;

    public AlterarStateParaEmSubmissao(Submissivel e_st) {
        this.m_e_st = e_st;
    }

    @Override
    public void run() {
        if (m_e_st.isInCPDefinida()) {
            m_e_st.setStateEmSubmissao();
        }
    }

}
