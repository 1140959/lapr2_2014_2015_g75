package eventoscientificos.domain;

import TOCS.iTOCS;

/**
 * Classe Tipo de Conflito
 * @author Paulo Maio 
 */
public class TipoConflito implements iTOCS {

    /**
     * Variável de instância que contém a descrição do tipo de conflito
     */
    private String m_strDescricao;

    /**
     * Variável de instância que contém um objeto do tipo
     * MecanismoDetecaoConflito (o mecanismo que irá detetar este tipo de
     * conflito)
     */
    private MecanismoDetecaoConflito m_mecanismo;

    /**
     * Construtor vazio. Apenas instancia um objeto da classe sem parâmetros
     * definidos
     */
    public TipoConflito() {
    }

    /**
     * Construtor com parâmetros: descrição e mecanismo. Atribui os mesmos às
     * variáveis de instância.
     *
     * @param strDescricao A descrição do tipo de conflito
     * @param mecanismo O mecanismo a aplicar na instância
     */
    public TipoConflito(String strDescricao, MecanismoDetecaoConflito mecanismo) {
        setDescricao(strDescricao);
        setMecanismoDetecaoConflito(mecanismo);
    }

    /**
     * Método de instância sem parâmetros que devolve a descrição do tipo de
     * conflito
     *
     * @return Descrição do tipo de conflito
     */
    public String getDescricao() {
        return m_strDescricao;
    }

    /**
     * Método de instância que, recebendo como parâmetro uma String, atribui a
     * mesma à variável "m_strDescricao"
     *
     * @param strDescricao Descrição do tipo de conflito
     */
    public void setDescricao(String strDescricao) {
        this.m_strDescricao = strDescricao;
    }

    /**
     * Método de instância sem parâmetros que retorna o mecanismo de deteção
     * atribuído à variável "m_mecanismo"
     *
     * @return O mecanismo atribuído à variável "m_mecanismo"
     */
    public MecanismoDetecaoConflito getMecanismoDetecaoConflito() {
        return m_mecanismo;
    }

    /**
     * Método de instância que, recebendo como parâmetro um objeto do tipo
     * "MecanismoDetecaoConflito", o atribui à variável "m_mecanismo"
     *
     * @param m_mecanismo O mecanismo que irá detetar este tipo de conflito
     */
    public void setMecanismoDetecaoConflito(MecanismoDetecaoConflito m_mecanismo) {
        this.m_mecanismo = m_mecanismo;
    }

    /**
     * Método de instância que valida o tipo de conflito (não implementado -
     * retorna sempre "True")
     *
     * @return Não implementado. Retorna sempre "True"
     */
    public boolean valida() {
        return true;
    }

    /**
     * Método toString. Devolve numa String os valores da instância no seguinte
     * formato: Descrição - Mecanismo de Deteção de conflito
     *
     * @return toString do tipo de conflito
     */
    @Override
    public String toString() {
        return getDescricao() + " - " + getMecanismoDetecaoConflito().toString();
    }

    @Override
    public String showData() {
        return this.toString();
    }

}
