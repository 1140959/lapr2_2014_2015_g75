package eventoscientificos.domain;

import TOCS.iTOCS;
import java.util.Objects;

/**
 * Classe Proponente
 * @author Paulo Maio 
 */
public class Proponente implements iTOCS {

    /**
     * nome do proponente
     */
    private final String m_strNome;
    /**
     * dados do utilizador que vai ser proponente
     */
    private Utilizador m_utilizador;

    /**
     * Construtor da classe que recebe um utilizador por parametro
     *
     * @param u Proponente
     */
    public Proponente(Utilizador u) {
        m_strNome = u.getNome();
        this.setUtilizador(u);
    }

    /**
     * metodo para leitura do nome do Proponente
     *
     * @return String
     */
    public String getNome() {
        return m_strNome;
    }

    /**
     * Metodo para alteração do utilizador
     *
     * @param u
     */
    private void setUtilizador(Utilizador u) {
        m_utilizador = u;
    }

    /**
     * Metodo para leitura do utilizador
     *
     * @return Utilizador
     */
    public Utilizador getUtilizador() {
        return this.m_utilizador;
    }

    /**
     * Metodo para validar um proponente
     *
     * @return boolean
     */
    public boolean valida() {
        System.out.println("Proponente:valida: " + this.toString());
        return true;
    }

    /**
     * Metodo toString
     *
     * @return toString
     */
    @Override
    public String toString() {
        return m_utilizador.showData();
    }

    /**
     * Metodo equals
     *
     * @param outroObjeto outroObjeto objeto de comparaçao do metodo equals
     * @return boolean de comparaçao do metodo equals
     */
    @Override
    public boolean equals(Object outroObjeto) {
        if (this == outroObjeto) {
            return true;
        }
        if (outroObjeto == null || this.getClass() != outroObjeto.getClass()) {
            return false;
        }

        Proponente outroProponente = (Proponente) outroObjeto;

        return this.m_strNome.equals(outroProponente.m_strNome)
                && this.m_utilizador.equals(outroProponente.m_utilizador);
    }

    /**
     * Metodo hasCode
     *
     * @return int
     */
    @Override
    public int hashCode() {
        int hash = 3;
        hash = 53 * hash + Objects.hashCode(this.m_strNome);
        hash = 53 * hash + Objects.hashCode(this.m_utilizador);
        return hash;
    }

    /**
     * Metodo que implementa a interface iTOCS
     *
     * @return String
     */
    @Override
    public String showData() {
        return this.toString();
    }
}
