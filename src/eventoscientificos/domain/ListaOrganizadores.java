package eventoscientificos.domain;

import TOCS.iTOCS;
import java.util.ArrayList;
import java.util.List;

/**
 * Classe Lista de Organizadores
 * @author Paulo Maio 
 */
public class ListaOrganizadores implements iTOCS {

    private List<Organizador> m_listaOrganizadores;

    public ListaOrganizadores() {
        m_listaOrganizadores = new ArrayList<Organizador>();
    }

    public List<Organizador> getListaOrganizadores() {
        return this.m_listaOrganizadores;
    }

    public boolean addOrganizador(Utilizador u) {
        if (u == null) {
            return false;
        }
        System.out.println("ListaOrganizadores: addOrganizador: " + u.showData());
        Organizador o = new Organizador(u);

        if (o.valida() && validaOrganizador(o)) {
            return addOrganizador(o);
        } else {
            return false;
        }
    }

    private boolean addOrganizador(Organizador o) {
        return m_listaOrganizadores.add(o);
    }

    private boolean validaOrganizador(Organizador o) {
        System.out.println("ListaOrganizadores: validaOrganizadore:" + o.showData());
        return true;
    }

    public boolean hasOrganizador(String strID) {
        for (Organizador org : this.m_listaOrganizadores) {
            if (org.getUtilizador().getUsername().equals(strID)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public String toString() {
        String retorno = "";
        for (Organizador org : m_listaOrganizadores) {
            retorno += " " + org.getUtilizador().getUsername();
        }
        return retorno;
    }

    @Override
    public String showData() {
        return this.toString();
    }
    public boolean equals(Object u){
        if(!(u instanceof ListaOrganizadores))
            return false;
        
        ListaOrganizadores lo = (ListaOrganizadores)u;
        return true;
    }

}
