package eventoscientificos.domain;

import java.util.ArrayList;
import java.util.List;

/**
 * Classe Lista de Administradores
 *
 * @author David
 */
public class ListaAdministradores {

    /**
     * A lista de administrador da ListaAdministradores
     */
    private List<Administrador> listaAdministradores;

    public ListaAdministradores() {
        listaAdministradores = new ArrayList<>();
    }

    /**
     * Método que retorna uma novo Administrador.
     *
     * @param id ID
     * @return novo administrador.
     */
    public Administrador novoAdministrador(String id) {
        Administrador admin = new Administrador();
        admin.setUsername(id);
        return admin;
    }

    /**
     * Método que adiciona um administrador à lista de Administradores.
     *
     * @param admin Administrador.
     * @return lista de administrador.
     */
    public boolean addAdministrador(Administrador admin) {
        return listaAdministradores.add(admin);
    }

    /**
     * Método isAdministrador
     * @param id ID
     * @return resultado isAdministrador.
     */
    public boolean isAdministrador(String id) {
        boolean contem = false;
        for (Administrador admin : listaAdministradores) {
            if (admin.getUsername().equals(id)) {
                contem = true;
            }
        }
        return contem;
    }

}
