package eventoscientificos.domain;

import java.util.List;

/**
 * Interface Comissão Programa
 * @author Nuno Silva
 */
public interface CP {

    public Revisor novoMembroCP(Utilizador u);

    public boolean addMembroCP(Revisor r);

    public List<Revisor> getRevisores();
    
    public boolean valida();
}
