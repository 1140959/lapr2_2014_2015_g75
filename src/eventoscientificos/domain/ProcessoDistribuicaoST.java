package eventoscientificos.domain;

import java.util.List;
import java.util.Objects;

/**
 * Classe ProcessoDistribuicaoST
 * @author iazevedo
 */
public class ProcessoDistribuicaoST implements ProcessoDistribuicao {

    ListaRevisoes m_lstRevisoes;


    @Override
    public boolean equals(Object outroObjeto) {
        if (this == outroObjeto) {
            return true;
        }
        if (outroObjeto == null || this.getClass() != outroObjeto.getClass()) {
            return false;
        }

        ProcessoDistribuicaoST outroProcessoDistribuicaoST = (ProcessoDistribuicaoST) outroObjeto;
        
        return this.m_lstRevisoes.equals(outroProcessoDistribuicaoST.m_lstRevisoes);
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 29 * hash + Objects.hashCode(this.m_lstRevisoes);
        return hash;
    }

    @Override
    public String getId() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean Valido() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Revisor> getListaDeRevisoes() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
