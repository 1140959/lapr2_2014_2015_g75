package eventoscientificos.domain;

import TOCS.iTOCS;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Classe Revisor
 * @author Nuno Silva
 */
public class Revisor implements iTOCS {

    private String m_strNome;
    private Utilizador m_utilizador;

    private List<Topico> m_listaTopicos = new ArrayList<Topico>();

    public Revisor(Utilizador u) {
        m_strNome = u.getNome();
        m_utilizador = u;
    }

    public boolean valida() {
        System.out.println("Organizador:valida: " + this.showData());
        return true;
    }

    public String getNome() {
        return this.m_strNome;
    }

    public Utilizador getUtilizador() {
        return this.m_utilizador;
    }

    @Override
    public String toString() {
        return " " + m_strNome + " ";
    }

    @Override
    public boolean equals(Object outroObjeto) {
        if (this == outroObjeto) {
            return true;
        }
        if (outroObjeto == null || this.getClass() != outroObjeto.getClass()) {
            return false;
        }

        Revisor outroRevisor = (Revisor) outroObjeto;

        return this.m_strNome.equals(outroRevisor.m_strNome)
                && this.m_utilizador.equals(outroRevisor.m_utilizador);
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + Objects.hashCode(this.m_strNome);
        hash = 59 * hash + Objects.hashCode(this.m_utilizador);
        return hash;
    }

    public List<Topico> getListaTopicos() {
        return m_listaTopicos;
    }

    public void setListaTopicos(List<Topico> listaTopicos) {
        m_listaTopicos.addAll(listaTopicos);
    }

    @Override
    public String showData() {
        return this.toString();
    }
}
