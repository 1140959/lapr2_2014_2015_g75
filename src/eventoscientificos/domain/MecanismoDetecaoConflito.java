package eventoscientificos.domain;

/**
 * Interface Mecanimo Deteção Conflito
 * @author Paulo Maio
 */
public interface MecanismoDetecaoConflito {

    public boolean detetarConflito(ProcessoLicitacao pl, Licitacao l, TipoConflito tc);
}
