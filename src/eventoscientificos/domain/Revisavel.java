package eventoscientificos.domain;

/**
 * Interface Revisavel
 * @author iazevedo
 */
public interface Revisavel {

    public ProcessoDistribuicao getProcessoDistribuicao();

    public void alteraParaEmDecisao();

}
