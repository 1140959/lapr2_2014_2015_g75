package eventoscientificos.domain;

import java.util.ArrayList;
import java.util.List;

/**
 * Classe Comissão Programa Evento
 * @author Paulo Maio
 */
public class CPEvento implements CP {

    /**
     * A lista de revisores da CPEvento
     */
    private List<Revisor> m_listaRevisor;

    /**
     * Constrói uma instância CPEvento sem parametros
     */
    public CPEvento() {
        this.m_listaRevisor = new ArrayList<Revisor>();
    }

    /**
     * Devolve a lista de revisores da CPEvento
     *
     * @return a lista de revisores
     */
    @Override
    public List<Revisor> getRevisores() {
        return this.m_listaRevisor;
    }

    /**
     * Cria um novo membro da comissao de programa recebendo por parametro
     * o utilizador
     * 
     * @param u o utilizador que podera ser o novo membro da comissao de programa
     * @return um revisor
     */
    @Override
    public Revisor novoMembroCP(Utilizador u) {
        Revisor r = new Revisor(u);

        if (r.valida() && validaMembroCP(r)) {
            return r;
        } else {
            return null;
        }
    }

    /**
     * Valida o revisor recebido por parametro na comissao de programa
     * 
     * @param r o revisor
     * @return a validação do membro da comissao de programa
     */
    private boolean validaMembroCP(Revisor r) {
        System.out.println("CPEvento: validaMembroCP:" + r.showData());

        for (Revisor revisor : this.m_listaRevisor) {
            if (revisor.equals(r)) {
                return false;
            }
        }

        return true;
    }

    /**
     * Adiciona o revisor passado por parametro como novo membro da comissao de
     * programa
     * 
     * @param r o revisor
     * @return o resultado do registo do revisor
     */
    @Override
    public boolean addMembroCP(Revisor r) {
        if (r == null) {
            return false;
        }

        System.out.println("CPEvento: registaMembroCP: " + r.showData());

        if (r.valida() && validaMembroCP(r)) {
            return this.m_listaRevisor.add(r);
        } else {
            return false;
        }
    }

    /**
     * Valida a lista de revisores
     * 
     * @return resultado da operação de validação
     */
    @Override
    public boolean valida() {
        return m_listaRevisor.size() > 0;
    }

}
