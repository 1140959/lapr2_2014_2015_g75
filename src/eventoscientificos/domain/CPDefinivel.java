package eventoscientificos.domain;

/**
 * Interface Comissão Programa Definivel
 * @author Paulo Maio 
 */
public interface CPDefinivel {

    public CP novaCP();

    public boolean setCP(CP cp);
    
}
