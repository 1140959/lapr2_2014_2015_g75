package eventoscientificos.domain;

import TOCS.iTOCS;

/**
 * Classe Local
 * @author Nuno Silva
 */
public class Local implements iTOCS{

    /**
     * variavel String com o nome de um local
     */
    private String m_strLocal;

    /**
     * Construtor da classe sem parametros
     */
    public Local() {
    }

    /**
     * @param strLocal 
     * alteração dos valores da variavel local
     */
    
    public void setLocal(String strLocal) {
        this.m_strLocal = strLocal;
    }

    public String getM_strLocal() {
        return m_strLocal;
    }

    public boolean valida() {
        System.out.println("Local: valida:" + this.showData());
        return true;
    }

    @Override
    public String toString() {
        return this.m_strLocal;
    }

    @Override
    public String showData() {
        return this.toString();
    }

}
