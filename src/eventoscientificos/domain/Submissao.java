package eventoscientificos.domain;

import TOCS.iTOCS;
import eventoscientificos.state.SubmissaoState;
import eventoscientificos.state.SubmissaoStateCriada;
import java.util.Objects;

/**
 * Classe Submissão
 * @author Paulo Maio 
 */
public class Submissao implements iTOCS {

    /**
     * Artigo da submissao
     */
    private Artigo m_artigo;
    /**
     * ArtigoFinal da submissao
     */
    private Artigo m_artigoFinal;
    /**
     * Estado da submissao
     */
    private SubmissaoState m_state;
    /**
     * MecanismoDistribuição da submissao
     */
    private MecanismoDistribuicao m_mecanismo;

    /**
     * Construtor Classe vazio
     */
    public Submissao() {
        m_state = new SubmissaoStateCriada(this);
    }

    /**
     * Construtor da Classe que recebe como parametro um Artigo
     *
     * @param artigo Artigo
     */
    public Submissao(Artigo artigo) {
        this.m_artigo = artigo;
    }

    /**
     * Método que verifica o estado da Submissao
     *
     * @return um tipo de estado da submissao
     */
    public SubmissaoState getState() {
        return m_state;
    }

    /**
     * Metodo que devolve um artigo
     *
     * @return the getArtigo
     */
    public Artigo getArtigo() {
        return m_artigo;
    }

    /**
     * Metodo que devolve um artigoFinal
     *
     * @return the m_artigoFinal
     */
    public Artigo getArtigoFinal() {
        return m_artigoFinal;
    }

    /**
     * Metodo que devolve um Autor
     *
     * @return the getAutorCorrespondente
     */
    public Autor getAutorCorrespondente() {
        return m_artigo.getAutorCorrespondente();
    }

    /**
     * Metodo para efetuar a alteração de um Estado da uma Submissao, recebendo
     * por parametro o estado que vai ser alterado
     *
     * @param state Submissao state
     * @return the setState
     */
    public boolean setState(SubmissaoState state) {
        this.m_state = state;
        return true;
    }

    /**
     * Metodo para efetuar a alteração de uma artigo, recebendo como parametro
     * um outro artigo
     *
     * @param artigo Artigo
     */
    public void setArtigo(Artigo artigo) {
        this.m_artigo = artigo;
    }

    /**
     * @param m_artigoFinal the m_artigoFinal to set
     * @return booleano se artigo final valido
     */
    public boolean setArtigoFinal(Artigo m_artigoFinal) {
        this.m_artigoFinal = m_artigoFinal;
        return valida();
    }

    /**
     * Método que vai instanciar um novo artigo
     *
     * @return the novoArtigo
     */
    public Artigo novoArtigo() {
        return new Artigo();
    }

    /**
     * Metodo de validação de uma submissao
     *
     * @return the valida
     */
    public boolean valida() {
        return true;
    }

    /**
     * Metodo para verificação do estado da Submissao
     *
     * @return boolean
     */
    public boolean isInRegistado() {
        return this.m_state.isInSubmetido();
    }
    
    public boolean isInRejeitada(){
        return this.m_state.isInRejeitada();
    }

    /**
     * Metodo para verificação do estado da Submissao
     *
     * @return boolean
     */
    public boolean isInEmDetecaoConflitos() {
        return this.m_state.isInEmDetecaoConflitos();
    }

    /**
     * Metodo para verificação do estado da Submissao
     *
     * @return boolean
     */
    public boolean isInEmLicitacao() {
        return this.m_state.isInEmLicitacao();
    }

    /**
     * Metodo para verificação do estado da Submissao
     *
     * @return boolean
     */
    public boolean isInEmDistribuicao() {
        return this.m_state.isInEmDistribuicao();
    }

    /**
     * Metodo para verificação do estado da Submissao
     *
     * @return boolean
     */
    public boolean isInEmRevisao() {
        return this.m_state.isInEmRevisao();
    }

    /**
     * Metodo para verificação do estado da Submissao
     *
     * @return boolean
     */
    public boolean isInEmDecisao() {
        return this.m_state.isInEmDecisao();
    }

    /**
     * Metodo para verificação do estado da Submissao
     *
     * @return boolean
     */
    public boolean isInAceite() {
        return this.m_state.isInAceite();
    }

    /**
     * Metodo para verificação do estado da Submissao
     *
     * @return boolean
     */
    public boolean isInRetirada() {
        return this.m_state.isInRetirada();
    }
    
    /**
     * Metodo para verificação do estado da Submissao
     *
     * @return boolean
     */
    public boolean isInCameraReady() {
        return this.m_state.isInCameraReady();
    }

    /**
     * Metodo para modificar o estado da Submissao
     *
     * @return boolean
     */
    public boolean setStateSubmetido() {
        return this.m_state.setStateSubmetido();
    }

    /**
     * Metodo para modificar o estado da Submissao
     *
     * @return boolean
     */
    public boolean setStateEmDetecaoConflitos() {
        return this.m_state.setStateEmDetecaoConflitos();
    }

    /**
     * Metodo para modificar o estado da Submissao
     *
     * @return boolean
     */
    public boolean setStateEmLicitacao() {
        return this.m_state.setStateEmLicitacao();
    }

    /**
     * Metodo para modificar o estado da Submissao
     *
     * @return boolean
     */
    public boolean setStateEmDistribuicao() {
        return this.m_state.setStateEmDistribuicao();
    }

    /**
     * Metodo para modificar o estado da Submissao
     *
     * @return boolean
     */
    public boolean setStateEmRevisao() {
        return this.m_state.setStateEmRevisao();
    }

    /**
     * Metodo para modificar o estado da Submissao
     *
     * @return boolean
     */
    public boolean setStateEmDecisao() {
        return this.m_state.setStateEmDecisao();
    }

    /**
     * Metodo para modificar o estado da Submissao
     *
     * @return boolean
     */
    public boolean setStateAceite() {
        return this.m_state.setStateAceite();
    }

    /**
     * Metodo para modificar o estado da Submissao
     *
     * @return boolean
     */
    public boolean setStateRetirada() {
        return this.m_state.setStateRetirada();
    }
    
    public boolean setStateRejeitada() {
        return this.m_state.setStateRejeitada();
    }
    
    /**
     * Metodo para modificar o estado da Submissao
     *
     * @return boolean
     */
    public boolean setStateCameraReady() {
        return this.m_state.setCameraReady();
    }

    /**
     * Metodo toString
     */
    @Override
    public String toString() {
        return this.getArtigo().getTitulo();

    }

    /**
     * Metodo de criação de um clone de uma Submissao
     *
     * @return the clone
     */
    @Override
    public Submissao clone() {
        return new Submissao(this.getArtigo());
    }

    /**
     * Metodo de leitura do Mecanismo de Distribuição
     *
     * @return the m_mecanismo
     */
    public MecanismoDistribuicao getMecanismo() {
        return m_mecanismo;
    }

    /**
     * Metodo de alteração do MecanismoDistribuição recebendo outro mecanismo
     *
     * @param mecanismo the m_mecanismo to set
     */
    public void setMecanismo(MecanismoDistribuicao mecanismo) {
        this.m_mecanismo = mecanismo;
    }

    /**
     * Metodo equals
     *
     * @param outroObjeto objeto de comparaçao do metodo equals
     * @return return do metodo equals
     */
    @Override
    public boolean equals(Object outroObjeto) {
        if (this == outroObjeto) {
            return true;
        }
        if (outroObjeto == null || this.getClass() != outroObjeto.getClass()) {
            return false;
        }

        Submissao outraSubmissao = (Submissao) outroObjeto;

        return this.m_artigo.equals(outraSubmissao.m_artigo);
    }

    /**
     * Metodo hasCode
     *
     * @return the hasCode
     */
    @Override
    public int hashCode() {
        int hash = 3;
        hash = 41 * hash + Objects.hashCode(this.m_artigo);
        return hash;
    }

    /**
     * Metodo showData da interface iTOCS
     *
     * @return the showData
     */
    @Override
    public String showData() {
        return this.toString();
    }
    
    public boolean contemAutor(String email) {
        return m_artigo.getListaAutores().hasAutor(email);
    }

}
