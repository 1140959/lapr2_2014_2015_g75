package eventoscientificos.domain;

import TOCS.iTOCS;
import java.util.ArrayList;
import java.util.List;

/**
 * Classe Licitação
 * 
 * @author iazevedo
 */
public class Licitacao implements iTOCS{
    
    private String interesse;
    private List<TipoConflito> lst_conflitos;
    private Revisor revisor;
    private Submissao submissao;
    
    
    public Licitacao(){    
        lst_conflitos = new ArrayList<>();
    }

    private boolean valida() {
        return true;
    }
    
    public void setInteresse(String interesse){
        this.interesse = interesse;
    }

    public void setRevisor(Revisor r) {
        this.revisor = r;
    }

    public void setSubmissao(Submissao s) {
        this.submissao = s;
    }

    public boolean addTipoConflitoDetetado(TipoConflito tc) {
        return lst_conflitos.add(tc);
    }
    
    public void setConflitos(List<TipoConflito> ls){
        this.lst_conflitos = ls;
    }

    public String getInteresse() {
        return interesse;
    }

    public List<TipoConflito> getLst_conflitos() {
        return lst_conflitos;
    }

    public Revisor getRevisor() {
        return revisor;
    }

    public Submissao getSubmissao() {
        return submissao;
    }

    @Override
    public String toString() {
        return interesse + lst_conflitos + revisor + submissao;
    }

    @Override
    public String showData() {
        return toString();
    }

    
    
}
