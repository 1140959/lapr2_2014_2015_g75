package eventoscientificos.domain;

import java.util.List;

/**
 * Interface Mecanismo de Decisão
 * @author iazevedo
 */
public interface MecanismoDecisao {

    public List<Decisao> decide(ProcessoDecisao pd);
}
