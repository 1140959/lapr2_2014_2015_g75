package eventoscientificos.domain;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Classe Lista de Sessoes Tematicas
 * @author iazevedo
 */
public class ListaSessoesTematicas {

    private Evento m_evento;
    private List<SessaoTematica> m_listaST;

    public ListaSessoesTematicas(Evento e) {
        this.m_evento = e;
        m_listaST = new ArrayList<SessaoTematica>();
    }

    /**
     *
     * @param strID String ID
     * @return Lista de CP Definivel
     */
    public List<CPDefinivel> getListaCPDefiniveisEmDefinicaoDoUtilizador(String strID) {
        List<CPDefinivel> ls = new ArrayList<CPDefinivel>();

        for (SessaoTematica sessaoTematica : m_listaST) {
            ListaProponentes lpro = sessaoTematica.getListaProponentes();
            if (sessaoTematica.isInRegistado() && lpro.hasProponente(strID)) {
                ls.add(sessaoTematica);
            }
        }

        return ls;
    }

    /**
     *
     * @param strID String ID
     * @return lista de Sessao Tematica
     */
    public List<SessaoTematica> getListaSessaoCPDefiniveisEmDefinicaoDoUtilizador(String strID) {
        List<SessaoTematica> ls = new ArrayList<SessaoTematica>();

        for (SessaoTematica sessaoTematica : m_listaST) {
            ListaProponentes lpro = sessaoTematica.getListaProponentes();
            if (sessaoTematica.isInRegistado() && lpro.hasProponente(strID)) {
                ls.add(sessaoTematica);
            }
        }

        return ls;
    }

    public SessaoTematica novaSessaoTematica(String cod, String desc, Date dtInicioSub, Date dtFimSub, Date dtInicioDistr, Date dtLimRevisao, Date dtLimArtigoFinal) {
        return new SessaoTematica(cod, desc, dtInicioSub, dtFimSub, dtInicioDistr, dtLimRevisao, dtLimArtigoFinal);
    }

    public boolean registaSessaoTematica(SessaoTematica st) {
        if (st.valida() && validaST(st)) {
            return addST(st);
        } else {
            return false;
        }
    }

    private boolean addST(SessaoTematica st) {
        if (st.setStateRegistado()) {
            if (m_listaST.add(st)) {
                if (this.m_evento.setStateSTDefinidas()) {
                    return true;
                } else {
                    m_listaST.remove(st);
                }
            }
        }
        return false;
    }

    private boolean validaST(SessaoTematica st) {
        System.out.println("ListaSessoesTematicas: validaST:" + st.showData());
        return true;
    }

    public List<Decidivel> getDecisiveis(String strID) {
        List<Decidivel> ld = new ArrayList<>();
        return ld;
    }

    public List<Submissivel> getListaSubmissiveisEmSubmissao() {
        List<Submissivel> ls = new ArrayList<Submissivel>();
        for (SessaoTematica st : this.m_listaST) {
            if (st.isInEmSubmissao()) {
                ls.add(st);
            }
        }
        return ls;
    }

    public List<SessaoTematica> getListaSessaoEmSubmissao() {
        List<SessaoTematica> ls = new ArrayList<SessaoTematica>();
        for (SessaoTematica st : this.m_listaST) {
            if (st.isInEmSubmissao()) {
                ls.add(st);
            }
        }
        return ls;
    }

    public boolean getListaEventosSubmissaoById(String id) {
        List<SessaoTematica> ls = new ArrayList<SessaoTematica>();
        for (SessaoTematica st : this.m_listaST) {
            for (Submissao s : st.getSubmissoes()) {
                if (s.getArtigo().getIdUtilizador().equals(id)) {
                    return true;
                }
            }
        }
        return false;
    }

    public List<Revisavel> getRevisaveisEmRevisaoDoRevisor(String strID) {
        List<Revisavel> lr = new ArrayList<>();
        for (SessaoTematica st : this.m_listaST) {

            if (st.isInEmRevisao() && st.hasRevisor(strID)) {
                lr.add(st);
            }
        }
        return lr;
    }

    public List<SessaoTematica> getListaDeSessaoTematica() {
        return this.m_listaST;
    }

    public List<SessaoTematica> getListaSubmissaoSessaoDoAutor(String email) {
        List<SessaoTematica> ls = new ArrayList<>();
        for (SessaoTematica sessao : m_listaST) {
            if (sessao.contemSubmissaoSessaoDoAutor(email)) {
                ls.add(sessao);
            }
        }
        return ls;
    }

    public boolean contemSubmissaoRetirada() {
        for (SessaoTematica sessao : m_listaST) {
            for (Submissao submissao : sessao.getListaSubmissoes().getSubmissoes()) {
                if (submissao.isInRetirada()) {
                    return true;
                }
            }
        }
        return false;
    }

    public SessaoTematica getSessaoTematicaByCodigo(String codigo) {
        for (SessaoTematica sessao : m_listaST) {
            if (sessao.getCodigo().equals(codigo)) {
                return sessao;
            }
        }
        return null;
    }

    public List<SessaoTematica> getListaSessaoComSubmissaoRetiradas(String m_strID) {
        List<SessaoTematica> ls = new ArrayList<>();
        for (SessaoTematica sessao : m_listaST) {
            ListaProponentes lpro = sessao.getListaProponentes();
            if(sessao.contemSubmissaoRetirada() && lpro.hasProponente(m_strID)) {
                ls.add(sessao);
            }
        }
        return ls;
    }
    
    public List<Licitavel> getLicitacoesEmLicitacao(){
        ArrayList<Licitavel> ls = new ArrayList<>();
        return ls;
    }
    
}
