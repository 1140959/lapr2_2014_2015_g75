/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 ...
 */
package eventoscientificos.controllers;

import eventoscientificos.domain.Artigo;
import eventoscientificos.domain.Autor;
import eventoscientificos.domain.Empresa;
import eventoscientificos.domain.Evento;
import eventoscientificos.domain.RegistoEventos;
import eventoscientificos.domain.SessaoTematica;
import eventoscientificos.domain.Submissao;
import eventoscientificos.domain.Submissivel;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Classe alterar SubmissaoController
 *
 * @author nunosilva
 * @author 1140958@isep.ipp.pt
 */
public class AlterarSubmissaoController {

    private Empresa m_empresa;
    private RegistoEventos m_regEvento;
    private Submissivel m_submissivel;
    private Submissao m_submissao;
    private String m_strEmail;
    private Submissao sClone;
    private Artigo aClone;

    /**
     * Construtor da class AlterarSubmissaoController. Instancia um novo objeto
     *
     * @param empresa O objeto empresa
     * @param id o Id do utilizador
     */
    public AlterarSubmissaoController(Empresa empresa, String id) {
        m_empresa = empresa;
        m_strEmail = empresa.getRegistoUtilizadores().getUtilizadorByID(id).getEmail();
        m_regEvento = empresa.getRegistoEventos();
        m_submissao = new Submissao();
        sClone = new Submissao();
        aClone= new Artigo();
    }

    /**
     *
     * Método que vai criar uma lista de Evento em que o Autor tem artigos
     *
     * @return List de Sessoes Temáticas
     */
    public List<Evento> getListaSubmissaoEventoDoAutor() {
        return m_regEvento.getListaSubmissaoEventoDoAutor(m_strEmail);
    }

    /**
     *
     *
     * Método que vai criar uma lista de SessaoTematica em que o Autor tem
     * artigos
     *
     * @return List de Sessoes Temáticas
     */
    public List<SessaoTematica> getListaSubmissaoSessaoDoAutor() {
        return m_regEvento.getListaSubmissaoSessaoDoAutor(m_strEmail);
    }

    /**
     * Metodo que ira cria uma lista de Submissao a um Evento ou Sessao,
     * consoante o tipo de submissivel que receber como parametro
     *
     * @param submissivel O tipo de submissivel
     * @return Lista de Submissao
     */
    public List<Submissao> getListaSubmissaoDoAutor(Submissivel submissivel) {
        return submissivel.getListaSubmissaoByEmail(m_strEmail);
    }

    /**
     * Metodo que recebe uma submissao como parametro e que devolve um artigo
     *
     * @param submissao Variavel do tipo submissao
     * @return artigo
     */
    public Artigo selectSubmissao(Submissao submissao) {
        this.m_submissao = submissao;
        return m_submissao.getArtigo();
    }

    /**
     * @param autor é uma instancia de autor com todos os elementos de uma autor
     * @return false se o autor que está a ser validado não for inserido no
     * sistema
     */
    public boolean validaDadosAutor(Autor autor) {
        return autor.valida();
    }

    /**
     * @param lista A lista de Autores Correspondentes
     *
     * @return list de uma lista de Autores Correspondentes
     */
    public List<Autor> getPossiveisAutoresCorrespondentes(List<Autor> lista) {
        ArrayList<Autor> possiveis = new ArrayList<>();

        for (Autor possivel : lista) {
            if (m_empresa.getRegistoUtilizadores().getUtilizadorByEmail(possivel.getEmail()) != null) {
                possiveis.add(possivel);
            }
        }
        return possiveis;
    }

    public void setDadosTituloResumo(String strTitulo, String strResumo) {
        aClone.setTitulo(strTitulo);
        aClone.setResumo(strResumo);
    }

    public void setDadosAutorCorrespondente(Autor autorCorrespondente) {
        aClone.setAutorCorrespondente(autorCorrespondente);
    }

    public void setDadosAutores(List<Autor> listAutor) {
        aClone.getListaAutores().novoAutor();
        for (Autor a : listAutor) {
            aClone.getListaAutores().addAutor(a);
        }
    }

    public void setListaPalavras(List<String> listPalavras) {
        for (String palavra : listPalavras) {
            aClone.setPalavraChave(palavra);
        }
    }

    public void setDadosFicheiro(String strFicheiro) {
        aClone.setFicheiro(strFicheiro);
    }

    public void setDadosSubmissao(Date data, String idUser) {

        aClone.setDataSubmissao(data);
        aClone.setIdUtilizador(idUser);

        sClone.setArtigo(aClone);

    }

    public void criarClone(Submissivel est) {
        m_submissivel = est;
        System.out.println(m_submissivel);
        sClone = m_submissivel.getListaSubmissoes().novaSubmissao();
        aClone = sClone.novoArtigo();
    }

    public void alterarSubmissao(Submissivel est) {
        m_submissivel = est;
        m_submissivel.alteraSubmissao(m_submissao, sClone);
        sClone.setStateSubmetido();
    }

    /**
     * Metodo que vai devolver uma string com o titulo de um artigo
     *
     * @param artigo O objeto Artigo
     * @return String do titulo do artigo
     */
    public String getTitulo(Artigo artigo) {
        return artigo.getTitulo();
    }

    /**
     * Metodo que vai devolver uma string com o resumo de um artigo
     *
     * @param artigo O objeto artigo
     * @return String do resumo do artigo
     */
    public String getResumo(Artigo artigo) {
        return artigo.getResumo();
    }

    /**
     * Metodo que vai devolver uma string com o caminho do ficheiro de um artigo
     *
     * @param artigo O objeto artigo
     * @return String do ficheiro do artigo
     */
    public String getFicheiro(Artigo artigo) {
        return artigo.getFicheiro();
    }

    /**
     * Metodo que vai devolver uma lista de autores de um artigo
     *
     * @param artigo O objeto artigo
     * @return List autor
     */
    public List<Autor> getAutores(Artigo artigo) {
        return artigo.getListaAutores().getAutores();
    }

    /**
     * Metodo que vai devolver uma lista de palavras chave que foram colocadas
     * no artigo
     *
     * @param artigo O objeto artigo
     * @return List String de Lista de palavras chave
     */
    public List<String> getPalavrasChave(Artigo artigo) {
        return artigo.getListaPalavrasChave();
    }

    /**
     * Metodo que vai devolver uma string com o Autor correspondente de um
     * artigo
     *
     * @param artigo O objeto artigo
     * @return Autor do Autor correspondente do artigo
     */
    public Autor getAutorCorrespondente(Artigo artigo) {
        return artigo.getAutorCorrespondente();
    }
}
