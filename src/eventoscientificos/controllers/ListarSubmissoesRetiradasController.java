package eventoscientificos.controllers;

import eventoscientificos.domain.Artigo;
import eventoscientificos.domain.Empresa;
import eventoscientificos.domain.Evento;
import eventoscientificos.domain.RegistoEventos;
import eventoscientificos.domain.SessaoTematica;
import eventoscientificos.domain.Submissao;
import eventoscientificos.domain.Submissivel;
import java.util.List;

/**
 * Classe ListarSubmissoesRetiradasController
 * @author Carlos
 */
public class ListarSubmissoesRetiradasController {

    /**
     * A empresa da ListarSubmissoesRetiradasController
     */
    private Empresa m_empresa;

    /**
     * O registo de eventos da ListarSubmissoesRetiradasController
     */
    private RegistoEventos m_regEvento;

    /**
     * A submissao da ListarSubmissoesRetiradasController
     */
    private Submissao m_submissao;

    /**
     * O artigo da ListarSubmissoesRetiradasController
     */
    private Artigo m_artigo;
    
    /**
     * O id da ListarSubmissoesRetiradasController
     */
    private String m_strID;

    /**
     * Constrói uma instância de ListarSubmissoesRetiradasController recebendo a empresa
     * como parametro uma empresa e o id que pertence ao organizador ou proponente
     *
     * @param empresa a empresa do ListarSubmissoesRetiradasController
     * @param id id do organizador proponente
     */
    public ListarSubmissoesRetiradasController(Empresa empresa, String id) {
        this.m_empresa = empresa;
        this.m_regEvento = empresa.getRegistoEventos();
        this.m_strID = id;
    }

    /**
     * Devolve a lista de eventos com a submissao retiradas
     * 
     * @return a lista de eventos com a submissao retiradas
     */
    public List<Evento> getListaEventosComSubmissaoRetiradas() {
        return this.m_regEvento.getListaEventosComSubmissaoRetiradas(m_strID);
    }

    /**
     * Devolve a lista de sessao tematicas com submissao retiradas do evento
     * passado por parametro
     * 
     * @return devolve uma lista de sessao tematicas
     */
    public List<SessaoTematica> getListaSessaoComSubmissaoRetiradas() {
        return this.m_regEvento.getListaSessaoComSubmissaoRetiradas(m_strID);
    }

    /**
     * Devolve uma lista de submissão retiradas do submissivel passado por parametro
     * 
     * @param submissivel submissivel passada parado
     * @return devolve uma lista de submissao
     */
    public List<Submissao> getListaSubmissaoRetiradas(Submissivel submissivel) {
        return submissivel.getListaSubmissoesRetiradas();
    }

    /**
     * Devolve uma String com a conteudo textural da submissao selecionada
     * 
     * @param submissao submissao selecionada
     * @return conteudo textural da submissao
     */
    public String selectSubmissao(Submissao submissao) {
        this.m_submissao = submissao;
        return submissao.getArtigo().toString();
    }
}
