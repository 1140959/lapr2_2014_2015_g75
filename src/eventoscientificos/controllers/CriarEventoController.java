package eventoscientificos.controllers;

import eventoscientificos.domain.AlterarStateParaEmDistribuicao;
import eventoscientificos.domain.AlterarStateParaEmSubmissao;
import eventoscientificos.domain.Empresa;
import eventoscientificos.domain.Evento;
import eventoscientificos.domain.RegistoEventos;
import eventoscientificos.domain.RegistoUtilizadores;
import eventoscientificos.domain.Utilizador;
import java.util.Date;

/**
 * Classe criarEventoController
 *
 * @author Nuno Silva
 * @author 1140959@isep.ipp.pt
 */
public class CriarEventoController {

    /**
     * A empresa do controller
     */
    private Empresa m_empresa;

    /**
     * o registo de eventos do controller
     */
    private RegistoEventos m_regEvento;

    /**
     * o registo de utilizadores do controller
     */
    private RegistoUtilizadores m_regUtilizadores;

    /**
     * o evento do controller
     */
    private Evento m_evento;

    /**
     * Cria uma instancia CriarEventoController recebendo uma empresa
     *
     * @param empresa A empresa
     */
    public CriarEventoController(Empresa empresa) {
        m_empresa = empresa;
        m_regEvento = empresa.getRegistoEventos();
        m_regUtilizadores = empresa.getRegistoUtilizadores();
    }

    /**
     * cria um novo evento no controller
     */
    public void novoEvento() {
        m_evento = m_regEvento.novoEvento();
    }

    /**
     * altera o titulo do evento do controller
     *
     * @param strTitulo O titulo do evento
     */
    public void setTitulo(String strTitulo) {
        m_evento.setTitulo(strTitulo);
    }

    /**
     * altera a descrição do evento do controller
     *
     * @param strDescricao A descricao do evento
     */
    public void setDescricao(String strDescricao) {
        m_evento.setDescricao(strDescricao);
    }

    /**
     * altera o local do evento
     *
     * @param strLocal O local do evento
     */
    public void setLocal(String strLocal) {
        m_evento.setLocal(strLocal);
    }

    /**
     * altera a data de inicio do evento
     *
     * @param strDataInicio A data de inicio do evento
     */
    public void setDataInicio(Date strDataInicio) {
        m_evento.setDataInicio(strDataInicio);
    }

    /**
     * altera a data de fim do evento
     *
     * @param strDataFim A data de fim do evento
     */
    public void setDataFim(Date strDataFim) {
        m_evento.setDataFim(strDataFim);
    }

    /**
     * altera a data de inicio de submisssao do evento
     *
     * @param strDataInicioSubmissao A data de inicio de submissao
     */
    public void setDataInicioSubmissao(Date strDataInicioSubmissao) {
        m_evento.setDataInicioSubmissao(strDataInicioSubmissao);
    }

    /**
     * altera a data de fim de submissão do evento
     *
     * @param strDataFimSubmissao A data de fim de submissao
     */
    public void setDataFimSubmissao(Date strDataFimSubmissao) {
        m_evento.setDataFimSubmissao(strDataFimSubmissao);
    }

    /**
     * altera a data de inicio de distribuição do evento
     *
     * @param strDataInicioDistribuicao A data de inicio de distribuiçao
     */
    public void setDataInicioDistribuicao(Date strDataInicioDistribuicao) {
        m_evento.setDataInicioDistribuicao(strDataInicioDistribuicao);
    }

    /**
     * altera a data limite de revisao do evento
     *
     * @param strDataLimiteRevisao a data limite de revisao
     */
    public void setDataLimiteRevisao(Date strDataLimiteRevisao) {
        m_evento.setDataLimiteRevisao(strDataLimiteRevisao);
    }

    /**
     * altera a data limite de submissao do artigo final de um evento
     *
     * @param strDataLimiteSubmissaoFinal a data limite de submissao final
     */
    public void setDataLimiteSubmissaoFinal(Date strDataLimiteSubmissaoFinal) {
        m_evento.setDataLimiteSubmissaoFinal(strDataLimiteSubmissaoFinal);
    }

    /**
     * devolve um utilizador atravez do seu id
     *
     * @param id do utilizador
     * @return utilizador
     */
    public Utilizador getUtilizadorByID(String id) {
        return m_empresa.getRegistoUtilizadores().getUtilizadorByID(id);
    }

    /**
     * adiciona um organizador ao evento
     *
     * @param strId o id do utilizador
     * @return de organizador adicionado ao evento
     */
    public boolean addOrganizador(String strId) {
        Utilizador u = m_regUtilizadores.getUtilizadorByID(strId);
        return m_evento.getListaOrganizadores().addOrganizador(u);
    }

    /**
     * devolve o evento
     *
     * @return evento
     */
    public Evento getEvento() {
        return m_evento;
    }

    /**
     * regista o evento no registo de eventos da empresa
     *
     * @return o evento registado
     */
    public Evento registaEvento() {
        if (m_regEvento.registaEvento(m_evento)) {
            createTimers();
            return m_evento;
        }
        return null;
    }

    /**
     * cria os times do evento a serem despoletados no uc14
     */
    private void createTimers() {
        AlterarStateParaEmSubmissao task1 = new AlterarStateParaEmSubmissao(m_evento);
        this.m_empresa.schedule(task1, m_evento.getDataInicioSubmissao());

        AlterarStateParaEmDistribuicao task2 = new AlterarStateParaEmDistribuicao(m_evento);
        this.m_empresa.schedule(task2, m_evento.getDataInicioDistribuicao());

        DetetarConflitosController task3 = new DetetarConflitosController(m_empresa, m_evento);
        this.m_empresa.schedule(task3, m_evento.getDataFimSubmissao());

    }
}
