package eventoscientificos.controllers;

import eventoscientificos.domain.Artigo;
import eventoscientificos.domain.Autor;
import eventoscientificos.domain.Empresa;
import eventoscientificos.domain.Evento;
import eventoscientificos.domain.Submissao;
import eventoscientificos.domain.Utilizador;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Classe SubmeterArtigoFinalController
 *
 * @author David
 */
public class SubmeterArtigoFinalController {

    private Empresa m_empresa;
    private Submissao m_submissao;
    private Artigo m_artigo;

    /**
     * Construtor de SubmeterArtigoFinalController
     *
     * @param empresa Empresa
     */
    public SubmeterArtigoFinalController(Empresa empresa) {
        this.m_empresa = empresa;
    }

    /**
     * Método que retorna uma lista de possiveis autores correspondentes.
     *
     * @param lista Lista de autores correspondentes.
     * @return lista de autores correspondentes possiveis.
     */
    public List<Autor> getPossiveisAutoresCorrespondentes(List<Autor> lista) {
        ArrayList<Autor> possiveis = new ArrayList<>();

        for (Autor possivel : lista) {

            if (this.m_empresa.getRegistoUtilizadores().getUtilizadorByEmail(possivel.getEmail()) != null) {
                possiveis.add(possivel);
            }
        }

        return possiveis;
    }

    /**
     * Método que retorna a info de um Resumo.
     *
     * @return do Resumo.
     */
    public String getInfoResumo() {
        return this.m_submissao.showData() + this.m_artigo.getInfo();
    }

    /**
     * Método que retorna uma lista de submissões aceites do autor.
     *
     * @param email Email do autor
     * @return Lista de submissões aceites do autor.
     */
    public List<Submissao> getListaSubmissoesDoAutorEmAceite(String email) {
        return m_empresa.getRegistoEventos().getListaSubmissaoEmAceite(email);
    }

    /**
     * Método que retorna uma lista de Eventos em submissão.
     *
     * @return Lista de eventos em submissão.
     */
    public List<Evento> getListaEventosEmSubmissao() {
        return this.m_empresa.getRegistoEventos().getListaEventosEmSubmissao();
    }

    /**
     * Método que altera os daos do artigo.
     *
     * @param strTitulo Titulo do artigo.
     * @param strResumo Resumo do artigo.
     */
    public void setDados(String strTitulo, String strResumo) {
        this.m_artigo.setTitulo(strTitulo);
        this.m_artigo.setResumo(strResumo);
    }

    /**
     * Método que altera o Autor Correspondente.
     *
     * @param autor Autor Correspondente.
     */
    public void setCorrespondente(Autor autor) {
        this.m_artigo.setAutorCorrespondente(autor);
    }

    /**
     * Método que altera o ficheiro do artigo.
     *
     * @param strFicheiro Ficheiro do artigo.
     */
    public void setFicheiro(String strFicheiro) {
        this.m_artigo.setFicheiro(strFicheiro);
    }

    /**
     * Método que altera as palavras chave do artigo.
     *
     * @param palavrasChave Palavras chave do artigo.
     */
    public void setPalavrasChave(List<String> palavrasChave) {
        for (String palavra : palavrasChave) {
            this.m_artigo.setPalavraChave(palavra);
        }
    }

    /**
     * Método que adiciona palavras chave ao artigo.
     *
     * @param palavraChave Palavra chave do artigo.
     */
    public void addPalavra(String palavraChave) {
        this.m_artigo.setPalavraChave(palavraChave);
    }

    /**
     * Método que permite alterar a data de submissão do artigo.
     *
     * @param data Data de submissão do artigo.
     */
    public void setData(Date data) {
        this.m_artigo.setDataSubmissao(data);
    }

    /**
     * Método que insere o utilizador da submissao.
     *
     * @param id ID do utilizador.
     */
    public void setUser(String id) {
        this.m_artigo.setIdUtilizador(id);
    }

    public void selectSubmissao(Submissao est, String id) {
        this.m_submissao = est;
        this.m_artigo = this.m_submissao.novoArtigo();
        this.m_artigo.setIdUtilizador(id);
    }

    /**
     * Método que cria um novo Autor.
     *
     * @param strNome Nome do autor.
     * @param strAfiliacao Afilicação do autor.
     * @param strEmail Email do autor.
     * @return do novo autor criado.
     */
    public Autor novoAutor(String strNome, String strAfiliacao, String strEmail) {
        return this.m_artigo.getListaAutores().novoAutor(strNome, strAfiliacao, strEmail);
    }

    /**
     * Método que permite criar um novo Autor.
     *
     * @return novo autor.
     */
    public Autor novoAutor() {
        return m_artigo.getListaAutores().novoAutor();
    }

    /**
     * Método que permite adicionar um autor ao artigo.
     *
     * @param autor o Autor.
     * @return do Autor adicionado.
     */
    public boolean addAutor(Autor autor) {
        return this.m_artigo.getListaAutores().addAutor(autor);
    }
    
    /**
     * devolve o utilizador atraves do seu id
     * 
     * @param idUtilizador da submissao
     * @return o utilizador
     */
    public Utilizador getUtilizadorByID(String idUtilizador){
        return m_empresa.getRegistoUtilizadores().getUtilizadorByID(idUtilizador);
    }

    /**
     * Método que regista a submissão de um artigo.
     *
     * @return resultado da submissão de um artigo.
     */
    public boolean registarSubmissao() {
        m_artigo.setDataSubmissao(Calendar.getInstance().getTime());
        
        if(this.m_submissao.setArtigoFinal(m_artigo)) {
            return this.m_submissao.setStateCameraReady();
        }

        return false;
    }

}
