package eventoscientificos.controllers;

import eventoscientificos.domain.Empresa;
import java.io.File;
import utils.CodificacaoAritmetica;
import utils.LerXML;

/**
 * Classe ImportarDadosNegocio
 *
 * @author Carlos
 */
public class ImportarDadosNegocio {

    private Empresa m_empresa;
    private LerXML lerXML;
    private static final String NOME_DO_FICHEIRO_UTILIZADORES = "utilizador.xml";
    private static final String NOME_DO_FICHEIRO_LOCAL = "local.xml";
    private static final String NOME_DO_FICHEIRO_EVENTO = "evento.xml";

    /**
     * Construtor de ImportarDadosNegocio
     *
     * @param empresa Empresa
     */
    public ImportarDadosNegocio(Empresa empresa) {
        this.m_empresa = empresa;
        this.lerXML = new LerXML();
    }

    /**
     * Metodo que importa dados de utilizadores, locais e eventos
     *
     * @return do resultado da importação de utilizadores, locais e eventos em
     * boolean
     */
    public boolean importarDados() {
        
        File f = new File("Evento.xml");
        File f1 = new File("Evento_codificado.xml");
        File tabela = new File("tabelaFicheiros.csv");
        
        CodificacaoAritmetica descodifica = new CodificacaoAritmetica(tabela);
        
        if (f.exists() && !f.isDirectory()) {
            System.out.println("não descodifica");
        }else if (f1.exists() && !f1.isDirectory()){
            descodifica.descodificaFicheiro(f1);
            System.out.println("descodifica");
        }else{
            System.out.println("não existe");
        }
        
        File f2 = new File("utilizador.xml");
        File f3 = new File("utilizador_codificado.xml");
        
        if (f2.exists() && !f2.isDirectory()) {
            System.out.println("não descodifica");
        }else if (f3.exists() && !f3.isDirectory()){
            descodifica.descodificaFicheiro(f3);
            System.out.println("descodifica");
        }else{
            System.out.println("não existe");
        }
        
        File f4 = new File("local.xml");
        File f5 = new File("local_codificado.xml");
        
        if (f4.exists() && !f4.isDirectory()) {
            System.out.println("não descodifica");
        }else if (f5.exists() && !f5.isDirectory()){
            descodifica.descodificaFicheiro(f5);
            System.out.println("descodifica");
        }else{
            System.out.println("não existe");
        }
        
        if (!importarUtilizadores()) {
            return false;
        }

        if (!importarLocais()) {
            return false;
        }

        if (!importarEventos()) {
            return false;
        }

        return true;
    }

    /**
     * Metodo que importa dados de utilizadores
     *
     * @return boolean da leitura do ficheiro xml
     */
    private boolean importarUtilizadores() {
        return this.lerXML.lerUtilizadorXML(this.m_empresa, ImportarDadosNegocio.NOME_DO_FICHEIRO_UTILIZADORES);
    }

    /**
     * Metodo que importa dados de locais
     *
     * @return boolean da leitura do ficheiro xml
     */
    private boolean importarLocais() {
        return this.lerXML.lerLocalXML(ImportarDadosNegocio.NOME_DO_FICHEIRO_LOCAL);
    }

    /**
     * Metodo que importa dados de eventos
     *
     * @return boolean da leitura do ficheiro xml
     */
    private boolean importarEventos() {
        return this.lerXML.lerEventoXML(this.m_empresa, ImportarDadosNegocio.NOME_DO_FICHEIRO_EVENTO);
    }
}
