package eventoscientificos.controllers;

import eventoscientificos.domain.Autor;
import eventoscientificos.domain.Empresa;
import eventoscientificos.domain.Evento;
import eventoscientificos.domain.RegistoEventos;
import eventoscientificos.domain.SessaoTematica;
import eventoscientificos.domain.Utilizador;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Class CarregarArtigosPorFicheiroController
 *
 * @author Carlos
 */
public class CarregarArtigosPorFicheiroController {

    /**
     * A empresa do CarregarArtigosPorFicheiroController
     */
    private Empresa m_empresa;

    /**
     * O registo de eventos do CarregarArtigosPorFicheiroController
     */
    private RegistoEventos m_regEvento;

    /**
     * O evento do CarregarArtigosPorFicheiroController
     */
    private Evento m_evento;

    /**
     * Construtor de CarregarArtigosPorFicheiroController
     *
     * @param empresa Empresa
     */
    public CarregarArtigosPorFicheiroController(Empresa empresa) {
        this.m_empresa = empresa;
        this.m_regEvento = empresa.getRegistoEventos();
    }

    /**
     * Metodo que retorna lista de eventos em submissao
     *
     * @return lista de eventos em submissao
     */
    public List<Evento> getListaEventosEmSubmissao() {
        return this.m_empresa.getRegistoEventos().getListaEventosEmSubmissao();
    }

    /**
     * Metodo que seleciona o evento
     *
     * @param est o evento
     */
    public void selectEvento(Evento est) {
        this.m_evento = est;
    }

    /**
     * Metodo de importação de ficheiro
     *
     * @param ficheiro ficheiro de artigos
     * @return resultado boolean da importação
     */
    public boolean importarFicheiro(String ficheiro) {
        Scanner sc;
        String linhaActual;
        String split = ";";
        String linhaErros = "";
        int linha = 0;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");

        try {
            sc = new Scanner(new File(ficheiro), "UTF8");
            while (sc.hasNextLine()) {
                linhaActual = sc.nextLine();

                if (linha == 0) {
                    linha++;
                    continue;
                }

                String[] dados = linhaActual.split(split);

                if (!validarLinha(dados.length)) {
                    linhaErros += "Linha: " + linha + " Erro: Tamanho de campos invalidos!\n";
                    continue;
                }

                SubmeterArtigoController controller = new SubmeterArtigoController(m_empresa);

                if (dados[0].trim().isEmpty()) {
                    controller.selectSubmissivel(m_evento);
                } else {
                    SessaoTematica st = m_evento.getSessaoTematicaByCodigo(dados[0]);
                    if (st == null) {
                        linhaErros += "Linha: " + linha + " Erro: Sessão Temática invalida!\n";
                        continue;
                    }
                    controller.selectSubmissivel(st);
                }

                // Titulo e Resumo
                controller.setDados(dados[1], dados[5]);

                // Palavras chave
                List palavrasChave = new ArrayList<>();
                Collections.addAll(palavrasChave, dados[4].split(","));
                controller.setPalavrasChave(palavrasChave);

                // Ficheiro
                controller.setFicheiro(dados[6]);

                // Data
                try {
                    controller.setData(sdf.parse(dados[3]));
                } catch (ParseException ex) {
                    linhaErros += "Linha: " + linha + " Erro: Formato da data invalida!\n";
                    continue;
                }

                // Autores
                List<Autor> lst = new ArrayList<>();
                for (int i = 7; i < dados.length; i++) {
                    String nome = dados[i];
                    String afiliacao = dados[++i];
                    String email = dados[++i];
                    Autor aut = controller.novoAutor(nome, afiliacao, email);
                    lst.add(aut);
                    if (!controller.addAutor(aut)) {
                        linhaErros += "Linha: " + linha + " Erro: Erro a adicionar o autor!\n";
                        continue;
                    }
                }

                boolean validacaoAutorCorrespondente = false;

                // Autor Correspondente
                for (Autor l : lst) {
                    Utilizador u = m_empresa.getRegistoUtilizadores().getUtilizadorByID(dados[2]);
                    if (u != null) {
                        if (u.getEmail().equals(l.getEmail())) {
                            controller.setCorrespondente(l);
                            validacaoAutorCorrespondente = true;
                        }
                    }
                    u = m_empresa.getRegistoUtilizadores().getUtilizadorByEmail(dados[2]);
                    if (u != null) {
                        if (u.getEmail().equals(l.getEmail())) {
                            controller.setCorrespondente(l);
                            validacaoAutorCorrespondente = true;
                        }
                    }
                    System.out.println("\n\n\n\n\n\nid|email " + dados[2] + "\n" + l.getEmail());
                }

                if (!validacaoAutorCorrespondente) {
                    linhaErros += "Linha: " + linha + " Erro: Erro a adicionar o autor correspondente!\n";
                    continue;
                }

                if (!controller.registarSubmissao()) {
                    linhaErros += "Linha: " + linha + " Erro: Erro a adicionar a submissão!\n";
                    continue;
                }

            }
        } catch (FileNotFoundException ex) {
            linhaErros += "Ficheiro não encontrado!\n";
        }

        if (!linhaErros.isEmpty()) {
            PrintWriter writer = null;
            try {
                writer = new PrintWriter("ErroCSV.txt", "UTF-8");
                writer.println(linhaErros);
            } catch (FileNotFoundException | UnsupportedEncodingException ex) {
                return false;
            }
            writer.close();
            return false;
        }
        return true;
    }

    /**
     * Metodo que valida a Linha do ficheiro
     *
     * @param tamanho da linha
     * @return resultado boolean da validação da linha
     */
    private boolean validarLinha(int tamanho) {
        int aux = tamanho - 7;
        System.out.println("AUX " + ((aux % 3) == 0));
        return tamanho > 9 && ((aux % 3) == 0);
    }
}
