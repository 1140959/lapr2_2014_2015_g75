package eventoscientificos.controllers;

import eventoscientificos.domain.Artigo;
import eventoscientificos.domain.Empresa;
import eventoscientificos.domain.Evento;
import eventoscientificos.domain.ListaSubmissoes;
import eventoscientificos.domain.MecanismoDistribuicao;
import eventoscientificos.domain.Submissao;
import java.util.ArrayList;
import java.util.List;

/**
 * Classe DistribuirRevisoesController
 *
 * @author 1081019
 */
public class DistribuirRevisoesController {

    private Empresa m_empresa;
    private List<Submissao> m_lstSubmissoes;
    private MecanismoDistribuicao m_mecanismoDistribuicao;

    /**
     * Construtor de DistribuirRevisoesController
     *
     * @param empresa Empresa
     */
    public DistribuirRevisoesController(Empresa empresa) {
        m_empresa = empresa;
    }

    /**
     * Metodo que retorna a lista de eventos
     *
     * @param organizadorId O organizador
     * @return da lista de eventos
     */
    public List<Evento> GetEventos(String organizadorId) {
        return m_empresa.getEventosOrganizador(organizadorId);
    }

    /**
     * Metodo que retorna a lista de artigos
     *
     * @param lstSubs lista de submissoes
     * @return da lista de artigos
     */
    public List<Artigo> GetArtigos(ListaSubmissoes lstSubs) {
        List<Artigo> listaArtigos = new ArrayList<>();

        for (Submissao submissao : lstSubs.getListaSubmissoes()) {
            listaArtigos.add(submissao.getArtigo());
        }

        return listaArtigos;
    }

    /**
     * Metodo que retorna uma lista de mecanismos de distribuição
     *
     * @return de listaMecanismos
     */
    public List<String> GetListaMecanismos() {
        List<String> listaMecanismos = new ArrayList<String>();

        listaMecanismos.add("Mecanismo");

        return listaMecanismos;
    }

}
