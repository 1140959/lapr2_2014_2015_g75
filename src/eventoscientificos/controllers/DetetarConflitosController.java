package eventoscientificos.controllers;


import eventoscientificos.domain.Empresa;
import eventoscientificos.domain.Licitavel;
import eventoscientificos.domain.Licitacao;
import eventoscientificos.domain.MecanismoDetecaoConflito;
import eventoscientificos.domain.ProcessoLicitacao;
import eventoscientificos.domain.RegistoTipoConflitos;
import eventoscientificos.domain.Revisor;
import eventoscientificos.domain.Submissao;
import eventoscientificos.domain.TipoConflito;
import java.util.List;
import java.util.ListIterator;
import java.util.TimerTask;

/**
 * Classe DetetarConflitosController
 * @author Paulo Maio
 */
public class DetetarConflitosController extends TimerTask {

    /**
     * Variável de instância do tipo objeto Empresa (a Empresa onde serão
     * detetados os conflitos)
     */
    private Empresa m_empresa;

    /**
     * Interface Licitavel passada como parâmetro (será implementada ou num
     * Evento ou numa Sessão Temática)
     */
    private Licitavel m_e_st;

    /**
     * Construtor de classe que recebendo como parâmetros dois objetos (Empresa
     * e Licitavel) os atribui às variáveis de instância.
     *
     * @param emp A Empresa onde serão detetados os conflitos
     * @param e_st O Evento/ST onde serão detetados os conflitos
     */
    public DetetarConflitosController(Empresa emp, Licitavel e_st) {
        this.m_empresa = emp;
        this.m_e_st = e_st;
    }

    @Override
    /**
     * Método que desencadeia o processo de deteção de conflitos.
     */
    public void run() {

        /**
         * Inicia o processo de deteção sobre o Evento/Sessão Temática, que
         * coloca o mesmo no estado EmDetecaoConflito e instancia um novo
         * ProcessoLicitacao
         */
        ProcessoLicitacao pl = m_e_st.iniciaDetecao();

        /**
         * Obtém da Empresa o RegistoTipoConflitos que, por sua vez irá conter a
         * listagem dos Tipos de Conflitos passíveis de deteção
         */
        RegistoTipoConflitos rc = m_empresa.getRegistoTipoConflitos();

        /**
         * Obtém a listagem dos tipos de conflitos existentes no
         * RegistoTipoConflitos
         */
        List<TipoConflito> ltc = rc.getListaTipoConflitos();

        /**
         * Obtém a lista de submissões existentes no Evento/Sessão Temática
         */
        List<Submissao> ls = m_e_st.getSubmissoes();

        /**
         * Para cada submissão de Evento/Sessão Temática obtém a lista de
         * Revisores
         */
        for (ListIterator<Submissao> itS = ls.listIterator(); itS.hasNext();) {
            Submissao s = itS.next();
            List<Revisor> lr = m_e_st.getRevisores();
            /**
             * Para cada Revisor instancia uma nova Licitação de Evento/Sessão
             * Temática
             */
            for (ListIterator<Revisor> itR = lr.listIterator(); itR.hasNext();) {
                Revisor r = itR.next();
                Licitacao l = pl.novaLicitação(r, s);
                /**
                 * Para cada Tipo de Conflito obtém o mecanismo coorrespondente.
                 * Sobre esse mecanismo opera o método "detetarConflito" do
                 * mecanismo cujos parâmetros são: o ProcessoLicitação, a
                 * Licitação e o TipoConflito. Pelo facto dos Mecanismos não
                 * estarem implementados e serem apenas classes "mockup", se o
                 * mecanismo for o mecanismo 1, o retorno do método
                 * detetarConflito será sempre um boolean "false". Se o
                 * mecanismo for o 2, o retorno do método detetarConflito será
                 * sempre um boolean "true".
                 */
                for (ListIterator<TipoConflito> itC = ltc.listIterator(); itC.hasNext();) {
                    TipoConflito c = itC.next();
                    MecanismoDetecaoConflito m = c.getMecanismoDetecaoConflito();
                    m.detetarConflito(pl, l, c);
                }
            }
        }

        /**
         * No final, atribui as modificações relacionadas com o Processo de
         * Licitação do Evento/Sessão Temática recebido como parâmetro na
         * instanciação do DetetarConflitosController
         */
        m_e_st.setProcessoLicitacao(pl);
    }

}
