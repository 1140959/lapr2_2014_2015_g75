package eventoscientificos.controllers;

import eventoscientificos.domain.Empresa;
import eventoscientificos.domain.Decidivel;
import eventoscientificos.domain.Decisao;
import eventoscientificos.domain.MecanismoDecisao;
import eventoscientificos.domain.ProcessoDecisao;
import eventoscientificos.domain.RegistoEventos;
import java.util.List;

/**
 * Classe DecidirSubmissoesController
 * @author nunosilva
 */
public class DecidirSubmissoesController {

    /**
     * Variavel empresa do DecidirSubmissaoController
     */
    private Empresa m_empresa;
    /**
     * Variavel registoEventos do DecidirSubmissaoController
     */
    private RegistoEventos m_regEvento;
    /**
     * Variavel decidivel do DecidirSubmissaoController
     */
    private Decidivel m_decidivel;
    /**
     * Variavel processoDecisao do DecidirSubmissaoController
     */
    private ProcessoDecisao m_processoDecisao;
    /**
     * Variavel idUtilizador do DecidirSubmissaoController
     */
    private String idUtilizador;

    /**
     * Constroi uma instancia de DecidirSubmissaoController recebendo a empresa
     * e o idUtilizador como parametros
     *
     * @param empresa empresa do DecidirSubmissaoController
     * @param id idUtilizador do DecidirSubmissaoController
     */
    public DecidirSubmissoesController(Empresa empresa, String id) {
        m_empresa = empresa;
        idUtilizador = id;
        m_regEvento = empresa.getRegistoEventos();

    }

    /**
     * Devolve uma lista de Eventos e Sessões Tematicas que contenham submissoes
     * em StateEmDecidido
     *
     * @return lista de decidiveis
     */
    public List<Decidivel> getDecisiveis() {
        
        return m_regEvento.getDecisiveis(idUtilizador);

    }

    /**
     * Cria uma instancia de processoDecisao recebendo por paramentro um
     * decidivel
     *
     * @param d tipo decidivel, podendo ser evento ou sessao
     */
    public void novoProcessoDecisao(Decidivel d) {
        m_decidivel = d;
        m_processoDecisao = d.novoProcessoDecisao();

    }

    /**
     * Devolve uma lista de Mecanismos de decisao desenvolvidos inicialmente
     * pelo administrador
     *
     * @return lista de MecanismosDecisao
     */
    public List<MecanismoDecisao> getMecanismosDecisao() {
        List<MecanismoDecisao> lm = m_empresa.getMecanismosDecisao();
        return lm;
    }

    /**
     * Recebendo um mecanismo de decisao por parametro vai envair o mesmo para
     * executar a alteração de um processoDecisao
     *
     * @param m MecanismoDecisao
     */
    public void setMecanismoDecisao(MecanismoDecisao m) {
        m_processoDecisao.setMecanismoDecisao(m);
        m_processoDecisao.decide();
    }

    /**
     * Devolve true se o decidivel for alterado corretamente,e false se isso nao
     * acontecer
     *
     * @return boolean true or false
     */
    public boolean registaPD() {
        m_decidivel.setPD(m_processoDecisao);
        return true;
    }

    /**
     * devolve true se a notificacao foi efetuada corretamente e false se nao
     * foi enviada
     *
     * @return boolean true or false
     */
    public boolean notifica() {
        return m_processoDecisao.notifica();
    }

    /**
     * devolve uma lista de decisoes de um determinado processo de decisao
     *
     * @return lista de decisoes
     */
    public List<Decisao> getDecisoes() {
        return m_processoDecisao.getDecisoes();
    }

    /**
     * executad o setAceitacao de uma decisao que recebe como parametro,
     * enviando uma string também recebida por parametro
     *
     * @param d decisao
     * @param a string com a indicação da aceitacao
     */
    public void setAceitacao(Decisao d, String a) {
        d.setAceitacao(a);
    }
}
