package eventoscientificos.controllers;

import eventoscientificos.domain.Empresa;
import eventoscientificos.domain.MecanismoDetecaoConflito;
import eventoscientificos.domain.TipoConflito;

/**
 * Classe DefinirTipoConflitoController
 *
 * @author nunosilva
 */
public class DefinirTipoConflitoController {

    private Empresa m_empresa;

    /**
     * Construtor de DefinirTipoConflitoController
     *
     * @param empresa Empresa
     */
    public DefinirTipoConflitoController(Empresa empresa) {
        m_empresa = empresa;
    }

    /**
     * Metodo que cria um novo tipo de conflito
     *
     * @param strDescricao descrição do conflito
     * @param mecanismo MecanismoDetecaoConflito
     * @return do tipo de conflito
     */
    public TipoConflito novoTipoConflito(String strDescricao, MecanismoDetecaoConflito mecanismo) {
        return m_empresa.getRegistoTipoConflitos().novoTipoConflito(strDescricao, mecanismo);
    }

    /**
     * Metodo que regista o tipo de conflito
     *
     * @param tpConflito O tipo de conflito
     * @return resultado do regista o tipo de conflito
     */
    public boolean registaTipoConflito(TipoConflito tpConflito) {
        return m_empresa.getRegistoTipoConflitos().registaTipoConflito(tpConflito);
    }
}
