package eventoscientificos.controllers;

import eventoscientificos.domain.CP;
import eventoscientificos.domain.CPDefinivel;
import eventoscientificos.domain.Empresa;
import eventoscientificos.domain.Evento;
import eventoscientificos.domain.RegistoEventos;
import eventoscientificos.domain.Revisor;
import eventoscientificos.domain.SessaoTematica;
import eventoscientificos.domain.Utilizador;
import java.util.List;

/**
 * Classe DefinirCPController
 *
 * @author Nuno Silva
 */
public class DefinirCPController {

    /**
     * A empresa do DefinicirCPController
     */
    private Empresa m_empresa;

    /**
     * O registo de eventos do DefinicirCPController
     */
    private RegistoEventos m_regEventos;

    /**
     * A cp definivel (Evento Cientifico ou Sessao Tematica) do
     * DefinicirCPController
     */
    private CPDefinivel m_CPDefinivel;

    /**
     * A comissao de programas do DefinicirCPController
     */
    private CP m_cp;

    /**
     * O revisor do DefinicirCPController
     */
    private Revisor m_revisor;

    /**
     * Constrói uma instância de DefinicirCPController recebendo a empresa como
     * parametro uma empresa
     *
     * @param empresa a empresa do DefinicirCPController
     */
    public DefinirCPController(Empresa empresa) {
        this.m_empresa = empresa;
        this.m_regEventos = empresa.getRegistoEventos();
    }

    /**
     * Devolve uma Lista de CPDefinivel recebendo como parametro o id do
     * utilizador
     *
     * @param strID o id do utilizador
     * @return uma lista de CPDefinivel
     */
    public List<CPDefinivel> getListaCPDefiniveisEmDefinicao(String strID) {
        return this.m_regEventos.getListaCPDefiniveisEmDefinicaoDoUtilizador(strID);
    }

    /**
     * Devolve uma lista de Evento recebendo como parametro o id do utilizador
     *
     * @param strID o id do utilizador
     * @return uma lista de Evento
     */
    public List<Evento> getListaCPDefiniveisEmDefinicaoEvento(String strID) {
        return this.m_regEventos.getListaEventoCPDEmDefinicaoDoUtilizadorEvento(strID);
    }

    /**
     * Devolve uma lista de SessaoTematica recebendo como parametro o id do
     * utilizador
     *
     * @param strID o id do utilizador
     * @return uma lista de SessaoTematica
     */
    public List<SessaoTematica> getListaCPDefiniveisEmDefinicaoSessao(String strID) {
        return this.m_regEventos.getListaSessaoRegistadosDoUtilizador(strID);
    }

    /**
     * Cria uma nova CP (Comissao de Programa) no DefinirCPController recebendo
     * por paramentro uma CPDefinivel (Evento ou SessaoTematica)
     *
     * @param cpDefinivel um CPDefinivel onde posteriormente sera criado a CP
     */
    public void novaCP(CPDefinivel cpDefinivel) {
        this.m_CPDefinivel = cpDefinivel;
        this.m_cp = cpDefinivel.novaCP();
    }

    /**
     * Valida e cria um novo membro na comissao de programas recebendo por
     * paramentro o id do utilizador
     *
     * @param strId o id do utilizador
     * @return a validação da criação do novo membro da comissao de programas
     */
    public boolean novoMembroCP(String strId) {
        Utilizador u = this.m_empresa.getRegistoUtilizadores().getUtilizadorByID(strId);

        if (u != null) {
            this.m_revisor = this.m_cp.novoMembroCP(u);
            return true;
        }
        return false;
    }

    /**
     * Adiciona um revisor a comissao de programas
     *
     * @return a validação da adição do revisor a comissao de programa
     */
    public boolean addMembroCP() {
        return this.m_cp.addMembroCP(this.m_revisor);
    }

    /**
     * Regista a CP (comissao de programa) na CPDefinivel (Evento ou
     * SesstaoTematica)
     *
     * @return o resultado da operação do registo da CP
     */
    public boolean registaCP() {
        return this.m_CPDefinivel.setCP(this.m_cp);
    }

}
