package eventoscientificos.controllers;

import eventoscientificos.domain.Empresa;
import eventoscientificos.domain.Evento;
import eventoscientificos.domain.RegistoEventos;
import eventoscientificos.domain.SessaoTematica;
import eventoscientificos.domain.Submissao;
import eventoscientificos.domain.Submissivel;
import java.util.List;

/**
 * Classe RetirarSubmissaoController
 * @author Carlos
 */
public class RetirarSubmissaoController {

    /**
     * A empresa do RetirarSubmissaoController
     */
    private Empresa m_empresa;

    /**
     * O registoEventos do RetirarSubmissaoController
     */
    private RegistoEventos m_regEvento;

    /**
     * A submissao do RetirarSubmissaoController
     */
    private Submissao m_submissao;

    /**
     * O email do RetirarSubmissaoController
     */
    private String m_strEmail;

    /**
     * Constrói uma instância de RetirarSubmissaoController recebendo a empresa
     * como parametro uma empresa e o email id do utilizador
     *
     * @param empresa empresa do RetirarSubmissaoController
     * @param id id do utilizador
     */
    public RetirarSubmissaoController(Empresa empresa, String id) {
        m_empresa = empresa;
        m_strEmail = empresa.getRegistoUtilizadores().getUtilizadorByID(id).getEmail();
        m_regEvento = empresa.getRegistoEventos();
    }

    /**
     * Devolve uma lista de eventos que contenham submissoes que podem ainda ser
     * retiradas
     *
     * @return lista de eventos
     */
    public List<Evento> getListaSubmissaoEventoDoAutor() {
        return m_regEvento.getListaSubmissaoEventoDoAutor(m_strEmail);
    }

    /**
     * Devolve uma lista de sessões tematicas que contenham submissoes que podem
     * ainda ser retiradas
     *
     * @return uma lista de sessoes tematicas
     */
    public List<SessaoTematica> getListaSubmissaoSessaoDoAutor() {
        return m_regEvento.getListaSubmissaoSessaoDoAutor(m_strEmail);
    }

    /**
     * Devolve uma lista de submissoes do utilizador que ainda podem ser
     * retiradas
     *
     * @param submissivel Submissivel que contem submissoes
     * @return uma lisa de submissiveis
     */
    public List<Submissao> getListaSubmissaoDoAutor(Submissivel submissivel) {
        return submissivel.getListaSubmissaoByEmail(m_strEmail);
    }

    /**
     * Devolve uma String com o conteudo textural da submissao selecionada
     *
     * @param submissao submissao seleciona pelo utilizador
     * @return devolve o conteudo textural da submissao
     */
    public String selectSubmissao(Submissao submissao) {
        this.m_submissao = submissao;
        return submissao.getArtigo().toString();
    }

    /**
     * Modifica o estado da submissao para SubmissaoStateRetirada
     * 
     * @return devolve o sucesso da operacao
     */
    public boolean setStateRetirado() {
        return this.m_submissao.setStateRetirada();
    }
}
