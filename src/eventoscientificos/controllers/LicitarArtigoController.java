package eventoscientificos.controllers;

import eventoscientificos.domain.Empresa;
import eventoscientificos.domain.Licitacao;
import eventoscientificos.domain.Licitavel;
import eventoscientificos.domain.ProcessoLicitacao;
import eventoscientificos.domain.Submissivel;
import eventoscientificos.domain.TipoConflito;
import eventoscientificos.domain.Utilizador;
import java.util.ArrayList;
import java.util.List;

/**
 * Classe LicitarArtigoController
 *
 * @author nunosilvax
 */
public class LicitarArtigoController {

    private Submissivel sub;
    private Licitavel licit;
    private Licitacao licitacao;
    private Empresa m_empresa;
    private ProcessoLicitacao processoLicit;

    /**
     * Construtor de LicitarArtigoController
     *
     * @param empresa Empresa
     */
    public LicitarArtigoController(Empresa empresa) {
        m_empresa = empresa;
    }

    public List<Submissivel> getListaEventosSessoes(Utilizador utilizador) {
        return new ArrayList<>();
    }
    
    public List<Licitavel> getListaLicitacoes(Utilizador utilizador){
        return new ArrayList<>();
    }
    
    public void setInteresse(String interesse){
        licitacao.setInteresse(interesse);   
    }
    
    public void setConflitos(List<TipoConflito> conflitos){
        licitacao.setConflitos(conflitos);
    }
    
    public void registaLicitacao(){
        processoLicit.registaLicitacao();
    }
}
