package eventoscientificos.controllers;

import eventoscientificos.domain.Empresa;
import eventoscientificos.domain.Utilizador;

/**
 * Class AlterarUtilizadorController
 * @author nunosilva
 */
public class AlterarUtilizadorController {

    /**
     * Variável de instância do tipo Empresa
     */
    private Empresa m_empresa;

    /**
     * Variável de instância do tipo Utilizador
     */
    private Utilizador m_user;

    /**
     * @param id O ID (username) do utilizador que será alterado
     * @param empresa A Empresa onde o Utilizador será alterado
     */
    public AlterarUtilizadorController(String id, Empresa empresa) {
        m_empresa = empresa;
        m_user = m_empresa.getRegistoUtilizadores().getUtilizadorByID(id);
    }

    /**
     * Construtor vazio da classe AlterarUtilizadorController
     */
    public AlterarUtilizadorController() {
        m_empresa = this.m_empresa;
        m_user = this.m_user;

    }

    /**
     *
     * @param strID O ID do Utilizador a ser alterado
     * @return Utilizador a ser alterado
     */
    public Utilizador getUtilizador(String strID) {
        m_user = m_empresa.getRegistoUtilizadores().getUtilizadorByID(strID);
        return m_user;
    }

    /**
     * Método de instância que recebe os seguintes dados: nome, username,
     * password e email. Altera os dados de um utilizador caso os dados
     * recebidos por parâmetro passem nas validações próprias da classes de
     * utilizador e de registo de utilizadores.
     *
     * @param strNome Nome do Utilizador
     * @param strUsername Username do Utilizador
     * @param strPwd Password do Utilizador
     * @param strEmail Email do Utilizador
     * @return "true" caso a alteração de dados seja efetuada (significa que
     * passou nos métodos de validação). "false", caso a alteração não ocorra
     * (significa que não passou nos métodos de validação).
     */
    public boolean alteraDados(String strNome, String strUsername, String strPwd, String strEmail) {
        Utilizador uClone = m_user.clone();
        uClone.setNome(strNome);
        uClone.setEmail(strEmail);
        uClone.setUsername(strUsername);
        uClone.setPassword(strPwd);

        return m_empresa.getRegistoUtilizadores().alteraUtilizador(m_user, uClone);
    }

    /**
     * Devolve o valor da variável Empresa correspondente à instância do
     * AlterarUtilizadorController.
     *
     * @return A Empresa instanciada.
     */
    public Empresa getM_empresa() {
        return m_empresa;
    }

    /**
     * Devolve o valor da variável Utilizador correspondente à instância do
     * AlterarUtilizadorController.
     *
     * @return O Utilizador instanciado.
     */
    public Utilizador getM_user() {
        return m_user;
    }

}
