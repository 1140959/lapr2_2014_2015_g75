package eventoscientificos.controllers;

import eventoscientificos.domain.AlterarStateParaEmDistribuicao;
import eventoscientificos.domain.AlterarStateParaEmSubmissao;
import eventoscientificos.domain.Empresa;
import eventoscientificos.domain.Evento;
import eventoscientificos.domain.ListaSessoesTematicas;
import eventoscientificos.domain.Proponente;
import eventoscientificos.domain.RegistoEventos;
import eventoscientificos.domain.RegistoUtilizadores;
import eventoscientificos.domain.SessaoTematica;
import eventoscientificos.domain.Utilizador;
import java.util.Date;
import java.util.List;

/**
 * Classe CriarSessaotematicaController
 *
 * @author nunosilva
 * @author 1081019
 */
public class CriarSessaoTematicaController {

    private Empresa m_empresa;
    private RegistoEventos m_regEventos;
    private RegistoUtilizadores m_regUtilizadores;
    private Evento m_evento;
    private SessaoTematica m_st;
    private Proponente m_prop;

    /**
     * Construtor de CriarSessaoTematicaController
     *
     * @param empresa Empresa
     */
    public CriarSessaoTematicaController(Empresa empresa) {
        m_empresa = empresa;
        m_evento = new Evento();
        m_regEventos = empresa.getRegistoEventos();
        m_regUtilizadores = empresa.getRegistoUtilizadores();
    }

    /**
     * Metodo que retorna a lista de eventos registados do utilizador
     *
     * @param strID ID
     * @return da lista de eventos registados do utilizador
     */
    public List<Evento> getListaEventosRegistadosDoUtilizador(String strID) {
        return m_regEventos.getListaEventosRegistadosDoUtilizador(strID);
    }

    /**
     * Metodo que retorna a lista de eventos
     *
     * @return da lista de eventos
     */
    public List<Evento> getListaEventos() {
        return m_regEventos.getListaEventos();
    }

    /**
     * Metodo que insere/modifica o evento
     *
     * @param e O evento
     */
    public void setEvento(Evento e) {
        this.m_evento = e;
    }

    /**
     * Metodo que efetua a inserção de dados da SessaoTematica
     *
     * @param cod codigo da ST
     * @param desc descrição da ST
     * @param dtInicioSub data de inicio de submissao
     * @param dtFimSub data de fim de submissao
     * @param dtInicioDistr data de inicio de distribuição
     * @param dtLimRevisao data limite de revisão
     * @param dtLimArtigoFinal data limite de submissao do artigo final
     */
    public void setDados(String cod, String desc, Date dtInicioSub, Date dtFimSub, Date dtInicioDistr, Date dtLimRevisao, Date dtLimArtigoFinal) {
        ListaSessoesTematicas lsST = this.m_evento.getListaDeSessoesTematicas();
        m_st = lsST.novaSessaoTematica(cod, desc, dtInicioSub, dtFimSub, dtInicioDistr, dtLimRevisao, dtLimArtigoFinal);
    }

    /**
     * Metodo que adiciona o proponente pelo ID de utilizador
     *
     * @param strId ID
     * @return do proponente adicionado
     */
    public boolean addProponente(String strId) {
        Utilizador u = m_regUtilizadores.getUtilizadorByID(strId);

        m_prop = m_st.getListaProponentes().novoProponente(u);

        return (m_prop != null);
    }

    /**
     * Metodo que regista um proponente na SessaoTematica
     *
     * @return do resultado do registo do proponente na SessaoTematica
     */
    public boolean registaProponente() {
        return m_st.getListaProponentes().registaProponente(m_prop);
    }

    /**
     * Metodo que retorna o utilizador pelo ID
     *
     * @param id ID
     * @return do utilizador pelo ID
     */
    public Utilizador getUtilizadorByID(String id) {
        return m_empresa.getRegistoUtilizadores().getUtilizadorByID(id);
    }

    /**
     * Metodo que regista a sessao tematica e inicia os timers
     *
     * @return nulo
     */
    public SessaoTematica registaSessaoTematica() {
        if (m_evento.getListaDeSessoesTematicas().registaSessaoTematica(m_st)) {
            createTimers();
            return m_st;
        }
        return null;
    }

    /**
     * Metodo que cria os timers
     */
    private void createTimers() {
        AlterarStateParaEmSubmissao task1 = new AlterarStateParaEmSubmissao(m_st);
        this.m_empresa.schedule(task1, m_st.getDataInicioSubmissao());

        AlterarStateParaEmDistribuicao task2 = new AlterarStateParaEmDistribuicao(m_st);
        this.m_empresa.schedule(task2, m_st.getDataInicioDistribuicao());

        DetetarConflitosController task3 = new DetetarConflitosController(m_empresa, m_st);
        this.m_empresa.schedule(task3, m_st.getDataFimSubmissao());
    }

    /**
     * Metodo que retorna a SessaoTematica
     *
     * @return de SessaoTematica
     */
    public SessaoTematica getSessaoTematica() {
        return this.m_st;
    }

    /**
     * Metodo que cria um array com os titulos dos Eventos cientificos
     *
     * @param eventos Eventos cientificos
     * @return do array com os titulos dos Eventos cientificos
     */
    public String[] getTitulosEventos(List<Evento> eventos) {
        String[] s = new String[eventos.size()];
        for (int i = 0; i < eventos.size(); i++) {
            Evento e = eventos.get(i);
            s[i] = e.getTitulo();
        }
        return s;
    }

    /**
     * Metodo que regista um proponente
     *
     * @param username do Utilizador
     */
    public void registaProponente(String username) {
        addProponente(username);
        m_st.getListaProponentes().registaProponente(m_prop);
    }
}
