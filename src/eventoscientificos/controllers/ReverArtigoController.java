package eventoscientificos.controllers;

import eventoscientificos.domain.Empresa;
import eventoscientificos.domain.ListaRevisoes;
import eventoscientificos.domain.ProcessoDistribuicao;
import eventoscientificos.domain.RegistoEventos;
import eventoscientificos.domain.Revisao;
import eventoscientificos.domain.Revisavel;
import java.util.List;

/**
 * Classe ReverArtigoController
 *
 * @author nunosilva
 * @author 1140958@isep.ipp.pt
 */
public class ReverArtigoController {

    /**
     * Variavel do Tipo Empresa
     */
    private Empresa m_empresa;
    /**
     * Variavel do Tipo String identificadora do id o utilizador
     */
    private String idUtilizador;
    /**
     * Variavel do Tipo RegistoEventos
     */
    private RegistoEventos m_regEvento;
    /**
     * Variavel do Tipo Revisavel
     */
    private Revisavel m_revisavel;
    /**
     * Variavel do tipo Revisao
     */
    private Revisao m_revisao;
    /**
     * Variavel do Tipo ListaRevisoes, contendo uma lista de revisoes
     */
    private ListaRevisoes m_listaRevisoes;

    /**
     * Cria uma instancia CriarEventoController recebendo uma empresa
     *
     * @param empresa Empresa
     * @param id, identificador do utilizador autenticado no sistema
     */
    public ReverArtigoController(Empresa empresa, String id) {
        idUtilizador = id;
        m_empresa = empresa;
        m_regEvento = new RegistoEventos();
        m_revisao = new Revisao();
        m_listaRevisoes = new ListaRevisoes();
        m_regEvento = m_empresa.getRegistoEventos();
    }

    /**
     * @param idUtilizador , identificador do utilizador
     *
     * Método que vai retornar uma lista de elementos Revisaveis que se
     * encontram no estado revisor de um determinado revisor através do seu
     * identificador que é recebido por parametro
     * @return Return de lista de Revisaveis
     *
     */
    public List<Revisavel> getRevisaveisEmRevisaoDoRevisor(String idUtilizador) {
        return m_regEvento.getRevisaveisEmRevisaoDoRevisor(idUtilizador);
    }

    /**
     * @param idUtilizador , identificador do utilizador
     * @param r, uma variavel do Tipo Revisavel
     *
     * Método que vai returnar uma lista com todas as revisoes que através do
     * ProcessoDistruicao foram entregues ao utilizador autenticado
     * @return Lista de revisoes
     *
     */
    public List<Revisao> selecionaRevisavel(Revisavel r, String idUtilizador) {
        m_revisavel = r;
        ProcessoDistribuicao pd = r.getProcessoDistribuicao();

//        m_listaRevisoes = pd.getListaDeRevisoes();
//        return m_listaRevisoes.getRevisoesRevisor(idUtilizador);
        return null;
    }

    /**
     * Metodo que seleciona a revisao
     *
     * @param rev , variavel Tipo revisao
     */
    public void selecionaRevisao(Revisao rev) {
        m_revisao = rev;
    }

    /**
     *
     * @param confianca Confiança
     * @param adequacao Adequação
     * @param originalidade originalidade
     * @param qualidade qualidade
     * @param recomendacao recomendado
     * @param just justificaçao
     *
     * Método que vai alterar os parametros de avaliação de uma determinada
     * Revisao
     */
    public void setDadosRevisao(String confianca, String just, String adequacao, String originalidade, String qualidade, String recomendacao) {

        m_revisao.setConfianca(Integer.parseInt(confianca));
        m_revisao.setJustificacao(just);
        m_revisao.setAdequacao(Integer.parseInt(adequacao));
        m_revisao.setOriginalidade(Integer.parseInt(originalidade));
        m_revisao.setM_Qualidade(Integer.parseInt(qualidade));
        m_revisao.setM_Recomendacao(Integer.parseInt(recomendacao));

    }

    /**
     * Metodo que valida se a lista de revisoes é valida e altera o estado para
     * em Decisao.
     *
     * @param rev variavel Tipo revisao
     */
    public void valida(Revisavel rev) {
        if (m_listaRevisoes.valida(m_revisao)) {
            m_revisavel.alteraParaEmDecisao();
        }
    }
}
