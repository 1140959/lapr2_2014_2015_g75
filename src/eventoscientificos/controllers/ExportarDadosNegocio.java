package eventoscientificos.controllers;

import eventoscientificos.domain.Empresa;
import java.io.File;
import utils.CodificacaoAritmetica;
import utils.EscreverXML;

/**
 * Classe ExportarDadosNegocio
 *
 * @author Carlos
 */
public class ExportarDadosNegocio {

    private Empresa m_empresa;
    private EscreverXML escreverXML;

    private static final String NOME_DO_FICHEIRO_UTILIZADORES = "utilizador.xml";
    private static final String NOME_DO_FICHEIRO_LOCAL = "local.xml";
    private static final String NOME_DO_FICHEIRO_EVENTO = "evento.xml";

    /**
     * Construtor de ExportarDadosNegocio
     *
     * @param empresa Empresa
     */
    public ExportarDadosNegocio(Empresa empresa) {
        this.m_empresa = empresa;
        this.escreverXML = new EscreverXML();
    }

    /**
     * Metodo que exporta dados de utilizadores, locais e eventos.
     *
     * @return resultado da exportação em boolean
     */
    public boolean exportarDados() {
        if (!exportarUtilizadores()) {
            System.out.println("erro exportarUtilizadores");
            return false;
        }

        if (!exportarLocais()) {
            System.out.println("erro exportarLocais");
            return false;
        }

        if (!exportarEventos()) {
            System.out.println("erro exportarEventos");
            return false;
        }
        
        File tabela = new File("tabelaFicheiros.csv");
        CodificacaoAritmetica ficheiros = new CodificacaoAritmetica(tabela);
        ficheiros.codificaFicheiro(new File(NOME_DO_FICHEIRO_UTILIZADORES));
        ficheiros.codificaFicheiro(new File(NOME_DO_FICHEIRO_LOCAL));
        ficheiros.codificaFicheiro(new File(NOME_DO_FICHEIRO_EVENTO));
        
        return true;
    }

    /**
     * metodo que escreve em ficheiro xml os utilizadores
     *
     * @return resultado da escrita em boolean
     */
    private boolean exportarUtilizadores() {
        return this.escreverXML.escreverUtilizadorXML(this.m_empresa, ExportarDadosNegocio.NOME_DO_FICHEIRO_UTILIZADORES);
    }

    /**
     * metodo que escreve em ficheiro xml os Locais
     *
     * @return resultado da escrita em boolean
     */
    private boolean exportarLocais() {
        return this.escreverXML.escreverLocalXML(this.m_empresa, ExportarDadosNegocio.NOME_DO_FICHEIRO_LOCAL);
    }

    /**
     * metodo que escreve em ficheiro xml os Eventos
     *
     * @return resultado da escrita em boolean
     */
    private boolean exportarEventos() {
        return this.escreverXML.escreverEventoXML(this.m_empresa, ExportarDadosNegocio.NOME_DO_FICHEIRO_EVENTO);
    }

}
