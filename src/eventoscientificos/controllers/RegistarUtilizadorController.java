package eventoscientificos.controllers;

import eventoscientificos.domain.Empresa;
import eventoscientificos.domain.RegistoUtilizadores;
import eventoscientificos.domain.Utilizador;

/**
 * Classe RegistarUtilizadorController
 * @author Nuno Silva
 * @author 1140958@isep.ipp.pt
 */
public class RegistarUtilizadorController {

    /**
     * Variável de instância que contém um objeto do tipo Empresa (onde será
     * guardado o RegistoUtilizadores)
     */
    private Empresa m_empresa;

    /**
     * Variável de instância que contém um objeto do tipo RegistoUtilizadores
     * (onde será guardado o Utilizador)
     */
    private RegistoUtilizadores m_registo;

    /**
     * Variável de instância do tipo Utilizador (o utilizador a ser registado)
     */
    private Utilizador m_utilizador;

    /**
     * Construtor da classe que, recebendo como parâmetro um objeto do tipo
     * Empresa, atribui a mesma à variável m_empresa da instância e obtém o
     * registo de utlizadores existente nessa empresa, atribuindo-o à variável
     * de instância m_registo
     *
     * @param empresa A empresa onde será feito o registo do utilizador
     */
    public RegistarUtilizadorController(Empresa empresa) {
        m_empresa = empresa;
        m_registo = empresa.getRegistoUtilizadores();
    }

    /**
     * Método de instância sem parâmetros que instancia um novo utilizador e
     * atribui o mesmo à variável m_utilizador da instância
     */
    public void novoUtilizador() {
        m_utilizador = m_registo.novoUtilizador();
    }

    /**
     * Método de instância que, recebendo como parâmetros: username, password,
     * nome e email atribui os mesmos aos dados do Utilizador a registar.
     *
     * @param strUsername O Username do Utilizador
     * @param strPassword A Password do Utilizador
     * @param strNome O Nome do Utilizador
     * @param strEmail O Email do Utilizador
     * @return Devolve o Utilizador com os dados atribuídos.
     */
    public Utilizador setDados(String strUsername, String strPassword, String strNome, String strEmail) {
        m_utilizador = m_registo.novoUtilizador();
        m_utilizador.setUsername(strUsername);

        m_utilizador.setPassword(strPassword);
        m_utilizador.setNome(strNome);
        m_utilizador.setEmail(strEmail);

        if (m_registo.registaUtilizador(m_utilizador)) {
            return m_utilizador;
        } else {
            return null;
        }
    }
}
