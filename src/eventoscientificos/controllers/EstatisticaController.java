package eventoscientificos.controllers;

import eventoscientificos.domain.Empresa;
import eventoscientificos.domain.Evento;
import eventoscientificos.domain.RegistoEventos;
import eventoscientificos.domain.SessaoTematica;
import eventoscientificos.domain.Submissao;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Classe EstatisticaEventoController
 *
 * @author Bruno
 */
public class EstatisticaController {

    private Empresa m_empresa;
    private RegistoEventos m_regEventos;

    public EstatisticaController(Empresa empresa) {
        this.m_empresa = empresa;
        this.m_regEventos = m_empresa.getRegistoEventos();
    }

    public List<Evento> getListaEventosByUser(String idUtilizador){
        return m_regEventos.getListaEventosDoUtilizador(idUtilizador);
    }
    
    public List<Evento> getListaEventos(){
        return m_regEventos.getListaEventos();
    }
    
    public double getTaxaAceitacaoEvento(Evento evento) {
        double count = 0;
        double aceite = 0;

        for (Evento e : m_regEventos.getListaEventos()) {
            for (Submissao sub : e.getSubmissoes()) {
                if (sub.isInAceite()) {
                    aceite++;
                    count++;
                }
                if (sub.isInRejeitada()) {
                    count++;
                }
            }

            for (SessaoTematica st : e.getListaDeSessoesTematicas().getListaDeSessaoTematica()) {
                for (Submissao sub2 : st.getSubmissoes()) {
                    if (sub2.isInAceite()) {
                        aceite++;
                        count++;
                    }
                    if (sub2.isInRejeitada()) {
                        count++;
                    }
                }
            }

        }
        
        PrintWriter writer = null;
            try {
                writer = new PrintWriter("EstatisticaEvento.csv", "UTF-8");
                writer.println(evento.getTitulo() + ";" + (aceite/count) * 100 + "%");
            } catch (FileNotFoundException | UnsupportedEncodingException ex) {
                Logger.getLogger(CarregarArtigosPorFicheiroController.class.getName()).log(Level.SEVERE, null, ex);
            }
            writer.close();
            
        return aceite / count;
    }
}
