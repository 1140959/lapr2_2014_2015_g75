package eventoscientificos.controllers;

import eventoscientificos.domain.Artigo;
import eventoscientificos.domain.Autor;
import eventoscientificos.domain.Empresa;
import eventoscientificos.domain.Evento;
import eventoscientificos.domain.SessaoTematica;
import eventoscientificos.domain.Submissao;
import eventoscientificos.domain.Submissivel;
import eventoscientificos.domain.Utilizador;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Classe SubmeterArtigoController
 *
 * @author Paulo Maio
 */
public class SubmeterArtigoController {

    private Empresa m_empresa;
    private Submissivel m_submissivel;
    private Submissao m_submissao;
    private Artigo m_artigo;

    /**
     * Construtor de SubmeterArtigoController
     *
     * @param empresa Empresa
     */
    public SubmeterArtigoController(Empresa empresa) {
        this.m_empresa = empresa;
    }

    /**
     * Método que retorna uma lista de AutoresCorrespondentes possiveis.
     *
     * @param lista Lista de Autor
     * @return Lista de Autores possiveis.
     */
    public List<Autor> getPossiveisAutoresCorrespondentes(List<Autor> lista) {
        ArrayList<Autor> possiveis = new ArrayList<>();

        for (Autor possivel : lista) {

            if (this.m_empresa.getRegistoUtilizadores().getUtilizadorByEmail(possivel.getEmail()) != null) {
                possiveis.add(possivel);
            }
        }

        return possiveis;
    }

    /**
     * Método que retorna uma lista de Submissiveis.
     *
     * @return lista de submissiveis em submissão.
     */
    public List<Submissivel> getListaSubmissiveisEmSubmissao() {
        return this.m_empresa.getRegistoEventos().getListaSubmissiveisEmSubmissao();
    }

    /**
     * Método que retorna uma lista de eventos em submissão.
     *
     * @return lista de eventos em submissão.
     */
    public List<Evento> getListaEventosEmSubmissao() {
        return this.m_empresa.getRegistoEventos().getListaEventosEmSubmissao();
    }

    /**
     * Método que retorna uma lista de Sessões Temáticas em submissão.
     *
     * @param evento Evento
     * @return lista de Sessões Temáticas em submissão.
     */
    public List<SessaoTematica> getListaSessaoEmSubmissao(Evento evento) {
        return this.m_empresa.getRegistoEventos().getListaSessaoEmSubmissao(evento);
    }

    /**
     * Método que altera o titulo e o resumo do artigo
     *
     * @param strTitulo Titulo do artigo
     * @param strResumo Resumo do artigo
     */
    public void setDados(String strTitulo, String strResumo) {
        this.m_artigo.setTitulo(strTitulo);
        this.m_artigo.setResumo(strResumo);
    }

    /**
     * Método que altera o Autor Correspondente
     *
     * @param autor Autor Correspondente
     */
    public void setCorrespondente(Autor autor) {
        this.m_artigo.setAutorCorrespondente(autor);
    }

    /**
     * Metodo que altera o ficheiro do Artigo
     *
     * @param strFicheiro Ficheiro do artigo
     */
    public void setFicheiro(String strFicheiro) {
        this.m_artigo.setFicheiro(strFicheiro);
    }

    /**
     * Metodo que altera as palavras chave do artigo submetido.
     *
     * @param palavrasChave Palavras chave do artigo.
     */
    public void setPalavrasChave(List<String> palavrasChave) {
        for (String palavra : palavrasChave) {
            this.m_artigo.setPalavraChave(palavra);
        }
    }

    /**
     * Método que permite adicionar um palavra chave ao artigo submetido.
     *
     * @param palavraChave Palavra chave do artigo.
     */
    public void addPalavra(String palavraChave) {
        this.m_artigo.setPalavraChave(palavraChave);
    }

    /**
     * Método que permite alterar a data de submissão do artigo.
     *
     * @param data Data de submissão do artigo.
     */
    public void setData(Date data) {
        this.m_artigo.setDataSubmissao(data);
    }

    /**
     * Metodo que insere o utilizador que submete o artigo.
     *
     * @param utilizador Utilizador
     */
    public void setUtilizador(String utilizador) {
        this.m_artigo.setIdUtilizador(utilizador);
    }

    /**
     * Método que permite selecionar um novo Artigo submissivel.
     *
     * @param est Submissivel
     */
    public void selectSubmissivel(Submissivel est) {
        this.m_submissivel = est;
        this.m_submissao = this.m_submissivel.getListaSubmissoes().novaSubmissao();
        this.m_artigo = this.m_submissao.novoArtigo();
    }

    /**
     * Método que cria um novo Autor.
     *
     * @param strNome Nome do autor.
     * @param strAfiliacao Afilicação do autor.
     * @param strEmail Email do autor.
     * @return do novo autor criado.
     */
    public Autor novoAutor(String strNome, String strAfiliacao, String strEmail) {
        return this.m_artigo.getListaAutores().novoAutor(strNome, strAfiliacao, strEmail);
    }

    /**
     * Método que permite criar um novo Autor.
     *
     * @return novo autor.
     */
    public Autor novoAutor() {
        return m_artigo.getListaAutores().novoAutor();
    }

    /**
     * Método que permite adicionar um autor ao artigo.
     *
     * @param autor o Autor.
     * @return do Autor adicionado.
     */
    public boolean addAutor(Autor autor) {
        return this.m_artigo.getListaAutores().addAutor(autor);
    }

    /**
     * Método que regista a submissão de um artigo.
     *
     * @return resultado da submissão de um artigo.
     */
    public boolean registarSubmissao() {
        m_artigo.setDataSubmissao(Calendar.getInstance().getTime());
        this.m_submissao.setArtigo(m_artigo);

        if (this.m_submissivel.getListaSubmissoes().addSubmissao(m_submissao)) {
            System.out.println("\n\n\n\n\n " + this.m_submissao.setStateSubmetido());
            return this.m_submissao.setStateSubmetido();
        }

        return false;
    }
    
    /**
     * devolve o utilizador atraves do seu id
     * 
     * @param idUtilizador da submissao
     * @return o utilizador
     */
    public Utilizador getUtilizadorByID(String idUtilizador){
        return m_empresa.getRegistoUtilizadores().getUtilizadorByID(idUtilizador);
    }

    /**
     * Método que retorna uma submissão.
     *
     * @return de submissão.
     */
    public Submissao getSubmissao() {
        return this.m_submissao;
    }

}
