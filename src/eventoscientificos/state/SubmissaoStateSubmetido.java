/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.state;

import eventoscientificos.domain.Submissao;

/**
 *
 * @author Carlos
 */
public class SubmissaoStateSubmetido extends SubmissaoStateImpl {

    public SubmissaoStateSubmetido(Submissao e) {
        super(e);
    }

    @Override
    public boolean valida() {
        System.out.println("SubmissaoStateSubmetido: valida:" + getSubmissao().toString());
        return true;
    }

    @Override
    public boolean isInSubmetido() {
        return true;
    }

    @Override
    public boolean setStateSubmetido() {
        return true;
    }

    @Override
    public boolean setStateEmDetecaoConflitos() {
        if (valida()) {
            return getSubmissao().setState(new SubmissaoStateEmDetecaoConflitos(getSubmissao()));
        }
        return false;
    }

    @Override
    public boolean setStateRetirada() {
        if (valida()) {
            return getSubmissao().setState(new SubmissaoStateRetirada(getSubmissao()));
        }
        return false;
    }

    @Override
    public String toString() {
        return "SubmissaoStateSubmetido";
    }

    @Override
    public boolean setCameraReady() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
