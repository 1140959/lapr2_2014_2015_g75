/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.state;

import eventoscientificos.domain.Evento;

/**
 *
 * @author Paulo Maio 
 */
public class EventoStateEmSubmissao extends EventoStateImpl {

    public EventoStateEmSubmissao(Evento e) {
        super(e);
    }

    @Override
    public boolean valida() {
        System.out.println("EventoStateEmSubmissao: valida:" + getEvento().toString());
        return true;
    }

    @Override
    public boolean isInEmSubmissao() {
        return true;
    }

    @Override
    public boolean setStateEmSubmissao() {
        return true;
    }

    @Override
    public boolean setStateEmDetecaoConflitos() {
        if (valida()) {
            return getEvento().setState(new EventoStateEmDetecaoConflitos(getEvento()));
        }
        return false;
    }

    @Override
    public String toString() {
        return "EventoStateEmSubmissao";
    }
    
    @Override
    public boolean setCameraReady() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
