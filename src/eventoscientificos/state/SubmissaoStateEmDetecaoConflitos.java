/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.state;

import eventoscientificos.domain.Submissao;

/**
 *
 * @author Carlos
 */
public class SubmissaoStateEmDetecaoConflitos extends SubmissaoStateImpl {

    public SubmissaoStateEmDetecaoConflitos(Submissao s) {
        super(s);
    }

    @Override
    public boolean valida() {
        System.out.println("SubmissaoStateEmDetecaoConflitos: valida:" + getSubmissao().toString());
        return true;
    }

    @Override
    public boolean isInEmDetecaoConflitos() {
        return true;
    }

    @Override
    public boolean setStateEmDetecaoConflitos() {
        return true;
    }

    @Override
    public boolean setStateEmLicitacao() {
        if (valida()) {
            return getSubmissao().setState(new SubmissaoStateEmLicitacao(getSubmissao()));
        }
        return false;
    }
    
    @Override
    public boolean setStateRetirada() {
        if (valida()) {
            return getSubmissao().setState(new SubmissaoStateRetirada(getSubmissao()));
        }
        return false;
    }

    @Override
    public String toString() {
        return "SubmissaoStateEmDetecaoConflitos";
    }
    
    @Override
    public boolean setCameraReady() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
