/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.state;

/**
 *
 * @author Carlos
 */
public interface SubmissaoState {

    boolean valida();

    boolean isInCriado();
    
    boolean isInSubmetido();

    boolean isInEmDetecaoConflitos();

    boolean isInEmLicitacao();

    boolean isInEmDistribuicao();

    boolean isInEmRevisao();

    boolean isInEmDecisao();

    boolean isInAceite();
    
    boolean isInRejeitada();
    
    boolean isInRetirada();
    
    boolean isInCameraReady();

    boolean setStateSubmetido();

    boolean setStateEmDetecaoConflitos();

    boolean setStateEmLicitacao();

    boolean setStateEmDistribuicao();

    boolean setStateEmRevisao();

    boolean setStateEmDecisao();

    boolean setStateAceite();
    
    boolean setStateRejeitada();
    
    boolean setStateRetirada();

    boolean setCameraReady();

}
