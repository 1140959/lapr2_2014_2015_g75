/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.state;

import eventoscientificos.domain.Submissao;

/**
 *
 * @author Carlos
 */
public class SubmissaoStateCriada extends SubmissaoStateImpl {

    public SubmissaoStateCriada(Submissao s) {
        super(s);
        s.setState(this);
    }

    @Override
    public boolean valida() {
        System.out.println("SubmissaoStateCriada: valida:" + getSubmissao().toString());
        return true;
    }

    @Override
    public boolean setStateSubmetido() {
        if (valida()) {
            return getSubmissao().setState(new SubmissaoStateSubmetido(getSubmissao()));
        }
        return false;
    }
    

    @Override
    public String toString() {
        return "SubmissaoStateCriada";
    }
    
    @Override
    public boolean setCameraReady() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
