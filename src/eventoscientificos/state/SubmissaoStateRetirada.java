/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.state;

import eventoscientificos.domain.Submissao;

/**
 *
 * @author Carlos
 */
public class SubmissaoStateRetirada extends SubmissaoStateImpl{

    public SubmissaoStateRetirada(Submissao s) {
        super(s);
    }

    @Override
    public boolean valida() {
        System.out.println("SubmissaoStateRetirada: valida:" + getSubmissao().toString());
        return true;
    }
    
    @Override
    public boolean isInRetirada() {
        return true;
    }
    
    @Override
    public boolean setStateRetirada() {
        return true;
    }
    
    @Override
    public String toString() {
        return "SubmissaoStateRetirada";
    }
    
}
