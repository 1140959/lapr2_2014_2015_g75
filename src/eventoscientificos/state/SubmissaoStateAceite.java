/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.state;

import eventoscientificos.domain.Submissao;

/**
 *
 * @author Carlos
 */
public class SubmissaoStateAceite extends SubmissaoStateImpl {

    public SubmissaoStateAceite(Submissao s) {
        super(s);
    }

    @Override
    public boolean valida() {
        System.out.println("SubmissaoStateAceite: valida:" + getSubmissao().toString());
        return true;
    }

    @Override
    public boolean isInAceite() {
        return true;
    }

    @Override
    public boolean setStateAceite() {
        return true;
    }

    @Override
    public boolean setCameraReady() {
        if (valida()) {
            return getSubmissao().setState(new SubmissaoStateCameraReady(getSubmissao()));
        }
        return false;
    }
    
    @Override
    public String toString() {
        return "SubmissaoStateAceite";
    }

}
