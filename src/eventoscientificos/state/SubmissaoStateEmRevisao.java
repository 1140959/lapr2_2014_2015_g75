/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.state;

import eventoscientificos.domain.Submissao;

/**
 *
 * @author Carlos
 */
public class SubmissaoStateEmRevisao extends SubmissaoStateImpl {

    public SubmissaoStateEmRevisao(Submissao s) {
        super(s);
    }

    @Override
    public boolean valida() {
        System.out.println("SubmissaoStateEmRevisao: valida:" + getSubmissao().toString());
        return true;
    }
    
    @Override
    public boolean isInEmRevisao() {
        return true;
    }

    @Override
    public boolean setStateEmRevisao() {
        return true;
    }
    
    @Override
    public boolean setStateEmDecisao() {
        if (valida()) {
            return getSubmissao().setState(new SubmissaoStateEmDecisao(getSubmissao()));
        }
        return false;
    }
    
    @Override
    public boolean setStateRetirada() {
        if (valida()) {
            return getSubmissao().setState(new SubmissaoStateRetirada(getSubmissao()));
        }
        return false;
    }

    @Override
    public String toString() {
        return "SubmissaoStateEmRevisao";
    }
    
    @Override
    public boolean setCameraReady() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
