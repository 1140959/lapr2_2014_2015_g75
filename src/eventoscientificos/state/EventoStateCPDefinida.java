/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.state;

import eventoscientificos.domain.Evento;

/**
 *
 * @author Paulo Maio 
 */
public class EventoStateCPDefinida extends EventoStateImpl {

    public EventoStateCPDefinida(Evento e) {
        super(e);
    }
    
    @Override
    public boolean valida() {
        System.out.println("EventoStateCPDefinida: valida:" + getEvento().toString());
        return true;
    }

    @Override
    public boolean isInCPDefinida() {
        return true;
    }

    @Override
    public boolean setStateCPDefinida() {
        return true;
    }

    @Override
    public boolean setStateEmSubmissao() {
        if (valida()) {
            return getEvento().setState(new EventoStateEmSubmissao(getEvento()));
        }
        return false;
    }

    @Override
    public String toString() {
        return "EventoStateCPDefinida";
    }
    
    @Override
    public boolean setCameraReady() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
