/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.state;

import eventoscientificos.domain.Submissao;

/**
 *
 * @author Carlos
 */
public class SubmissaoStateEmLicitacao extends SubmissaoStateImpl {

    public SubmissaoStateEmLicitacao(Submissao s) {
        super(s);
    }

    @Override
    public boolean valida() {
        System.out.println("SubmissaoStateEmLicitacao: valida:" + getSubmissao().toString());
        return true;
    }

    @Override
    public boolean isInEmLicitacao() {
        return true;
    }

    @Override
    public boolean setStateEmLicitacao() {
        return true;
    }
    
    @Override
    public boolean setStateEmDistribuicao() {
        if (valida()) {
            return getSubmissao().setState(new SubmissaoStateEmDistribuicao(getSubmissao()));
        }
        return false;
    }
    
    @Override
    public boolean setStateRetirada() {
        if (valida()) {
            return getSubmissao().setState(new SubmissaoStateRetirada(getSubmissao()));
        }
        return false;
    }

    @Override
    public String toString() {
        return "SubmissaoStateEmLicitacao";
    }
    
    @Override
    public boolean setCameraReady() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
