/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.state;

import eventoscientificos.domain.Submissao;

/**
 *
 * @author Carlos
 */
public class SubmissaoStateCameraReady extends SubmissaoStateImpl {

    public SubmissaoStateCameraReady(Submissao s) {
        super(s);
    }

    @Override
    public boolean valida() {
        System.out.println("SubmissaoStateCameraReady: valida:" + getSubmissao().toString());
        return true;
    }

    @Override
    public boolean isInCameraReady() {
        return true;
    }

    @Override
    public boolean setCameraReady() {
        return true;
    }
    
    @Override
    public String toString() {
        return "SubmissaoStateCameraReady";
    }

}
