/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.state;

import eventoscientificos.domain.SessaoTematica;

/**
 *
 * @author Paulo Maio 
 */
public class STStateCriada extends STStateImpl {

    public STStateCriada(SessaoTematica st) {
        super(st);
        st.setState(this);
    }

    @Override
    public boolean valida() {
        System.out.println("STStateCriada: valida:" + getSessaoTematica().toString());
        return true;
    }

    @Override
    public boolean setStateRegistado() {
        if (valida()) {
            return getSessaoTematica().setState(new STStateRegistado(getSessaoTematica()));
        }
        return false;
    }

    @Override
    public String toString() {
        return "STStateCriada";
    }

}
