/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.state;

import eventoscientificos.domain.Submissao;

/**
 *
 * @author Carlos
 */
public abstract class SubmissaoStateImpl implements SubmissaoState {

    private Submissao m_submissao;

    public SubmissaoStateImpl(Submissao e) {
        m_submissao = e;
    }

    public Submissao getSubmissao() {
        return this.m_submissao;
    }

    @Override
    public boolean isInCriado() {
        return false;
    }

    @Override
    public boolean isInSubmetido() {
        return false;
    }

    @Override
    public boolean isInEmDetecaoConflitos() {
        return false;
    }

    @Override
    public boolean isInEmLicitacao() {
        return false;
    }

    @Override
    public boolean isInEmDistribuicao() {
        return false;
    }

    @Override
    public boolean isInEmRevisao() {
        return false;
    }

    @Override
    public boolean isInEmDecisao() {
        return false;
    }

    @Override
    public boolean isInAceite() {
        return false;
    }
    
    @Override
    public boolean isInRejeitada() {
        return false;
    }
    
    @Override
    public boolean isInRetirada() {
        return false;
    }
    
    @Override
    public boolean isInCameraReady() {
        return false;
    }

    @Override
    public boolean setStateSubmetido() {
        return false;
    }

    @Override
    public boolean setStateEmDetecaoConflitos() {
        return false;
    }

    @Override
    public boolean setStateEmLicitacao() {
        return false;
    }

    @Override
    public boolean setStateEmDistribuicao() {
        return false;
    }

    @Override
    public boolean setStateEmRevisao() {
        return false;
    }

    @Override
    public boolean setStateEmDecisao() {
        return false;
    }

    @Override
    public boolean setStateAceite() {
        return false;
    }
    
    @Override
    public boolean setStateRejeitada() {
        return false;
    }

    @Override
    public boolean setStateRetirada() {
        return false;
    }

    @Override
    public boolean setCameraReady() {
        return false;
    }

}
