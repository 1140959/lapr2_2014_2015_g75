/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.state;

import eventoscientificos.domain.Submissao;

/**
 *
 * @author Carlos
 */
public class SubmissaoStateEmDecisao extends SubmissaoStateImpl {

    public SubmissaoStateEmDecisao(Submissao s) {
        super(s);
    }

    @Override
    public boolean valida() {
        System.out.println("SubmissaoStateEmDecisao: valida:" + getSubmissao().toString());
        return true;
    }

    @Override
    public boolean isInEmDecisao() {
        return true;
    }

    @Override
    public boolean setStateEmDecisao() {
        return true;
    }

    @Override
    public boolean setStateAceite() {
        if (valida()) {
            return getSubmissao().setState(new SubmissaoStateAceite(getSubmissao()));
        }
        return false;
    }
    
    @Override
    public boolean setStateRejeitada() {
        if (valida()) {
            return getSubmissao().setState(new SubmissaoStateRejeitada(getSubmissao()));
        }
        return false;
    }
    
    @Override
    public boolean setStateRetirada() {
        if (valida()) {
            return getSubmissao().setState(new SubmissaoStateRetirada(getSubmissao()));
        }
        return false;
    }

    @Override
    public String toString() {
        return "SubmissaoStateEmDecisao";
    }
    
    @Override
    public boolean setCameraReady() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
