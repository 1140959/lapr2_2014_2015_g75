/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.state;

import eventoscientificos.domain.SessaoTematica;

/**
 *
 * @author Paulo Maio 
 */
public class STStateEmDistribuicao extends STStateImpl {

    public STStateEmDistribuicao(SessaoTematica st) {
        super(st);
    }

    @Override
    public boolean valida() {
        System.out.println("STStateEmDistribuicao: valida:" + getSessaoTematica().toString());
        return true;
    }

    @Override
    public boolean setStateEmDistribuicao() {
        return true;
    }

    @Override
    public boolean isInEmDistribuicao() {
        return true;
    }

    @Override
    public boolean setStateEmRevisao() {
        if (valida()) {
            return getSessaoTematica().setState(new STStateEmRevisao(getSessaoTematica()));
        }
        return false;
    }

    @Override
    public String toString() {
        return "STStateEmDistribuicao";
    }
    
}
