/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.state;

import eventoscientificos.domain.Evento;

/**
 *
 * @author Paulo Maio 
 */
public class EventoStateRegistado extends EventoStateImpl {

    public EventoStateRegistado(Evento e) {
        super(e);
    }

    @Override
    public boolean valida() {
        System.out.println("EventoStateRegistado: valida:" + getEvento().toString());
        return true;
    }

    @Override
    public boolean setStateRegistado() {
        return true;
    }

    @Override
    public boolean isInRegistado() {
        return true;
    }

    @Override
    public boolean setStateSTDefinidas() {
        if (valida()) {
            return getEvento().setState(new EventoStateSTDefinidas(getEvento()));
        }
        return false;
    }

    @Override
    public String toString() {
        return "EventoStateRegistado";
    }
    
    @Override
    public boolean setCameraReady() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
