/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.state;

import eventoscientificos.domain.Submissao;

/**
 *
 * @author Carlos
 */
public class SubmissaoStateRejeitada extends SubmissaoStateImpl {

    public SubmissaoStateRejeitada(Submissao s) {
        super(s);
    }

    @Override
    public boolean isInRejeitada() {
        return true;
    }

    @Override
    public boolean setStateRejeitada() {
        return true;
    }

    @Override
    public boolean valida() {
        System.out.println("SubmissaoStateRejeitada: valida:" + getSubmissao().toString());
        return true;
    }

    @Override
    public String toString() {
        return "SubmissaoStateRejeitada";
    }
}
