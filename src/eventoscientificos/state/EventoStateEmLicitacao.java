/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.state;

import eventoscientificos.domain.Evento;

/**
 *
 * @author Paulo Maio 
 */
public class EventoStateEmLicitacao extends EventoStateImpl {

    public EventoStateEmLicitacao(Evento e) {
        super(e);
    }

    @Override
    public boolean valida() {
        System.out.println("EventoStateEmLicitacao: valida:" + getEvento().toString());
        return true;
    }

    @Override
    public boolean isInEmLicitacao() {
        return true;
    }

    @Override
    public boolean setStateEmLicitacao() {
        return true;
    }

    @Override
    public boolean setStateEmDistribuicao() {
        if (valida()) {
            return getEvento().setState(new EventoStateEmDistribuicao(getEvento()));
        }
        return false;
    }

    @Override
    public String toString() {
        return "EventoStateEmLicitacao";
    }

    @Override
    public boolean setCameraReady() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
