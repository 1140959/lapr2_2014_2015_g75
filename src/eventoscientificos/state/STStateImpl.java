/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.state;

import eventoscientificos.domain.SessaoTematica;

/**
 *
 * @author Paulo Maio 
 */
public abstract class STStateImpl implements STState
{
    private SessaoTematica m_st;
    public STStateImpl(SessaoTematica st)
    {
        this.m_st = st;
    }

    public SessaoTematica getSessaoTematica()
    {
        return this.m_st;
    }
    
    @Override
    public boolean isInRegistado()
    {
        return false;
    }

    @Override
    public boolean isInCPDefinida()
    {
        return false;
    }

    @Override
    public boolean isInEmSubmissao()
    {
       return false;
    }

    @Override
    public boolean isInEmDetecaoConflitos()
    {
        return false;
    }

    @Override
    public boolean isInEmLicitacao()
    {
       return false;
    }

    @Override
    public boolean isInEmDistribuicao()
    {
       return false;
    }

    @Override
    public boolean isInEmRevisao()
    {
        return false;
    }

    @Override
    public boolean isInEmDecisao()
    {
        return false;
    }

    @Override
    public boolean isInEmDecidido()
    {
        return false;
    }

    @Override
    public boolean setStateRegistado()
    {
        return false;
    }

    @Override
    public boolean setStateCPDefinida()
    {
        return false;
    }

    @Override
    public boolean setStateEmSubmissao()
    {
        return false;
    }

    @Override
    public boolean setStateEmDetecaoConflitos()
    {
        return false;
    }

    @Override
    public boolean setStateEmLicitacao()
    {
        return false;
    }

    @Override
    public boolean setStateEmDistribuicao()
    {
        return false;
    }

    @Override
    public boolean setStateEmRevisao()
    {
        return false;
    }

    @Override
    public boolean setStateEmDecisao()
    {
        return false;
    }

    @Override
    public boolean setStateEmDecidido()
    {
        return false;
    }
    
}
