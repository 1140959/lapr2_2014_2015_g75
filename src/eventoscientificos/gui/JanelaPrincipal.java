/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.gui;

import eventoscientificos.controllers.ExportarDadosNegocio;
import java.awt.CardLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import eventoscientificos.domain.Empresa;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.JOptionPane;

/**
 *
 * @author David
 */
public class JanelaPrincipal extends JFrame {

    private Empresa m_empresa;
    private InicioGUI pUCi;
    private CriarEventoGUI pUC2;
    private DefinirCPGUI pUC3;
    private SubmeterArtigoGUI pUC4;
    private ReverArtigoGUI pUC5;
    private CriarSessaoTematicaGUI pUC6;
    private AlterarUtilizadorGUI pUC9;
    private AlterarSubmissaoGUI pUC10;
    private SubmeterArtigoFinalGUI pUC15;
    private RetirarSubmissaoGUI pU16;
    private ListarSubmissoesRetiradasGUI pU17;
    private CarregarArtigosPorFicheiroGUI pU18;
    private EstatisticaEventoGUI pU19A;
    private JPanel cardPanel;
    private CardLayout cardLayout;
    private String idUtilizador;

    public JanelaPrincipal(Empresa empresa, String idUtilizador) {
        super("Eventos cientificos");
        this.idUtilizador = idUtilizador;
        this.m_empresa = empresa;

        // Instanciação de Variaveis
        this.pUCi = new InicioGUI();
        this.pUC2 = new CriarEventoGUI(m_empresa);
        this.pUC3 = new DefinirCPGUI(m_empresa, this.idUtilizador);
        this.pUC4 = new SubmeterArtigoGUI(m_empresa, this.idUtilizador);
        this.pUC5 = new ReverArtigoGUI(m_empresa, this.idUtilizador);
        this.pUC6 = new CriarSessaoTematicaGUI(m_empresa, this.idUtilizador);
        this.pUC9 = new AlterarUtilizadorGUI(this.idUtilizador, m_empresa);
        this.pUC10 = new AlterarSubmissaoGUI(this.idUtilizador, m_empresa);
        this.pUC15 = new SubmeterArtigoFinalGUI(this.idUtilizador, m_empresa);
        this.pU16 = new RetirarSubmissaoGUI(m_empresa, this.idUtilizador);
        this.pU17 = new ListarSubmissoesRetiradasGUI(this.m_empresa, this.idUtilizador);
        this.pU18 = new CarregarArtigosPorFicheiroGUI(this.m_empresa, this.idUtilizador);
        this.pU19A = new EstatisticaEventoGUI(this.m_empresa, this.idUtilizador);
        cardLayout = new CardLayout();

        // Adições ao Frame
        this.setJMenuBar(criarMenu());

        this.cardPanel = new JPanel(cardLayout);
        this.cardPanel.add(pUCi, "-1");
        this.cardPanel.add(pUC2, "2");
        this.cardPanel.add(pUC3, "3");
        this.cardPanel.add(pUC4, "4");
        this.cardPanel.add(pUC5, "5");
        this.cardPanel.add(pUC6, "6");
        this.cardPanel.add(pUC9, "9");
        this.cardPanel.add(pUC10, "10");
        this.cardPanel.add(pUC15, "15");
        this.cardPanel.add(pU16, "16");
        this.cardPanel.add(pU17, "17");
        this.cardPanel.add(pU18, "18");
        this.cardPanel.add(pU19A, "19");

        this.add(this.cardPanel);
        this.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);

        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                Object[] options = {"Sim",
                    "Não"};
                int n = JOptionPane.showOptionDialog(JanelaPrincipal.this,
                        "Deseja guardar os dados inseridos?",
                        "Aviso!",
                        JOptionPane.YES_NO_OPTION,
                        JOptionPane.WARNING_MESSAGE,
                        null,
                        options,
                        options[0]);

                if (n == 0) {
                    ExportarDadosNegocio edn = new ExportarDadosNegocio(m_empresa);
                    edn.exportarDados();
                }
                System.exit(0);
            }
        });

        this.setSize(700, 720);
        this.setResizable(false);
        this.setLocationRelativeTo(null);
        this.setVisible(true);
    }

    private JMenuBar criarMenu() {
        JMenuBar menuBar = new JMenuBar();

        JMenu utilizador = new JMenu("Utilizador");
        JMenuItem itemlogout = new JMenuItem("Logout");
        itemlogout.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                new JanelaLogin(m_empresa, false);
                dispose();
            }
        });
        utilizador.add(itemlogout);

        JMenuItem itemAlterarUtilizador = new JMenuItem("Alterar Utilizador");
        itemAlterarUtilizador.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent ae) {
                cardLayout.show(cardPanel, "9");
            }
        });
        utilizador.add(itemAlterarUtilizador);

        /*Evento*/
        JMenu evento = new JMenu("Evento");
        JMenuItem itemCriarEvento = new JMenuItem("Criar Evento");
        itemCriarEvento.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent ae) {

                pUC2 = new CriarEventoGUI(m_empresa);
                cardPanel.add(pUC2, "2");
                cardLayout.show(cardPanel, "2");
            }
        });

        if (!m_empresa.getListaAdministradores().isAdministrador(idUtilizador)) {
            itemCriarEvento.setEnabled(false);
        }

        evento.add(itemCriarEvento);

        /*Sessão Temática*/
        JMenu sessao = new JMenu("Sessao Tematica");
        JMenuItem itemCriarST = new JMenuItem("Criar Sessão Temática");
        itemCriarST.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent ae) {
                pUC6 = new CriarSessaoTematicaGUI(m_empresa, idUtilizador);
                cardPanel.add(pUC6, "6");
                cardLayout.show(cardPanel, "6");
            }
        });
        sessao.add(itemCriarST);

        /*Definir CP*/
        JMenu cp = new JMenu("CP");
        JMenuItem itemDefinirCP = new JMenuItem("CP");
        itemDefinirCP.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent ae) {
                pUC3 = new DefinirCPGUI(m_empresa, idUtilizador);
                cardPanel.add(pUC3, "3");
                cardLayout.show(cardPanel, "3");
            }
        });
        cp.add(itemDefinirCP);

        /*Artigo*/
        JMenu artigo = new JMenu("Artigo");

        JMenuItem itemSubmissao = new JMenuItem("Submeter");
        itemSubmissao.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                pUC4 = new SubmeterArtigoGUI(m_empresa, idUtilizador);
                cardPanel.add(pUC4, "4");
                cardLayout.show(cardPanel, "4");
            }
        });

        JMenuItem itemSubmissaoFinal = new JMenuItem("Submeter Artigo Final");
        itemSubmissaoFinal.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                pUC15 = new SubmeterArtigoFinalGUI(idUtilizador, m_empresa);
                cardPanel.add(pUC15, "15");
                cardLayout.show(cardPanel, "15");
            }
        });

        JMenuItem itemAlterar = new JMenuItem("Alterar Submissão");
        itemAlterar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                pUC10 = new AlterarSubmissaoGUI(idUtilizador, m_empresa);
                cardPanel.add(pUC10, "10");
                cardLayout.show(cardPanel, "10");
            }
        });

        JMenuItem itemRemover = new JMenuItem("Remover Submissão");
        itemRemover.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                cardLayout.show(cardPanel, "16");
            }
        });

        JMenuItem itemRever = new JMenuItem("ReverArtigo");
        itemRever.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent ae) {
                cardLayout.show(cardPanel, "5");
            }
        });

        JMenuItem itemListarRetiradas = new JMenuItem("Listar Submissoes Retiradas");
        itemListarRetiradas.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent ae) {
                cardLayout.show(cardPanel, "17");
            }
        });

        JMenuItem itemCarregarCSV = new JMenuItem("Carregar Artigos Por Ficheiro");
        itemCarregarCSV.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent ae) {
                cardLayout.show(cardPanel, "18");
            }
        });
        if (!m_empresa.getListaAdministradores().isAdministrador(idUtilizador)) {
            itemCarregarCSV.setEnabled(false);
        }

        artigo.add(itemSubmissao);
        artigo.add(itemSubmissaoFinal);
        artigo.add(itemAlterar);
        artigo.add(itemRemover);
        artigo.add(itemRever);
        artigo.add(itemListarRetiradas);
        artigo.add(itemCarregarCSV);
        /*Fim do Artigo*/
        
        // ESTATISTICAS
        JMenu estatisticas = new JMenu("Estatisticas");
        JMenuItem itemEstatisticaEvento = new JMenuItem("Gerar para Evento");
        itemEstatisticaEvento.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent ae) {
                pU19A = new EstatisticaEventoGUI(m_empresa, idUtilizador);
                cardPanel.add(pU19A, "19");
                cardLayout.show(cardPanel, "19");
            }
        });
        estatisticas.add(itemEstatisticaEvento);

        /*Adicionar menus na JMenuBar*/
        menuBar.add(utilizador);
        menuBar.add(evento);
        menuBar.add(sessao);
        menuBar.add(cp);
        menuBar.add(artigo);
        menuBar.add(estatisticas);

        return menuBar;
    }

}
