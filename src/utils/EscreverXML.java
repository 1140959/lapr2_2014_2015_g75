/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import eventoscientificos.domain.Artigo;
import eventoscientificos.domain.Autor;
import eventoscientificos.domain.Empresa;
import eventoscientificos.domain.Evento;
import eventoscientificos.domain.Organizador;
import eventoscientificos.domain.Proponente;
import eventoscientificos.domain.RegistoEventos;
import eventoscientificos.domain.RegistoUtilizadores;
import eventoscientificos.domain.Revisor;
import eventoscientificos.domain.SessaoTematica;
import eventoscientificos.domain.Submissao;
import eventoscientificos.domain.Utilizador;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 *
 * @author Carlos
 */
public class EscreverXML {

    private Empresa m_empresa;
    private List<String> local = new ArrayList<>();
    private SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");

    public boolean escreverUtilizadorXML(Empresa empresa, String caminho) {
        this.m_empresa = empresa;
        RegistoUtilizadores r = m_empresa.getRegistoUtilizadores();
        try {
            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

            // root elements
            Document doc = docBuilder.newDocument();
            Element rootElement = doc.createElement("ListaUtilizadores");
            doc.appendChild(rootElement);

            int numeroUtilizadores = 0;
            for (Utilizador u : r.getListaUtilizadores()) {
                if (!empresa.getListaAdministradores().isAdministrador(u.getUsername())) {
                    numeroUtilizadores++;
                }
            }

            Element numeroUtilizador = doc.createElement("NumeroElementos");
            numeroUtilizador.appendChild(doc.createTextNode(Integer.toString(numeroUtilizadores)));
            rootElement.appendChild(numeroUtilizador);

            Element utilizadorUtilizadores = doc.createElement("Utilizadores");
            rootElement.appendChild(utilizadorUtilizadores);

            if (r.getListaUtilizadores().size() > 0) {

                for (Utilizador u : r.getListaUtilizadores()) {
                    if (!empresa.getListaAdministradores().isAdministrador(u.getUsername())) {
                        tagsUtilizador(doc, utilizadorUtilizadores, u);
                    }
                }
            }

            doc.normalizeDocument();

            // write the content into xml file
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource source = new DOMSource(doc);
            StreamResult result = new StreamResult(new File(caminho));

            // Output to console for testing
            // StreamResult result = new StreamResult(System.out);
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.transform(source, result);

            System.out.println("File saved!");
            return true;
        } catch (ParserConfigurationException | TransformerException pce) {
            return false;
        }
    }

    public boolean escreverLocalXML(Empresa empresa, String caminho) {
        this.m_empresa = empresa;
        RegistoEventos r = m_empresa.getRegistoEventos();

        local = new ArrayList<>();
        for (Evento e : empresa.getRegistoEventos().getListaEventos()) {
            if (!local.contains(e.getLocal())) {
                local.add(e.getLocal());
            }
        }

        try {
            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

            // root elements
            Document doc = docBuilder.newDocument();
            Element rootElement = doc.createElement("ListaLocais");
            doc.appendChild(rootElement);

            Element numeroLocal = doc.createElement("NumElementos");
            numeroLocal.appendChild(doc.createTextNode(Integer.toString(local.size())));
            rootElement.appendChild(numeroLocal);

            Element locais = doc.createElement("Locais");
            rootElement.appendChild(locais);

            if (local.size() > 0) {
                for (int i = 0; i < local.size(); i++) {
                    tagsLocal(doc, locais, local, i);
                }
            }

            doc.normalizeDocument();

            // write the content into xml file
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource source = new DOMSource(doc);
            StreamResult result = new StreamResult(new File(caminho));

            // Output to console for testing
            // StreamResult result = new StreamResult(System.out);
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.transform(source, result);

            System.out.println("File saved!");

            return true;
        } catch (ParserConfigurationException | TransformerException pce) {
            return false;
        }
    }

    public boolean escreverEventoXML(Empresa empresa, String caminho) {
        this.m_empresa = empresa;
        RegistoEventos r = m_empresa.getRegistoEventos();
        try {
            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

            // root elements
            Document doc = docBuilder.newDocument();
            Element rootElement = doc.createElement("ListaEventos");
            doc.appendChild(rootElement);

            Element numeroEventos = doc.createElement("NumeroEventos");
            numeroEventos.appendChild(doc.createTextNode(Integer.toString(r.getListaEventos().size())));
            rootElement.appendChild(numeroEventos);

            Element eventos = doc.createElement("Eventos");
            rootElement.appendChild(eventos);

            if (r.getListaEventos().size() > 0) {

                for (Evento e : m_empresa.getRegistoEventos().getListaEventos()) {
                    tagsEvento(doc, eventos, e);
                }
            }

            doc.normalize();

            // write the content into xml file
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource source = new DOMSource(doc);
            StreamResult result = new StreamResult(new File(caminho));

            // Output to console for testing
            // StreamResult result = new StreamResult(System.out);
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.transform(source, result);

            System.out.println("File saved!");

            return true;
        } catch (ParserConfigurationException | TransformerException pce) {
            return false;
        }
    }

    private void tagsUtilizador(Document doc, Element utilizadorUtilizadores, Utilizador u) {
        // Utilizador
        Element utilizador = doc.createElement("Utilizador");
        utilizadorUtilizadores.appendChild(utilizador);

        // Username
        Element username = doc.createElement("Username");
        username.appendChild(doc.createTextNode(u.getUsername()));
        utilizador.appendChild(username);

        // Nome
        Element nome = doc.createElement("Nome");
        nome.appendChild(doc.createTextNode(u.getNome()));
        utilizador.appendChild(nome);

        // Email
        Element email = doc.createElement("Email");
        email.appendChild(doc.createTextNode(u.getEmail()));
        utilizador.appendChild(email);

        // Password
        Element password = doc.createElement("Password");
        password.appendChild(doc.createTextNode(u.getPwdCodificada()));
        utilizador.appendChild(password);
    }

    private void tagsLocal(Document doc, Element locais, List<String> local, int i) {
        // Local
        Element localtag = doc.createElement("Local");
        locais.appendChild(localtag);

        // LocalID
        Element localID = doc.createElement("LocalID");
        localID.appendChild(doc.createTextNode(Integer.toString(i)));
        localtag.appendChild(localID);

        // Designacao
        Element designacao = doc.createElement("Designacao");
        designacao.appendChild(doc.createTextNode(local.get(i)));
        localtag.appendChild(designacao);
    }

    private void tagsEvento(Document doc, Element eventos, Evento e) {
        // Evento
        Element evento = doc.createElement("Evento");
        eventos.appendChild(evento);

        // Titulo
        Element eventoTitulo = doc.createElement("Titulo");
        eventoTitulo.appendChild(doc.createTextNode(e.getTitulo()));
        evento.appendChild(eventoTitulo);

        // Descricao
        Element eventoDescricao = doc.createElement("Descricao");
        eventoDescricao.appendChild(doc.createTextNode(e.getDescricao()));
        evento.appendChild(eventoDescricao);

        // Data Inicio
        Element eventoDataInicio = doc.createElement("DataInicio");
        eventoDataInicio.appendChild(doc.createTextNode(sdf.format(e.getDataInicio())));
        evento.appendChild(eventoDataInicio);

        // Data Fim
        Element eventoDataFim = doc.createElement("DataFim");
        eventoDataFim.appendChild(doc.createTextNode(sdf.format(e.getDataFim())));
        evento.appendChild(eventoDataFim);

        // Data Inicio Submissao
        Element eventoDataSubmissao = doc.createElement("DataInicioSubmissao");
        eventoDataSubmissao.appendChild(doc.createTextNode(sdf.format(e.getDataInicioSubmissao())));
        evento.appendChild(eventoDataSubmissao);

        // Data Fim Submissao
        Element eventoDataFimSubmissao = doc.createElement("DataFimSubmissao");
        eventoDataFimSubmissao.appendChild(doc.createTextNode(sdf.format(e.getDataFimSubmissao())));
        evento.appendChild(eventoDataFimSubmissao);

        // Data Inicio Distribuicao
        Element eventoDataInicioDistribuicao = doc.createElement("DataInicioDistribuicao");
        eventoDataInicioDistribuicao.appendChild(doc.createTextNode(sdf.format(e.getDataInicioDistribuicao())));
        evento.appendChild(eventoDataInicioDistribuicao);

        // Data Limite Revisao
        Element eventoDataLimiteRevisao = doc.createElement("DataLimiteRevisao");
        eventoDataLimiteRevisao.appendChild(doc.createTextNode(sdf.format(e.getDataLimiteRevisao())));
        evento.appendChild(eventoDataLimiteRevisao);

        // Data Limite Submissao Final
        Element eventoDataLimiteSubmissaoFinal = doc.createElement("DataLmiteSubmissaoFinal");
        eventoDataLimiteSubmissaoFinal.appendChild(doc.createTextNode(sdf.format(e.getDataLimiteSubmissaoFinal())));
        evento.appendChild(eventoDataLimiteSubmissaoFinal);

        // Estado Evento
        Element eventoEstadoEvento = doc.createElement("EstadoEvento");
        eventoEstadoEvento.appendChild(doc.createTextNode(e.getState().toString()));
        evento.appendChild(eventoEstadoEvento);

        // Local
        Element eventoLocal = doc.createElement("Local");
        eventoLocal.appendChild(doc.createTextNode(Integer.toString(local.indexOf(e.getLocal().toString()))));
        evento.appendChild(eventoLocal);

        // <ListaOrganizadores>
        Element eventoListaOrganizadores = doc.createElement("ListaOrganizadores");
        evento.appendChild(eventoListaOrganizadores);

        // Numero de Organizadores
        Element eventoNumeroOrganizadores = doc.createElement("NumeroOrganizadores");
        eventoNumeroOrganizadores.appendChild(doc.createTextNode(Integer.toString(e.getListaOrganizadores().getListaOrganizadores().size())));
        eventoListaOrganizadores.appendChild(eventoNumeroOrganizadores);

        // <Organizadores>
        Element eventoOrganizadores = doc.createElement("Organizadores");
        eventoListaOrganizadores.appendChild(eventoOrganizadores);

        for (Organizador organizador : e.getListaOrganizadores().getListaOrganizadores()) {
            // Organizador
            Element eventoOrganizador = doc.createElement("Organizador");
            eventoOrganizador.appendChild(doc.createTextNode(organizador.getUtilizador().getUsername()));
            eventoOrganizadores.appendChild(eventoOrganizador);
        }

        // Existe CP
        if (getValorEventoState(e.getState().toString()) > 2) {
            // <ListaOrganizadores>
            Element eventoCP = doc.createElement("CP");
            evento.appendChild(eventoCP);

            // Numero de Organizadores
            Element eventoNumeroMembrosCP = doc.createElement("NumeroMembrosCP");
            eventoNumeroMembrosCP.appendChild(doc.createTextNode(Integer.toString(e.getRevisores().size())));
            eventoCP.appendChild(eventoNumeroMembrosCP);

            // MembrosCP
            Element eventoMembrosCP = doc.createElement("MembrosCP");
            eventoCP.appendChild(eventoMembrosCP);

            for (Revisor revisor : e.getRevisores()) {
                // MembroCP
                Element eventoMembroCP = doc.createElement("MembroCP");
                eventoMembroCP.appendChild(doc.createTextNode(revisor.getUtilizador().getUsername()));
                eventoMembrosCP.appendChild(eventoMembroCP);
            }
        } else {
            // <ListaOrganizadores>
            Element eventoCP = doc.createElement("CP");
            evento.appendChild(eventoCP);

            // Numero de Organizadores
            Element eventoNumeroMembrosCP = doc.createElement("NumeroMembrosCP");
            eventoNumeroMembrosCP.appendChild(doc.createTextNode(Integer.toString(0)));
            eventoCP.appendChild(eventoNumeroMembrosCP);

            // MembrosCP
            Element eventoMembrosCP = doc.createElement("MembrosCP");
            eventoCP.appendChild(eventoMembrosCP);
        }

        // Eviste ST
        if (getValorEventoState(e.getState().toString()) > 1) {
            // <ListaSessoesTematicas>
            Element eventoListaSessoesTematicas = doc.createElement("ListaSessoesTematicas");
            evento.appendChild(eventoListaSessoesTematicas);

            // NumeroSessoesTematicas
            Element eventoNumeroSessoesTematicas = doc.createElement("NumeroSessoesTematicas");
            eventoNumeroSessoesTematicas.appendChild(doc.createTextNode(Integer.toString(e.getListaDeSessoesTematicas().getListaDeSessaoTematica().size())));
            eventoListaSessoesTematicas.appendChild(eventoNumeroSessoesTematicas);

            // <SessoesTematicas>
            Element eventoSessoesTematicas = doc.createElement("SessoesTematicas");
            eventoListaSessoesTematicas.appendChild(eventoSessoesTematicas);

            for (SessaoTematica sessao : e.getListaDeSessoesTematicas().getListaDeSessaoTematica()) {
                tagsSessao(doc, eventoSessoesTematicas, sessao);
            }

        } else {
            // <ListaSessoesTematicas>
            Element eventoListaSessoesTematicas = doc.createElement("ListaSessoesTematicas");
            evento.appendChild(eventoListaSessoesTematicas);

            // NumeroSessoesTematicas
            Element eventoNumeroSessoesTematicas = doc.createElement("NumeroSessoesTematicas");
            eventoNumeroSessoesTematicas.appendChild(doc.createTextNode(Integer.toString(0)));
            eventoListaSessoesTematicas.appendChild(eventoNumeroSessoesTematicas);

            // <SessoesTematicas>
            Element eventoSessoesTematicas = doc.createElement("SessoesTematicas");
            eventoListaSessoesTematicas.appendChild(eventoSessoesTematicas);
        }

        // Existe Submissoes
        if (getValorEventoState(e.getState().toString()) > 2
                && !e.getSubmissoes().isEmpty()) {
            // ListaSubmissoesEvento
            Element eventoListaSubmissoesEvento = doc.createElement("ListaSubmissoesEvento");
            evento.appendChild(eventoListaSubmissoesEvento);

            // NumeroSubmissoes
            Element eventoNumeroSubmissoes = doc.createElement("NumeroSubmissoes");
            eventoNumeroSubmissoes.appendChild(doc.createTextNode(Integer.toString(e.getSubmissoes().size())));
            eventoListaSubmissoesEvento.appendChild(eventoNumeroSubmissoes);

            // Submissoes
            Element eventoSubmissoes = doc.createElement("Submissoes");
            eventoListaSubmissoesEvento.appendChild(eventoSubmissoes);
            for (Submissao submissao : e.getSubmissoes()) {
                // Submissao
                Element eventoSubmissao = doc.createElement("Submissao");
                eventoSubmissoes.appendChild(eventoSubmissao);

                String atrb = "INICIAL";
                // EstadoSubmissao
                Element eventoEstadoSubmissao = doc.createElement("EstadoSubmissao");
                eventoEstadoSubmissao.appendChild(doc.createTextNode(submissao.getState().toString()));
                eventoSubmissao.appendChild(eventoEstadoSubmissao);

                tagSubmissao(doc, eventoSubmissao, submissao, atrb);

                if (getValorSubmissaoState(submissao.getState().toString()) > 8) {
                    atrb = "FINAL";
                    tagSubmissao(doc, eventoSubmissao, submissao, atrb);
                }

                // verificar se tem listarevisoes
                if (getValorSessaoState(e.getState().toString()) > 5) {
                    // ListaRevisoes
                    Element eventoListaRevisoes = doc.createElement("ListaRevisoes");
                    eventoSubmissao.appendChild(eventoListaRevisoes);

                    // NumeroRevisoes
                    Element sessaoNumeroRevisoes = doc.createElement("NumeroRevisoes");
                    sessaoNumeroRevisoes.appendChild(doc.createTextNode(Integer.toString(0)));
                    eventoListaRevisoes.appendChild(sessaoNumeroRevisoes);

                    // Revisoes
                    Element sessaoRevisoes = doc.createElement("Revisoes");
                    eventoListaRevisoes.appendChild(sessaoRevisoes);
                } else {
                    // ListaRevisoes
                    Element eventoListaRevisoes = doc.createElement("ListaRevisoes");
                    eventoSubmissao.appendChild(eventoListaRevisoes);

                    // NumeroRevisoes
                    Element sessaoNumeroRevisoes = doc.createElement("NumeroRevisoes");
                    sessaoNumeroRevisoes.appendChild(doc.createTextNode(Integer.toString(0)));
                    eventoListaRevisoes.appendChild(sessaoNumeroRevisoes);

                    // Revisoes
                    Element sessaoRevisoes = doc.createElement("Revisoes");
                    eventoListaRevisoes.appendChild(sessaoRevisoes);
                }
            }

        } else {
            // ListaSubmissoesEvento
            Element eventoListaSubmissoesEvento = doc.createElement("ListaSubmissoesEvento");
            evento.appendChild(eventoListaSubmissoesEvento);

            // NumeroSubmissoes
            Element eventoNumeroSubmissoes = doc.createElement("NumeroSubmissoes");
            eventoNumeroSubmissoes.appendChild(doc.createTextNode(Integer.toString(0)));
            eventoListaSubmissoesEvento.appendChild(eventoNumeroSubmissoes);

            // Submissoes
            Element eventoSubmissoes = doc.createElement("Submissoes");
            eventoListaSubmissoesEvento.appendChild(eventoSubmissoes);
        }
    }

    private void tagsSessao(Document doc, Element eventoSessoesTematicas, SessaoTematica sessao) {
        // SessaoTematica
        Element sessaoSessaoTematica = doc.createElement("SessaoTematica");
        eventoSessoesTematicas.appendChild(sessaoSessaoTematica);

        // CodigoST
        Element sessaoCodigoST = doc.createElement("CodigoST");
        sessaoCodigoST.appendChild(doc.createTextNode(sessao.getCodigo()));
        sessaoSessaoTematica.appendChild(sessaoCodigoST);

        // DescricaoST
        Element sessaoDescricaoST = doc.createElement("DescricaoST");
        sessaoDescricaoST.appendChild(doc.createTextNode(sessao.getDetalhes()));
        sessaoSessaoTematica.appendChild(sessaoDescricaoST);

        // DataInicioSubmissao
        Element sessaoDataInicioSubmissao = doc.createElement("DataInicioSubmissao");
        sessaoDataInicioSubmissao.appendChild(doc.createTextNode(sdf.format(sessao.getDataInicioSubmissao())));
        sessaoSessaoTematica.appendChild(sessaoDataInicioSubmissao);

        // DataFimSubmissao
        Element sessaoDataFimSubmissao = doc.createElement("DataFimSubmissao");
        sessaoDataFimSubmissao.appendChild(doc.createTextNode(sdf.format(sessao.getDataFimSubmissao())));
        sessaoSessaoTematica.appendChild(sessaoDataFimSubmissao);

        // DataInicioDistribuicao
        Element sessaoDataInicioDistribuicao = doc.createElement("DataInicioDistribuicao");
        sessaoDataInicioDistribuicao.appendChild(doc.createTextNode(sdf.format(sessao.getDataInicioDistribuicao())));
        sessaoSessaoTematica.appendChild(sessaoDataInicioDistribuicao);

        // DataLimiteRevisao
        Element sessaoDataLimiteRevisao = doc.createElement("DataLimiteRevisao");
        sessaoDataLimiteRevisao.appendChild(doc.createTextNode(sdf.format(sessao.getDataLimiteRevisao())));
        sessaoSessaoTematica.appendChild(sessaoDataLimiteRevisao);

        // DataLmiteSubmissaoFinal
        Element sessaoDataLmiteSubmissaoFinal = doc.createElement("DataLmiteSubmissaoFinal");
        sessaoDataLmiteSubmissaoFinal.appendChild(doc.createTextNode(sdf.format(sessao.getDataLimiteSubmissaoFinal())));
        sessaoSessaoTematica.appendChild(sessaoDataLmiteSubmissaoFinal);

        // EstadoST
        Element sessaoEstadoST = doc.createElement("EstadoST");
        sessaoEstadoST.appendChild(doc.createTextNode(sessao.getState().toString()));
        sessaoSessaoTematica.appendChild(sessaoEstadoST);

        // ListaProponentes
        Element sessaoListaProponentes = doc.createElement("ListaProponentes");
        sessaoSessaoTematica.appendChild(sessaoListaProponentes);

        // NumeroProponentes
        Element sessaoNumeroProponentes = doc.createElement("NumeroProponentes");
        sessaoNumeroProponentes.appendChild(doc.createTextNode(Integer.toString(sessao.getListaProponentes().getListaProponentes().size())));
        sessaoListaProponentes.appendChild(sessaoNumeroProponentes);

        // Proponentes
        Element sessaoProponentes = doc.createElement("Proponentes");
        sessaoListaProponentes.appendChild(sessaoProponentes);

        for (Proponente proponente : sessao.getListaProponentes().getListaProponentes()) {
            // Proponente
            Element sessaoProponente = doc.createElement("Proponente");
            sessaoProponente.appendChild(doc.createTextNode(proponente.getUtilizador().getUsername()));
            sessaoProponentes.appendChild(sessaoProponente);
        }

        // Existe CPST
        if (getValorSessaoState(sessao.getState().toString()) > 1) {
            // CPSessao
            Element sessaoCPSessao = doc.createElement("CPSessao");
            sessaoSessaoTematica.appendChild(sessaoCPSessao);

            // NumeroMembrosCPST
            Element sessaoNumeroMembrosCPST = doc.createElement("NumeroMembrosCPST");
            sessaoNumeroMembrosCPST.appendChild(doc.createTextNode(Integer.toString(sessao.getRevisores().size())));
            sessaoCPSessao.appendChild(sessaoNumeroMembrosCPST);

            // MembrosCPSessao
            Element sessaoMembrosCPSessao = doc.createElement("MembrosCPSessao");
            sessaoCPSessao.appendChild(sessaoMembrosCPSessao);

            for (Revisor revisor : sessao.getRevisores()) {
                // MembroCPSessao
                Element sessaoMembroCPSessao = doc.createElement("MembroCPSessao");
                sessaoMembroCPSessao.appendChild(doc.createTextNode(revisor.getUtilizador().getUsername()));
                sessaoMembrosCPSessao.appendChild(sessaoMembroCPSessao);
            }
        } else {
            // CPSessao
            Element sessaoCPSessao = doc.createElement("CPSessao");
            sessaoSessaoTematica.appendChild(sessaoCPSessao);

            // NumeroMembrosCPST
            Element sessaoNumeroMembrosCPST = doc.createElement("NumeroMembrosCPST");
            sessaoNumeroMembrosCPST.appendChild(doc.createTextNode(Integer.toString(0)));
            sessaoCPSessao.appendChild(sessaoNumeroMembrosCPST);

            // MembrosCPSessao
            Element sessaoMembrosCPSessao = doc.createElement("MembrosCPSessao");
            sessaoCPSessao.appendChild(sessaoMembrosCPSessao);
        }

        // Existe Submissoes
        if (getValorSessaoState(sessao.getState().toString()) > 2
                && !sessao.getSubmissoes().isEmpty()) {
            // ListaSubmissoes
            Element sessaoListaSubmissoes = doc.createElement("ListaSubmissoes");
            sessaoSessaoTematica.appendChild(sessaoListaSubmissoes);

            // NumeroSubmissoes
            Element sessaoNumeroSubmissoes = doc.createElement("NumeroSubmissoes");
            sessaoNumeroSubmissoes.appendChild(doc.createTextNode(Integer.toString(sessao.getSubmissoes().size())));
            sessaoListaSubmissoes.appendChild(sessaoNumeroSubmissoes);

            // Submissoes
            Element sessaoSubmissoes = doc.createElement("Submissoes");
            sessaoListaSubmissoes.appendChild(sessaoSubmissoes);

            for (Submissao submissao : sessao.getSubmissoes()) {
                // Submissao
                Element sessaoSubmissao = doc.createElement("Submissao");
                sessaoSubmissoes.appendChild(sessaoSubmissao);

                String atrb = "INICIAL";
                // EstadoSubmissao
                Element sessaoEstadoSubmissao = doc.createElement("EstadoSubmissao");
                sessaoEstadoSubmissao.appendChild(doc.createTextNode(submissao.getState().toString()));
                sessaoSubmissao.appendChild(sessaoEstadoSubmissao);

                tagSubmissao(doc, sessaoSubmissao, submissao, atrb);

                if (getValorSubmissaoState(submissao.getState().toString()) > 9) {
                    atrb = "FINAL";
                    tagSubmissao(doc, sessaoSubmissao, submissao, atrb);
                }

                // verificar se tem listarevisoes
                if (getValorSessaoState(sessao.getState().toString()) > 6) {
                    // ListaRevisoes
                    Element sessaoListaRevisoes = doc.createElement("ListaRevisoes");
                    sessaoSubmissao.appendChild(sessaoListaRevisoes);

                    // NumeroRevisoes
                    Element sessaoNumeroRevisoes = doc.createElement("NumeroRevisoes");
                    sessaoNumeroRevisoes.appendChild(doc.createTextNode(Integer.toString(0)));
                    sessaoListaRevisoes.appendChild(sessaoNumeroRevisoes);

                    // Revisoes
                    Element sessaoRevisoes = doc.createElement("Revisoes");
                    sessaoListaRevisoes.appendChild(sessaoRevisoes);
                } else {
                    // ListaRevisoes
                    Element sessaoListaRevisoes = doc.createElement("ListaRevisoes");
                    sessaoSubmissao.appendChild(sessaoListaRevisoes);

                    // NumeroRevisoes
                    Element sessaoNumeroRevisoes = doc.createElement("NumeroRevisoes");
                    sessaoNumeroRevisoes.appendChild(doc.createTextNode(Integer.toString(0)));
                    sessaoListaRevisoes.appendChild(sessaoNumeroRevisoes);

                    // Revisoes
                    Element sessaoRevisoes = doc.createElement("Revisoes");
                    sessaoListaRevisoes.appendChild(sessaoRevisoes);
                }
            }
        } else {
            // ListaSubmissoes
            Element sessaoListaSubmissoes = doc.createElement("ListaSubmissoes");
            sessaoSessaoTematica.appendChild(sessaoListaSubmissoes);

            // NumeroSubmissoes
            Element sessaoNumeroSubmissoes = doc.createElement("NumeroSubmissoes");
            sessaoNumeroSubmissoes.appendChild(doc.createTextNode(Integer.toString(0)));
            sessaoListaSubmissoes.appendChild(sessaoNumeroSubmissoes);

            // Submissoes
            Element sessaoSubmissoes = doc.createElement("Submissoes");
            sessaoListaSubmissoes.appendChild(sessaoSubmissoes);
        }

    }

    private void tagSubmissao(Document doc, Element sessaoSubmissao, Submissao submissao, String atrb) {
        Artigo artigo;
        if(atrb.equals("INICIAL")) {
            artigo = submissao.getArtigo();
        } else {
            artigo = submissao.getArtigoFinal();
        }
        
        // Artigo
        Element sessaoArtigo = doc.createElement("Artigo");
        sessaoArtigo.setAttribute("tipo", atrb);
        sessaoSubmissao.appendChild(sessaoArtigo);

        // AutorCorrespondente
        Element sessaoAutorCorrespondente = doc.createElement("AutorCorrespondente");
        sessaoAutorCorrespondente.appendChild(doc.createTextNode(artigo.getAutorCorrespondente().getEmail()));
        sessaoArtigo.appendChild(sessaoAutorCorrespondente);

        // AutorSubmissao
        Element sessaoAutorSubmissao = doc.createElement("AutorSubmissao");
        sessaoAutorSubmissao.appendChild(doc.createTextNode(artigo.getIdUtilizador()));
        sessaoArtigo.appendChild(sessaoAutorSubmissao);

        // DataSubmissao
        Element sessaoDataSubmissao = doc.createElement("DataSubmissao");
        System.out.println(submissao.getArtigo().getDataSubmissao());
        sessaoDataSubmissao.appendChild(doc.createTextNode(sdf.format(artigo.getDataSubmissao())));
        sessaoArtigo.appendChild(sessaoDataSubmissao);

        // TituloArtigo
        Element sessaoTituloArtigo = doc.createElement("TituloArtigo");
        sessaoTituloArtigo.appendChild(doc.createTextNode(artigo.getTitulo()));
        sessaoArtigo.appendChild(sessaoTituloArtigo);

        // Resumo
        Element sessaoResumo = doc.createElement("Resumo");
        sessaoResumo.appendChild(doc.createTextNode(artigo.getResumo()));
        sessaoArtigo.appendChild(sessaoResumo);

        // ListaAutores
        Element sessaoListaAutores = doc.createElement("ListaAutores");
        sessaoArtigo.appendChild(sessaoListaAutores);

        // NumeroAutores
        Element sessaoNumeroAutores = doc.createElement("NumeroAutores");
        sessaoNumeroAutores.appendChild(doc.createTextNode(Integer.toString(artigo.getListaAutores().getAutores().size())));
        sessaoListaAutores.appendChild(sessaoNumeroAutores);

        // Autores
        Element sessaoAutores = doc.createElement("Autores");
        sessaoListaAutores.appendChild(sessaoAutores);

        for (Autor autor : artigo.getListaAutores().getAutores()) {
            // Autor
            Element sessaoAutor = doc.createElement("Autor");
            sessaoAutores.appendChild(sessaoAutor);

            // NomeAutor
            Element sessaoNomeAutor = doc.createElement("NomeAutor");
            sessaoNomeAutor.appendChild(doc.createTextNode(autor.getNome()));
            sessaoAutor.appendChild(sessaoNomeAutor);

            // EmailAutor
            Element sessaoEmailAutor = doc.createElement("EmailAutor");
            sessaoEmailAutor.appendChild(doc.createTextNode(autor.getEmail()));
            sessaoAutor.appendChild(sessaoEmailAutor);

            // Filiacao
            Element sessaoFiliacao = doc.createElement("Filiacao");
            sessaoFiliacao.appendChild(doc.createTextNode(autor.getAfiliacao()));
            sessaoAutor.appendChild(sessaoFiliacao);

            // UsernameAutor
            String usernameAutor = "";
            Utilizador u = this.m_empresa.getRegistoUtilizadores().getUtilizadorByEmail(autor.getEmail());
            if (u != null) {
                usernameAutor = u.getEmail();
            }
            Element sessaoUsernameAutor = doc.createElement("UsernameAutor");
            sessaoUsernameAutor.appendChild(doc.createTextNode(usernameAutor));
            sessaoAutor.appendChild(sessaoUsernameAutor);
        }

        // PalavrasChave
        String pc = "";
        for (String palavrasChave : artigo.getListaPalavrasChave()) {
            pc += palavrasChave + ";";
        }
        pc = pc.substring(0, pc.length() - 1);

        Element sessaoPalavrasChave = doc.createElement("PalavrasChave");
        sessaoPalavrasChave.appendChild(doc.createTextNode(pc));
        sessaoArtigo.appendChild(sessaoPalavrasChave);

        // Ficheiro
        Element sessaoFicheiro = doc.createElement("Ficheiro");
        sessaoFicheiro.appendChild(doc.createTextNode(artigo.getFicheiro()));
        sessaoArtigo.appendChild(sessaoFicheiro);
    }

    private int getValorEventoState(String str) {
        if (str.equals("EventoStateCriado")) {
            return 0;
        }

        if (str.equals("EventoStateRegistado")) {
            return 1;
        }

        if (str.equals("EventoStateSTDefinidas")) {
            return 2;
        }

        if (str.equals("EventoStateCPDefinida")) {
            return 3;
        }

        if (str.equals("EventoStateEmSubmissao")) {
            return 4;
        }

        if (str.equals("EventoStateEmDetecaoConflitos")) {
            return 5;
        }

        if (str.equals("EventoStateEmLicitacao")) {
            return 6;
        }

        if (str.equals("EventoStateEmDistribuicao")) {
            return 7;
        }

        if (str.equals("EventoStateEmRevisao")) {
            return 8;
        }

        if (str.equals("EventoStateEmDecisao")) {
            return 9;
        }

        if (str.equals("EventoStateEmDecidido")) {
            return 10;
        }

        return 11;
    }

    private int getValorSessaoState(String str) {
        if (str.equals("STStateCriada")) {
            return 0;
        }

        if (str.equals("STStateRegistado")) {
            return 1;
        }

        if (str.equals("STStateCPDefinida")) {
            return 2;
        }

        if (str.equals("STStateEmSubmissao")) {
            return 3;
        }

        if (str.equals("STStateEmDetecaoConflitos")) {
            return 4;
        }

        if (str.equals("STStateEmLicitacao")) {
            return 5;
        }

        if (str.equals("STStateEmDistribuicao")) {
            return 6;
        }

        if (str.equals("STStateEmRevisao")) {
            return 7;
        }

        if (str.equals("STStateEmDecisao")) {
            return 8;
        }

        if (str.equals("STStateEmDecidido")) {
            return 9;
        }

        return 10;
    }

    private int getValorSubmissaoState(String str) {
        if (str.equals("SubmissaoStateCriado")) {
            return 0;
        }

        if (str.equals("SubmissaoStateSubmetido")) {
            return 1;
        }

        if (str.equals("SubmissaoStateEmDetecaoConflitos")) {
            return 2;
        }

        if (str.equals("SubmissaoStateEmLicitacao")) {
            return 3;
        }

        if (str.equals("SubmissaoStateEmDistribuicao")) {
            return 4;
        }

        if (str.equals("SubmissaoStateEmRevisao")) {
            return 5;
        }

        if (str.equals("SubmissaoStateEmDecisao")) {
            return 6;
        }

        if (str.equals("SubmissaoStateRetirada")) {
            return 7;
        }

        if (str.equals("SubmissaoStateRejeitada")) {
            return 8;
        }

        if (str.equals("SubmissaoStateAceite")) {
            return 9;
        }

        if (str.equals("SubmissaoStateCameraReady")) {
            return 10;
        }

        return 0;
    }
}
