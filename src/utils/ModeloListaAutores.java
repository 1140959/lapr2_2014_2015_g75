package utils;

import eventoscientificos.domain.Autor;
import java.util.ArrayList;
import java.util.List;
import javax.swing.AbstractListModel;


public class ModeloListaAutores extends AbstractListModel {

    private List<Autor> listaAutores;

    public ModeloListaAutores() {
        this.listaAutores = new ArrayList<>();
    }

    public ModeloListaAutores(List<Autor> listaAutores) {
        this.listaAutores = listaAutores;
    }

    public List<Autor> getListaAutores() {
        return listaAutores;
    }
    
    @Override
    public int getSize() {
        return this.listaAutores.size();
    }

    @Override
    public Autor getElementAt(int i) {
        return this.listaAutores.get(i);
    }

    public boolean addElement(Autor autor) {
        boolean autorAdicionado = this.listaAutores.add(autor);
        if (autorAdicionado) {
            fireIntervalAdded(this, 0, getSize());
        }
        return autorAdicionado;
    }

    public void addElementAt(int indice, Autor autor) {
        this.listaAutores.add(indice, autor);
        fireIntervalAdded(this, 0, getSize());
    }

    public boolean removeElement(Autor autor) {
        int indice = this.listaAutores.indexOf(autor);
        boolean autorRemovido = this.listaAutores.remove(autor);
        if (autorRemovido) {
            fireIntervalRemoved(this, indice, indice);
        }
        return autorRemovido;
    }

    public void removeElementAt(int indice) {
        this.listaAutores.remove(indice);
        fireIntervalRemoved(this, indice, indice);
    }

    public void removeAllElements() {
        this.listaAutores.clear();
        fireIntervalRemoved(this, getSize(), getSize());
    }

    public boolean contains(Autor autor) {
        return this.listaAutores.contains(autor);
    }

}
