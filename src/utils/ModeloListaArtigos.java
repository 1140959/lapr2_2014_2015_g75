/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import eventoscientificos.domain.Artigo;
import eventoscientificos.domain.SessaoTematica;
import eventoscientificos.domain.Submissao;
import java.util.ArrayList;
import java.util.List;
import javax.swing.AbstractListModel;

/**
 *
 * @author David
 */
public class ModeloListaArtigos extends AbstractListModel {
    
    private List<Artigo> listaArtigo;
    
    public ModeloListaArtigos() {
        listaArtigo = new ArrayList<>();
    }

    public ModeloListaArtigos(List<Artigo> artigo) {
        this.listaArtigo = new ArrayList<>(artigo);
    }
    
    public void setLista(List<Submissao> lista) {
        for (Submissao lst : lista) {
            listaArtigo.add(lst.getArtigo());
        }
    }
    
    public List<Artigo> getListaSessaoTematica() {
        return this.listaArtigo;
    }

    public boolean addElement(Artigo sessaoTematica) {
        boolean sessaoAdicionado = this.listaArtigo.add(sessaoTematica);
        if (sessaoAdicionado) {
            fireIntervalAdded(this, 0, getSize());
        }
        return sessaoAdicionado;
    }

    public void addElementAt(int indice, Artigo sessaoTematica) {
        this.listaArtigo.add(indice, sessaoTematica);
        fireIntervalAdded(this, 0, getSize());
    }

    public boolean removeElement(Artigo sessaoTematica) {
        int indice = this.listaArtigo.indexOf(sessaoTematica);
        boolean sessaoRemovido = this.listaArtigo.remove(sessaoTematica);
        if (sessaoRemovido) {
            fireIntervalRemoved(this, indice, indice);
        }
        return sessaoRemovido;
    }

    public void removeElementAt(int indice) {
        this.listaArtigo.remove(indice);
        fireIntervalRemoved(this, indice, indice);
    }

    public void removeAllElements() {
        this.listaArtigo.clear();
        fireIntervalRemoved(this, getSize(), getSize());
    }

    public boolean contains(SessaoTematica sessaoTematica) {
        return this.listaArtigo.contains(sessaoTematica);
    }
    
    public Artigo getSessaoAt(int i){
        return listaArtigo.get(i);
    }

    @Override
    public int getSize() {
        return this.listaArtigo.size();
    }

    @Override
    public Object getElementAt(int i) {
        return this.listaArtigo.get(i);
    }
    
}
