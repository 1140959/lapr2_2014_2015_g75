/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import eventoscientificos.domain.CPDefinivel;
import java.util.List;
import javax.swing.AbstractListModel;

/**
 *
 * @author Carlos
 */
public class ModeloListaCPDefinivel extends AbstractListModel {
    
    private List<CPDefinivel> listaCPDefinivel;

    public ModeloListaCPDefinivel(List<CPDefinivel> listaCPDefinivel) {
        this.listaCPDefinivel = listaCPDefinivel;
    }
    
    public List<CPDefinivel> getListaCPDefinivel() {
        return this.listaCPDefinivel;
    }
    
    public boolean addElement(CPDefinivel cpDefinivel) {
        boolean cpAdicionado = this.listaCPDefinivel.add(cpDefinivel);
        if (cpAdicionado) {
            fireIntervalAdded(this, 0, getSize());
        }
        return cpAdicionado;
    }

    public void addElementAt(int indice, CPDefinivel cpDefinivel) {
        this.listaCPDefinivel.add(indice, cpDefinivel);
        fireIntervalAdded(this, 0, getSize());
    }
    
    public boolean removeElement(CPDefinivel cpDefinivel) {
        int indice = this.listaCPDefinivel.indexOf(cpDefinivel);
        boolean cpRemovido = this.listaCPDefinivel.remove(cpDefinivel);
        if (cpRemovido) {
            fireIntervalRemoved(this, indice, indice);
        }
        return cpRemovido;
    }

    public void removeElementAt(int indice) {
        this.listaCPDefinivel.remove(indice);
        fireIntervalRemoved(this, indice, indice);
    }
    
    public void removeAllElements() {
        this.listaCPDefinivel.clear();
        fireIntervalRemoved(this, getSize(), getSize());
    }

    public boolean contains(CPDefinivel cpDefinivel) {
        return this.listaCPDefinivel.contains(cpDefinivel);
    }
    
    @Override
    public int getSize() {
        return this.listaCPDefinivel.size();
    }

    @Override
    public Object getElementAt(int i) {
        return this.listaCPDefinivel.get(i);
    }
    
    
}
