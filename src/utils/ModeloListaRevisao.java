/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import eventoscientificos.domain.Revisao;
import java.util.ArrayList;
import java.util.List;
import javax.swing.AbstractListModel;

/**
 *
 * @author BrunoFernandes
 */
public class ModeloListaRevisao extends AbstractListModel{
private List<Revisao> listaRevisao;

    public ModeloListaRevisao() {
         this.listaRevisao=new ArrayList<>();
    }

    public ModeloListaRevisao(List<Revisao> listaRevisao) {
        this.listaRevisao = new ArrayList<>(listaRevisao);
    }
    
    public boolean addElement(Revisao proponente) {
        boolean proponenteAdicionado = this.listaRevisao.add(proponente);
        if (proponenteAdicionado) {
            fireIntervalAdded(this, 0, getSize());
        }
        return proponenteAdicionado;
    }

    public void addElementAt(int indice, Revisao proponente) {
        this.listaRevisao.add(indice, proponente);
        fireIntervalAdded(this, 0, getSize());
    }

    public boolean removeElement(Revisao proponente) {
        int indice = this.listaRevisao.indexOf(proponente);
        boolean proponenteRemovido = this.listaRevisao.remove(proponente);
        if (proponenteRemovido) {
            fireIntervalRemoved(this, indice, indice);
        }
        return proponenteRemovido;
    }

    public void removeElementAt(int indice) {
        this.listaRevisao.remove(indice);
        fireIntervalRemoved(this, indice, indice);
    }

    public void removeAllElements() {
        this.listaRevisao.clear();
        fireIntervalRemoved(this, getSize(), getSize());
    }

    public boolean contains(Revisao proponente) {
        return this.listaRevisao.contains(proponente);
    }


    @Override
    public int getSize() {
    return this.listaRevisao.size();}

    @Override
    public Object getElementAt(int index) {
   return this.listaRevisao.get(index);}
    
}
