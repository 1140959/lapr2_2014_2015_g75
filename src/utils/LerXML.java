/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import eventoscientificos.controllers.CriarEventoController;
import eventoscientificos.controllers.CriarSessaoTematicaController;
import eventoscientificos.controllers.DefinirCPController;
import eventoscientificos.controllers.RegistarUtilizadorController;
import eventoscientificos.controllers.SubmeterArtigoController;
import eventoscientificos.controllers.SubmeterArtigoFinalController;
import eventoscientificos.domain.Autor;
import eventoscientificos.domain.Empresa;
import eventoscientificos.domain.Submissivel;
import eventoscientificos.domain.Utilizador;
import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 *
 * @author Carlos
 */
public class LerXML {

    private Document doc;
    private Empresa m_empresa;
    private List<Integer> idLocal;
    private List<String> nomeLocal;
    private CriarEventoController controllerCE;
    private CriarSessaoTematicaController controllerCST;
    private SubmeterArtigoController controllerSA;
    private SubmeterArtigoFinalController controllerSAF;

    private static final SimpleDateFormat df = new SimpleDateFormat("yyyy/MM/dd");

    public boolean lerUtilizadorXML(Empresa empresa, String caminho) {
        this.m_empresa = empresa;
        File fXmlFile = new File(caminho);
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        dbFactory.setValidating(false);
        dbFactory.setNamespaceAware(true);

        try {
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            this.doc = dBuilder.parse(fXmlFile);
            this.doc.getDocumentElement().normalize();

            // validação da tag ListaUtilizadores
            if (!validaTag("ListaUtilizadores", 1)) {
                System.out.println("Erro: tag ListaUtilizadores");
                return false;
            }

            // validação da tag NumeroElementos
            if (!validaTag("NumeroElementos", 1)) {
                System.out.println("Erro: tag NumeroElementos");
                return false;
            }

            int numeroUtilizadores = getNumeroObjectos("NumeroElementos", 0);

            // validar o numero de utilizadores
            if (numeroUtilizadores == 0) {
                System.out.println("Erro: Numeros Invalidos - tag NumeroElementos");
                return false;
            }

            // validacao da tag Utilizadores
            if (!validaTag("Utilizadores", 1)) {
                System.out.println("Erro: tag Utilizadores");
                return false;
            }

            // validacao da tag Utilizadores
            if (!validaTag("Utilizador", numeroUtilizadores)) {
                System.out.println("Erro: tag Utilizador");
                return false;
            }

            System.out.println("Loop Utilizadores");

            NodeList nodeListUtilizador = doc.getElementsByTagName("Utilizador");

            // Loop Utilizador
            for (int index = 0; index < numeroUtilizadores; index++) {
                Node nNode = nodeListUtilizador.item(index);

                if (nNode.getNodeType() == Node.ELEMENT_NODE) {

                    if (!loopUtilizador(nNode)) {
                        System.out.println("Erro:  Loop Utilizador");
                        return false;
                    }
                }
            }

            return true;

        } catch (ParserConfigurationException | SAXException | IOException ex) {
            System.out.println(ex.getMessage());
        }

        return false;
    }

    public boolean lerLocalXML(String caminho) {
        File fXmlFile = new File(caminho);
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        dbFactory.setValidating(false);
        dbFactory.setNamespaceAware(true);

        try {
            idLocal = new ArrayList<>();
            nomeLocal = new ArrayList<>();

            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            doc = dBuilder.parse(fXmlFile);
            doc.getDocumentElement().normalize();

            // validação da tag ListaLocais
            if (!validaTag("ListaLocais", 1)) {
                System.out.println("Erro: tag ListaLocais");
                return false;
            }

            // validação da tag NumeroElementos
            if (!validaTag("NumElementos", 1)) {
                System.out.println("Erro: tag NumElementos");
                return false;
            }

            int numeroLocal = getNumeroObjectos("NumElementos", 0);

            // validar o numero de local
            if (numeroLocal == 0) {
                System.out.println("Erro: Numeros Invalidos - tag NumeroElementos");
                return false;
            }

            // validacao da tag Locais
            if (!validaTag("Locais", 1)) {
                System.out.println("Erro: tag Locais");
                return false;
            }

            // validacao da tag Local
            if (!validaTag("Local", numeroLocal)) {
                System.out.println("Erro: tag Local");
                return false;
            }

            System.out.println("Loop Local");

            NodeList nodeListUtilizador = doc.getElementsByTagName("Local");

            // Loop Utilizador
            for (int index = 0; index < numeroLocal; index++) {
                Node nNode = nodeListUtilizador.item(index);

                if (nNode.getNodeType() == Node.ELEMENT_NODE) {

                    if (!loopLocal(nNode)) {
                        System.out.println("Erro:  Loop Local");
                        return false;
                    }
                }
            }

            return true;
        } catch (ParserConfigurationException | SAXException | IOException ex) {
            System.out.println(ex.getMessage());
        }

        return false;
    }

    public boolean lerEventoXML(Empresa empresa, String caminho) {
        this.m_empresa = empresa;
        File fXmlFile = new File(caminho);
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        dbFactory.setValidating(false);
        dbFactory.setNamespaceAware(true);

        try {
            String tags = "";
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            doc = dBuilder.parse(fXmlFile);
            doc.getDocumentElement().normalize();

            // validação da tag ListaEventos
            if (!validaTag("ListaEventos", 1)) {
                System.out.println("Erro: tag ListaEventos");
                return false;
            }

            // validação da tag NumeroEventos
            if (!validaTag("NumeroEventos", 1)) {
                System.out.println("Erro: tag NumeroEventos");
                return false;
            }

            int numeroEventos = getNumeroObjectos("NumeroEventos", 0);

            // validar o numero de NumeroEventos
            if (numeroEventos == 0) {
                System.out.println("Erro: Numeros Invalidos - tag NumeroEventos");
                return false;
            }

            // validacao da tag Eventos
            if (!validaTag("Eventos", 1)) {
                System.out.println("Erro: tag Eventos");
                return false;
            }

            // validacao da tag Evento
            if (!validaTag("Evento", numeroEventos)) {
                System.out.println("Erro: tag Evento");
                return false;
            }

            NodeList nodeList = doc.getElementsByTagName("*");

            for (int i = 0; i < nodeList.getLength(); i++) {
                Element element = (Element) nodeList.item(i);
                tags += "<" + element.getNodeName() + ">";
            }

            String[] tagsEventos = (tags.split("<Eventos>"))[1].split("<Evento>");

            System.out.println("Loop Eventos");

            NodeList nodeListEvento = doc.getElementsByTagName("Evento");

            // Loop Evento
            for (int index = 0; index < numeroEventos; index++) {
                Node nNode = nodeListEvento.item(index);

                if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                    if (!loopEvento(nNode, index, tagsEventos[index + 1])) {
                        System.out.println("Erro:  Loop Evento");
                        return false;
                    }
                }
            }

            System.out.println("fim");
            return true;
        } catch (ParserConfigurationException | SAXException | IOException ex) {
            System.out.println(ex.getMessage());
        }

        return false;
    }

    private boolean loopUtilizador(Node nNode) {
        if (!validaTagNode(nNode, "Username", 1) || !validaTagNode(nNode, "Password", 1)
                || !validaTagNode(nNode, "Nome", 1) || !validaTagNode(nNode, "Email", 1)) {
            System.out.println("erro 1");
            return false;
        }

        RegistarUtilizadorController controllerRU = new RegistarUtilizadorController(m_empresa);
        controllerRU.novoUtilizador();

        Element eElement = (Element) nNode;

        String pwd = eElement.getElementsByTagName("Password").item(0).getTextContent();

        if (!validaCodificacao(pwd.split(";")[0])) {
            return false;
        }

        System.out.println(pwd);

        if (pwd.split(";")[0].equals("CA")) {
            pwd = descodificarPwd(pwd);
        } else {
            pwd = pwd.split(";")[1];
        }

        return controllerRU.setDados(eElement.getElementsByTagName("Username").item(0).getTextContent(),
                pwd,
                eElement.getElementsByTagName("Nome").item(0).getTextContent(),
                eElement.getElementsByTagName("Email").item(0).getTextContent()) != null;
    }

    private boolean loopLocal(Node nNode) {
        if (!validaTagNode(nNode, "LocalID", 1) || !validaTagNode(nNode, "Designacao", 1)) {
            return false;
        }

        Element eElement = (Element) nNode;

        if (!tryParseInt(eElement.getElementsByTagName("LocalID").item(0).getTextContent())) {
            return false;
        }

        idLocal.add(Integer.parseInt(eElement.getElementsByTagName("LocalID").item(0).getTextContent()));
        nomeLocal.add(eElement.getElementsByTagName("Designacao").item(0).getTextContent());

        return true;
    }

    private boolean loopEvento(Node nNodeEvento, int posicao, String tagsEventos) {
        if (!validaTagNode(nNodeEvento, "Titulo", 1) || !validaTagNode(nNodeEvento, "Descricao", 1)
                || !validaTagNode(nNodeEvento, "Local", 1) || !validaTagNode(nNodeEvento, "ListaOrganizadores", 1)) {
            System.out.println("erro 20");
            return false;
        }

        // validar tags das datas DataInicioSubmissao
        // DataFimSubmissao DataInicioDistribuicao DataLimiteRevisao
        // DataLmiteSubmissaoFinal
        String[] teste = {"DataInicio", "DataFim", "DataInicioSubmissao", "DataFimSubmissao",
            "DataInicioDistribuicao", "DataLimiteRevisao",
            "DataLmiteSubmissaoFinal"};
        if (!validarTagDatas(tagsEventos.split("<ListaOrganizadores>")[0], teste)) {
            System.out.println("erro 50");
            return false;
        }

        // validação da tag NumeroOrganizadores
        if (!validaTagNode(nNodeEvento, "NumeroOrganizadores", 1)) {
            System.out.println("Erro: tag NumeroOrganizadores");
            return false;
        }

        int numeroOrganizadores = getNumeroObjectosNode(nNodeEvento, "NumeroOrganizadores");

        // validar o numero de organizadores
        if (numeroOrganizadores == 0) {
            System.out.println("Erro: Numeros Invalidos - tag NumeroOrganizadores");
            return false;
        }

        // validacao da tag Organizadores
        if (!validaTagNode(nNodeEvento, "Organizadores", 1)) {
            System.out.println("Erro: tag Organizadores");
            return false;
        }

        // validacao da tag Organizador
        if (!validaTagNode(nNodeEvento, "Organizador", numeroOrganizadores)) {
            System.out.println("Erro: tag Organizador");
            return false;
        }

        Element eElement = (Element) nNodeEvento;

        // validacao da tag EstadoEvento
        if (!validaTagNode(nNodeEvento, "EstadoEvento", 1)) {
            System.out.println("Erro: tag EstadoEvento");
            return false;
        }

        int numeroEventoState = getValorEventoSate(eElement.getElementsByTagName("EstadoEvento").item(0).getTextContent());

        if (numeroEventoState == 0) {
            System.out.println("Erro: EstadoEvento");
            return false;
        }

        controllerCE = new CriarEventoController(m_empresa);
        controllerCE.novoEvento();

        controllerCE.setTitulo(eElement.getElementsByTagName("Titulo").item(0).getTextContent());
        controllerCE.setDescricao(eElement.getElementsByTagName("Descricao").item(0).getTextContent());
        controllerCE.setLocal(eElement.getElementsByTagName("Local").item(0).getTextContent());

        try {
            controllerCE.setDataInicio(df.parse(eElement.getElementsByTagName("DataInicio").item(0).getTextContent()));
            controllerCE.setDataFim(df.parse(eElement.getElementsByTagName("DataFim").item(0).getTextContent()));
            controllerCE.setDataInicioSubmissao(df.parse(eElement.getElementsByTagName("DataInicioSubmissao").item(0).getTextContent()));
            controllerCE.setDataFimSubmissao(df.parse(eElement.getElementsByTagName("DataFimSubmissao").item(0).getTextContent()));
            controllerCE.setDataInicioDistribuicao(df.parse(eElement.getElementsByTagName("DataInicioDistribuicao").item(0).getTextContent()));
            controllerCE.setDataLimiteRevisao(df.parse(eElement.getElementsByTagName("DataLimiteRevisao").item(0).getTextContent()));
            controllerCE.setDataLimiteSubmissaoFinal(df.parse(eElement.getElementsByTagName("DataLmiteSubmissaoFinal").item(0).getTextContent()));
        } catch (ParseException ex) {
            return false;
        }

        System.out.println("Loop Organizadores");

        for (int index = 0; index < numeroOrganizadores; index++) {

            if (nNodeEvento.getNodeType() == Node.ELEMENT_NODE) {
                if (!loopOrganizador(nNodeEvento, index, controllerCE)) {
                    System.out.println("Erro:  Loop Organizador");
                    return false;
                }
            }
        }

        controllerCE.registaEvento();

        // Sessão Temática
        // validacao da tag ListaSessoesTematicas
        if (!validaTagNode(nNodeEvento, "ListaSessoesTematicas", 1)) {
            System.out.println("Erro: tag ListaSessoesTematicas");
            return false;
        }

        // validação da tag NumeroSessoesTematicas
        if (!validaTagNode(nNodeEvento, "NumeroSessoesTematicas", 1)) {
            System.out.println("Erro: tag NumeroSessoesTematicas");
            return false;
        }

        int numeroSessoesTematicas = getNumeroObjectosNode(nNodeEvento, "NumeroSessoesTematicas");

        // validar o numero de Sessoes Tematicas
        if (numeroSessoesTematicas < 0) {
            System.out.println(controllerCE.getEvento().getTitulo());
            System.out.println("Erro: Numeros Invalidos - tag NumeroSessoesTematicas");
            return false;
        }

        // validacao da tag SessoesTematicas
        if (!validaTagNode(nNodeEvento, "SessoesTematicas", 1)) {
            System.out.println("Erro: tag SessoesTematicas");
            return false;
        }

        // verificar se deve ter Sessao Tematica
        if (numeroEventoState < 2 && (tagsEventos.contains("<SessaoTematica>")
                || tagsEventos.contains("<ListaProponentes>")
                || tagsEventos.contains("<Proponentes>")
                || tagsEventos.contains("<Proponente>"))) {
            System.out.println("erro 660");
            return false;
        }

        if (numeroEventoState > 1) {
            // validacao da tag SessaoTematica
            if (!validaTagNode(nNodeEvento, "SessaoTematica", numeroSessoesTematicas)) {
                System.out.println("Erro: tag SessaoTematica");
                return false;
            }

            NodeList nodeListSessao = ((Element) nNodeEvento).getElementsByTagName("SessaoTematica");

            System.out.println("Loop Sessões Temáticas");
            for (int index = 0; index < numeroSessoesTematicas; index++) {
                Node nNodeSessao = nodeListSessao.item(index);

                if (nNodeSessao.getNodeType() == nNodeSessao.ELEMENT_NODE) {
                    // validar o numero de Sessoes Tematicas

                    // validação da tag ListaProponentes
                    if (!validaTagNode(nNodeSessao, "ListaProponentes", 1)) {
                        System.out.println("Erro: tag ListaProponentes");
                        return false;
                    }

                    // validação da tag NumeroProponentes
                    if (!validaTagNode(nNodeSessao, "NumeroProponentes", 1)) {
                        System.out.println("Erro: tag NumeroProponentes");
                        return false;
                    }

                    int numeroProponentes = getNumeroObjectosNode(nNodeSessao, "NumeroProponentes");

                    // validar o numero de proponentes
                    if (numeroProponentes == 0) {
                        System.out.println("Erro: Numeros Invalidos - tag NumeroProponentes");
                        return false;
                    }

                    // validacao da tag Proponentes
                    if (!validaTagNode(nNodeSessao, "Proponentes", 1)) {
                        System.out.println("Erro: tag Proponentes");
                        return false;
                    }

                    // validacao da tag Proponente
                    if (!validaTagNode(nNodeSessao, "Proponente", numeroProponentes)) {
                        System.out.println("Erro: tag Proponente");
                        return false;
                    }

                    System.out.println("Loop Sessao");
                    if (!loopSessao(nNodeSessao, posicao, numeroProponentes, tagsEventos)) {
                        System.out.println("Erro:  Loop Sessao Tematica");
                        return false;
                    }
                }
            }
        }

        // CP
        // validacao da tag CP
        if (!validaTagNode(nNodeEvento, "CP", 1)) {
            System.out.println("Erro: tag CP");
            return false;
        }

        // validação da tag NumeroMembrosCP
        if (!validaTagNode(nNodeEvento, "NumeroMembrosCP", 1)) {
            System.out.println("Erro: tag NumeroMembrosCP");
            return false;
        }

        int numeroMembrosCP = getNumeroObjectosNode(nNodeEvento, "NumeroMembrosCP");

        // validar o numero de numeroMembrosCP
        if (numeroMembrosCP < 0) {
            System.out.println("Erro: Numeros Invalidos - tag numeroMembrosCP");
            return false;
        }

        // validacao da tag MembrosCP
        if (!validaTagNode(nNodeEvento, "MembrosCP", 1)) {
            System.out.println("Erro: tag MembrosCP");
            return false;
        }

        if (numeroEventoState > 2) {
            // validacao da tag MembroCP
            if (!validaTagNode(nNodeEvento, "MembroCP", numeroMembrosCP)) {
                System.out.println("Erro: tag MembroCP");
                return false;
            }

            DefinirCPController controllerDCP = new DefinirCPController(m_empresa);
            controllerDCP.novaCP(controllerCE.getEvento());

            System.out.println("Loop MembroCPEvento");
            for (int index = 0; index < numeroMembrosCP; index++) {
                if (nNodeEvento.getNodeType() == Node.ELEMENT_NODE) {
                    if (nNodeEvento.getNodeType() == Node.ELEMENT_NODE) {
                        if (!loopMembrosCP(nNodeEvento, index, controllerDCP, "MembroCP")) {
                            System.out.println("Erro:  Loop MembroCPEvento");
                            return false;
                        }
                    }
                }
            }

            if (!controllerDCP.registaCP()) {
                System.out.println("erro 747474747");
                return false;
            }
        }

        // ListaSubmissoes
        // validacao da tag ListaSubmissoesEvento
        if (!validaTagNode(nNodeEvento, "ListaSubmissoesEvento", 1)) {
            System.out.println("Erro: tag ListaSubmissoesEvento");
            return false;
        }

        NodeList nodeListSubmissaoEvento = ((Element) nNodeEvento).getElementsByTagName("ListaSubmissoesEvento");

        Node nNode = nodeListSubmissaoEvento.item(0);

        // validação da tag NumeroSubmissoes
        if (!validaTagNode(nNode, "NumeroSubmissoes", 1)) {
            System.out.println("Erro: tag NumeroSubmissoes");
            return false;
        }

        int numeroSubmissoesEvento = getNumeroObjectosNode(nNode, "NumeroSubmissoes");

        // validar o numero NumeroSubmissoes
        if (numeroSubmissoesEvento < 0) {
            System.out.println("Erro: Numeros Invalidos - tag NumeroSubmissoes");
            return false;
        }

        // validacao da tag Submissoes
        if (!validaTagNode(nNode, "Submissoes", 1)) {
            System.out.println("Erro: tag Submissoes Evento");
            return false;
        }

        if (numeroEventoState > 3) {
            // Submissoes
            if (!validaTagNode(nNode, "Submissao", numeroSubmissoesEvento)) {
                System.out.println("Erro: tag Submissao 2");
                return false;
            }

            controllerCE.getEvento().setStateEmSubmissao();

            NodeList nodeListSubmissao = ((Element) nNode).getElementsByTagName("Submissao");

            System.out.println("Loop Submissao");
            for (int index = 0; index < numeroSubmissoesEvento; index++) {

                Node nNodeSubmissao = nodeListSubmissao.item(index);

                if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                    if (!loopSubmissao(nNodeSubmissao, index, tagsEventos, controllerCE.getEvento())) {
                        System.out.println("Erro:  Loop Submissao");
                        return false;
                    }
                }
            }
        }

        // estado detecao conflitos
        if (numeroEventoState > 4) {
            controllerCE.getEvento().setStateEmDetecaoConflitos();
        }

        // estado EventoStateEmLicitacao
        if (numeroEventoState > 5) {
            controllerCE.getEvento().setStateEmLicitacao();
        }

        // estado EmDistribuicao
        if (numeroEventoState > 6) {
            controllerCE.getEvento().setStateEmDistribuicao();
        }

        // estado EmRevisao
        if (numeroEventoState > 7) {
            controllerCE.getEvento().setStateEmRevisao();
        }

        // estado EmDecidido
        if (numeroEventoState > 8) {
            controllerCE.getEvento().setStateEmDecidido();
        }

        // estado EmDecidido
        if (numeroEventoState > 8) {
            controllerCE.getEvento().setStateEmDecidido();
        }

        // estado EmDecidido
        if (numeroEventoState > 9) {
            controllerCE.getEvento().setStateEmDecisao();
        }

        return true;
    }

    private boolean loopSessao(Node nNode, int posicao, int numeroProponentes, String tagsEventos) {
        if (!validaTagNode(nNode, "CodigoST", 1) || !validaTagNode(nNode, "DescricaoST", 1)
                || !validaTagNode(nNode, "DataInicioSubmissao", 1) || !validaTagNode(nNode, "DataFimSubmissao", 1)
                || !validaTagNode(nNode, "DataInicioDistribuicao", 1) || !validaTagNode(nNode, "DataLimiteRevisao", 1)
                || !validaTagNode(nNode, "DataLmiteSubmissaoFinal", 1)) {
            // DataLimiteRevisao Por Validar!
            System.out.println("erro 1");
            return false;
        }

        // validacao da tag EstadoST
        if (!validaTagNode(nNode, "EstadoST", 1)) {
            System.out.println("Erro: tag EstadoST");
            return false;
        }

        Element eElement = (Element) nNode;

        int numeroSessaoState = getValorSessaoState(eElement.getElementsByTagName("EstadoST").item(0).getTextContent());

        if (numeroSessaoState == 0) {
            System.out.println("Erro: EstadoST");
            return false;
        }

        controllerCST = new CriarSessaoTematicaController(m_empresa);
        controllerCST.setEvento(controllerCE.getEvento());
        try {
            controllerCST.setDados(eElement.getElementsByTagName("CodigoST").item(0).getTextContent(),
                    eElement.getElementsByTagName("DescricaoST").item(0).getTextContent(),
                    df.parse(eElement.getElementsByTagName("DataInicioSubmissao").item(0).getTextContent()),
                    df.parse(eElement.getElementsByTagName("DataFimSubmissao").item(0).getTextContent()),
                    df.parse(eElement.getElementsByTagName("DataInicioDistribuicao").item(0).getTextContent()),
                    df.parse(eElement.getElementsByTagName("DataLimiteRevisao").item(0).getTextContent()),
                    df.parse(eElement.getElementsByTagName("DataLmiteSubmissaoFinal").item(0).getTextContent()));
        } catch (ParseException ex) {
            return false;
        }

        System.out.println("Loop Proponente");
        for (int index = 0; index < numeroProponentes; index++) {

            if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                if (!loopProponente(nNode, index, controllerCST)) {
                    System.out.println("Erro:  Loop Proponente");
                    return false;
                }
            }
        }

        controllerCST.registaSessaoTematica();

        //CPST
        // validacao da tag CPSessao
        if (!validaTagNode(nNode, "CPSessao", 1)) {
            System.out.println("Erro: tag CPSessao");
            return false;
        }

        // validação da tag NumeroMembrosCPST
        if (!validaTagNode(nNode, "NumeroMembrosCPST", 1)) {
            System.out.println("Erro: tag NumeroMembrosCPST");
            return false;
        }

        int numeroMembrosCPST = getNumeroObjectosNode(nNode, "NumeroMembrosCPST");

        // validar o numero NumeroMembrosCPST
        if (numeroMembrosCPST < 0) {
            System.out.println("Erro: Numeros Invalidos - tag NumeroMembrosCPST");
            return false;
        }

        // validacao da tag MembrosCPSessao
        if (!validaTagNode(nNode, "MembrosCPSessao", 1)) {
            System.out.println("Erro: tag MembrosCPSessao");
            return false;
        }

        // verificar se deve ter CPST
        if (numeroSessaoState < 1 && tagsEventos.contains("<MembroCPSessao>")) {
            System.out.println("erro 700");
            return false;
        }

        if (numeroSessaoState > 1) {
            // validacao da tag MembroCPSessao
            if (!validaTagNode(nNode, "MembroCPSessao", numeroMembrosCPST)) {
                System.out.println("Erro: tag MembroCPSessao");
                return false;
            }

            DefinirCPController controllerDCP = new DefinirCPController(m_empresa);
            controllerDCP.novaCP(controllerCST.getSessaoTematica());

            System.out.println("Loop MembroCPSessao");
            for (int index = 0; index < numeroMembrosCPST; index++) {

                if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                    if (!loopMembrosCP(nNode, index, controllerDCP, "MembroCPSessao")) {
                        System.out.println("Erro:  Loop MembroCPSessao");
                        return false;
                    }
                }
            }

            if (!controllerDCP.registaCP()) {
                System.out.println("erro 10000");
                return false;
            }
        }

        // ListaSubmissoes
        // validacao da tag ListaSubmissoes
        if (!validaTagNode(nNode, "ListaSubmissoes", 1)) {
            System.out.println("Erro: tag ListaSubmissoes");
            return false;
        }

        // validação da tag NumeroSubmissoes
        if (!validaTagNode(nNode, "NumeroSubmissoes", 1)) {
            System.out.println("Erro: tag NumeroSubmissoes");
            return false;
        }

        int numeroSubmissoes = getNumeroObjectosNode(nNode, "NumeroSubmissoes");

        // validar o numero NumeroSubmissoes
        if (numeroSubmissoes < 0) {
            System.out.println("Erro: Numeros Invalidos - tag NumeroSubmissoes");
            return false;
        }

        // validacao da tag Submissoes
        if (!validaTagNode(nNode, "Submissoes", 1)) {
            System.out.println("Erro: tag Submissoes");
            return false;
        }

        // Submissao
        if (numeroSessaoState > 2) {
            // validacao da tag Submissao
            if (!validaTagNode(nNode, "Submissao", numeroSubmissoes)) {
                System.out.println("Erro: tag Submissao");
                return false;
            }

            if (numeroSubmissoes != 0) {
                NodeList nodeListSubmissao = ((Element) nNode).getElementsByTagName("Submissao");

                System.out.println("Loop Submissao");
                for (int index = 0; index < numeroSubmissoes; index++) {

                    Node nNodeSubmissao = nodeListSubmissao.item(index);

                    if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                        if (!loopSubmissao(nNodeSubmissao, index, tagsEventos, controllerCST.getSessaoTematica())) {
                            System.out.println("Erro:  Loop Submissao");
                            return false;
                        }
                    }
                }
            }
            controllerCST.getSessaoTematica().setStateEmSubmissao();
        }

        if (numeroSessaoState > 3) {
            controllerCST.getSessaoTematica().setStateEmDetecaoConflitos();
        }

        if (numeroSessaoState > 4) {
            controllerCST.getSessaoTematica().setStateEmLicitacao();
        }

        if (numeroSessaoState > 5) {
            controllerCST.getSessaoTematica().setStateEmDistribuicao();
        }

        if (numeroSessaoState > 6) {
            controllerCST.getSessaoTematica().setStateEmRevisao();
        }

        if (numeroSessaoState > 7) {
            controllerCST.getSessaoTematica().setStateEmDecisao();
        }

        if (numeroSessaoState > 8) {
            controllerCST.getSessaoTematica().setStateEmDecidido();
        }

        return true;
    }

    private boolean loopSubmissao(Node nNode, int posicao, String tagsEventos, Submissivel sub) {
        Element eElement = (Element) nNode;
        // validacao da tag EstadoSubmissao
        if (!validaTagNode(nNode, "EstadoSubmissao", 1)) {
            System.out.println("Erro: tag EstadoSubmissao");
            return false;
        }

        int numeroSubmissaoState = getValorSubmissaoState(eElement.getElementsByTagName("EstadoSubmissao").item(0).getTextContent());

        if (numeroSubmissaoState == 0) {
            System.out.println("Erro: EstadoSubmissao");
            return false;
        }

        int numeroArtigo = 1;
        if (numeroSubmissaoState > 9) {
            numeroArtigo = 2;
            if (!validaTagNode(nNode, "Artigo", numeroArtigo)) {
                System.out.println("Erro: Artigo 2");
                return false;
            }

            if (!validarAtb(nNode, "Artigo", 0, "INICIAL")) {
                System.out.println("Erro: atb");
                return false;
            }

            if (!validarAtb(nNode, "Artigo", 1, "FINAL")) {
                System.out.println("Erro: atb");
                return false;
            }
        }

        if (!validaTagNode(nNode, "Artigo", numeroArtigo) && !validarAtb(nNode, "Artigo", 0, "INICIAL")) {
            System.out.println("Erro: Artigo 1");
            return false;
        }

        NodeList nodeListArtigo = ((Element) nNode).getElementsByTagName("Artigo");

        System.out.println("Loop Artigo");
        for (int index = 0; index < numeroArtigo; index++) {
            Node nNodeArtigo = nodeListArtigo.item(index);

            if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                if (!loopArtigo(nNodeArtigo, index, tagsEventos, sub, numeroArtigo)) {
                    System.out.println("Erro:  Loop Artigo");
                    return false;
                }
            }

            System.out.println(controllerSA.getSubmissao().getState());
            if (numeroSubmissaoState > 1) {
                controllerSA.getSubmissao().setStateEmDetecaoConflitos();
            }

            System.out.println(controllerSA.getSubmissao().getState());
            if (numeroSubmissaoState > 2) {
                controllerSA.getSubmissao().setStateEmLicitacao();
            }

            if (numeroSubmissaoState > 3) {
                controllerSA.getSubmissao().setStateEmDistribuicao();
            }

            if (numeroSubmissaoState > 4) {
                controllerSA.getSubmissao().setStateEmRevisao();
            }

            if (numeroSubmissaoState > 5) {
                controllerSA.getSubmissao().setStateEmDecisao();
            }

            // retirar submissao
            if (numeroSubmissaoState == 7) {
                controllerSA.getSubmissao().setStateRetirada();
            }

            if (numeroSubmissaoState == 8) {
                controllerSA.getSubmissao().setStateRejeitada();
            }

            if (numeroSubmissaoState > 8) {
                controllerSA.getSubmissao().setStateAceite();
            }

            if (numeroSubmissaoState > 10) {
                controllerSA.getSubmissao().setStateCameraReady();
            }
        }

        return true;
    }

    private boolean loopArtigo(Node nNode, int posicao, String tagsEventos, Submissivel sub, int numeroArtigo) {
        if (!validaTagNode(nNode, "AutorCorrespondente", 1) || !validaTagNode(nNode, "AutorSubmissao", 1)
                || !validaTagNode(nNode, "DataSubmissao", 1) || !validaTagNode(nNode, "TituloArtigo", 1)
                || !validaTagNode(nNode, "Resumo", 1) || !validaTagNode(nNode, "PalavrasChave", 1)
                || !validaTagNode(nNode, "Ficheiro", 1) || !validaTagNode(nNode, "AutorSubmissao", 1)) {
            System.out.println("erro 20202");
            return false;
        }

        if (!validaTagNode(nNode, "ListaAutores", 1)) {
            System.out.println("Erro: ListaAutores");
            return false;
        }

        if (!validaTagNode(nNode, "NumeroAutores", 1)) {
            System.out.println("Erro: NumeroAutores");
            return false;
        }

        int numeroAutores = getNumeroObjectosNode(nNode, "NumeroAutores");

        // validar o numero de numeroAutores
        if (numeroAutores == 0) {
            System.out.println("Erro: Numeros Invalidos - tag numeroAutores");
            return false;
        }

        // validar o numero de Autores
        if (!validaTagNode(nNode, "Autores", 1)) {
            System.out.println("Erro: Autores");
            return false;
        }

        // validacao da tag Autor
        if (!validaTagNode(nNode, "Autor", numeroAutores)) {
            System.out.println("Erro: tag Autor");
            return false;
        }

        Element eElement = (Element) nNode;

        if (posicao == 0) {
            controllerSA = new SubmeterArtigoController(m_empresa);
            controllerSA.selectSubmissivel(sub);
            controllerSA.setDados(eElement.getElementsByTagName("TituloArtigo").item(0).getTextContent(),
                    eElement.getElementsByTagName("Resumo").item(0).getTextContent());
            controllerSA.setFicheiro(eElement.getElementsByTagName("Ficheiro").item(0).getTextContent());
            List<String> lstPC = Arrays.asList(eElement.getElementsByTagName("PalavrasChave").item(0).getTextContent().split(";"));
            controllerSA.setPalavrasChave(lstPC);

            List<Autor> lstAutor = new ArrayList<>();

            NodeList nodeListAutor = ((Element) nNode).getElementsByTagName("Autor");

            System.out.println("Loop Autor");
            List<Autor> list = new ArrayList<>();
            for (int index = 0; index < numeroAutores; index++) {
                Node nNodeAutor = nodeListAutor.item(index);

                if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                    if (!loopAutores(nNodeAutor, posicao, tagsEventos,
                            eElement.getElementsByTagName("AutorCorrespondente").item(0).getTextContent(), list)) {
                        System.out.println("Erro:  Loop Autor");
                        return false;
                    }
                }
            }

            String ac = eElement.getElementsByTagName("AutorCorrespondente").item(0).getTextContent();
            String as = eElement.getElementsByTagName("AutorSubmissao").item(0).getTextContent();

            boolean valida = false, valida2 = false;
            for (Autor a : list) {
                // validar autor correspondete
                Utilizador u = m_empresa.getRegistoUtilizadores().getUtilizadorByID(ac);
                Utilizador u2 = m_empresa.getRegistoUtilizadores().getUtilizadorByID(as);
                if (u != null) {
                    if (u.getEmail().equals(a.getEmail())) {
                        controllerSA.setCorrespondente(a);
                        valida = true;
                    }
                }
                if (u2 != null) {
                    if (u2.getEmail().equals(a.getEmail())) {
                        controllerSA.setUtilizador(as);
                        valida = true;
                    }
                }
                u = m_empresa.getRegistoUtilizadores().getUtilizadorByEmail(ac);
                u2 = m_empresa.getRegistoUtilizadores().getUtilizadorByEmail(ac);
                if (u != null) {
                    if (u.getEmail().equals(a.getEmail())) {
                        controllerSA.setCorrespondente(a);
                        valida = true;
                    }
                }
                if (u2 != null) {
                    if (u2.getEmail().equals(a.getEmail())) {
                        controllerSA.setUtilizador(as);
                        valida = true;
                    }
                }
            }

            if (valida == false) {
                System.out.println("erro ac");
                return false;
            }

            if (!controllerSA.registarSubmissao()) {
                System.out.println("erro 44");
                return false;
            }
        } else {
            String as = eElement.getElementsByTagName("AutorSubmissao").item(0).getTextContent();
            boolean asvalida = false;
            String id = "";
            Utilizador us = m_empresa.getRegistoUtilizadores().getUtilizadorByID(as);
            if (us != null) {
                id = us.getUsername();
                asvalida = true;
            }
            us = m_empresa.getRegistoUtilizadores().getUtilizadorByEmail(as);
            if (us != null) {
                id = us.getUsername();
                asvalida = true;
            }
            if (asvalida == false) {
                System.out.println("erro as");
                return false;
            }

            controllerSAF = new SubmeterArtigoFinalController(m_empresa);
            controllerSAF.selectSubmissao(controllerSA.getSubmissao(),
                    id);
            controllerSAF.setDados(eElement.getElementsByTagName("TituloArtigo").item(0).getTextContent(),
                    eElement.getElementsByTagName("Resumo").item(0).getTextContent());
            controllerSAF.setFicheiro(eElement.getElementsByTagName("Ficheiro").item(0).getTextContent());
            List<String> lstPC = Arrays.asList(eElement.getElementsByTagName("PalavrasChave").item(0).getTextContent().split(";"));
            controllerSAF.setPalavrasChave(lstPC);

            List<Autor> lstAutor = new ArrayList<>();

            NodeList nodeListAutor = ((Element) nNode).getElementsByTagName("Autor");

            System.out.println("Loop Autor");
            List<Autor> list = new ArrayList<>();
            for (int index = 0; index < numeroAutores; index++) {
                Node nNodeAutor = nodeListAutor.item(index);

                if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                    if (!loopAutores(nNodeAutor, posicao, tagsEventos,
                            eElement.getElementsByTagName("AutorCorrespondente").item(0).getTextContent(), list)) {
                        System.out.println("Erro:  Loop Autor");
                        return false;
                    }
                }
            }

            String ac = eElement.getElementsByTagName("AutorCorrespondente").item(0).getTextContent();

            boolean valida = false;
            for (Autor a : list) {
                // validar autor correspondete
                Utilizador u = m_empresa.getRegistoUtilizadores().getUtilizadorByID(ac);
                if (u != null) {
                    if (u.getEmail().equals(a.getEmail())) {
                        controllerSAF.setCorrespondente(a);
                        valida = true;
                    }
                }
                u = m_empresa.getRegistoUtilizadores().getUtilizadorByEmail(ac);
                if (u != null) {
                    if (u.getEmail().equals(a.getEmail())) {
                        controllerSAF.setCorrespondente(a);
                        valida = true;
                    }
                }
            }

            if (valida == false) {
                System.out.println("\n\n\n\n\n\n\n\n\n\n\n\n\nerro 123123");
                return false;
            }

            if (!controllerSAF.registarSubmissao()) {
                System.out.println("erro 44");
                return false;
            }
        }

        return true;
    }

    private boolean loopAutores(Node nNode, int numeroArtigo, String tagsEventos, String id, List<Autor> list) {
        if (!validaTagNode(nNode, "NomeAutor", 1) || !validaTagNode(nNode, "EmailAutor", 1)
                || !validaTagNode(nNode, "Filiacao", 1) || !validaTagNode(nNode, "UsernameAutor", 1)) {
            System.out.println("erro 123456789");
            return false;
        }

        Element eElement = (Element) nNode;

        System.out.println(numeroArtigo + " ola");
        if (numeroArtigo == 0) {
            Autor aut = controllerSA.novoAutor(eElement.getElementsByTagName("NomeAutor").item(0).getTextContent(),
                    eElement.getElementsByTagName("Filiacao").item(0).getTextContent(),
                    eElement.getElementsByTagName("EmailAutor").item(0).getTextContent());

            list.add(aut);

            return controllerSA.addAutor(aut);
        } else {
            Autor aut = controllerSAF.novoAutor(eElement.getElementsByTagName("NomeAutor").item(0).getTextContent(),
                    eElement.getElementsByTagName("Filiacao").item(0).getTextContent(),
                    eElement.getElementsByTagName("EmailAutor").item(0).getTextContent());

            list.add(aut);

            return controllerSAF.addAutor(aut);
        }
    }

    private boolean loopMembrosCP(Node nNode, int index, DefinirCPController controller, String tag) {
        Element eElement = (Element) nNode;

        if (!controller.novoMembroCP(eElement.getElementsByTagName(tag).item(index).getTextContent())) {
            System.out.println("Erro:  validar membro cp");
            return false;
        }

        return controller.addMembroCP();
    }

    private boolean loopOrganizador(Node nNode, int index, CriarEventoController controller) {
        Element eElement = (Element) nNode;

        return controller.addOrganizador(eElement.getElementsByTagName("Organizador").item(index).getTextContent());
    }

    private boolean loopProponente(Node nNode, int index, CriarSessaoTematicaController controller) {
        Element eElement = (Element) nNode;

        if (!controller.addProponente(eElement.getElementsByTagName("Proponente").item(index).getTextContent())) {
            System.out.println("erro");
            return false;
        }

        return controller.registaProponente();
    }

    private boolean validaTag(String str, int tamanho) {
        NodeList nodeList = this.doc.getElementsByTagName(str);

        return !(nodeList == null || nodeList.getLength() != tamanho);
    }

    private boolean validaTagNode(Node node, String str, int tamanho) {
        Element eElement = ((Element) node);

        return eElement.getElementsByTagName(str).getLength() == tamanho;
    }

    private boolean validarTagDatas(String evento, String[] datas) {
        evento = evento.replace("<", "");
        int i = 0;
        for (String data : datas) {
            for (String split : evento.split(">")) {
                if (data.equals(split)) {
                    i++;
                }
            }
            if (i != 1) {
                return false;
            }
            i = 0;
        }
        return true;
    }

    private boolean validaCodificacao(String cod) {
        return cod.equals("CA") || cod.equals("NFN");
    }

    private boolean validarAtb(Node node, String str, int i, String atr) {
        Element eElement = ((Element) node);

        if (eElement.getElementsByTagName(str).item(i).hasAttributes()) {
            if (eElement.getElementsByTagName(str).item(i).getAttributes().getNamedItem("tipo").toString().contains(atr)) {
                return true;
            }
        }

        return false;
    }

    private int getNumeroObjectos(String str, int index) {
        NodeList nList = doc.getElementsByTagName(str);

        Node nNode = nList.item(index);

        String valorNumerico = ((Element) nNode).getTextContent();

        if (tryParseInt(valorNumerico)) {
            int numero = Integer.parseInt(valorNumerico);
            if (numero > -1) {
                return numero;
            }
        }
        return -10;
    }

    private int getNumeroObjectosNode(Node node, String str) {
        NodeList nList = ((Element) node).getElementsByTagName(str);

        Node nNode = nList.item(0);

        String valorNumerico = ((Element) nNode).getTextContent();

        if (tryParseInt(valorNumerico)) {
            int numero = Integer.parseInt(valorNumerico);
            if (numero > -1) {
                return numero;
            }
        }
        return -10;
    }

    private boolean tryParseInt(String value) {
        try {
            Integer.parseInt(value);
            return true;
        } catch (NumberFormatException nfe) {
            return false;
        }
    }

    private String descodificarPwd(String pwd) {
        File ficheiro = new File("tabela.csv");
        String[] pos = pwd.split(";");
        int tabela = Integer.parseInt(pos[1]);
        CodificacaoAritmetica desc = new CodificacaoAritmetica(ficheiro, tabela);
        return desc.descodifica(new BigDecimal(pos[3]));
    }

    private int getValorEventoSate(String str) {
        if (str.equals("EventoStateCriado")) {
            return 0;
        }

        if (str.equals("EventoStateRegistado")) {
            return 1;
        }

        if (str.equals("EventoStateSTDefinidas")) {
            return 2;
        }

        if (str.equals("EventoStateCPDefinida")) {
            return 3;
        }

        if (str.equals("EventoStateEmSubmissao")) {
            return 4;
        }

        if (str.equals("EventoStateEmDetecaoConflitos")) {
            return 5;
        }

        if (str.equals("EventoStateEmLicitacao")) {
            return 6;
        }

        if (str.equals("EventoStateEmDistribuicao")) {
            return 7;
        }

        if (str.equals("EventoStateEmRevisao")) {
            return 8;
        }

        if (str.equals("EventoStateEmDecisao")) {
            return 9;
        }

        if (str.equals("EventoStateEmDecidido")) {
            return 10;
        }

//        if (str.equals("EventoStateEmDecisao")) {
//            return 11;
//        }
        return 0;
    }

    private int getValorSessaoState(String str) {
        if (str.equals("STStateCriada")) {
            return 0;
        }

        if (str.equals("STStateRegistado")) {
            return 1;
        }

        if (str.equals("STStateCPDefinida")) {
            return 2;
        }

        if (str.equals("STStateEmSubmissao")) {
            return 3;
        }

        if (str.equals("STStateEmDetecaoConflitos")) {
            return 4;
        }

        if (str.equals("STStateEmLicitacao")) {
            return 5;
        }

        if (str.equals("STStateEmDistribuicao")) {
            return 6;
        }

        if (str.equals("STStateEmRevisao")) {
            return 7;
        }

        if (str.equals("STStateEmDecisao")) {
            return 8;
        }

        if (str.equals("STStateEmDecidido")) {
            return 9;
        }

        return 10;
    }

    private int getValorSubmissaoState(String str) {
        if (str.equals("SubmissaoStateCriado")) {
            return 0;
        }

        if (str.equals("SubmissaoStateSubmetido")) {
            return 1;
        }

        if (str.equals("SubmissaoStateEmDetecaoConflitos")) {
            return 2;
        }

        if (str.equals("SubmissaoStateEmLicitacao")) {
            return 3;
        }

        if (str.equals("SubmissaoStateEmDistribuicao")) {
            return 4;
        }

        if (str.equals("SubmissaoStateEmRevisao")) {
            return 5;
        }

        if (str.equals("SubmissaoStateEmDecisao")) {
            return 6;
        }

        if (str.equals("SubmissaoStateRetirada")) {
            return 7;
        }

        if (str.equals("SubmissaoStateRejeitada")) {
            return 8;
        }

        if (str.equals("SubmissaoStateAceite")) {
            return 9;
        }

        if (str.equals("SubmissaoStateCameraReady")) {
            return 10;
        }

        return 0;
    }

}
