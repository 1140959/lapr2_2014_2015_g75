package utils;

import java.util.ArrayList;
import java.util.List;
import javax.swing.AbstractListModel;

public class ModeloListaStrings extends AbstractListModel {
    
    private List<String> lista;
    
    public ModeloListaStrings(){
        this.lista=new ArrayList<>();
    }

    @Override
    public int getSize() {
        return lista.size();
    }

    @Override
    public String getElementAt(int i) {
        return lista.get(i);
    }
    
    public void addElement(String texto){
        lista.add(texto);
        fireIntervalAdded(this, 0, getSize());
    }
    
    public void removeElementAt(int i){
        lista.remove(i);
        fireIntervalRemoved(this, i, i);
    }
    
    public void removeAllElements(){
        lista.clear();
        fireIntervalRemoved(this, getSize(), getSize());
    }
    
    public boolean contains(String texto){
        return lista.contains(texto);
    }
    
    public List<String> getLista(){
        return new ArrayList<>(lista);
    }
  
}
