/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import eventoscientificos.domain.Evento;
import eventoscientificos.domain.Revisor;
import eventoscientificos.domain.SessaoTematica;
import java.util.ArrayList;
import java.util.List;
import javax.swing.AbstractListModel;

/**
 *
 * @author Carlos
 */
public class ModeloListaRevisor extends AbstractListModel {

    private List<Revisor> listaRevisor;

    public ModeloListaRevisor() {
        this.listaRevisor = new ArrayList<>();
    }

    public List<Revisor> getListaRevisor() {
        return this.listaRevisor;
    }

    public boolean addElement(Revisor revisor) {
        boolean revisorAdicionado = this.listaRevisor.add(revisor);
        if (revisorAdicionado) {
            fireIntervalAdded(this, 0, getSize());
        }
        return revisorAdicionado;
    }

    public void addElementAt(int indice, Revisor revisor) {
        this.listaRevisor.add(indice, revisor);
        fireIntervalAdded(this, 0, getSize());
    }

    public boolean removeElement(Revisor revisor) {
        int indice = this.listaRevisor.indexOf(revisor);
        boolean revisorRemovido = this.listaRevisor.remove(revisor);
        if (revisorRemovido) {
            fireIntervalRemoved(this, indice, indice);
        }
        return revisorRemovido;
    }

    public void removeElementAt(int indice) {
        this.listaRevisor.remove(indice);
        fireIntervalRemoved(this, indice, indice);
    }

    public void removeAllElements() {
        this.listaRevisor.clear();
        fireIntervalRemoved(this, getSize(), getSize());
    }

    public boolean contains(Revisor revisor) {
        return this.listaRevisor.contains(revisor);
    }

    @Override
    public int getSize() {
        return this.listaRevisor.size();
    }

    @Override
    public Object getElementAt(int i) {
        return this.listaRevisor.get(i);
    }

}
