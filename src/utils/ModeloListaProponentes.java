package utils;

import eventoscientificos.domain.Proponente;
import java.util.ArrayList;
import java.util.List;
import javax.swing.AbstractListModel;

/**
 *
 * @author 1081019
 */
public class ModeloListaProponentes extends AbstractListModel{
    
    private List<Proponente> listaProponentes;
    
    public ModeloListaProponentes(){
        this.listaProponentes=new ArrayList<>();
    }
    
    public List<Proponente> getListaProponentes() {
        return this.listaProponentes;
    }

    public boolean addElement(Proponente proponente) {
        boolean proponenteAdicionado = this.listaProponentes.add(proponente);
        if (proponenteAdicionado) {
            fireIntervalAdded(this, 0, getSize());
        }
        return proponenteAdicionado;
    }

    public void addElementAt(int indice, Proponente proponente) {
        this.listaProponentes.add(indice, proponente);
        fireIntervalAdded(this, 0, getSize());
    }

    public boolean removeElement(Proponente proponente) {
        int indice = this.listaProponentes.indexOf(proponente);
        boolean proponenteRemovido = this.listaProponentes.remove(proponente);
        if (proponenteRemovido) {
            fireIntervalRemoved(this, indice, indice);
        }
        return proponenteRemovido;
    }

    public void removeElementAt(int indice) {
        this.listaProponentes.remove(indice);
        fireIntervalRemoved(this, indice, indice);
    }

    public void removeAllElements() {
        this.listaProponentes.clear();
        fireIntervalRemoved(this, getSize(), getSize());
    }

    public boolean contains(Proponente proponente) {
        return this.listaProponentes.contains(proponente);
    }

    @Override
    public int getSize() {
        return listaProponentes.size();
    }

    @Override
    public Object getElementAt(int i) {
        return listaProponentes.get(i);
    }
    
}
