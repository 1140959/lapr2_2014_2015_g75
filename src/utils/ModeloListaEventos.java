/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import eventoscientificos.domain.CPDefinivel;
import eventoscientificos.domain.Evento;
import java.util.ArrayList;
import java.util.List;
import javax.swing.AbstractListModel;

/**
 *
 * @author Carlos
 */
public class ModeloListaEventos extends AbstractListModel {
    
    private List<String> listaTitulosEvento;
    private List<Evento> listaEvento;

    public ModeloListaEventos() {
        listaTitulosEvento = new ArrayList<>();
        listaEvento = new ArrayList<>();
    }
    
    public ModeloListaEventos(List<Evento> listaEvento) {
        this.listaEvento = new ArrayList<>(listaEvento);
        this.listaTitulosEvento= new ArrayList<>();
        for (Evento evento : this.listaEvento) {
            listaTitulosEvento.add(evento.getTitulo());
        }
    }
    
    public List<String> getListaEventos() {
        return this.listaTitulosEvento;
    }
    
    public boolean addElement(String evento) {
        boolean eventoAdicionado = this.listaTitulosEvento.add(evento);
        if (eventoAdicionado) {
            fireIntervalAdded(this, 0, getSize());
        }
        return eventoAdicionado;
    }

    public void addElementAt(int indice, String evento) {
        this.listaTitulosEvento.add(indice, evento);
        fireIntervalAdded(this, 0, getSize());
    }
    
    public boolean removeElement(Evento evento) {
        int indice = this.listaTitulosEvento.indexOf(evento);
        boolean eventoRemovido = this.listaTitulosEvento.remove(evento);
        if (eventoRemovido) {
            fireIntervalRemoved(this, indice, indice);
        }
        return eventoRemovido;
    }

    public void removeElementAt(int indice) {
        this.listaTitulosEvento.remove(indice);
        fireIntervalRemoved(this, indice, indice);
    }
    
    public void removeAllElements() {
        this.listaTitulosEvento.clear();
        fireIntervalRemoved(this, getSize(), getSize());
    }

    public boolean contains(CPDefinivel cpDefinivel) {
        return this.listaTitulosEvento.contains(cpDefinivel);
    }
    
    public Evento getEventoAt(int i){
        return listaEvento.get(i);
    }

    @Override
    public int getSize() {
        return this.listaTitulosEvento.size();
    }

    @Override
    public Object getElementAt(int i) {
        return this.listaTitulosEvento.get(i);
    }
   
    
}
