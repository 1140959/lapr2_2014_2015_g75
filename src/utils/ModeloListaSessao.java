/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import eventoscientificos.domain.SessaoTematica;
import java.util.ArrayList;
import java.util.List;
import javax.swing.AbstractListModel;

/**
 *
 * @author Carlos
 */
public class ModeloListaSessao extends AbstractListModel {

    private List<SessaoTematica> listaSessao;
    private List<String> listaTituloCodigoSessao;

    public ModeloListaSessao() {
        listaSessao = new ArrayList<>();
        listaTituloCodigoSessao = new ArrayList<>();
    }

    public ModeloListaSessao(List<SessaoTematica> listaSessao) {
        this.listaSessao = new ArrayList<>(listaSessao);
        this.listaTituloCodigoSessao = new ArrayList<>();
        for (SessaoTematica sessao : this.listaSessao) {
            listaTituloCodigoSessao.add(sessao.getCodigo() + "-" + sessao.getDescricao());
        }
    }

    public List<SessaoTematica> getListaSessaoTematica() {
        return this.listaSessao;
    }

    public boolean addElement(SessaoTematica sessaoTematica) {
        boolean sessaoAdicionado = this.listaSessao.add(sessaoTematica);
        if (sessaoAdicionado) {
            fireIntervalAdded(this, 0, getSize());
        }
        return sessaoAdicionado;
    }

    public void add(SessaoTematica sessao) {
        listaSessao.add(sessao);
        addElement(sessao.getCodigo());
    }
    public boolean addElement(String sessaoTematica) {
        boolean sessaoAdicionado = this.listaTituloCodigoSessao.add(sessaoTematica);
        if (sessaoAdicionado) {
            fireIntervalAdded(this, 0, getSize());
        }
        return sessaoAdicionado;
    }

    public void addElementAt(int indice, SessaoTematica sessaoTematica) {
        this.listaSessao.add(indice, sessaoTematica);
        fireIntervalAdded(this, 0, getSize());
    }

    public boolean removeElement(SessaoTematica sessaoTematica) {
        int indice = this.listaSessao.indexOf(sessaoTematica);
        boolean sessaoRemovido = this.listaSessao.remove(sessaoTematica);
        if (sessaoRemovido) {
            fireIntervalRemoved(this, indice, indice);
        }
        return sessaoRemovido;
    }

    public void removeElementAt(int indice) {
        this.listaSessao.remove(indice);
        fireIntervalRemoved(this, indice, indice);
    }

    public void removeAllElements() {
        this.listaTituloCodigoSessao.clear();
        fireIntervalRemoved(this, getSize(), getSize());
    }

    public boolean contains(SessaoTematica sessaoTematica) {
        return this.listaSessao.contains(sessaoTematica);
    }

    public SessaoTematica getSessaoAt(int i) {
        return listaSessao.get(i);
    }

    @Override
    public int getSize() {
        return this.listaTituloCodigoSessao.size();
    }

    @Override
    public Object getElementAt(int i) {
        return this.listaTituloCodigoSessao.get(i);
    }

}
