/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import eventoscientificos.domain.CPDefinivel;
import eventoscientificos.domain.Evento;
import eventoscientificos.domain.Submissao;
import java.util.ArrayList;
import java.util.List;
import javax.swing.AbstractListModel;

/**
 *
 * @author Carlos
 */
public class ModeloListaSubmissoesInicial extends AbstractListModel {
    
    private List<String> listaTitulosArtigoInicial;
    private List<Submissao> listaSubmissao;
    
    public ModeloListaSubmissoesInicial() {
        listaSubmissao = new ArrayList<>();
        listaTitulosArtigoInicial = new ArrayList<>();
    }

    public ModeloListaSubmissoesInicial(List<Submissao> listaSubmissao) {
        this.listaSubmissao = new ArrayList<>(listaSubmissao);
        this.listaTitulosArtigoInicial= new ArrayList<>();
        for (Submissao submissao : this.listaSubmissao) {
            listaTitulosArtigoInicial.add(submissao.getArtigo().getTitulo());
        }
    }
    
    public List<String> getListaSubmissao() {
        return this.listaTitulosArtigoInicial;
    }
    
    public boolean addElement(String submissao) {
        boolean eventoAdicionado = this.listaTitulosArtigoInicial.add(submissao);
        if (eventoAdicionado) {
            fireIntervalAdded(this, 0, getSize());
        }
        return eventoAdicionado;
    }

    public void addElementAt(int indice, String submissao) {
        this.listaTitulosArtigoInicial.add(indice, submissao);
        fireIntervalAdded(this, 0, getSize());
    }
    
    public boolean removeElement(Submissao submissao) {
        int indice = this.listaTitulosArtigoInicial.indexOf(submissao);
        boolean eventoRemovido = this.listaTitulosArtigoInicial.remove(submissao);
        if (eventoRemovido) {
            fireIntervalRemoved(this, indice, indice);
        }
        return eventoRemovido;
    }

    public void removeElementAt(int indice) {
        this.listaTitulosArtigoInicial.remove(indice);
        fireIntervalRemoved(this, indice, indice);
    }
    
    public void removeAllElements() {
        this.listaTitulosArtigoInicial.clear();
        fireIntervalRemoved(this, getSize(), getSize());
    }

    public boolean contains(Submissao submissao) {
        return this.listaTitulosArtigoInicial.contains(submissao);
    }
    
    public Submissao getSubmissaoAt(int i){
        return listaSubmissao.get(i);
    }

    @Override
    public int getSize() {
        return this.listaTitulosArtigoInicial.size();
    }

    @Override
    public Object getElementAt(int i) {
        return this.listaTitulosArtigoInicial.get(i);
    }
   
    
}
