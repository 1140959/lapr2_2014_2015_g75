/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import eventoscientificos.domain.Revisavel;
import java.util.ArrayList;
import java.util.List;
import javax.swing.AbstractListModel;

/**
 *
 * @author BrunoFernandes
 */
public class ModeloListaRevisavel extends AbstractListModel {

    private List<Revisavel> listaRevisavel;

    public ModeloListaRevisavel() {
        listaRevisavel = new ArrayList<>();
    }

    public ModeloListaRevisavel(List<Revisavel> listRevisavel) {
        this.listaRevisavel = new ArrayList<>(listRevisavel);
    }

    public List<Revisavel> getListaRevisavel() {
        return this.listaRevisavel;
    }

    @Override
    public int getSize() {
        return this.listaRevisavel.size();
    }

    @Override
    public Object getElementAt(int index) {
        return this.listaRevisavel.get(index);
    }

}
