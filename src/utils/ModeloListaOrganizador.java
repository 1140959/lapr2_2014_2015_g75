/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import eventoscientificos.domain.Organizador;
import java.util.ArrayList;
import java.util.List;
import javax.swing.AbstractListModel;

/**
 *
 * @author David
 */
public class ModeloListaOrganizador extends AbstractListModel{
    
    private List<Organizador> listaOrganizadores;
    
    public ModeloListaOrganizador(){
        this.listaOrganizadores=new ArrayList<>();
    }
    
    public List<Organizador> getListaRevisor() {
        return this.listaOrganizadores;
    }

    public boolean addElement(Organizador organizador) {
        boolean organizadorAdicionado = this.listaOrganizadores.add(organizador);
        if (organizadorAdicionado) {
            fireIntervalAdded(this, 0, getSize());
        }
        return organizadorAdicionado;
    }

    public void addElementAt(int indice, Organizador organizador) {
        this.listaOrganizadores.add(indice, organizador);
        fireIntervalAdded(this, 0, getSize());
    }

    public boolean removeElement(Organizador organizador) {
        int indice = this.listaOrganizadores.indexOf(organizador);
        boolean organizadorRemovido = this.listaOrganizadores.remove(organizador);
        if (organizadorRemovido) {
            fireIntervalRemoved(this, indice, indice);
        }
        return organizadorRemovido;
    }

    public void removeElementAt(int indice) {
        this.listaOrganizadores.remove(indice);
        fireIntervalRemoved(this, indice, indice);
    }

    public void removeAllElements() {
        this.listaOrganizadores.clear();
        fireIntervalRemoved(this, getSize(), getSize());
    }

    public boolean contains(Organizador organizador) {
        return this.listaOrganizadores.contains(organizador);
    }

    @Override
    public int getSize() {
        return listaOrganizadores.size();
    }

    @Override
    public Object getElementAt(int i) {
        return listaOrganizadores.get(i);
    }
    
    public List<Organizador> getListaOrganizadores(){
        return listaOrganizadores;
    }
    
}
