package utils;

import eventoscientificos.domain.Evento;
import java.util.ArrayList;
import java.util.List;
import javax.swing.DefaultComboBoxModel;


public class ModeloComboEvento extends DefaultComboBoxModel {

    private List<Evento> listaEvento;
    private List<String> listaTituloEvento;

    public ModeloComboEvento(List<Evento> listaEvento) {
        
        this.listaEvento = new ArrayList<>(listaEvento);
        this.listaTituloEvento= new ArrayList<>();
        for (Evento evento : this.listaEvento) {
            listaTituloEvento.add(evento.getTitulo());
        }
    }

    public boolean setListEvento(List<Evento> listaEvento) {
        if (listaEvento.isEmpty()) {
            return false;
        }
        this.listaEvento = listaEvento;
        actualizar();
        return true;
    }

    public void actualizar() {
        removeAllElements();
        for (String evento : listaTituloEvento) {
            addElement(evento);
        }
    }
}
