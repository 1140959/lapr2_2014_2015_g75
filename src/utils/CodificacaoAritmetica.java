package utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author 1140959@isep.ipp.pt
 */
public class CodificacaoAritmetica {

    private final List<BigDecimal> probabilidades;
    private final List<String> caracteres;
    private File ficheiroTabela;

    /**
     * Construtor para codificação / descodificação de palavras (linhas)
     *
     * @param ficheiroTabela O ficheiro tabela
     * @param indexTabela o indice da tabela
     */
    public CodificacaoAritmetica(File ficheiroTabela, int indexTabela) {
        this.ficheiroTabela = ficheiroTabela;
        this.caracteres = new ArrayList<>();
        this.probabilidades = new ArrayList<>();
        lerCaracteres();
        lerTabela(indexTabela);
    }

    /**
     * Construtor para codificação / descodificação de Ficheiros
     *
     * @param ficheiroTabela O ficheiro Tabela
     */
    public CodificacaoAritmetica(File ficheiroTabela) {
        this.ficheiroTabela = ficheiroTabela;
        this.caracteres = new ArrayList<>();
        this.probabilidades = new ArrayList<>();
        lerCaracteres();
        lerTabela(1);
    }

    /**
     * Construtor para codificação / descodificação de palavras recebendo as
     * listas
     *
     * @param caracteres Caracter da codificacao
     * @param probabilidades Probablidades
     */
    public CodificacaoAritmetica(List<String> caracteres, List<BigDecimal> probabilidades) {
        this.caracteres = caracteres;
        this.probabilidades = probabilidades;
    }

    private boolean lerTabela(int indexTabela) {
        boolean sucesso = false;
        try {
            Scanner sc = new Scanner(ficheiroTabela, "UTF8");
            boolean primeiro = false;
            while (sc.hasNextLine()) {
                String[] array = sc.nextLine().split(";");
                if (primeiro && array.length > 0) {
                    double doub = Double.parseDouble(array[indexTabela + 1].replaceAll(",", "."));
                    probabilidades.add(BigDecimal.valueOf(doub));
                }
                primeiro = true;
            }
            probabilidades.remove(probabilidades.size() - 1); // remove o ultimo elemento da tabela
            sucesso = true;
        } catch (FileNotFoundException ex) {
            System.out.println("O Ficheiro " + ficheiroTabela + " não foi encontrado");
        }
//        System.out.println(probabilidades.toString());
        return sucesso;
    }

    private boolean lerCaracteres() {
        boolean sucesso = false;
        try {
            Scanner sc = new Scanner(ficheiroTabela, "UTF8");
            boolean primeiro = false;
            while (sc.hasNextLine()) {
                String[] array = sc.nextLine().split(";");
                if (primeiro) {
                    caracteres.add(array[0]);
                }
                primeiro = true;
            }
            caracteres.remove(caracteres.size() - 1); // remove o ultimo elemento da tabela
            sucesso = true;
        } catch (FileNotFoundException ex) {
            System.out.println("O Ficheiro " + ficheiroTabela + " não foi encontrado");
        }
//        System.out.println(caracteres.toString());
        return sucesso;
    }

    public BigDecimal codifica(String password) {
        BigDecimal a = BigDecimal.valueOf(0);
        BigDecimal aAux = BigDecimal.valueOf(0);
        BigDecimal b = BigDecimal.valueOf(1);
        BigDecimal p;
        String caracter;

        /* FORMULA : p*(b-a)+a */
        for (int i = 0; i < password.length(); i++) {
            caracter = String.valueOf(password.charAt(i));

            if (caracteres.indexOf(caracter) == -1) {
                caracter = caracteres.get(caracteres.size() - 1);
            }

            p = BigDecimal.valueOf(0);
            for (int ii = 0; ii < caracteres.indexOf(caracter); ii++) {
                p = p.add(probabilidades.get(ii));
            }

            a = a.add(p.multiply(b.subtract(a)));
            p = p.add(probabilidades.get(caracteres.indexOf(caracter)));
            b = aAux.add(p.multiply(b.subtract(aAux)));
            aAux = a;

        }
        return a;
    }

    public String descodifica(BigDecimal codificado) {
        String sol = "";
        BigDecimal a = codificado;
        BigDecimal sub = BigDecimal.valueOf(0);

        BigDecimal inf, sup;
        BigDecimal intervalo = BigDecimal.valueOf(0);

        while ((!a.equals(BigDecimal.valueOf(0)))) {
            inf = BigDecimal.valueOf(0);
            sup = BigDecimal.valueOf(0);
            for (int i = 0; i < caracteres.size(); i++) {

                sup = sup.add(probabilidades.get(i));

                if (a.compareTo(inf) >= 0 && a.compareTo(sup) == -1) {
                    sub = inf;
                    intervalo = sup.subtract(sub);
                    sol += caracteres.get(i);
                }
                inf = sup;
            }
            BigDecimal aux = a.subtract(sub);
            a = aux.divide(intervalo);
        }
        return sol;
    }

    public void codificaFicheiro(File leitura) {
        try {
            try (Scanner sc = new Scanner(leitura, "UTF8")) {
                String[] nome = leitura.getName().split("\\.");
                String novoNome = nome[0] + "_codificado.xml";
                try (PrintWriter writer = new PrintWriter(novoNome, "UTF-8")) {
                    while (sc.hasNextLine()) {
                        String linha = sc.nextLine().replaceAll(" ", "#").replaceAll("\t", "%").replaceAll(";", "»");
                        BigDecimal codificado = codifica(linha);
                        writer.println(codificado.toString());
                    }
                }
            }
            leitura.delete();
        } catch (FileNotFoundException | UnsupportedEncodingException ex) {
            System.out.println("O Ficheiro " + leitura + " não foi encontrado");
        }
    }

    public void descodificaFicheiro(File leitura) {
        try {
            try (Scanner sc = new Scanner(leitura, "UTF8")) {
                String[] nome = leitura.getName().split("_");
                String novoNome = nome[0] + ".xml";
                try (PrintWriter writer = new PrintWriter(novoNome, "UTF-8")) {
                    while (sc.hasNextLine()) {
                        BigDecimal decimal = new BigDecimal(sc.nextLine());
                        String descodificado = descodifica(decimal).replaceAll("#", " ").replaceAll("%", "\t").replaceAll("»", ";");
                        writer.println(descodificado);
                    }
                }
            }
            leitura.delete();

        } catch (FileNotFoundException | UnsupportedEncodingException ex) {
            System.out.println("O Ficheiro " + leitura + " não foi encontrado");
        }

    }
}
