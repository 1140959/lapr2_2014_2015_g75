package utils;

import eventoscientificos.domain.Evento;
import java.util.ArrayList;
import java.util.List;
import javax.swing.DefaultComboBoxModel;


public class ModeloEventos extends DefaultComboBoxModel {

    private List<Evento> eventos;
    
    public ModeloEventos(List<Evento> eventos) {
        this.eventos = eventos;
    }

    public boolean setListSessao(List<Evento> eventos) {
        if (eventos.isEmpty()) {
            return false;
        }
        this.eventos = eventos;
        actualizar();
        return true;
    }

    public void actualizar() {
        removeAllElements();
        for (Evento e : eventos) {
            addElement(String.format("%s", e.titulosEventos()));
        }
    }
    
    public Evento getEvento(int index) {
        return this.eventos.get(index);
    }
}

